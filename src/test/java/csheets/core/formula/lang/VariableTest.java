/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.Cell;
import csheets.core.Value;
import csheets.core.formula.Reference;
import csheets.core.formula.util.ExpressionVisitor;
import java.text.ParseException;
import java.util.SortedSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author José Marques
 */
        //In Progress
public class VariableTest {

    Variable testVar;
    Variable testVar2;
    Variable testVar3;

    @Before
    public void setUp() throws ParseException {
        testVar = new Variable("_varTest", Value.parseNumericValue("5"));
        testVar2 = new Variable("_varTest2", Value.parseNumericValue("10"));
        testVar3 = new Variable("_varTest2", Value.parseNumericValue("10"));
    }

    /**
     * Test of compareTo method, of class Variable.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        //Boolean test = (testVar.compareTo(testVar2) < 0);
        assertEquals(-1, testVar.compareTo(testVar2));
        assertEquals(0, testVar2.compareTo(testVar3));
    }

}
