/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.columnSort;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author 1140445
 */
public class SortTest {
 
    public SortTest() {
    }
 
    @BeforeClass
    public static void setUpClass() {
    }
 
    @AfterClass
    public static void tearDownClass() {
    }
 
    @Before
    public void setUp() {
    }
 
    @After
    public void tearDown() {
    }
 
    /**
     * Test of descendSort method, of class Sort.
     */
    @Test
    public void testDescendSort() throws FormulaCompilationException {
        System.out.println("descendSort");
 
        Workbook wb = new Workbook(2);
        Spreadsheet s = wb.getSpreadsheet(0);
 
        Cell c1 = s.getCell(0, 0);
        Cell c2 = s.getCell(1, 0);
        Cell c3 = s.getCell(2, 0);
 
        c1.setContent("Are");
        c2.setContent("Aret");
        c3.setContent("Abet");
 
        String[] expectedList = {"Aret", "Are", "Abet"};
 
        Cell[] list = {c1, c2, c3};
 
        Sort instance = new Sort();
        String[] resultList = instance.descendSort(list);
 
        Assert.assertArrayEquals(expectedList, resultList);
 
    }
 
    /**
     * Test of ascendSort method, of class Sort.
     */
    @Test
    public void testAscendSort() throws FormulaCompilationException {
        System.out.println("ascendSort");
 
        Workbook wb = new Workbook(2);
        Spreadsheet s = wb.getSpreadsheet(0);
 
        Cell c1 = s.getCell(0, 0);
        Cell c2 = s.getCell(1, 0);
        Cell c3 = s.getCell(2, 0);
 
        c1.setContent("Bzf");
        c2.setContent("Bge");
        c3.setContent("Bap");
 
        String[] expectedList = {"Bap", "Bge", "Bzf"};
 
        Cell[] list = {c1, c2, c3};
 
        Sort instance = new Sort();
        String[] resultList = instance.ascendSort(list);
 
        Assert.assertArrayEquals(expectedList, resultList);
    }
 
}
