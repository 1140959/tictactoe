/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importexporttext.ui;

import csheets.CleanSheets;
import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.SortedSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pedro
 */
public class ExportTControllerTest {
    
    CleanSheets app = new CleanSheets();
    UIController uiController = new UIController(app);
    Workbook workbook = new Workbook();
    ExportTController instance = new ExportTController(";", "test.txt", "yes", this.uiController);

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of ExportTextProcess method, of class ExportTController.
     */
    @Test
    public void testExportTextProcess() throws IOException {
        try {
        System.out.println("ExportTextProcess");
        ExportTController instance = null;
            workbook.addSpreadsheet();
            Address a1 = new Address(1, 1);
            Cell c1 = workbook.getSpreadsheet(0).getCell(a1);
            c1.setContent("first cell");

            Address a2 = new Address(2, 1);
            Cell c2 = workbook.getSpreadsheet(0).getCell(a2);
            c2.setContent("second cell");

            Address a3 = new Address(1, 2);
            Cell c3 = workbook.getSpreadsheet(0).getCell(a3);
            c3.setContent("third cell");

            Address a4 = new Address(2, 2);
            Cell c4 = workbook.getSpreadsheet(0).getCell(a4);
            c4.setContent("fourth cell");

            uiController.setActiveCell(c1);
            File file = new File("test_export.txt");
            
//            instance.ExportTextProcess();
//            File expResult = new File ("test_export.txt");
//            File result = new File ("test.txt");
//            assertEquals(expResult, result);
//            SortedSet<Cell> expResult = workbook.getSpreadsheet(0).getCells(a1, a4);
//        
//            SortedSet<Cell> result = instance.getCellRange(a1);
//
//            while (!expResult.isEmpty()) {
//                assertEquals(expResult.first().getContent(), result.first().getContent());
//
//                expResult.remove(expResult.first());
//                result.remove(result.first());
//            }
        } catch (FormulaCompilationException ex) {
            Logger.getLogger(ExportTControllerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of getName method, of class ExportTController.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
       
        String expResult = "";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of actionPerformed method, of class ExportTController.
     */
    @Test
    public void testActionPerformed() {
        System.out.println("actionPerformed");
//        ActionEvent e = null;
//        ExportTController instance = null;
//        instance.actionPerformed(e);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
        /**
     * Test of getCellRange method, of class ExportTController.
     */
    @Test
    public void testGetCellRange() {
        try {
            System.out.println("getCellRange");

            workbook.addSpreadsheet();
            Address a1 = new Address(1, 1);
            Cell c1 = workbook.getSpreadsheet(0).getCell(a1);
            c1.setContent("first cell");

            Address a2 = new Address(2, 1);
            Cell c2 = workbook.getSpreadsheet(0).getCell(a2);
            c2.setContent("second cell");

            Address a3 = new Address(1, 2);
            Cell c3 = workbook.getSpreadsheet(0).getCell(a3);
            c3.setContent("third cell");

            Address a4 = new Address(2, 2);
            Cell c4 = workbook.getSpreadsheet(0).getCell(a4);
            c4.setContent("fourth cell");

//            SortedSet<Cell> expResult = workbook.getSpreadsheet(0).getCells(a1, a4);
        
//            SortedSet<Cell> result = instance.getCellRange(a1);

//            while (!expResult.isEmpty()) {
//                assertEquals(expResult.first().getContent(), result.first().getContent());

//                expResult.remove(expResult.first());
//                result.remove(result.first());
//            }
        } catch (FormulaCompilationException ex) {
            Logger.getLogger(ExportTControllerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void testLastColumn() {
        try {
            System.out.println("getCellRange");
            workbook.addSpreadsheet();
            Address a1 = new Address(1, 1);
            Cell c1 = workbook.getSpreadsheet(0).getCell(a1);
            c1.setContent("first cell");

            Address a2 = new Address(2, 1);
            Cell c2 = workbook.getSpreadsheet(0).getCell(a2);
            c2.setContent("second cell");

            Address a3 = new Address(1, 2);
            Cell c3 = workbook.getSpreadsheet(0).getCell(a3);
            c3.setContent("third cell");

            Address a4 = new Address(2, 2);
            Cell c4 = workbook.getSpreadsheet(0).getCell(a4);
            c4.setContent("fourth cell");
            
            Address expResult = a3;
//            Address result = instance.lastColumn(a1);
//            assertEquals(expResult, result);
        } catch (FormulaCompilationException ex) {
            Logger.getLogger(ExportTControllerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
