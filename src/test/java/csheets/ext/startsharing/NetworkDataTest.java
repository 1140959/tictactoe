/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dmonteiro
 */
public class NetworkDataTest {
    
    private NetworkData nd_test;
    private InetAddress ip_test;
    private String name_test;
    private int port_test;
    
    public NetworkDataTest() {
        
        try{
        this.ip_test = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(NetworkDataTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.name_test = "test";
        this.port_test = 15000;
        
        
        nd_test = new NetworkData(ip_test,name_test,port_test);
    
    }
    
    
    @BeforeClass
    public static void setUpClass() {
        
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of port method, of class NetworkData.
     */
    @Test
    public void testPort() {
        System.out.println("port");
        NetworkData instance = nd_test;
        int expResult = 15000;
        int result = instance.port();
        assertEquals(expResult, result);
    }

    /**
     * Test of ip method, of class NetworkData.
     */
    @Test
    public void testIp() throws UnknownHostException {
        System.out.println("ip");
        NetworkData instance = nd_test;
        InetAddress expResult = InetAddress.getLocalHost();
        InetAddress result = instance.ip();
        assertEquals(expResult, result);

    }

    /**
     * Test of name method, of class NetworkData.
     */
    @Test
    public void testName() {
        System.out.println("name");
        NetworkData instance = nd_test;
        String expResult = "test";
        String result = instance.name();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class NetworkData.
     */
    @Test
    public void testEquals() throws UnknownHostException {
        System.out.println("equals");
        NetworkData new_test = new NetworkData(InetAddress.getLocalHost(),"test",15000);
        Object obj = new_test;
        NetworkData instance = nd_test;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEqualsNOT() throws UnknownHostException {
        System.out.println("equals");
        NetworkData new_test = new NetworkData(InetAddress.getLocalHost(),"test_error",20000);
        Object obj = new_test;
        NetworkData instance = nd_test;
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
