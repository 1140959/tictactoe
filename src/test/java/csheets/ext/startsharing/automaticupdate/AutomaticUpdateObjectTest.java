/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing.automaticupdate;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.style.StylableCell;
import csheets.ext.style.StyleExtension;
import java.awt.Color;
import java.awt.Font;
import java.text.Format;
import javax.swing.border.Border;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dmonteiro
 */
public class AutomaticUpdateObjectTest {
    
    private Cell cell;
    private AutomaticUpdateObject auo;
    private StylableCell style;
    private Workbook wb;
    private String content_test;

    
    public AutomaticUpdateObjectTest(){   
   
        wb = new Workbook();
        wb.addSpreadsheet();
              
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws FormulaCompilationException {
        

        Cell cell = wb.getSpreadsheet(0).getCell(0,1);
        cell.setContent("teste");
        content_test = cell.getContent();
        style = (StylableCell) cell.getExtension(StyleExtension.NAME);
        auo = new AutomaticUpdateObject(cell.getAddress(), cell.getContent(), style.getFormat(), style.getFont(), style.getHorizontalAlignment(), style.getVerticalAlignment(), style.getBackgroundColor(), style.getBorder());

    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCell method, of class AutomaticUpdateObject.
     * @throws FormulaCompilationException
     */
    @Test
    public void testGetCell() throws FormulaCompilationException {
        System.out.println("getCell");
        AutomaticUpdateObject instance = auo;
        
        Cell cellToTest = wb.getSpreadsheet(0).getCell(0,2);
        cellToTest.setContent(content_test);
        
        Cell expResult = cellToTest;
        String result = instance.getCellContent();
        assertTrue(expResult.getContent().equals(result));
    }

    /**
     * Test of getStyle method, of class AutomaticUpdateObject.
     */
    @Test
    public void testGetStyle() {
        System.out.println("getStyle");
        AutomaticUpdateObject instance = auo;
        
        Cell cellToTest = wb.getSpreadsheet(0).getCell(0, 3);
        StylableCell style_test = (StylableCell) cellToTest.getExtension(StyleExtension.NAME);
        
        
        StylableCell expResult = style_test;
        assertEquals(expResult.getFont(), instance.getFont());
        assertEquals(expResult.getBorder(), instance.getBorder());
        assertEquals(expResult.getBackgroundColor(), instance.getBackgroundColor());
        
        
    }

    /**
     * Test of changeAdress method, of class AutomaticUpdateObject.
     */
    @Test
    public void testChangeAdress() {
        System.out.println("changeAdress");
        Address a = new Address(3,2);
        AutomaticUpdateObject instance = auo;
        instance.changeAddress(a);
    }

    /**
     * Test of getAddress method, of class AutomaticUpdateObject.
     */
    @Test
    public void testGetAddress() {
        System.out.println("getAddress");
        AutomaticUpdateObject instance = auo;
        Address expResult = new Address(0,1);
        Address result = instance.getAddress();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBackgroundColor method, of class AutomaticUpdateObject.
     */
    @Test
    public void testGetBackgroundColor() {
        System.out.println("getBackgroundColor");
        AutomaticUpdateObject instance = auo;
        Color expResult = style.getBackgroundColor();
        Color result = instance.getBackgroundColor();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBorder method, of class AutomaticUpdateObject.
     */
    @Test
    public void testGetBorder() {
        System.out.println("getBorder");
        AutomaticUpdateObject instance = auo;
        Border expResult = style.getBorder();
        Border result = instance.getBorder();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCellContent method, of class AutomaticUpdateObject.
     */
    @Test
    public void testGetCellContent() {
        System.out.println("getCellContent");
        AutomaticUpdateObject instance = auo;
        String expResult = "teste";
        String result = instance.getCellContent();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFormat method, of class AutomaticUpdateObject.
     */
    @Test
    public void testGetFormat() {
        System.out.println("getFormat");
        AutomaticUpdateObject instance = auo;
        Format expResult = style.getFormat();
        Format result = instance.getFormat();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFont method, of class AutomaticUpdateObject.
     */
    @Test
    public void testGetFont() {
        System.out.println("getFont");
        AutomaticUpdateObject instance = auo;
        Font expResult = style.getFont();
        Font result = instance.getFont();
        assertEquals(expResult, result);
    }

    /**
     * Test of getHorizontalAlignment method, of class AutomaticUpdateObject.
     */
    @Test
    public void testGetHorizontalAlignment() {
        System.out.println("getHorizontalAlignment");
        AutomaticUpdateObject instance = auo;
        int expResult = style.getHorizontalAlignment();
        int result = instance.getHorizontalAlignment();
        assertEquals(expResult, result);
    }

    /**
     * Test of getVerticalAlignment method, of class AutomaticUpdateObject.
     */
    @Test
    public void testGetVerticalAlignment() {
        System.out.println("getVerticalAlignment");
        AutomaticUpdateObject instance = auo;
        int expResult = style.getVerticalAlignment();
        int result = instance.getVerticalAlignment();
        assertEquals(expResult, result);
    }
    
}
