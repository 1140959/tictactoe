/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing.automaticupdate;

import csheets.CleanSheets;
import csheets.ext.startsharing.ui.SharingPanel;
import csheets.ui.ctrl.UIController;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dmonteiro
 */
public class AutomaticUpdateControllerTest {
    
    private CleanSheets newapp;
    private SharingPanel sp;
    private UIController uicontrl;
    private AutomaticUpdateController auc;
    
    public AutomaticUpdateControllerTest() {
        newapp = new CleanSheets();
        uicontrl = new UIController(newapp);
        sp = new SharingPanel(uicontrl);
        auc = new AutomaticUpdateController(uicontrl,sp);
        
    }
    
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addSharingPanel method, of class AutomaticUpdateController.
     */
    @Test
    public void testAddSharingPanel() {
        System.out.println("addSharingPanel");
        
        CleanSheets newapp_test = new CleanSheets();
        UIController uicontrl_test = new UIController(newapp_test);
        SharingPanel sp_test = new SharingPanel(uicontrl_test);

        AutomaticUpdateController instance = auc;
        instance.addSharingPanel(sp_test);
    }
    
}
