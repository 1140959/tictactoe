/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing;

import java.net.InetAddress;
import java.net.UnknownHostException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dmonteiro
 */
public class NetworkInstanceTest {
    
    private NetworkData new_test;
    private NetworkInstance ni_test;
    
    public NetworkInstanceTest() throws UnknownHostException {
        
        new_test = new NetworkData(InetAddress.getLocalHost(),"test",15000);
        ni_test = new NetworkInstance(new_test);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNetworkData method, of class NetworkInstance.
     */
    @Test
    public void testGetNetworkData() throws UnknownHostException {
        System.out.println("getNetworkData");
        NetworkInstance instance = ni_test;
        NetworkData toTest = new NetworkData(InetAddress.getLocalHost(),"test",15000);
        NetworkData expResult = toTest;
        NetworkData result = instance.getNetworkData();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class NetworkInstance.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        NetworkInstance instance = ni_test;
        String name_test = new_test.name();
        String ip_test = new_test.ip().toString();
        String expResult = name_test + " -> " + ip_test;
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class NetworkInstance.
     */
    @Test
    public void testEquals() throws UnknownHostException {
        System.out.println("equals");
        NetworkData toTest = new NetworkData(InetAddress.getLocalHost(),"test",15000);
        NetworkInstance niTest = new NetworkInstance(toTest);
        Object obj = niTest;
        NetworkInstance instance = ni_test;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEqualsNOT() throws UnknownHostException {
        System.out.println("equals");
        NetworkData toTest = new NetworkData(InetAddress.getLocalHost(),"testToFail",20000);
        NetworkInstance niTest = new NetworkInstance(toTest);
        Object obj = niTest;
        NetworkInstance instance = ni_test;
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

}
