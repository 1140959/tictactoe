/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author josemiranda
 */
public class BattleshipsTest {
    Player player1;
    Player player2;
    Game g;
    GamesFactory factory;
    
    public BattleshipsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        String name1="joao";
        String name2="luis";
        File file = new File("/file");
        ImageIcon icon=null;
        try {
            BufferedImage image = ImageIO.read(file);
            icon = new ImageIcon(image);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        
        player1 = new Player(name1, icon);
        player2 = new Player(name2, icon);
        
        factory= new GamesFactory();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getPlayer1 method, of class Battleships.
     */
    @Test
    public void testGetPlayer1() {
        System.out.println("getPlayer1");
        Battleships instance = factory.createBattleshipsGame(player1, player2);
        Player expResult = player1;
        Player result = instance.getPlayer1();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPlayer2 method, of class Battleships.
     */
    @Test
    public void testGetPlayer2() {
        System.out.println("getPlayer2");
        Battleships instance = factory.createBattleshipsGame(player1, player2);
        Player expResult = player2;
        Player result = instance.getPlayer2();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Battleships.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Battleships instance = factory.createBattleshipsGame(player1, player2);
        String expResult = "Battleships: joao Vs luis";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
