/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import csheets.ext.startsharing.NetworkData;
import csheets.ext.startsharing.NetworkInstance;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author josemiranda
 */
public class PlayerPacketTest {
    
    PlayerPacket packet;
    private InetAddress ip_test;
    ImageIcon icon;
    
    public PlayerPacketTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        System.out.println("name");
        File file = new File("/file");
        icon=null;
        try {
            BufferedImage image = ImageIO.read(file);
            icon = new ImageIcon(image);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        try {
            ip_test=InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(PlayerPacketTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        NetworkData ndata=new NetworkData(ip_test, "joao", 1000);
        NetworkInstance instance = new NetworkInstance(ndata);
        Player player = new Player("joao", icon);
        packet = new PlayerPacket(player, instance);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class PlayerPacket.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        PlayerPacket instance = packet;
        String expResult = "joao";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getIp method, of class PlayerPacket.
     */
    @Test
    public void testGetIp() {
        System.out.println("getIp");
        PlayerPacket instance = packet;
        InetAddress expResult=null;
        try {
            expResult = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(PlayerPacketTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        InetAddress result = instance.getIp();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPort method, of class PlayerPacket.
     */
    @Test
    public void testGetPort() {
        System.out.println("getPort");
        PlayerPacket instance = packet;
        int expResult = 1000;
        int result = instance.getPort();
        assertEquals(expResult, result);
    }

   
}
