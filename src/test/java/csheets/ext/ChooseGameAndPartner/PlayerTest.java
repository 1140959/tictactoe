/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author josemiranda
 */
public class PlayerTest {
    
    public PlayerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of name method, of class Player.
     */
    @Test
    public void testName() {
        System.out.println("name");
        File file = new File("/file");
        ImageIcon icon=null;
        try {
            BufferedImage image = ImageIO.read(file);
            icon = new ImageIcon(image);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        Player instance = new Player("joao", icon);
        String expResult = "joao";
        String result = instance.name();
        assertEquals(expResult, result);
    }

    /**
     * Test of icon method, of class Player.
     */
    @Test
    public void testIcon() {
        System.out.println("icon");
        File file = new File("/file");
        ImageIcon icon=null;
        try {
            BufferedImage image = ImageIO.read(file);
            icon = new ImageIcon(image);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        Player instance = new Player("joao", icon);
        ImageIcon expResult = icon;
        ImageIcon result = instance.icon();
        assertEquals(expResult, result);
    }
    
}
