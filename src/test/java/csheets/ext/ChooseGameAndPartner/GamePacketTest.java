/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import csheets.ext.startsharing.NetworkData;
import csheets.ext.startsharing.NetworkInstance;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author josemiranda
 */
public class GamePacketTest {
    NetworkInstance instance;
    GamePacket gpacket;
    Game g;
    Player player1;
    Player player2;
    
    public GamePacketTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        String name1="joao";
        String name2="luis";
        File file = new File("/file");
        ImageIcon icon=null;
        try {
            BufferedImage image = ImageIO.read(file);
            icon = new ImageIcon(image);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        InetAddress ip=null;
        try {
            ip = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(GamePacketTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        NetworkData ndata= new NetworkData(ip, name2, 0);
        instance= new NetworkInstance(ndata);
        player1 = new Player(name1, icon);
        player2 = new Player(name2, icon);
        Game g = new Battleships(player1, player2);
        gpacket= new GamePacket(g, instance);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class GamePacket.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        GamePacket instance = gpacket;
        String expResult = "Battleships: joao Vs luis";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
