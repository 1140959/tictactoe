/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author josemiranda
 */
public class GamesFactoryTest {
    
    Player player1;
    Player player2;
    GamesFactory factory;
    
    public GamesFactoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        String name1="joao";
        String name2="luis";
        File file = new File("/file");
        ImageIcon icon=null;
        try {
            BufferedImage image = ImageIO.read(file);
            icon = new ImageIcon(image);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        
        player1 = new Player(name1, icon);
        player2 = new Player(name2, icon);
        
        factory= new GamesFactory();
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of createBattleshipsGame method, of class GamesFactory.
     */
    @Test
    public void testCreateBattleshipsGame() {
        System.out.println("createBattleshipsGame");
        Player p1 = player1;
        Player p2 = player2;
        GamesFactory instance = factory;
        String expResult = "Battleships: joao Vs luis";
        Battleships result = instance.createBattleshipsGame(p1, p2);
        assertEquals(expResult, result.toString());
    }

    /**
     * Test of createTicTacToeGame method, of class GamesFactory.
     */
    @Test
    public void testCreateTicTacToeGame() {
        System.out.println("createTicTacToeGame");
        Player p1 = player1;
        Player p2 = player2;
        GamesFactory instance = factory;
        String expResult = "Tic-Tac-Toe: joao Vs luis";
        TicTacToe result = instance.createTicTacToeGame(p1, p2);
        assertEquals(expResult, result.toString());
    }
    
}
