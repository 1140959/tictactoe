/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chat;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author LMendes
 */
public class ChatNetworkDataTest {

    private ChatNetworkData cnd;
    private InetAddress ip;
    private String nick;
    private int port;

    public ChatNetworkDataTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        try {
            this.ip = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(ChatNetworkDataTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.nick = "NickTest";
        this.port = 60000;

        cnd = new ChatNetworkData(ip, nick, port);

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getPort method, of class ChatNetworkData.
     */
    @Test
    public void testGetPort() {
        System.out.println("getPort");
        ChatNetworkData instance = cnd;
        int expResult = 60000;
        int result = instance.getPort();
        assertEquals(expResult, result);
    }

    /**
     * Test of getIpAddress method, of class ChatNetworkData.
     */
    @Test
    public void testGetIpAddress() {
        System.out.println("getIpAddress");
        ChatNetworkData instance = cnd;
        InetAddress expResult = null;
        try {
            expResult = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(ChatNetworkDataTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        InetAddress result = instance.getIpAddress();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNickname method, of class ChatNetworkData.
     */
    @Test
    public void testGetNickname() {
        System.out.println("getNickname");
        ChatNetworkData instance = cnd;
        String expResult = "NickTest";
        String result = instance.getNickname();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ChatNetworkData.
     */
    @Test
    public void testEquals() throws UnknownHostException {
        System.out.println("equals");
        ChatNetworkData new_test = new ChatNetworkData(InetAddress.getLocalHost(), "NickTest", 60000);
        Object obj = new_test;
        ChatNetworkData instance = cnd;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ChatNetworkData.
     */
    @Test
    public void testNotEquals() throws UnknownHostException {
        System.out.println("not equals");
        ChatNetworkData checkPort = new ChatNetworkData(ip, nick, 60001);
        ChatNetworkData checkNick = new ChatNetworkData(ip, "OtherNick", port);
        ChatNetworkData checkIP = new ChatNetworkData(null, nick, port);
        Object obj1 = checkPort;
        Object obj2 = checkNick;
        Object obj3 = checkIP;
        ChatNetworkData instance = cnd;
        boolean expResult = false;
        boolean result = instance.equals(obj1) || instance.equals(obj2) || instance.equals(obj3);
        assertEquals(expResult, result);
    }

}
