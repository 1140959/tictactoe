/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chat;

import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author LMendes
 */
public class ChatMessageConnectionTest {

    public ChatMessageConnectionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

//    /**
//     * Test of receiveMessageFrom method, of class ChatMessageConnection.
//      This test was not concluded, because I did not get the waited result, for the difficulty in testing, 
//      even so I has the certainty of the functioning of the service
//     */
//    @Test
//    public void testReceiveMessageFrom() {
//        boolean expResult = true;
//        boolean result = false;
//        try {
//            int port = 10500;
//            String nickName = "NickTest";
//            String msg = "This is a test";
//            InetAddress myIP = InetAddress.getByName("127.0.0.1");
//            Message message = new Message(msg);
//            ChatMessageConnection connection = new ChatMessageConnection(nickName, port);
//            ChatService.getInstance().startConnection(nickName);
//            result = connection.sendMessageTo(myIP, port, message);
//            result &= connection.receiveMessageFrom();
//
//        } catch (Exception ex) {
//            result = false;
//        }
//        assertEquals(expResult, result);
//    }
//    /**
//     * Test of sendMessageTo method, of class ChatMessageConnection.
//      This test was not concluded, because I did not get the waited result, for the difficulty in testing, 
//      even so I has the certainty of the functioning of the service
//     */
//    @Test
//    public void testSendMessageTo() {
//        boolean expResult = true;
//        boolean result;
//        try {
//            int port = 10501;
//            String nickName = "NickTest";
//            String msg = "This is a test";
//            InetAddress myIP = InetAddress.getByName("127.0.0.1");
//            Message message = new Message(msg);
//            ChatService.getInstance().startConnection(nickName);
//            result=ChatService.getInstance().sendMessage(myIP, port, message);
//        } catch (Exception ex) {
//            result = false;
//        }
//        assertEquals(expResult, result);
//    }
//
    /**
     * Test of getMyNickName method, of class ChatMessageConnection.
     */
    @Test
    public void testGetMyNickName() throws SocketException {
        System.out.println("getMyNickName");
        int port = 10502;
        String nickName = "NickTest";
        ChatMessageConnection instance = new ChatMessageConnection(nickName, port);
        String expResult = "NickTest";
        String result = instance.getMyNickName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMyIpAddress method, of class ChatMessageConnection.
     */
    @Test
    public void testGetMyIpAddress() throws SocketException {
        System.out.println("getMyIpAddress");
        int port = 10503;
        String nickName = "NickTest";
        ChatMessageConnection instance = new ChatMessageConnection(nickName, port);
        InetAddress iAddress;
        String IpAddress = "null";
        try {
            iAddress = InetAddress.getLocalHost();
            IpAddress = iAddress.getHostAddress();
        } catch (UnknownHostException ex) {
        }
        String expResult = IpAddress;
        String result = instance.getMyIpAddress();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMyPort method, of class ChatMessageConnection.
     */
    @Test
    public void testGetMyPort() throws SocketException {
        System.out.println("getMyPort");
        int port = 10504;
        String nickName = "NickTest";
        ChatMessageConnection instance = new ChatMessageConnection(nickName, port);
        int expResult = port;
        int result = instance.getMyPort();
        assertEquals(expResult, result);
    }
}
