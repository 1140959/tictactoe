/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.functionwizardextension;

import csheets.core.formula.Function;
import csheets.core.formula.lang.Average;
import csheets.core.formula.lang.Factorial;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author 11011_000
 */
public class FunctionWizardTest {
    
    public FunctionWizardTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getFunctions method, of class FunctionWizard.
     */
    @Test
    public void testGetFunctionsFactorial() {
        System.out.println("Get functions wizard test - Factorial function.");
        final Function[] functionList;
        FunctionWizard wizard = new FunctionWizard();
        functionList=wizard.getFunctions();
        Function func = null;
        
        // find and get the Factorial function
        for (int i = 0; i < functionList.length; i++) {
            if (functionList[i].getIdentifier().equals("FACT")) {
                func = functionList[i];
            }
        }
        assertTrue(func instanceof Factorial);
    }


    @Test
    public void testGetFunctionsAverage() {
        System.out.println("Get functions wizard test - Average function.");
        final Function[] functionList;
        FunctionWizard wizard = new FunctionWizard();
        functionList=wizard.getFunctions();
        Function func = null;
        
        // find and get the Average function
        for (int i = 0; i < functionList.length; i++) {
            if (functionList[i].getIdentifier().equals("AVERAGE")) {
                func = functionList[i];
            }
        }
        assertTrue(func instanceof Average);
    }

    
}
