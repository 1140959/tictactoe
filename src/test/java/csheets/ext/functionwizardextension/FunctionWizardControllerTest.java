/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.functionwizardextension;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.Function;
import csheets.ui.ctrl.UIController;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 11011_000
 */
public class FunctionWizardControllerTest {
    
    public FunctionWizardControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getFunctionValue method, of class FunctionWizardController.
     */
    //@Test
    public void testGetFunctionValue() throws IllegalValueTypeException {
        System.out.println("Get function value wizard test.");
        UIController uiController = null;
        FunctionWizardController instance = new FunctionWizardController(uiController);
        Function[] functionList;
        
        functionList=instance.getFunctions();
        
        Function func = null;
        functionList = instance.getFunctions();
        
        // find and get the Average function
        for (int i = 0; i < functionList.length; i++) {
            if (functionList[i].getIdentifier().equals("AVERAGE")) {
                func = functionList[i];
            }
        }

        // Parameters list
        List<Double> parameters = new ArrayList<>();
        parameters.add(new Double(1));
        parameters.add(new Double(2));
        parameters.add(new Double(3));
        parameters.add(new Double(4));
        parameters.add(new Double(5));
        
        // calculate the result of the average
        /*Value value = instance.getFunctionValue(func, parameters);

        double result = value.toDouble();
        double expected = 3; // AVERAGE(1,2,3,4,5) = 3

        assertEquals("The average should be 3.", expected, result, 0);*/
    }
    
}
