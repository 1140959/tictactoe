package csheets.ext.navigationwindow.ui;

import csheets.CleanSheets;
import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.ui.ctrl.UIController;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * NavigationController test class
 * @author 1100588
 */
public class NavigationControllerTest {
    
    public NavigationControllerTest() {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
 
    @AfterClass
    public static void tearDownClass() {
    }
 
    @Before
    public void setUp() {
    }
 
    @After
    public void tearDown() {
    }
    
    /**
     * Test method of getSpreadsheetCount()
     */
    @Test
    public void testGetSpreadsheetCount() {
        System.out.println("Test: getSpreadsheetCount()");
        
        CleanSheets cleansheets = new CleanSheets();
        UIController uiController = new UIController(cleansheets);
        
        int num_sheets = 2;
        Workbook workbook = new Workbook(num_sheets);
        Spreadsheet spreadsheet = workbook.getSpreadsheet(0);
        
        Cell[][] cells = new Cell[1][1];
        uiController.setActiveCell(spreadsheet.getCell(1,1));
        
        NavigationController instance = new NavigationController(uiController);
        
        int expected = num_sheets;
        int result = instance.getSpreadsheetCount();
        
        assertEquals(expected, result);
    }
    
    /**
     * Test method of getSpreadsheetTitle()
     */
    @Test
    public void testGetSpreadsheetTitle() {
        System.out.println("Test: getSpreadsheetTitle()");
        
        CleanSheets cleansheets = new CleanSheets();
        UIController uiController = new UIController(cleansheets);
        
        Workbook workbook = new Workbook(2);
        Spreadsheet spreadsheet = workbook.getSpreadsheet(0);
        
        Cell[][] cells = new Cell[1][1];
        cells[0][0] = spreadsheet.getCell(1,1);
        uiController.setActiveCell(cells[0][0]);
        
        spreadsheet.setTitle("Test Title");
        
        NavigationController instance = new NavigationController(uiController);
        instance.getSpreadsheetCount();
        
        String expected = "Test Title";
        String result = instance.getSpreadsheetTitle(0);
        
        assertEquals(expected, result);
    }
    
    /**
     * Test method of getSpreadsheetRows()
     */
    @Test
    public void testGetSpreadsheetRows() {
        System.out.println("Test: getSpreadsheetRows()");
        
        CleanSheets cleansheets = new CleanSheets();
        UIController uiController = new UIController(cleansheets);
        
        Workbook workbook = new Workbook(2);
        Spreadsheet spreadsheet = workbook.getSpreadsheet(0);
        
        Cell[][] cells = new Cell[1][1];
        cells[0][0] = spreadsheet.getCell(1, 1);
        uiController.setActiveCell(cells[0][0]);
        
        NavigationController instance = new NavigationController(uiController);
        instance.getSpreadsheetCount();
        instance.getSpreadsheetTitle(0);
        
        int expected = 1;
        int result = instance.getSpreadsheetRows();
        
        assertEquals(expected, result);
    }
    
    /**
     * Test method of getSpreadsheetColumns()
     */
    @Test
    public void testGetSpreadsheetColumns() {
        System.out.println("Test: getSpreadsheetColumns()");
        
        CleanSheets cleansheets = new CleanSheets();
        UIController uiController = new UIController(cleansheets);
        
        Workbook workbook = new Workbook(2);
        Spreadsheet spreadsheet = workbook.getSpreadsheet(0);
        
        Cell[][] cells = new Cell[1][1];
        cells[0][0] = spreadsheet.getCell(1, 1);
        uiController.setActiveCell(cells[0][0]);
        
        NavigationController instance = new NavigationController(uiController);
        instance.getSpreadsheetCount();
        instance.getSpreadsheetTitle(0);
        
        int expected = 1;
        int result = instance.getSpreadsheetColumns();
        
        assertEquals(expected, result);
    }
}
