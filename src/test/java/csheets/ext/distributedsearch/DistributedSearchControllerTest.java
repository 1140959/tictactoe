/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.distributedsearch;

import csheets.CleanSheets;
import csheets.core.Workbook;
import csheets.ext.distributedsearch.ui.DistributedSearchPanel;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paulo Silva - 1960554@isep.ipp.pt
 */
public class DistributedSearchControllerTest {
    /*
    Create the attributes to use in global tests
    */
       
        
    public DistributedSearchControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of sendData method, of class DistributedSearchController.
     */
    @Test
    public void testSendData() {
        System.out.println("sendData");
       // instance.sendData();
        
    }

    /**
     * Test of startListen method, of class DistributedSearchController.
     */
    @Test
    public void testStartListen() {
        System.out.println("startListen");
        //instance.startListen(sp);
    
    }

    /**
     * Test of getName method, of class DistributedSearchController.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
       
        String expResult = "";
//        String result = instance.getName();
//        assertEquals(expResult, result);

    }

    /**
     * Test of actionPerformed method, of class DistributedSearchController.
     */
    @Test
    public void testActionPerformed() {
        System.out.println("actionPerformed");
//        ActionEvent e = new ActionEvent(instance, 0, null);
//        instance.actionPerformed(e);
        
    }

    /**
     * Test of receivedDataObserversWorkbook method, of class DistributedSearchController.
     */
    @Test
    public void testReceivedDataObserversWorkbook() {
        System.out.println("receivedDataObserversWorkbook");
//        instance.receivedDataObserversWorkbook();
    }

    /**
     * Test of getTextSearch method, of class DistributedSearchController.
     */
    @Test
    public void testGetTextSearch() {
        System.out.println("getTextSearch");
        
        String expResult = "workbook";
//        String result = instance.getTextSearch();
//        assertEquals(expResult, result);

    }
    
}
