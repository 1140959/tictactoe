/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.managephoneemail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author David
 */
public class CountryCodeTest {
    
    private static CountryCode code ;
    public static void loadObject(){
        code = new CountryCode("+351", "PT", "Portugal", "[0-9]{9}");
    }
    
    public CountryCodeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCode method, of class CountryCode.
     */
    @Test
    public void testGetCode() {
        loadObject();
        System.out.println("getCode");
        String expected = "+351";
        String obtained = code.getCode();
        assertEquals(expected, obtained);
    }

    /**
     * Test of setCode method, of class CountryCode.
     */
    @Test
    public void testSetCode() {
        loadObject();
        System.out.println("setCode");
        String expected = "+1";
        code.setCode(expected);
        String obtained = code.getCode();
        assertTrue(expected.equals(obtained));
    }

    /**
     * Test of getCountry method, of class CountryCode.
     */
    @Test
    public void testGetCountry() {
        loadObject();
        System.out.println("getCountry");
      String expected = "Portugal";
        String obtained = code.getCountry();
        assertTrue(expected.equals(obtained));
    }

    /**
     * Test of setCountry method, of class CountryCode.
     */
    @Test
    public void testSetCountry() {
        loadObject();
        System.out.println("setCountry");
        String expected = "Canada";
        code.setCountry(expected);
        String obtained = code.getCountry();
        assertTrue(expected.equals(obtained));
    }

    /**
     * Test of getValidation method, of class CountryCode.
     */
    @Test
    public void testGetValidation() {
        loadObject();
        System.out.println("getValidation");
        String expected = "[0-9]{9}";
        String obtained = code.getValidation();
        assertTrue(expected.equals(obtained));
    }

    /**
     * Test of setValidation method, of class CountryCode.
     */
    @Test
    public void testSetValidation() {
        loadObject();
        System.out.println("setValidation");
        String expected = "[AZ]";
        code.setValidation(expected);
        String obtained = code.getValidation();
        assertTrue(expected.equals(obtained));
    }
    
}
