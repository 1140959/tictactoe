/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.managephoneemail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author David
 */
public class PhoneNumberTest {
    
    private static PhoneNumber phone;
    private static CountryCode code;
    
    private void loadObject(){
        code = new CountryCode("+351", "PT", "Portugal", "[0-9]{9}");
        phone = new PhoneNumber(code, "123456789");
    }
    
    public PhoneNumberTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCountry method, of class PhoneNumber.
     */
    @Test
    public void testGetCountry() {
        System.out.println("getCountry");
        loadObject();
         String expected = "+351";
        String obtained = phone.getCountry().getCode();
        assertEquals(expected, obtained);
    }

    /**
     * Test of setCountry method, of class PhoneNumber.
     */
    @Test
    public void testSetCountry() {
        System.out.println("setCountry");
        loadObject();
        String expected = "spain";
        phone.setCountry(new CountryCode("1", "2", "spain", "3"));
        String obtained = phone.getCountry().getCountry();
        assertTrue(expected.equals(obtained));
    }

    /**
     * Test of getNumber method, of class PhoneNumber.
     */
    @Test
    public void testGetNumber() {
        System.out.println("getNumber");
        String expected = "123456789";
        String obtained = phone.getNumber();
        assertEquals(expected, obtained);
    }

    /**
     * Test of setNumber method, of class PhoneNumber.
     */
    @Test
    public void testSetNumber() {
        System.out.println("setNumber");
        loadObject();
        String expected = "791";
        phone.setNumber(expected);
        String obtained = phone.getNumber();
        assertTrue(expected.equals(obtained));
    }
    
}
