/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.managephoneemail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author David
 */
public class EmailTest {
    
    public EmailTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getAddress method, of class Email.
     */
    @Test
    public void testGetAddress() {
        System.out.println("getAddress");
       Email mail = new Email("ola@test.pt");
       String Expected = "ola@test.pt";
       String obtained = mail.getAddress();
        assertEquals(Expected, obtained);
    }

    /**
     * Test of setAddress method, of class Email.
     */
    @Test
    public void testSetAddress() {
        System.out.println("setAddress");
       Email mail = new Email("ola@test.pt");
       String Expected = "ola@test.pt";
       mail.setAddress(Expected);
       String obtained = mail.getAddress();
        assertEquals(Expected, obtained);
    }
    
}
