package csheets.ext.style.ui;

import csheets.CleanSheets;
import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.CellImpl;
import csheets.core.IllegalValueTypeException;
import csheets.core.Spreadsheet;
import csheets.core.Value;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.style.StylableCell;
import csheets.ext.style.StyleExtension;
import csheets.ui.ctrl.UIController;
import java.awt.Color;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1090698@isep.ipp.pt
 */
public class ConditionalFormattingControllerTest {

    public ConditionalFormattingControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getExpressionResult method, of class
     * ConditionalFormattingController. Test getExpressionResult when return
     * true
     *
     * @throws csheets.core.formula.compiler.FormulaCompilationException
     * @throws csheets.core.IllegalValueTypeException
     */
    @Test
    public void testGetExpressionResult() throws FormulaCompilationException, IllegalValueTypeException {
        System.out.println("getExpressionResult (True)");
        CleanSheets app = new CleanSheets();
        UIController uiController = new UIController(app);
        ConditionalFormattingController instance = new ConditionalFormattingController(uiController);

        String string = "=1=1";

        Workbook workbook = new Workbook(1);
        Spreadsheet sheet = workbook.getSpreadsheet(0);
        Cell cell = sheet.getCell(new Address(0, 0));
        cell.setContent("1");
        uiController.setActiveCell(cell);

        CellImpl celli = (CellImpl) uiController.getActiveCell();
        Value value = instance.getExpressionResult(string, (CellImpl) celli);

        boolean expResult = true;
        boolean result = value.toBoolean();

        assertEquals(expResult, result);
    }

    /**
     * Test of getExpressionResult method, of class
     * ConditionalFormattingController. Test getExpressionResult when return
     * false
     *
     * @throws csheets.core.formula.compiler.FormulaCompilationException
     * @throws csheets.core.IllegalValueTypeException
     */
    @Test
    public void testGetExpressionResultFalse() throws FormulaCompilationException, IllegalValueTypeException {
        System.out.println("getExpressionResult (False)");
        CleanSheets app = new CleanSheets();
        UIController uiController = new UIController(app);
        ConditionalFormattingController instance = new ConditionalFormattingController(uiController);

        String stringFormula = "=1=2";

        Workbook workbook = new Workbook(1);
        Spreadsheet sheet = workbook.getSpreadsheet(0);
        Cell cell = sheet.getCell(new Address(0, 0));
        cell.setContent("1");
        uiController.setActiveCell(cell);

        CellImpl celli = (CellImpl) uiController.getActiveCell();
        Value value = instance.getExpressionResult(stringFormula, (CellImpl) celli);

        boolean expResult = false;
        boolean result = value.toBoolean();

        assertEquals(expResult, result);
    }

    /**
     * Test of applyStyles method, of class ConditionalFormattingController.
     */
    @Test
    public void testApplyStyles() throws Exception {
        System.out.println("applyStyles");
        CleanSheets app = new CleanSheets();
        UIController uiController = new UIController(app);
        ConditionalFormattingController instance = new ConditionalFormattingController(uiController);

        String stringFormula = "=1=2";

        Workbook workbook = new Workbook(1);
        Spreadsheet sheet = workbook.getSpreadsheet(0);
        Cell cell = sheet.getCell(new Address(0, 0));
        cell.setContent("1");
        uiController.setActiveCell(cell);

        Color cor = new Color(100, 100, 100);

        CellImpl celli = (CellImpl) uiController.getActiveCell();

        instance.setColorTrue(cor);
        instance.setColorFalse(cor);
        instance.applyStyles(stringFormula, celli);

        StylableCell cell1 = (StylableCell) uiController.getActiveCell().getExtension(StyleExtension.NAME);
        cell1.setBackgroundColor(cor);

        StylableCell cell2 = (StylableCell) celli.getExtension(StyleExtension.NAME);

        Color result = cell1.getBackgroundColor();
        Color expResult = cell2.getBackgroundColor();

        assertEquals(expResult, result);
    }

    /**
     * Test of setColorTrue method, of class ConditionalFormattingController.
     */
    @Test
    public void testSetColorTrue() {
        System.out.println("setColorTrue");
        CleanSheets app = new CleanSheets();
        UIController uiController = new UIController(app);
        ConditionalFormattingController instance = new ConditionalFormattingController(uiController);

        Color cor = new Color(110, 0, 0);
        instance.setColorTrue(cor);

        Color expResult = new Color(110, 0, 0);
        Color result = instance.colorTrue;

        assertEquals(expResult, result);
    }

    /**
     * Test of setColorFalse method, of class ConditionalFormattingController.
     */
    @Test
    public void testSetColorFalse() {
        System.out.println("setColorFalse");
        CleanSheets app = new CleanSheets();
        UIController uiController = new UIController(app);
        ConditionalFormattingController instance = new ConditionalFormattingController(uiController);

        Color cor = new Color(220, 0, 0);
        instance.setColorFalse(cor);

        Color expResult = new Color(220, 0, 0);
        Color result = instance.colorFalse;
        assertEquals(expResult, result);
    }

    /**
     * Test of setBackgroundFalse method, of class
     * ConditionalFormattingController.
     */
    @Test
    public void testSetBackgroundFalse() {
        System.out.println("setBackgroundFalse");
        CleanSheets app = new CleanSheets();
        UIController uiController = new UIController(app);
        ConditionalFormattingController instance = new ConditionalFormattingController(uiController);

        Color cor = new Color(220, 0, 0);
        instance.setBackgroundFalse(cor);

        Color expResult = new Color(220, 0, 0);
        Color result = instance.backgroundFalse;
        assertEquals(expResult, result);
    }

    /**
     * Test of setBackgroundTrue method, of class
     * ConditionalFormattingController.
     */
    @Test
    public void testSetBackgroundTrue() {
        System.out.println("setBackgroundTrue");
        CleanSheets app = new CleanSheets();
        UIController uiController = new UIController(app);
        ConditionalFormattingController instance = new ConditionalFormattingController(uiController);

        Color cor = new Color(110, 0, 0);
        instance.setBackgroundTrue(cor);

        Color expResult = new Color(110, 0, 0);
        Color result = instance.backgroundTrue;

        assertEquals(expResult, result);
    }
//
//    /**
//     * Test of removeStyle method, of class ConditionalFormattingController.
//     *
//     * @throws csheets.core.formula.compiler.FormulaCompilationException
//     * @throws csheets.core.IllegalValueTypeException
//     */
//    @Test
//    public void testRemoveStyle() throws FormulaCompilationException, IllegalValueTypeException {
//        System.out.println("removeStyle");
//        CleanSheets app = new CleanSheets();
//        UIController uiController = new UIController(app);
//        ConditionalFormattingController instance = new ConditionalFormattingController(uiController);
//
//        Workbook workbook = new Workbook(1);
//        Spreadsheet sheet = workbook.getSpreadsheet(0);
//        Cell cell = sheet.getCell(new Address(0, 0));
//        cell.setContent("1");
//        String expression = "=1=1";
//        uiController.setActiveCell(cell);
//        
//        CellImpl celli = (CellImpl) uiController.getActiveCell();
//
//        Color cor = new Color(110, 0, 0);
//        instance.setBackgroundTrue(cor);
//        instance.applyStyles(expression, celli);
//
//        StylableCell stylableCell = (StylableCell) uiController.getActiveCell().getExtension(StyleExtension.NAME);
//        
//        instance.removeStyle();
//
//        Color expResult = new Color(255, 255, 255);
//        Color result = stylableCell.getBackgroundColor();
//
//        assertEquals(expResult, result);
//    }

}
