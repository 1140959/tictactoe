package csheets.ext.comments;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit test class to test Comment class
 *
 * @author 1000841@isep.ipp.pt <Egidio Caldeira>
 */
public class CommentTest {

    private Comment comment;
    private String str = "Egidio Caldeira" + ":\n" + "This is a test" + "\n";

    @BeforeClass
    public static void setUpClass() throws Exception {

    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        comment = new Comment("Egidio Caldeira", "This is a test");
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of toString method, of class Comment.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Comment instance = comment;
        String expResult = str;
        assertTrue((instance.toString()).equals(str));

    }

}
