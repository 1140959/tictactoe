package csheets.ext.comments;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;

/**
 * A Unit Test class to test CommentableCell.
 *
 * @see CommentableCell
 * @author Alexandre Braganca
 */
public class CommentableCellTest {
	
    private List<Comment> lst = new ArrayList<>();
    private Comment c1 = new Comment("Egidio Caldeira", "Test 1");
    private Comment c2 = new Comment("Tiago Silva", "this is the 2nd test");
        
	private boolean isNotified=false;
        
    @Before
    public void setUp() throws Exception {
        lst.add(c1);
        lst.add(c2);
    }

	/**
	 * A method that tests the property hasComment.
     * @throws java.lang.Exception
	 */
	//@Test 
    public void testHasComment() throws Exception {
		
		// create a workbook with 2 sheets
		Workbook wb=new Workbook(2);
		Spreadsheet s=wb.getSpreadsheet(0);
		// get the first cell
		Cell c=s.getCell(0,0);
		
		// activate the comments on the first cell
		CommentableCell cc=new CommentableCell(c);
		boolean hasComment = cc.hasComment();
		assertTrue(hasComment==false);	
        
		cc.setUserComment(c1);
		hasComment=cc.hasComment();
		assertTrue(hasComment);		
	}

	/**
	 * A method that tests the setter and getter of the user comment.
     * @throws java.lang.Exception
	 */
	@Test public void testSetGetUserComment() throws Exception {
		
		// create a workbook with 2 sheets
		Workbook wb=new Workbook(2);
		Spreadsheet s=wb.getSpreadsheet(0);
		// get the first cell
		Cell c=s.getCell(0,0);
		
		// activate the comments on the first cell
		CommentableCell cc=new CommentableCell(c);

		cc.setUserComment(c1);
		
		assertTrue((c1.toString().trim()).compareTo(cc.getUserComment().trim())==0);		
	}
	
	/**
	 * A method that tests the notifications for commented cell listeners.
     * @throws java.lang.Exception
	 * @see CommentableCellListener
	 */	
	@Test public void testCommentableCellListenner() throws Exception {
		
		// create a workbook with 2 sheets
		Workbook wb=new Workbook(2);
		Spreadsheet s=wb.getSpreadsheet(0);
		// get the first cell
		Cell c=s.getCell(0,0);
		
		// activate the comments on the first cell
		CommentableCell cc=new CommentableCell(c);
		
		CommentableCellListener listener=new CommentableCellListenerImpl();
		
		cc.addCommentableCellListener(listener);

		// modify the cell... this should create an event
		cc.setUserComment(c1);
		
		assertTrue(isNotified);		
	}

	/**
	 * A inner utility class used by the method testCommentableCellListenner.
	 */	
	class CommentableCellListenerImpl implements CommentableCellListener {

		@Override
		public void commentChanged(CommentableCell cell) {
			isNotified=true;
		}
		
	}
}
