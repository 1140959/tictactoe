/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.filesharing;

import java.util.ArrayList;
import java.util.Calendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rio
 */
public class FileListTest {
    
    public FileListTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addFile method, of class FileList.
     */
    @Test
    public void testAddFile() {
        System.out.println("addFile");
        String machine = "";
        String file = "";
        long size = 0L;
        FileList instance = new FileList();
        instance.addFile(machine, file, size);
        int expResult = 1;
        int result = instance.getLocalFiles().size();
        assertEquals(expResult,result);
        
    }

    /**
     * Test of exist method, of class FileList.
     */
    @Test
    public void testExist() {
        System.out.println("exist");
        String machine = "theMachine";
        String file = "thefile.txt";
        long size = 1024L;
        FileList instance = new FileList();
        instance.addFile(machine, file, size);
        boolean expResult = true;
        boolean result = instance.exist(machine, file, size);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNetWorkFiles method, of class FileList.
     */
    @Test
    public void testGetNetWorkFiles() {
        System.out.println("getNetWorkFiles");
        String machine = "theMachine";
        String file = "thefile.txt";
        long size = 1024L;
        FileList instance = new FileList();
        instance.addFile(machine, file, size);
        int expResult = 1;
        int result = instance.getNetWorkFiles().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLocalFiles method, of class FileList.
     */
    @Test
    public void testGetLocalFiles() {
        System.out.println("getLocalFiles");
        String machine = ""; // if empty then it's local
        String file = "thefile.txt";
        long size = 1024L;
        FileList instance = new FileList();
        instance.addFile(machine, file, size);
        int expResult = 1;
        int result = instance.getLocalFiles().size();
        assertEquals(expResult, result);
        
    }


    /**
     * Test of cleanOld method, of class FileList.
     */
    @Test
    public void testCleanOld() {
        System.out.println("cleanOld");
        FileList instance = new FileList();
        instance.cleanOld();
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }
    
}
