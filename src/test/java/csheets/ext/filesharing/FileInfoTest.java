/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.filesharing;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rio
 */
public class FileInfoTest {
    
    public FileInfoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of equals method, of class FileInfo.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        FileInfo instance = new FileInfo("","file",0,0L);
        FileInfo instance2 = new FileInfo("","file",0,0L);
        
        boolean expResult = true;
        boolean result = instance.equals(instance2);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getMachine method, of class FileInfo.
     */
    @Test
    public void testGetMachine() {
        System.out.println("getMachine");
        FileInfo instance = new FileInfo("TheMachine","file",0,0L);
        String expResult = "TheMachine";
        String result = instance.getMachine();
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class FileInfo.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        FileInfo instance = new FileInfo("","file",0,0);
        String expResult = "file";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSize method, of class FileInfo.
     */
    @Test
    public void testGetSize() {
        System.out.println("getSize");
        FileInfo instance = new FileInfo("","file",1024,0);
        long expResult = 1024L;
        long result = instance.getSize();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTime method, of class FileInfo.
     */
    @Test
    public void testGetTime() {
        System.out.println("getTime");
        FileInfo instance = new FileInfo("","file",0,10L);
        long expResult = 10L;
        long result = instance.getTime();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setSize method, of class FileInfo.
     */
    @Test
    public void testSetSize() {
        System.out.println("setSize");
        FileInfo instance = new FileInfo("","file",0,0L);
        long size = 10L;
        instance.setSize(size);
        long expResult = 10L;
        long result = instance.getSize();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMachine method, of class FileInfo.
     */
    @Test
    public void testSetMachine() {
        System.out.println("setMachine");
        String machine = "MasterMachine";
        FileInfo instance = new FileInfo("","file",0,0L);
        instance.setMachine(machine);
        String expResult = "MasterMachine";
        String result = instance.getMachine();
        assertEquals(expResult, result);
    }

    /**
     * Test of setName method, of class FileInfo.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "TheFile.txt";
        FileInfo instance = new FileInfo("","file",0,0L);
        instance.setName(name);
        String expResult = "TheFile.txt";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTime method, of class FileInfo.
     */
    @Test
    public void testSetTime() {
        System.out.println("setTime");
        long time = 10L;
        FileInfo instance = new FileInfo("","file",0,0L);
        instance.setTime(time);
       
        long expResult = 10L;
        long result = instance.getTime();
        assertEquals(expResult, result);
    }
    
}
