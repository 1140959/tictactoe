/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.address.ui.panels;

import csheets.CleanSheets;
import csheets.ext.Extension;
import csheets.ext.address.AddressContact;
import csheets.ext.address.AddressExtension;
import csheets.ext.address.AddressType;
import csheets.ext.address.ui.AddressEditionUI;
import csheets.ext.contacts.Agenda;
import csheets.ext.contacts.CompanyContact;
import csheets.ext.contacts.Contact;
import csheets.ext.contacts.Event;
import csheets.ext.contacts.PersonalContact;
import csheets.ext.contacts.Photograph;
import csheets.ext.contacts.Profession;
import csheets.ui.ctrl.UIController;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JFerreira
 */
public class AddressEditionControllerTest {

    public AddressEditionControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setSelectedContact method, of class AddressEditionController.
     */
    @Test
    public void testSetSelectedContact() {
        System.out.println("setSelectedContact");
        CleanSheets app = new CleanSheets();
        AddressExtension ext = new AddressExtension();
        Profession prof = new Profession("prof_teste");
        Photograph myPhoto = new Photograph("testeP");
        Calendar timestamp = Calendar.getInstance();
        Event e = new Event("e teste", timestamp);
        List<Event> myEvents = new LinkedList<Event>();
        myEvents.add(e);
        Agenda muyAgenda = new Agenda(myEvents);
        CompanyContact myCompany = new CompanyContact("teste", muyAgenda);
        PersonalContact thisCont = new PersonalContact("TesteI", "TesteF", myPhoto, myCompany, prof, muyAgenda);
        Contact selectedContact = thisCont;
        UIController uIController = new UIController(app);
        AddressEditionController instance = new AddressEditionController(new AddressEditionUI(ext, uIController), uIController);
        instance.setSelectedContact(selectedContact);
    }

    /**
     * Test of getSelectedContact method, of class AddressEditionController.
     */
    @Test
    public void testGetSelectedContact() {
        System.out.println("getSelectedContact");
        CleanSheets app = new CleanSheets();
        UIController uIController = new UIController(app);
        AddressExtension ext = new AddressExtension();
        Profession prof = new Profession("prof_teste");
        Photograph myPhoto = new Photograph("testeP");
        Calendar timestamp = Calendar.getInstance();
        Event e = new Event("e teste", timestamp);
        List<Event> myEvents = new LinkedList<Event>();
        myEvents.add(e);
        Agenda muyAgenda = new Agenda(myEvents);
        CompanyContact myCompany = new CompanyContact("teste", muyAgenda);
        PersonalContact thisCont = new PersonalContact("TesteI", "TesteF", myPhoto, myCompany, prof, muyAgenda);
        Contact selectedContact = thisCont;
        AddressEditionController instance = new AddressEditionController(new AddressEditionUI(ext, uIController), uIController);
        Contact expResult = null;
        Contact result = instance.getSelectedContact();
        assertEquals(expResult, result);
    }
}
