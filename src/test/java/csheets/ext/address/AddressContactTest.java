/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.address;

import csheets.CleanSheets;
import csheets.ext.Extension;
import csheets.ext.address.ui.AddressEditionUI;
import csheets.ext.address.ui.panels.AddressEditionController;
import csheets.ext.address.ui.panels.CreateAddressPopupPanel;
import csheets.ext.simple.ExtensionExample;
import csheets.ui.ctrl.UIController;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author jfferreira
 */
public class AddressContactTest {
    
    public AddressContactTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of retrieveInformation method, of class AddressContact.
     */
    @Test
    public void testRetrieveInformation() {
        System.out.println("retrieveInformation");
        UIController uiController = new UIController(new CleanSheets());
        Extension extension = new ExtensionExample();
        AddressEditionUI ui = new AddressEditionUI(extension, uiController);
        CreateAddressPopupPanel thePanel = new CreateAddressPopupPanel(new AddressEditionController(ui, uiController));
        AddressContact instance = new AddressContact("Test street","Test town","Test postalCode","Test city","Test country", AddressType.PRIMARY);
        instance.retrieveInformation(thePanel);
    }

    /**
     * Test of editThisAddress method, of class AddressContact.
     */
    @Test
    public void testEditThisAddress() {
        System.out.println("editThisAddress");
        AddressContact newAddress = new AddressContact("Test street","Test town","Test postalCode","Test city","Test country", AddressType.PRIMARY);
        AddressContact instance = new AddressContact("Test2 street","Test town","Test postalCode","Test city","Test country", AddressType.PRIMARY);;
        AddressContact expResult = instance;
        AddressContact result = instance.editThisAddress(newAddress);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class AddressContact.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        AddressContact instance = new AddressContact("Test street","Test town","Test postalCode","Test city","Test country", AddressType.PRIMARY);
        String expResult = "PRIMARY:Test street - Test town, Test postalCode - Test city, Test country";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
