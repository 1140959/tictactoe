/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.address;

import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import org.junit.*;

/**
 *
 * @author Joel Ferreira (1140777)
 */

public class AddressExtensionTest {

    public AddressExtensionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void createNewAdress(){ 
        System.out.println("Creation of an address");
        AddressContact add = new AddressContact("StreetTest","TestTown","TestPostalCode","TestCity","TestCountry");
        String res = add.getAddress();
        Assert.assertEquals("StreetTest - TestTown, TestPostalCode - TestCity, TestCountry", res);   
    }
    
    @Test
    public void editAddress() {
        System.out.println("Edit an address");
        AddressContact add = new AddressContact("StreetTest","TestTown","TestPostalCode","TestCity","TestCountry");
        AddressContact add2 = new AddressContact("StreetTest2","TestTown","TestPostalCode","TestCity","TestCountry");
        add.editThisAddress(add2);
        String res = add.getAddress();
        Assert.assertEquals("StreetTest2 - TestTown, TestPostalCode - TestCity, TestCountry", res); 
    }
    
    class AddressContact{
        
        private String street;
        private String town;
        private String postalCode;
        private String city;
        private String country; 

        public AddressContact(String street, String town, String postalCode, String city, String country) {
            this.street = street;
            this.town = town;
            this.postalCode = postalCode;
            this.city = city;
            this.country = country; 
        }
        
        public AddressContact editThisAddress(AddressContact newAddress) {
            this.street = newAddress.street;
            this.town = newAddress.town;
            this.postalCode = newAddress.postalCode;
            this.city = newAddress.city;
            this.country = newAddress.country;
            return this;
        }

        public String getAddress() {
            return street + " - " + town + ", " + postalCode + " - " + city + ", " + country;
        } 
        
    }
}

