/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.beanshell;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Antonio
 */
public class BeanShellImplTest {
    
    public BeanShellImplTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of runScript method, of class BeanShellImpl.
     */
    @Test
    public void testRunScript() throws Exception {
        System.out.println("runScript");
        String script = "foo = 5; bar = 20; foobar = foo + bar;";
        BeanShellScript instance = new BeanShellScript();
        String expResult = "25";
        String result = instance.runScript(script);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of runScript method, of class BeanShellImpl.
     */
    @Test
    public void testRunScriptComment() throws Exception {
        System.out.println("runScript");
        String script = "//this is comment";
        BeanShellScript instance = new BeanShellScript();
        String expResult = "";
        String result = instance.runScript(script);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of runScript method, of class BeanShellImpl.
     */
    @Test
    public void testRunScriptCostumComment() throws Exception {
        System.out.println("runScript");
        String script = ";this is comment\n;this another comment";
        BeanShellScript instance = new BeanShellScript();
        String expResult = "";
        String result = instance.runScript(script);
        assertEquals(expResult, result);
    }
    
    
    /**
     * Test of runScript method, of class BeanShellImpl.
     */
    @Test
    public void testRunScriptScriptAndComment() throws Exception {
        System.out.println("runScript");
        String script = ";5+5\n10+10";
        BeanShellScript instance = new BeanShellScript();
        String expResult = "20";
        String result = instance.runScript(script);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of runScript method, of class BeanShellImpl.
     */
    @Test
    public void testRunScriptOpenWorkBook() throws Exception {
        System.out.println("runScript");
        String script = "";
        BeanShellScript instance = new BeanShellScript();
        String expResult = "";
        String result = instance.runScript(script);
        assertEquals(expResult, result);
    }
    
}
