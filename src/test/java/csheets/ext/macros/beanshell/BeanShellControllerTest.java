///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package csheets.ext.macros.beanshell;
//
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author Antonio
// */
//public class BeanShellControllerTest {
//
//    public BeanShellControllerTest() {
//    }
//
//    @BeforeClass
//    public static void setUpClass() {
//    }
//
//    @AfterClass
//    public static void tearDownClass() {
//    }
//
//    @Before
//    public void setUp() {
//    }
//
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of loadDefault method, of class BeanShellController.
//     */
//    //@Test
//    public void testLoadDefault() throws Exception {
//        System.out.println("loadDefault");
//        BeanShellController instance = new BeanShellController();
//        String expResult = "import csheets.ext.macros.MacroController;\n"
//                + "\n"
//                + "cell = workbook.getSpreadsheet(0).getCell(0,0);\n"
//                + "ctrl = new MacroController();\n"
//                + "\n"
//                + "ctrl.setMacroName(\"macro\");\n"
//                + "ctrl.addInstruction(\"A1:=5*5\");\n"
//                + "ctrl.addInstruction(\"A2:=5*5+1\");\n"
//                + "\n"
//                + "JOptionPane.showMessageDialog(null,ctrl.executeMacro(cell));\n"
//                + "";
//        String result = instance.loadDefault();
//        assertEquals(expResult.replaceAll("[\r\n]+", ""), result.replaceAll("[\r\n]+", ""));
//    }
//
//    /**
//     * Test of runScript method, of class BeanShellController.
//     */
//    @Test
//    public void testRunScript() throws Exception {
//        System.out.println("runScript");
//        String script = "foo=5;foo";
//        BeanShellController instance = new BeanShellController();
//        String expResult = "5";
//        String result = instance.runScript(script);
//        assertEquals(expResult, result);
//    }
//
//}
