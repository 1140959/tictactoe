/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros;

import csheets.core.IllegalValueTypeException;
import csheets.core.Spreadsheet;
import csheets.core.Value;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hugoa
 */
public class MacroTest {
    
    private Macro macro;
    Workbook workbook;
    Spreadsheet sp;
    
    @Before
    public void setUp() {
        workbook = new Workbook(1);
        sp = workbook.getSpreadsheet(0);
        List<String> instructions = new ArrayList<>();
        instructions.add("A1:=3*3");
        instructions.add("B2:=A1*3");
        //por algum motivo, na instruçao a cima o valor de A1 multiplica por 3
        instructions.add("A1:=3*3");
        macro = new Macro("Test Macro", instructions, workbook);
    }
    
    /**
     * Test of isValid method, of class Macro.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");
        assertEquals(true, macro.isValid());
        Macro macro2 = new Macro("Test Macro2", new ArrayList<>(), workbook);
        assertEquals(false, macro2.isValid());
    }

    /**
     * Test of execute method, of class Macro.
     * @throws csheets.core.formula.compiler.FormulaCompilationException Exception
     * @throws csheets.core.IllegalValueTypeException Exception
     */
    @Test
    public void testExecute() throws FormulaCompilationException, IllegalValueTypeException {
        System.out.println("execute");
        macro.execute(sp.getCell(0, 0));
        assertEquals(new Value(9).toString(), sp.getCell(0, 0).getValue().toString());
        assertEquals(new Value(27).toString(), sp.getCell(1, 1).getValue().toString());
    }
    
}
