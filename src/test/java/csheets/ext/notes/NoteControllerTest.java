/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.notes;

import csheets.ext.contacts.CompanyContact;
import csheets.ext.contacts.Contact;
import csheets.ext.notes.ui.NotesDTO;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 */
public class NoteControllerTest {
    
    public NoteControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getContactActive method, of class NoteController.
     */
    @Test
    public void testGetContactActive() {
        System.out.println("getContactActive");
        NoteController instance = new NoteController();
        Contact expResult = new CompanyContact("Company", null);
        Contact result = instance.getContactActive();
        assertEquals(expResult, result);
    }

    /**
     * Test of createNote method, of class NoteController.
     * @throws java.lang.Exception
     */
    @Test
    public void testCreateNote() throws Exception {
        System.out.println("createNote");
        String description = "Teste de Lapr4";
        LocalDateTime date = LocalDateTime.now();
        NoteController instance = new NoteController();
        NotesDTO dto = new NotesDTO("Teste de Lapr4", LocalDateTime.now());
        NoteService serv = new NoteService();
        Note expResult = serv.create(dto);
        Note result = instance.createNote(description, date);
        assertEquals(expResult, result);
    }

    /**
     * Test of saveNote method, of class NoteController.
     *
     * @throws java.lang.Exception
     *
     * @Test
     * @SuppressWarnings("empty-statement")
     */
    public void testSaveNote() throws Exception {
        System.out.println("saveNote");
        NotesDTO dto = new NotesDTO("Testen çmdlgkdlf", LocalDateTime.now());
        NoteService serv = new NoteService();
        Note newNote = serv.create(dto);
        Contact contact = new CompanyContact("Company", null);
        List<Note> list = new ArrayList<>();
        contact.alterList(list);
        NoteController instance = new NoteController();
        boolean expResult = true;
        boolean result = instance.saveNote(newNote, contact);
        assertEquals(expResult, result);
    }

    /**
     * Test of editNote method, of class NoteController.
     * @throws java.lang.Exception
     */
    @Test
    public void testEditNote() throws Exception {
        NoteController controller = new NoteController();
        controller.getContactActive();
        System.out.println("editNote");
        String description = "Testepj aihoajsd";
        Integer position = 01;
        Contact contact = new CompanyContact("Company", null);
        NotesDTO dto = new NotesDTO("Testepj aihoajsd", LocalDateTime.now());
        NoteService serv = new NoteService();
        Note expResult = serv.create(dto);
        NotesDTO dto1 = new NotesDTO("Testepj aihoajsd", LocalDateTime.now());
        NoteService serv1 = new NoteService();
        Note expResult1 = serv1.create(dto1);
        List<Note> list = new ArrayList<>();
        list.add(expResult);
        list.add(expResult1);
        contact.alterList(list);
        Note result = controller.editNote(description,expResult);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeNote method, of class NoteController.
     *
     * @throws java.lang.Exception
     * @Test
     */
    public void testRemoveNote() throws Exception {
        NoteController controller = new NoteController();
        Contact contact = new CompanyContact("Company", null);
        NotesDTO dto2 = new NotesDTO("Testepj aihoajsd", LocalDateTime.now());
        NoteService serv2 = new NoteService();
        Note expResult2 = serv2.create(dto2);
        NotesDTO dto1 = new NotesDTO("Testepj aihoajsd", LocalDateTime.now());
        NoteService serv1 = new NoteService();
        Note expResult1 = serv1.create(dto1);
        List<Note> list = new ArrayList<>();
        list.add(expResult2);
        list.add(expResult1);
        contact.alterList(list);
        
        System.out.println("removeNote");
        
        boolean expResult = true;
        boolean result = controller.removeNote(contact, expResult1);
        assertEquals(expResult, result);
    }
    
}
