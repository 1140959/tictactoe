/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.notes;

import csheets.ext.notes.ui.NotesDTO;
import java.time.LocalDateTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 */
public class NoteTest {

    private Note note;

    public NoteTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of equals method, of class Note.
     * @throws java.lang.Exception
     */
    @Test
    public void testEquals() throws Exception {
        System.out.println("equals");
        String text = "TesteLAPR4";
        Object obj = new Note(text, LocalDateTime.now());
        NotesDTO dto = new NotesDTO("test teste teste", LocalDateTime.now());
        NoteService serv = new NoteService();
        Note instance = serv.create(dto);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Note.
     * @throws java.lang.Exception
     */
    @Test
    public void testToStringTitle() throws Exception {
        System.out.println("toString");
        NotesDTO dto = new NotesDTO("test teste teste", LocalDateTime.now());
        NoteService serv = new NoteService();
        Note instance = serv.create(dto);
        String expResult = "Titulo:test";
        String result = instance.toStringTitle();
        assertEquals(expResult, result);
    }
}
