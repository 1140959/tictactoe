/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.searchworkbooksextension;

import csheets.core.Workbook;
import java.io.File;
import java.net.URL;
import javax.swing.DefaultListModel;
import javax.swing.ListModel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * TODO: Before testing make sure the basePath is correct
 * @author Rui-Think
 */
public class WorkbookFileSearchServiceTest {

    private final class GetClassLocation {

        private GetClassLocation() {
            super();
        }
    }

    String basePath;
    

    public WorkbookFileSearchServiceTest() {
        // Get test folder path for getting files to test.
//        URL location;
//        final String classLocation = GetClassLocation.class.getName().replace('.', '/')
//                + ".class";
//        final ClassLoader loader = GetClassLocation.class.getClassLoader();
//        location = loader.getResource(classLocation);
//        String[] path = location.getPath().split("/");
//        StringBuilder builder = new StringBuilder();
//        for(int i=1;i<path.length-1;i++) {
//            builder.append(path[i]+"\\");
//        }
//        this.basePath = builder.toString();
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of startSearching method, of class WorkbookFileSearchService.
     */
    @Test
    public void testStartSearching() throws Exception {
//        System.out.println("startSearching");
//        String parentDirectory = this.basePath;
//        String fileType = "cls";
//        ListModel<String> workbookPathList = new DefaultListModel();
//        IFileSearchService instance = new WorkbookFileSearchService();
//        instance.startSearching(parentDirectory, fileType, workbookPathList);
//        assertEquals(15, workbookPathList.getSize());
    }

    /**
     * Test of startSearching method, of class WorkbookFileSearchService.
     */
    @Test
    public void testStartSearchingWithoutSubdirectories() throws Exception {
//        System.out.println("startSearching");
//        String parentDirectory = this.basePath+"\\"+"WithoutSubDirectories";
//        String fileType = "cls";
//        ListModel<String> workbookPathList = new DefaultListModel();
//        IFileSearchService instance = new WorkbookFileSearchService();
//        instance.startSearching(parentDirectory, fileType, workbookPathList);
//        assertEquals(7, workbookPathList.getSize());
    }
}
