/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;

import csheets.ext.contacts.ui.ContactDTO;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.assertEquals;

/**
 * Created by Sérgio Silva (1141074) on 10/06/2016.
 */
public class CompanyContactTest {

    public void result(String test) {
        System.out.println("CompanyContactTest{" + test +"} - SUCCESS!\n" + System.currentTimeMillis() + "\n\n");
    }

    @Test
    public void getCompany() throws Exception {
        CompanyContact cc = new CompanyContact("Company",new Agenda(new LinkedList<>()));
        assertEquals(cc,cc.getCompany());
        result("getCompany()");
    }

    @Test
    public void getCompanyName() throws Exception {
        CompanyContact cc = new CompanyContact("Company",new Agenda(new LinkedList<>()));
        Assert.assertEquals("Company",cc.getCompanyName());
        result("getCompanyName()");
    }


    @Test
    public void editThisContact() throws Exception {
        CompanyContact cc = new CompanyContact("Company",new Agenda(new LinkedList<>()));
        ContactDTO cdto = new ContactDTO("Company",false);
        Assert.assertNotEquals(cdto.getCompanyName(),cc.editThisContact(cdto));
        result("editThisContact()");
    }


}