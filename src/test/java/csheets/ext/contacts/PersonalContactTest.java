/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;

import csheets.ext.contacts.ui.ContactDTO;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;

/**
 * Created by Sérgio Silva (1141074) on 10/06/2016.
 */
public class PersonalContactTest {
    PersonalContact pc = new PersonalContact("Sérgio","Silva",new Photograph(""),null,null,new Agenda(new LinkedList<>()));
    CompanyContact cc = new CompanyContact("Company",new Agenda(new LinkedList<>()));
    ContactDTO cdto = new ContactDTO("Sérgio", "Silva", new File(""), cc, null, true);


    public void result(String test) {
        System.out.println("CompanyContactTest{" + test +"} - SUCCESS!\n" + System.currentTimeMillis() + "\n\n");
    }

    @Test
    public void hasCompleteName() throws Exception {
        assertEquals(true,pc.hasCompleteName(cdto.getFirstName(),cdto.getLastName()));
        result("hasCompleteName()");
    }

    @Test
    public void getName() throws Exception {
        assertEquals("[P] : Sérgio Silva",pc.toString());
        result("getName()");
    }

    @Test
    public void editThisContact() throws Exception {
        Assert.assertNotEquals(cdto.getFirstName(),pc.editThisContact(cdto));
        result("editThisContact()");
    }

}