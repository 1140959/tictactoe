/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts.ui;

import csheets.ext.contacts.Agenda;
import csheets.ext.contacts.CompanyContact;
import csheets.ext.contacts.CompanyContactBuilder;
import csheets.ext.contacts.Profession;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;

/**
 * Created by Sérgio Silva (1141074) on 10/06/2016.
 */
public class ContactDTOTest {
    @Before
    public void setUp() throws Exception {


    }

    @After
    public void tearDown() throws Exception {

    }

    public void result(String test) {
        System.out.println("ContactDTOTest{" + test +"} - SUCCESS!\n" + System.currentTimeMillis() + "\n\n");
    }

    @Test
    public void isPersonalContact() throws Exception {
        ContactDTO cdto = new ContactDTO("Company",false);
        assertEquals(false,cdto.isPersonalContact());
        result("isPersonalContact()");
    }

    @Test
    public void getFirstName() throws Exception {
        ContactDTO cdto = new ContactDTO("Sérgio","Silva",new File("sadasd"),null,null,true);
        Assert.assertEquals("Sérgio",cdto.getFirstName());
        result("getFirstName()");
    }

    @Test
    public void getLastName() throws Exception {
        ContactDTO cdto = new ContactDTO("Sérgio","Silva",new File("sadasd"),null,null,true);
        Assert.assertEquals("Silva",cdto.getLastName());
        result("getLastName()");
    }

    @Test
    public void getPhoto() throws Exception {
        File photo = new File("abc");
        ContactDTO cdto = new ContactDTO("Sérgio","Silva",photo,null,null,true);
        Assert.assertEquals(new File("abc"),cdto.getPhoto());
        result("getPhoto()");
    }

    @Test
    public void getCompanyName() throws Exception {
        CompanyContact cc = new CompanyContact("IBM",new Agenda(new LinkedList<>()));
        ContactDTO cdto = new ContactDTO("Sérgio","Silva",new File("sadasd"),cc,null,true);
        CompanyContactBuilder companyContactBuilder = new CompanyContactBuilder();
        companyContactBuilder.setCompanyName(cdto.getCompanyName());
        companyContactBuilder.createCompanyContact();
        Assert.assertNotEquals(cc.getCompanyName(),cdto.getCompanyName());
        result("getCompanyName()");
    }

    @Test
    public void getCompany() throws Exception {
        CompanyContact cc = new CompanyContact("IBM",new Agenda(new LinkedList<>()));
        ContactDTO cdto = new ContactDTO("Sérgio","Silva",new File("sadasd"),cc,null,true);
        Assert.assertEquals(cc,cdto.getCompany());
        result("getCompany()");
    }

    @Test
    public void getProfession() throws Exception {
        CompanyContact cc = new CompanyContact("IBM",new Agenda(new LinkedList<>()));
        Profession p = new Profession("Profissão teste");
        ContactDTO cdto = new ContactDTO("Sérgio","Silva",new File("sadasd"),cc,p,true);
        Assert.assertEquals(p,cdto.getProfession());
        result("getProfession()");
    }

}