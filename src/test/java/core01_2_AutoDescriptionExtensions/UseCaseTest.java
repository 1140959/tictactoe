/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core01_2_AutoDescriptionExtensions;

import csheets.CleanSheets;
import csheets.ext.Extension;
import csheets.ext.ExtensionManager;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * Use case test for load extensions
 *
 * @author Tiago Gabriel <1140775@isep.ipp.pt>
 */
public class UseCaseTest {

    ExtensionManagerMOCK mock = ExtensionManagerMOCK.getInstance();

    static List<Extension> allLoadedExtensions = new LinkedList<>();

    @Test
    public void checkIfLoadsExtensions() {
        System.out.println("LOADS EXTENSIONS");
        Extension[] extensions = mock.getExtensions();
        assertTrue("Should be true", (extensions.length == 0) && (allLoadedExtensions.size() > 0));
    }

    @Test
    public void successOfFrameLoading() {
        System.out.println("Checks if the frame is successfully loaded");
        MyFrameMOCK frame = new MyFrameMOCK(new ControllerMock(allLoadedExtensions, new LinkedList<>()));
        int size = frame.getAllExtensions().size();
        int size2 = frame.getSelectedExtensions().size();
        assertTrue("Should have model size equals to the list size", (size == allLoadedExtensions.size()));
        assertTrue("Should have the model size = 0", (size2 == 0));
    }

    @Test
    public void selectingExtensionOnTheList() {
        System.out.println("Selecting extensions");
        ControllerMock controller = new ControllerMock(allLoadedExtensions, new LinkedList<>());
        MyFrameMOCK frame = new MyFrameMOCK(controller);
        int sizeInicial = frame.getAllExtensions().size();
        controller.selectExtension(0);
        controller.selectExtension(0);
        int sizeFinal = frame.getAllExtensions().size();

        int dif = sizeInicial - sizeFinal;

        assertTrue("Should be == 2", dif == 2);
        assertTrue("Should be same size", dif == frame.getSelectedExtensions().size());
    }

    @Test
    public void deselectsAnExtension() {
        System.out.println("Deselecting extensions");
        ControllerMock controller = new ControllerMock(allLoadedExtensions, new LinkedList<>());
        MyFrameMOCK frame = new MyFrameMOCK(controller);
        int sizeInicial = frame.getAllExtensions().size();
        controller.selectExtension(0);
        controller.selectExtension(0);

        //deselect again
        controller.deselectExtension(0);
        controller.deselectExtension(0);
        int sizeFinal = frame.getAllExtensions().size();
        assertTrue("Should be same size", sizeFinal == sizeInicial);
    }

    @Test
    public void selectinOfAll() {
        System.out.println("Selecting all the extensions ");
        ControllerMock controller = new ControllerMock(allLoadedExtensions, new LinkedList<>());
        MyFrameMOCK frame = new MyFrameMOCK(controller);
        int sizeInicial = frame.getAllExtensions().size();

        //selects all of the list
        controller.selectAll();
        int size = frame.getAllExtensions().size();
        assertTrue("Should be empty", size == 0);

        //deselects all of the list
        controller.deselectAll();
        size = frame.getAllExtensions().size();
        assertTrue("Should have the same size", size == sizeInicial);

        controller.selectExtension(0);
        controller.selectExtension(0);
        controller.selectAll();
        size = frame.getAllExtensions().size();
        assertTrue("Should be empty", size == 0);
    }

    @Test
    public void loadSelectedExtensions() {
        System.out.println("Load Selected Extensions");
        List<Extension> myExtensions = new LinkedList<>();
        ControllerMock controller = new ControllerMock(allLoadedExtensions, myExtensions);
        MyFrameMOCK frame = new MyFrameMOCK(controller);
        int sizeInicial = frame.getAllExtensions().size();

        boolean loadExtensions = controller.loadExtensions();
        assertTrue("Should be false", loadExtensions == false);

        controller.selectExtension(0);
        controller.selectExtension(0);

        int size = frame.getAllExtensions().size();

        loadExtensions = controller.loadExtensions();
        assertTrue("Should be true", loadExtensions == true);

        int mine = myExtensions.size();

        assertTrue("Should be of the same size = 2", mine == (sizeInicial - size));
    }

    //This class already exists but it will be modified
    static class ExtensionManagerMOCK {

        /**
         * The singleton instance
         */
        private static final ExtensionManagerMOCK instance = new ExtensionManagerMOCK();

        /**
         * The name of the files in which extension properties are stored
         */
        private static final String PROPERTIES_FILENAME = "extensions.props";

        /**
         * The extensions that have been loaded
         */
        private SortedMap<String, Extension> extensionMap
                = new TreeMap<>();

        /**
         * The class loader used to load extensions
         */
        private Loader loader = new Loader();

        /**
         * Creates the extension manager.
         */
        private ExtensionManagerMOCK() {
            // Loads default extension properties
            Properties extProps = new Properties();
            InputStream stream = CleanSheets.class.getResourceAsStream("res/" + PROPERTIES_FILENAME);
            if (stream != null) {
                try {
                    extProps.load(stream);
                } catch (IOException e) {
                    System.err.println("Could not load default extension properties from: "
                            + PROPERTIES_FILENAME);
                } finally {
                    try {
                        if (stream != null) {
                            stream.close();
                        }
                    } catch (IOException e) {
                    }
                }
            }

            // Loads extensions
            Extension loaded = null;

            for (Map.Entry<Object, Object> entry : extProps.entrySet()) {
                // Resolves class path
                String classPathProp = (String) entry.getValue();
                URL classPath = null;
                if (classPathProp.length() > 0) {
                    // Looks for resource
                    classPath = ExtensionManager.class.getResource(classPathProp);
                    if (classPath == null) {
                        // Looks for file
                        File classPathFile = new File(classPathProp);
                        if (classPathFile.exists()) {
                            try {
                                classPath = classPathFile.toURL();
                            } catch (MalformedURLException e) {
                            }
                        }
                    }
                }

                // Loads class
                String className = (String) entry.getKey();
                if (classPath == null) {
                    loaded = load(className);
                } else {
                    loaded = load(className, classPath);
                }
                allLoadedExtensions.add(loaded);
            }
        }

        /**
         * Returns the singleton instance.
         *
         * @return the singleton instance
         */
        public static ExtensionManagerMOCK getInstance() {
            return instance;
        }

        /**
         * Returns the extensions that have been loaded.
         *
         * @return the extensions that have been loaded
         */
        public Extension[] getExtensions() {
            Collection<Extension> extensions = extensionMap.values();
            return extensions.toArray(new Extension[extensions.size()]);
        }

        /**
         * Returns the extension with the given name.
         *
         * @param name name
         * @return the extesion with the given name or null if none was found
         */
        public Extension getExtension(String name) {
            return extensionMap.get(name);
        }

        /**
         * Adds the given url to the class path, and loads the extension with
         * the given class name.
         *
         * @param className the complete class name of a class that extends the
         * abstract Extension class
         * @param url the URL of the JAR-file or directory that contains the
         * class
         * @return the extension that was loaded, or null if none was found.
         */
        public Extension load(String className, URL url) {
            loader.addURL(url);
            try {
                Class extensionClass = Class.forName(className, true, loader);
                return load(extensionClass);
            } catch (Exception e) {
                System.err.println("Failed to load extension class " + className + ".");
                return null;
            }
        }

        /**
         * Loads the extension with the given class name.
         *
         * @param className the complete class name of a class that extends the
         * abstract Extension class
         * @return the extension that was loaded, or null if none was found.
         */
        public Extension load(String className) {
            try {
                Class extensionClass = Class.forName(className);
                return load(extensionClass);
            } catch (Exception e) {
                System.err.println("Failed to load extension class " + className + ".");
                return null;
            }
        }

        /**
         * Instantiates the given extension class.
         *
         * @param extensionClass a class that extends the abstract Extension
         * class
         * @return the extension that was loaded, or null if none was found.
         */
        public Extension load(Class extensionClass) {
            try {
                Extension extension = (Extension) extensionClass.newInstance();
                //extensionMap.put(extension.getName(), extension);
                return extension;
            } catch (IllegalAccessException iae) {
                System.err.println("Could not access extension " + extensionClass.getName() + ".");
                return null;
            } catch (InstantiationException ie) {
                System.err.println("Could not load extension from " + extensionClass.getName() + ".");
                ie.printStackTrace();
                return null;
            }
        }

        /**
         * Returns the class loader used to load extensions.
         *
         * @return the class loader used to load extensions
         */
        public ClassLoader getLoader() {
            return loader;
        }

        /**
         * The class loader used to load extensions.
         */
        public static class Loader extends URLClassLoader {

            /**
             * Creates a new extension loader.
             */
            public Loader() {
                super(new URL[]{}, Loader.class.getClassLoader());
            }

            /**
             * Appends the specified URL to the list of URLs to search for
             * classes and resources.
             *
             * @param url the URL to be added to the search path of URL:s
             */
            protected void addURL(URL url) {
                super.addURL(url);
            }
        }
    }

}
