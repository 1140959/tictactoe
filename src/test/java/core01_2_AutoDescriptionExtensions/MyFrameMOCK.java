/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package core01_2_AutoDescriptionExtensions;

import csheets.ext.Extension;
import javax.swing.DefaultListModel;

/**
 * 
 * @author Tiago Gabriel <1140775@isep.ipp.pt>
 */
public class MyFrameMOCK {

    private DefaultListModel<Extension> allExtensions = new DefaultListModel<>();
    private DefaultListModel<Extension> selectedExtensions = new DefaultListModel<>();
    private final ControllerMock myController;

    public MyFrameMOCK(ControllerMock myController) {
        this.myController = myController;
        this.myController.setModels(allExtensions,selectedExtensions);
    }

    public DefaultListModel<Extension> getAllExtensions() {
        return allExtensions;
    }

    public DefaultListModel<Extension> getSelectedExtensions() {
        return selectedExtensions;
    }
    
    
}
