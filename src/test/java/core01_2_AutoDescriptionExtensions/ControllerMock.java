/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core01_2_AutoDescriptionExtensions;

import csheets.ext.Extension;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import javax.swing.DefaultListModel;

/**
 * An Mock class of the controller
 *
 * @author Tiago Gabriel <1140775@isep.ipp.pt>
 */
public class ControllerMock {

    private final List<Extension> allExtensions;
    private final List<Extension> selectedExtensions;

    private DefaultListModel<Extension> all;
    private DefaultListModel<Extension> selected;

    public ControllerMock(List<Extension> allExtensions, List<Extension> selectedExtensions) {
        this.allExtensions = allExtensions;
        this.selectedExtensions = selectedExtensions;
    }

    void setModels(DefaultListModel<Extension> allExtensions, DefaultListModel<Extension> selectedExtensions) {
        this.selected = selectedExtensions;
        this.all = allExtensions;
        for (Extension e : this.allExtensions) {
            all.addElement(e);
        }
    }

    void selectExtension(int index) {
        Extension remove = this.all.remove(index);
        if (remove != null) {
            this.selected.addElement(remove);
        }
    }

    void deselectExtension(int index) {
        int size = this.selected.size();
        if (size == 0) {
            return;
        }

        for (int i = 0; i < size; i++) {
            if (i == index) {
                Extension ext = this.selected.remove(i);
                this.all.addElement(ext);
            }
        }
    }

    void selectAll() {
        int size = this.all.size();
        Extension ext = null;
        while (size != 0) {
            ext = this.all.remove(0);
            this.selected.addElement(ext);
            size--;
        }
    }

    void deselectAll() {
        int size = this.selected.size();
        Extension ext = null;
        while (size != 0) {
            ext = this.selected.remove(0);
            this.all.addElement(ext);
            size--;
        }
    }

    boolean loadExtensions() {
        if (selected.isEmpty()) {
            return false;
        }
        Enumeration<Extension> elements = selected.elements();

        while (elements.hasMoreElements()) {
            Extension ext = elements.nextElement();
            this.selectedExtensions.add(ext);
        }
        return true;
    }

}
