/**
 *
 */
package csheets.persistence;

/**
 * An exception that violates a data integrity constraint like a duplicate key
 *
 * Class used during EAPLI - thank you teacher for the contribution!
 * @author Paulo Gandra Sousa
 *
 */
public class DataIntegrityViolationException extends Exception {

    /** Empty Construct */
    public DataIntegrityViolationException() {
    }

    /**
     * @param arg0 Message to be written
     */
    public DataIntegrityViolationException(String arg0) {
        super(arg0);
    }

    /**
     * @param arg0 Throwable Object
     */
    public DataIntegrityViolationException(Throwable arg0) {
        super(arg0);
    }

    /**
     * @param arg0 Message to be written
     * @param arg1 Throwable Object
     */
    public DataIntegrityViolationException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }
}
