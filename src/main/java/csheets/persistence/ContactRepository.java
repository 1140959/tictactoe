/*
 * LAPR4 - Cleansheets
 */
package csheets.persistence;

import csheets.ext.contacts.Contact;
import java.util.LinkedList;

/**
 * Interface for contacts repository
 * @author Tiago Gabriel 1140775
 */
public interface ContactRepository extends Repository<Contact,Long>{

    Contact findBySystemUser(String systemUser);

}