/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.persistence;

import csheets.ext.notes.Note;
import java.util.Iterator;

/**
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 */
public interface NoteRepository extends Repository<Note,Long>{
    
    @Override
    Iterable<Note> all();
}
