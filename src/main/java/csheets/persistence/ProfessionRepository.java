package csheets.persistence;

import csheets.ext.contacts.Profession;

/**
 * Created by Sérgio Silva (1141074) on 10/06/2016.
 */
public interface ProfessionRepository extends Repository<Profession,Long> {

    /**
     * Saves an entity either by creating it or updating it in the persistence.
     * store.
     *
     * @param entity Entity to save
     * @return Saved entity
     */
    Profession save(Profession entity);

    /**
     * Creates a new an entity in the persistence layer. if the entity already
     * exists it will throw an exception.
     *
     * FIXME check which exception to throw. it should not be a persistence
     * layer exception.
     *
     * @param entity
     * @return true or false
     * @throws csheets.persistence.DataIntegrityViolationException
     */
    boolean add(Profession entity) throws DataIntegrityViolationException;

    /**
     * Gets all entities from the repository.
     *
     * @return Iterable of all entities
     */
    Iterable<Profession> all();

    /**
     * Gets the entity with the specified id
     *
     * @param id Id of the entity
     * @return Entity
     */
    Profession findById(Long id);

    /**
     * removes the specified entity from the repository.
     *
     * @param entity
     */
    void delete(Profession entity);

    /**
     * Removes the entity with the specified ID from the repository.
     *
     * @param entityId The entity Id
     */
    void deleteById(Long entityId);

    /**
     * Returns the number of entities in the repository.
     *
     * @return Number of entities
     */
    long size();
}
