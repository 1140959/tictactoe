/*
 * LAPR4 - Cleansheets
 */
package csheets.persistence;

/**
 * The repository factory of all repositories
 *
 * @author Tiago Gabriel 1140775
 */
public interface RepositoryFactory {

    /**
     * Repository for all contacts
     *
     * @return Contact repository factory
     */
    ContactRepository contacts();

    /**
     * Repository for all notes
     *
     * @return Note repository factory
     */
    NoteRepository notes();
}
