/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package csheets.persistence;


/**
 * A generic interface for repositories.
 * Class used during EAPLI - thank you teacher for the contribution!         
 * @author Paulo Gandra Sousa
 * 
 * @param <T> the class we want to manage in the repository
 * @param <K> the class denoting the primary key of the entity
 */
public interface Repository<T, K> {

    /**
     * Saves an entity either by creating it or updating it in the persistence.
     * store.
     *
     * @param entity Entity to save
     * @return Saved entity
     */
    T save(T entity);

    /**
     * Creates a new an entity in the persistence layer. if the entity already
     * exists it will throw an exception.
     *
     * FIXME check which exception to throw. it should not be a persistence
     * layer exception.
     *
     * @param entity
     * @return true or false
     * @throws csheets.persistence.DataIntegrityViolationException
     */
    boolean add(T entity) throws DataIntegrityViolationException;

    /**
     * Gets all entities from the repository.
     *
     * @return Iterable of all entities
     */
    Iterable<T> all();

    /**
     * Gets the entity with the specified id
     *
     * @param id Id of the entity
     * @return Entity
     */
    T findById(K id);
    
        /**
     * removes the specified entity from the repository.
     *
     * @param entity
     */
    void delete(T entity);

    /**
     * Removes the entity with the specified ID from the repository.
     *
     * @param entityId The entity Id
     */
    void deleteById(K entityId);

    /**
     * Returns the number of entities in the repository.
     *
     * @return Number of entities
     */
    long size();
}
