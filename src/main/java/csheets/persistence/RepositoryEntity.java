/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.persistence;

import java.io.Serializable;

/**
 * Interface that identifies an entity as persistence
 *
 * @author Tiago Gabriel 1140775
 * @param <T> Persistent entity
 * @param <K> Entity's primary key
 */
public interface RepositoryEntity<T, K> extends Serializable {

    /**
     * Gets the id of this entity on the repository
     *
     * @return The primary key
     */
    K id();

    /**
     * Checks if this entity and the other are the same in the repository
     *
     * @param otherEntity Entity to Compare
     * @return True or false
     */
    boolean sameHas(T otherEntity);

    /**
     * Sets the entity's id
     *
     * @param id Primary key created
     */
    void setID(K id);
}
