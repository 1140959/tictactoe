package csheets.persistence;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Context of persistence for the Cleansheets application
 *
 * @author Tiago Gabriel 1140775
 */
public class PersistenceContext {

    /**
     * The program persistence Properties
     */
    private final Properties repositoryProperties = new Properties();

    /**
     * The properties resource
     */
    private final static String PROPERTIES_RESOURCE = "persistence.properties";

    /**
     * Repository factory key
     */
    private final static String REPOSITORY_FACTORY_KEY = "persistence.repositoryFactory";

    private static class LazyHolder {

        public static PersistenceContext INSTANCE = new PersistenceContext();
    }

    /**
     * This context of persistance instance
     *
     * @return The program's persistance context
     */
    private static PersistenceContext instance() {
        return LazyHolder.INSTANCE;
    }

    /**
     * The Persistence private constructor
     */
    private PersistenceContext() {
        loadProperties();
    }

    /**
     * Returns the repository factory described on the properties
     * @return Repository Factory
     */
    public static RepositoryFactory repositories() {
        final String factoryClassName = PersistenceContext.instance().getRepositoryFactory();
        try {
            return (RepositoryFactory) Class.forName(factoryClassName).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
            // FIXME handle exception properly
            Logger.getLogger(PersistenceContext.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Loads the properties of the file
     */
    private void loadProperties() {
        InputStream propertiesStream = null;
        try {
            propertiesStream = PersistenceContext.class.getClassLoader().getResourceAsStream(PROPERTIES_RESOURCE);
            if (propertiesStream != null) {
                this.repositoryProperties.load(propertiesStream);
            } else {
                throw new FileNotFoundException(
                        "property file '" + PROPERTIES_RESOURCE + "' not found in the classpath");
            }
        } catch (final IOException exio) {
            setDefaultProperties();
            Logger.getLogger(PersistenceContext.class.getName()).log(Level.SEVERE, null, exio);
        } finally {
            if (propertiesStream != null) {
                try {
                    propertiesStream.close();
                } catch (final IOException ex) {
                    Logger.getLogger(PersistenceContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void setDefaultProperties() {
        this.repositoryProperties.setProperty(REPOSITORY_FACTORY_KEY,
                "csheets.persistence.inMemory.InMemoryRepositoryFactory");
    }

    private String getRepositoryFactory() {
        return this.repositoryProperties.getProperty(REPOSITORY_FACTORY_KEY);
    }

}
