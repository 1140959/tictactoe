package csheets.persistence.inMemory;

import csheets.persistence.Repository;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Class used during EAPLI Created by nuno on 20/03/16.
 *
 * @param <T>
 * @param <K>
 */
public abstract class InMemoryRepository<T, K>
        implements Repository<T, K> {

    /**
     * The repository
     */
    protected final Map<K, T> repository = new HashMap<>();

    /**
     * Deletes a given entity of the repository
     *
     * @param entity Entity to be removed
     */
    @Override
    public void delete(T entity) {
        this.repository.remove(entity);
    }

    /**
     * Deletes an entity by its id
     *
     * @param entityId The entity Id
     */
    @Override
    public void deleteById(K entityId) {
        this.repository.remove(entityId);
    }

    /**
     * Saves an entity in the repository Return the same entity
     *
     * @param entity Entity to save
     * @return entity
     */
    @Override
    public T save(T entity) {
        K key = newPK(entity);
           this.repository.put(key, entity);
        return entity;  
    }
    
    @Override
    public Iterable<T> all() {
        return this.repository.values();
    }

    /**
     * This method is used for searching a list without using Optional and
     * Streams, thus returning null when no element is found.
     *
     * @param id K identifier for object
     * @return T if object identified by K is found, otherwise returns null.
     */
    @Override
    public T findById(K id) {
        return this.repository.get(id);
    }

    /**
     * This method is only used for showing the usage of Optional and streams to
     * avoid returning null. In either case, the client code must check for
     * NoSuchElementException.
     *
     * @return T
     */
    @Override
    public long size() {
        return this.repository.size();
    }

    /**
     * Gets an iterator of all the repository
     *
     * @return An iterator of entities
     */
    public Iterator<T> iterator() {
        return this.repository.values().iterator();
    }

    /**
     * Adds an entity to the memory repository
     *
     * @param entity Entity to add
     * @return True
     */
    @Override
    public boolean add(T entity) {
        this.repository.put(newPK(entity), entity);
        return true;
    }

    /**
     * Returns a new primary key
     *
     * @param entity Entity to be saved
     * @return New Primary key
     */
    protected abstract K newPK(T entity);
}
