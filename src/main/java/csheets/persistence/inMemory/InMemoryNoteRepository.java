/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.persistence.inMemory;

import csheets.ext.notes.Note;
import csheets.persistence.NoteRepository;

/**
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 */
public class InMemoryNoteRepository extends InMemoryRepository<Note, Long> implements NoteRepository {

    private static long nextID = 0L;

    @Override
    protected Long newPK(Note entity) {
        if (entity.id() == null) {
            entity.setID(nextID);
            nextID++;
        }
        return entity.id();
    }

    @Override
    public Iterable<Note> all() {
        return null;
    }

}
