package csheets.persistence.inMemory;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.PersonalContact;
import csheets.persistence.ContactRepository;
import java.util.LinkedList;
import java.util.List;

/**
 * Repository of contacts
 *
 * @author Tiago Gabriel 1140775
 */
public class InMemoryContactsRepository extends InMemoryRepository<Contact, Long>
        implements ContactRepository {

    private static long nextID = 0L;

    @Override
    protected Long newPK(Contact entity) {
        if (entity.id() == null) {
            entity.setID(nextID);
            nextID++;
        }
        return entity.id();
    }

    @Override
    public Contact findBySystemUser(String systemUser) {
        String[] splittedName = systemUser.split(" ");
        final int length = splittedName.length;
        final Iterable<Contact> all = all();
        PersonalContact aContact;
        String firstName;
        String lastName;

        // Checks the system user name
        if (length < 2) {
            firstName = splittedName[0];
            lastName = "Local User";
        } else {
            firstName = splittedName[0];
            lastName = splittedName[1];
        }

        //searches the name
        for (Contact c : all) {
            if (c instanceof PersonalContact) {
                aContact = (PersonalContact) c;
                if (aContact.hasCompleteName(firstName, lastName)) {
                    return aContact;
                }
            }
        }

        return null;
    }

}
