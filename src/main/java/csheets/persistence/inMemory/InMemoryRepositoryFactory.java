/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.persistence.inMemory;

import csheets.persistence.ContactRepository;
import csheets.persistence.NoteRepository;
import csheets.persistence.RepositoryFactory;

/**
 * In memory repository factory
 *
 * @author Tiago Gabriel 1140775
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

    private static ContactRepository contacts = null;

    private static NoteRepository notes = null;

    @Override
    public ContactRepository contacts() {
        if (contacts == null) {
            contacts = new InMemoryContactsRepository();
        }
        return contacts;
    }

    @Override
    public NoteRepository notes() {
        if (notes == null) {
            notes = new InMemoryNoteRepository();
        }
        return notes;
    }

}
