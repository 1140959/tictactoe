package csheets.persistence.inMemory;

import csheets.ext.contacts.Profession;
import csheets.persistence.ProfessionRepository;

/**
 * Repository of contacts
 *
 * @author Tiago Gabriel 1140775
 */
public class InMemoryProfessionRepository extends InMemoryRepository<Profession, Long>
        implements ProfessionRepository {

    private static long nextID = 0L;


    /**
     * Returns a new primary key
     *
     * @param entity Entity to be saved
     * @return New Primary key
     */
    @Override
    protected Long newPK(Profession entity) {
        return nextID++;
    }


}
