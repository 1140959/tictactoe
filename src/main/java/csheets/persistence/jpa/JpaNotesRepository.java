/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.persistence.jpa;

import csheets.ext.notes.Note;
import csheets.persistence.NoteRepository;

/**
 * JPA repository for notes
 *
 * @author Tiago Gabriel 1140775@isep.ipp.pt
 */
public class JpaNotesRepository extends JpaRepository<Note, Long>
        implements NoteRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

}
