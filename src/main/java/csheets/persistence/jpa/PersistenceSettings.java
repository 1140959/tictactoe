package csheets.persistence.jpa;

/**
 * The settings for JPA Persistence
 * @author Tiago Gabriel 1140775@isep.ipp.pt
 */
class PersistenceSettings {
    public static final String PERSISTENCE_UNIT_NAME = "lapr42nb_csheetsPU";
}
