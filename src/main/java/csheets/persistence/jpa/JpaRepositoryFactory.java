/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.persistence.jpa;

import csheets.persistence.ContactRepository;
import csheets.persistence.NoteRepository;
import csheets.persistence.RepositoryFactory;

/**
 * A factory for JPA repositories
 *
 * @author Tiago Gabriel 1140775
 */
public class JpaRepositoryFactory implements RepositoryFactory {

    @Override
    public ContactRepository contacts(){
        return new JpaContactsRepository();
    }

    @Override
    public NoteRepository notes() {
        return new JpaNotesRepository();
    }

}
