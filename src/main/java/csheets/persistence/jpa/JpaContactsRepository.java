package csheets.persistence.jpa;

import csheets.ext.contacts.Contact;
import csheets.persistence.ContactRepository;

/**
 * JPA contacts repository
 *
 * @author Tiago Gabriel 1140775@isep.ipp.pt
 */
public class JpaContactsRepository extends JpaRepository<Contact, Long>
        implements ContactRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public Contact findBySystemUser(String systemUser) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
