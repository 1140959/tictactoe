/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.searchworkbooksextension;

/**
 *
 * @author Rui-Think
 */
public class SearchWorkbooksExtensionException extends Exception{

    public SearchWorkbooksExtensionException(String message) {
        super(message);
    }
    
}
