/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.searchworkbooksextension;

import csheets.CleanSheets;
import csheets.ui.ctrl.OpenAction;
import csheets.ui.ctrl.UIController;
import java.io.File;
import javax.swing.ListModel;

/**
 *
 * @author Rui-Think
 */
public class SearchWorkbooksController{
    UIController uiController;
    public SearchWorkbooksController(UIController uiController) {
        this.uiController=uiController;
    }
    
    public void startSearching(String parentDirectory, String fileType, ListModel<String> workbookPathList) throws SearchWorkbooksExtensionException{
       new WorkbookFileSearchService().startSearching(parentDirectory, fileType, workbookPathList);
    }
    
    public void openFile(String filePath){
        File file = new File(filePath);
        new OpenAction().loadWorkbook(this.uiController.getApp(),file, this.uiController);
    }
}
