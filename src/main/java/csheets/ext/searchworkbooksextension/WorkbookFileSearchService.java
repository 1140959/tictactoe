/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.searchworkbooksextension;

import csheets.CleanSheets;
import csheets.core.Workbook;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.ListModel;

/**
 *
 * @author Rui-Think
 */
public class WorkbookFileSearchService implements IFileSearchService {

    private static final String[] FILES_EXTENSIONS = {"cls"};

    @Override
    public void startSearching(String parentDirectory, String fileType, ListModel<String> workbookPathList) throws SearchWorkbooksExtensionException {
//            File[] files = File.listRoots();
//          File directory = new File(directoryName);
        if (!this.isFileExtensionValid(fileType)) {
            throw new SearchWorkbooksExtensionException("Invalid file extension!");
        }

        if (parentDirectory != null && !parentDirectory.isEmpty() && !this.isDirectoryValid(parentDirectory)) {
            throw new SearchWorkbooksExtensionException("Invalid directory!");
        }

        if (parentDirectory != null && parentDirectory.isEmpty()) {
            this.searchFilesWithNoDirectory(fileType, workbookPathList);
        } else {
            this.searchFilesWithDirectory(parentDirectory, fileType, workbookPathList);
        }
    }

    private boolean isFileExtensionValid(String fileType) {
        for (String fileExtension : WorkbookFileSearchService.FILES_EXTENSIONS) {
            if (fileType.equals(fileExtension)) {
                return true;
            }
        }
        return false;
    }

    private boolean isDirectoryValid(String parentDirectory) {
        File directory = new File(parentDirectory);
        if (directory.exists() && directory.isDirectory()) {
            return true;
        }
        return false;
    }

    private void searchFilesWithNoDirectory(String fileType, ListModel<String> workbookPathList) {
        File[] files = File.listRoots();
        for (File file : files) {
            if (file.isFile()) {
                String filename = file.getName();
                String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
                if (extension.toLowerCase().equals(fileType.toLowerCase())) {
                    ((DefaultListModel) workbookPathList).addElement(filename);
                }
            } else if (file.isDirectory()) {
                searchFilesWithDirectory(file.getAbsolutePath(), fileType, workbookPathList);
            }
        }
    }

    private void searchFilesWithDirectory(String parentDirectory, String fileType, ListModel<String> workbookPathList) throws NullPointerException {
        try {

            File directory = new File(parentDirectory);
            File[] files = directory.listFiles();
            for (File file : files) {
                if (file.isFile()) {
                    String filename = file.getName();
                    String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
                    if (extension.toLowerCase().equals(fileType.toLowerCase())) {
                        ((DefaultListModel) workbookPathList).addElement(file.getAbsolutePath());
                    }
                } else if (file.isDirectory()) {
                    searchFilesWithDirectory(file.getAbsolutePath(), fileType, workbookPathList);
                }
            }
        } catch (NullPointerException exn) {
            
        }
    }

}
