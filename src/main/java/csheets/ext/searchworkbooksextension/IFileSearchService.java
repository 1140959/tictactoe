package csheets.ext.searchworkbooksextension;

import javax.swing.ListModel;

/**
 * Provides file-searching features.
 * @author Rui-Think
 */
public interface IFileSearchService {
        
    /**
     * Finds files and adds them to the listModel receveid by parameter as they're found.
     * @param parentDirectory Directory to find.
     * @param fileType The type of file (extension).
     * @param workbookPathList The listModel to be filled.
     * @throws csheets.ext.searchworkbooksextension.SearchWorkbooksExtensionException
     */
    void startSearching(String parentDirectory, String fileType, ListModel<String> workbookPathList) throws SearchWorkbooksExtensionException;
}
