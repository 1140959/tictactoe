/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.searchworkbooksextension;

import csheets.ext.searchworkbooksextension.ui.UIExtensionSearchWorkbooks;
import csheets.ext.Extension;
import csheets.ext.startsharing.ui.UIExtensionShare;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Rui-Think
 */
public class SearchWorkbookExtension extends Extension {

    /**
     * The name of the extension
     */
    public static final String NAME = "Search Workbooks";

    /**
     * Creates a new Example extension.
     */
    public SearchWorkbookExtension() {
        super(Long.parseLong("1989"), NAME, "Search Workbooks");
    }

    /**
     * Returns the user interface extension of this extension (an instance of
     * the class {@link  csheets.ext.simple.ui.UIExtensionExample}). In this
     * extension example we are only extending the user interface.
     *
     * @param uiController the user interface controller
     * @return a user interface extension, or null if none is provided
     */
    public UIExtension getUIExtension(UIController uiController) {
        return new UIExtensionSearchWorkbooks(this, uiController);
    }
}
