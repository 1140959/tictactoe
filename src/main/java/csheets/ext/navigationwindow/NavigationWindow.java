package csheets.ext.navigationwindow;

import csheets.ext.Extension;
import csheets.ext.navigationwindow.ui.UIExtensionNavigationWindow;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * Navigation Window extension.
 *
 * @author 1100588
 */
public class NavigationWindow extends Extension {

    /**
     * The version
     */
    public static final Long VERSION = 1L;

    /**
     * Description
     */
    public static final String DESCRIPTION = "Workbook navigation window";

    /**
     * Name of the extension.
     */
    public static final String NAME = "Navigation Window";

    /**
     * Creates a new Navigation Window extension.
     */
    public NavigationWindow() {
        super(VERSION, NAME, DESCRIPTION);
    }

    /**
     * Returns the user interface extension of this extension (an instance of
     * the class {@link  csheets.ext.navigationwindow.ui.UIExtensionNavigationWindow}). In
     * this extension example we are only extending the user interface.
     *
     * @param uiController the user interface controller
     * @return a user interface extension, or null if none is provided
     */
    @Override
    public UIExtension getUIExtension(UIController uiController) {
        return new UIExtensionNavigationWindow(this, uiController);
    }
}