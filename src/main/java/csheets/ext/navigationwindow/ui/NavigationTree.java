package csheets.ext.navigationwindow.ui;

import csheets.core.formula.Function;
import csheets.core.formula.lang.Language;
import java.util.Enumeration;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 * The extension's tree
 * @author 1100588
 */
class NavigationTree extends JTree {

    /**
     * Constructor
     */
    public NavigationTree() {
        super(new DefaultMutableTreeNode("Active workbook"));
    }
    
    /**
     * Adds an element to the tree
     * @param path
     * @return number of added elements
     */
    int addPath(String path) {
        DefaultTreeModel model = (DefaultTreeModel) this.getModel();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
        
        return addPath(root, path, model);
    }

    /**
     * Adds an element to the tree
     * @param root given node
     * @param path
     * @param model
     * @return number of added elements
     */
    static int addPath(DefaultMutableTreeNode root, String path, DefaultTreeModel model) {
        String[] paths = path.split("/");
        
        int num_paths = 0;
        
        DefaultMutableTreeNode rootNode = root;
        
        int i;
        for(i=0; i<paths.length; i++) {
            if(!paths[i].equals("")) {
                Enumeration<DefaultMutableTreeNode> e = rootNode.children();
                boolean exists = false;
                
                while(e.hasMoreElements()) {
                    DefaultMutableTreeNode node = e.nextElement();
                    if(node.getUserObject().equals(paths[i])) {
                        rootNode = node;
                        exists = true;
                        break;
                    }
                }
                
                if(!exists) {
                    DefaultMutableTreeNode tempNode = new DefaultMutableTreeNode(paths[i]);
                    rootNode.add(tempNode);
                    model.reload(rootNode);
                    rootNode = tempNode;
                    num_paths++;
                }
            }
        }
        return num_paths;
    }

    public void addFormulas() {
        DefaultMutableTreeNode formulaNode = new DefaultMutableTreeNode();
        
        DefaultTreeModel model = (DefaultTreeModel) this.getModel();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
        
        DefaultMutableTreeNode rootNode = root;
        rootNode.add(formulaNode);
        
        Language lang = Language.getInstance();
        Function[] func = lang.getFunctions();
        
        for(Function f : func) {
            DefaultMutableTreeNode tempNode = new DefaultMutableTreeNode(f.getIdentifier());
            formulaNode.add(tempNode);
        }
    }
    
}
