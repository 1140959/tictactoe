package csheets.ext.navigationwindow.ui;

import csheets.ext.Extension;
import csheets.ext.navigationwindow.NavigationWindow;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenu;

/**
 *
 * @author 1100588
 */
public class UIExtensionNavigationWindow extends UIExtension {
    
    /** 
     * The icon to display with the extension's name 
     */
    private Icon icon;
    
    /**
     * Navigation Window extension's menu
     */
    private NavigationWindowMenu menu;
    
    /**
     * Constructor
     * @param extension given extension
     * @param uiController extension's controller
     */
    public UIExtensionNavigationWindow(Extension extension, UIController uiController) {
        super(extension, uiController);
    }
    
    /**
     * Returns an icon to display with the extension's name.
     * @return an icon with a tree
     */
    public Icon getIcon() {
	if (icon == null)
            icon = new ImageIcon(NavigationWindow.class.getResource("res/img/logo.gif"));
        return icon;
    }
    /**
     * Returns an instance of a class that implements JMenu
     * @return a JMenu 
     */
    @Override
    public JMenu getMenu() {
        if(menu == null)
            menu = new NavigationWindowMenu(uiController);
        return menu;
    }

}
