package csheets.ext.navigationwindow.ui;

import csheets.ext.navigationwindow.NavigationWindow;
import csheets.ui.ctrl.UIController;
import javax.swing.JMenu;

/**
 *
 * @author 1100588
 */
class NavigationWindowMenu extends JMenu{
    
    /**
     * Creates a new action
     * @param uiController 
     */
    NavigationWindowMenu(UIController uiController) {
        super(NavigationWindow.NAME);
        add(new NavigationWindowAction(uiController));
    }
    
}
