package csheets.ext.navigationwindow.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author 1100588
 */
class NavigationWindowAction extends BaseAction {
    
    /**
     * The interface controller
     */
    protected UIController uiController;
    
    /**
     * Creates a new action
     * @param uiController 
     */
    public NavigationWindowAction(UIController uiController) {
        this.uiController = uiController;
    }

    @Override
    protected String getName() {
        return "Navigation Window";
    }

    /**
     * Implements the action
     * @param e 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        NavigationUI nui = new NavigationUI(uiController);
    }
    
}
