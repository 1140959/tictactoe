package csheets.ext.navigationwindow.ui;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.ui.ctrl.UIController;
import csheets.core.Workbook;
import csheets.ext.comments.CommentableCell;
import csheets.ext.comments.CommentsExtension;
import java.util.SortedSet;

/**
 *
 * @author 1100588
 */
class NavigationController {

    private UIController uiController;
    
    private Workbook workbook;
    
    private Spreadsheet spreadsheet;
    
    private Cell cell;
    
    public NavigationController(UIController uiController) {
        this.uiController = uiController;
    }

    /**
     * @return the number of existent spreadsheets 
     */
    int getSpreadsheetCount() {
        Cell cell = this.uiController.getActiveCell();
        this.workbook = cell.getSpreadsheet().getWorkbook();
        
        return this.workbook.getSpreadsheetCount();
    }
    
    /**
     * @param i spreadsheet's index
     * @return title of the spreadsheet with the given index
     */
    String getSpreadsheetTitle(int i) {
        this.spreadsheet = this.workbook.getSpreadsheet(i);
        
        return this.spreadsheet.getTitle();
    }
    
    /**
     * @return number of existent rows
     */
    int getSpreadsheetRows() {
        return this.spreadsheet.getRowCount();
    }
    
    /**
     * @return number of existent columns
     */
    int getSpreadsheetColumns() {
        return this.spreadsheet.getColumnCount();
    }
    
    /**
     * @param i
     * @return all the cells that belong to the given spreadsheet
     */
    SortedSet<Cell> getCellsByIndex(int i) {
        this.spreadsheet = this.workbook.getSpreadsheet(i);
        
        /* both addresses belong to the spreadsheet opposite edges */
        return this.spreadsheet.getCells(new Address(0,0), new Address(51,127));
    }

    /**
     * @param cell given cell
     * @return comments from the given cell
     */
    String getComment(Cell cell) {
        this.cell = cell;
        CommentableCell commentableCell = (CommentableCell) this.cell.getExtension(CommentsExtension.NAME);
        
        return commentableCell.getContent();
    }
    
    /**
     * @return the cell's address
     */
    Address getAddress() {
        return this.cell.getAddress();
    }
}
