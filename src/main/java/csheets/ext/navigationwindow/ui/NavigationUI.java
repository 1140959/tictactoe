package csheets.ext.navigationwindow.ui;

import csheets.core.Cell;
import csheets.ui.ctrl.UIController;
import java.util.SortedSet;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

/**
 * The Navigation Window UI
 * @author 1100588
 */
class NavigationUI extends JFrame {
    
    /**
     * The tree that will have all of the project's elements
     */
    private NavigationTree tree;
    
    /**
     * The extension's controller
     */
    private NavigationController controller;
    
    /**
     * Constructor
     * @param parent
     * @param uiController 
     */
    public NavigationUI(UIController uiController) {
        super("Navigation Window");
        
        controller = new NavigationController(uiController);
        
        tree = new NavigationTree();
        
        JScrollPane treePane;
        treePane = new JScrollPane(tree);
        add(treePane);
        
        addSpreadsheets();
        addComments();
        addFormulas();
        
        setSize(400,300);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * Adds the spreadsheets and its elements to the tree
     */
    private void addSpreadsheets() {
        String title;
        int sheet_num = controller.getSpreadsheetCount();
        
        int i;
        for(i=0; i<sheet_num; i++) {
            title = controller.getSpreadsheetTitle(i);
            tree.addPath(title + "/Row count:" + controller.getSpreadsheetRows());
            tree.addPath(title + "/Column count:" + controller.getSpreadsheetColumns());
        }
    }

    /**
     * Adds the cell's comments to the tree
     */
    private void addComments() {
        int sheet_num = controller.getSpreadsheetCount();
        
        int i;
        for(i=0; i<sheet_num; i++) {
            SortedSet<Cell> cells = controller.getCellsByIndex(i);
            
            for(Cell cell : cells) {
                if(!cell.getContent().equalsIgnoreCase("")) {
                    String comment = controller.getComment(cell);
                    tree.addPath(controller.getSpreadsheetTitle(i) + "/" + controller.getAddress() + "/" + comment);
                }
            }
        }
    }

    private void addFormulas() {
        tree.addFormulas();
    }
}
