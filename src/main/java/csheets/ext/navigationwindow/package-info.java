/**
 * Provides the navigation window extension.
 * This extension will have a sidebar where we can navigate through the elements of the project.
 * 
 * @author 1100588
 * 
 */

package csheets.ext.navigationwindow;


