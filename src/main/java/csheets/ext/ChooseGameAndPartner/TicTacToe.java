/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import csheets.core.Cell;
import csheets.core.CellListener;
import csheets.core.Spreadsheet;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.CellExtension;
import csheets.ext.ChooseGameAndPartner.ui.ChooseGameAndPartnerController;
import csheets.ext.style.StylableCell;
import csheets.ext.style.StyleExtension;
import java.awt.Color;
import java.awt.Toolkit;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JOptionPane;

/**
 *
 * @author josemiranda
 */
public class TicTacToe implements Game, Serializable, Runnable {

    Player player1;
    Player player2;
    Spreadsheet sheet;

    private int port = 22222;
    private String ip = "localhost";
    private Thread thread;

    private Socket socket;
    private DataOutputStream dos;
    private DataInputStream dis;
    private ServerSocket serverSocket;

    private List<Cell> board;

    private boolean yourTurn = false;
    private boolean circle = true;
    private boolean accepted = false;
    private boolean unableToCommunicateWithOpponent = false;
    private boolean won = false;
    private boolean enemyWon = false;
    private boolean tie = false;

    private int[][] wins = new int[][]{{0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {0, 4, 8}, {2, 4, 6}};

    public TicTacToe(Player player1, Player player2, Spreadsheet sheet) {
        this.player1 = player1;
        this.player2 = player2;
        this.sheet = sheet;

        paintBoard();
        if (!connect()) {
            initializeServer();
            JOptionPane.showMessageDialog(null, "YOU ARE THE FIRST PLAYER (X)");
        } else {
            JOptionPane.showMessageDialog(null, "YOU ARE THE SECOND PLAYER (0)");
        }

        thread = new Thread(this, "TicTacToe");
        thread.start();
    }

    private boolean connect() {
        try {
            //FIX ME
            socket = new Socket(ip, port);
            dos = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());
            accepted = true;
        } catch (IOException e) {
            System.out.println("Unable to connect to the address: " + ip + ":" + port + " | Starting a server");
            return false;
        }
        System.out.println("Successfully connected to the server.");
        return true;
    }

    private void initializeServer() {
        try {
            serverSocket = new ServerSocket(port);
//			serverSocket = new ServerSocket(port, 8, InetAddress.getByName(ip));
        } catch (Exception e) {
            e.printStackTrace();
        }
        yourTurn = true;
        circle = false;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    @Override
    public String toString() {
        return "Tic-Tac-Toe: " + this.player1 + " Vs " + this.player2;
    }

    void paintBoard() {
        board = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Cell cell = sheet.getCell(j, i);

                try {
                    cell.setContent("");
                } catch (FormulaCompilationException ex) {
                    Logger.getLogger(TicTacToe.class.getName()).log(Level.SEVERE, null, ex);
                }

                StylableCell stylableCell = (StylableCell) cell.getExtension(StyleExtension.NAME);
                Color boardColor = new Color(204, 230, 255);
                stylableCell.setBackgroundColor(boardColor);

                board.add(cell);
            }
        }

        try {
            //GAME CELL
            sheet.getCell(0, 4).setContent("PLAY");
        } catch (FormulaCompilationException ex) {
            Logger.getLogger(TicTacToe.class.getName()).log(Level.SEVERE, null, ex);
        }
        Cell cell = sheet.getCell(1, 4);
        StylableCell stylableCell = (StylableCell) cell.getExtension(StyleExtension.NAME);
        Color boardColor = new Color(204, 230, 255);
        stylableCell.setBackgroundColor(Color.lightGray);

        cell.addCellListener(new CellListener() {

            @Override
            public void valueChanged(Cell cell) {
                // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void contentChanged(Cell cell) {
                if (accepted) {
                    if (yourTurn && !unableToCommunicateWithOpponent && !won && !enemyWon) {
//                int x = cell.getAddress().getColumn();
//                int y = cell.getAddress().getRow();
//                int position = x + y;

                        int position = Integer.valueOf(cell.getContent());

                        try {
                            if (!circle) {
                                board.get(position).setContent("X");
                            } else {
                                board.get(position).setContent("O");
                            }
                            yourTurn = false;

                            dos.writeInt(position);
                            dos.flush();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }

                        System.out.println("DATA WAS SENT");
                        checkForWin();
                        checkForTie();

                    } else if (!yourTurn) {
                        JOptionPane.showMessageDialog(null, "IT IS NOT YOUR TURN");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "WAIT FOR AN OPPONENT");
                }
            }

            @Override
            public void dependentsChanged(Cell cell) {
            }

            @Override
            public void cellCleared(Cell cell) {
            }

            @Override
            public void cellCopied(Cell cell, Cell source) {
            }
        });

    }

    @Override
    public void run() {
        while (true) {
            System.out.println("Thread running");
            if (!yourTurn) {
             
                try {
                    System.out.println("Reading ....");
                    int space = dis.readInt();
                    System.out.println(space);
                    if (circle) {
                        board.get(space).setContent("X");
                    } else {
                        board.get(space).setContent("O");
                    }
                    checkForEnemyWin();
                    checkForTie();
                    yourTurn = true;
                    
                } catch (IOException ex) {
                    Logger.getLogger(TicTacToe.class.getName()).log(Level.SEVERE, null, ex);
                } catch (FormulaCompilationException ex) {
                    Logger.getLogger(TicTacToe.class.getName()).log(Level.SEVERE, null, ex);
                }
            
            }

            //paintBoard();
            if (!circle && !accepted) {
                listenForServerRequest();
            }

        }
    }

    private void listenForServerRequest() {
        Socket socket = null;
        try {
            socket = serverSocket.accept();
            dos = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());
            accepted = true;
            System.out.println("CLIENT HAS REQUESTED TO JOIN, AND WE HAVE ACCEPTED");
            JOptionPane.showMessageDialog(null, "CLIENT HAS REQUESTED TO JOIN, AND WE HAVE ACCEPTED");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // -------------------- CHECK RESULTS -------------------
    private void checkForWin() {
        for (int i = 0; i < wins.length; i++) {
            if (circle) {
                if (board.get(wins[i][0]).getContent() == "O" && board.get(wins[i][1]).getContent() == "O" && board.get(wins[i][2]).getContent() == "O") {
                    JOptionPane.showMessageDialog(null, "WIN");
                }
            } else {
                if (board.get(wins[i][0]).getContent() == "X" && board.get(wins[i][1]).getContent() == "X" && board.get(wins[i][2]).getContent() == "X") {

                    won = true;
                    JOptionPane.showMessageDialog(null, "WIN");
                }
            }
        }
    }

    private void checkForEnemyWin() {
        for (int i = 0; i < wins.length; i++) {
            if (circle) {
                if (board.get(wins[i][0]).getContent() == "X" && board.get(wins[i][1]).getContent() == "X" && board.get(wins[i][2]).getContent() == "X") {
                    enemyWon = true;
                    JOptionPane.showMessageDialog(null, "LOSE");
                }
            } else {
                if (board.get(wins[i][0]).getContent() == "O" && board.get(wins[i][1]).getContent() == "O" && board.get(wins[i][2]).getContent() == "O") {

                    enemyWon = true;
                    JOptionPane.showMessageDialog(null, "LOSE");
                }
            }
        }
    }

    private void checkForTie() {
        for (int i = 0; i < board.size(); i++) {
            if (board.get(i).getContent().trim().isEmpty()) {
                return;
            }
        }
        tie = true;
    }

}
