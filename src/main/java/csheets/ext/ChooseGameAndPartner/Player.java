/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.net.InetAddress;
import javax.swing.ImageIcon;

/**
 *
 * @author josemiranda 1140819
 */
public class Player implements Serializable{
    
    private String name;
    private ImageIcon icon;
    private InetAddress ip;
    
    /**
     * Constructor Player
     * @param name String
     * @param icon File
     */
    public Player(String name, ImageIcon icon,InetAddress ip){
        this.name=name;
        this.icon=icon;
        this.ip = ip;
    }
    
    /**
     * The player's name
     * @return String name
     */
    public String name(){
        return this.name;
    }
    
    public String ip(){
        return ip.toString();
    }
    
    /**
     * The player's icon/image
     * @return File icon
     */
    public ImageIcon icon(){
        return this.icon;
    }

    @Override
    public String toString() {
        return this.name;
    }
    
    
    
}
