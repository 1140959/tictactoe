/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner.ui;

import csheets.ext.ChooseGameAndPartner.ui.UIExtensionChooseGameAndPartner;
import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author josemiranda 1140819
 */
public class ChooseGameAndPartnerExtension extends Extension {

    /**
     * The version
     */
    public static final Long VERSION = 1L;

    /**
     * Description
     */
    public static final String DESCRIPTION = "Choose your game and have fun!";

    /**
     * The extension name
     */
    public static final String NAME = "Games";

    /**
     * Constructor
     */
    public ChooseGameAndPartnerExtension() {
        super(VERSION, NAME, DESCRIPTION);
    }

    /**
     * Returns the user interface extension of this extension
     *
     * @param uiController the user interface controller
     * @return a user interface extension, or null if none is provided
     */
    @Override
    public UIExtension getUIExtension(UIController uiController) {
        return new UIExtensionChooseGameAndPartner(this, uiController);
    }

}
