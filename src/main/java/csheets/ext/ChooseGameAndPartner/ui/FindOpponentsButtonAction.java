/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.net.UnknownHostException;
import javax.swing.JOptionPane;

/**
 *
 * @author josemiranda 1140819
 */
public class FindOpponentsButtonAction extends BaseAction{
    
    ChooseGameAndPartnerController controller;
    ChooseGameAndPartnerPanel panel;
    
    /**
     * Constructor
     * @param controller ChooseGameAndPartnerController
     * @param panel ChooseGameAndPartnerPanel
     */
    public FindOpponentsButtonAction(ChooseGameAndPartnerController controller, 
            ChooseGameAndPartnerPanel panel){
        
        this.controller=controller;
        this.panel=panel;
        
    }

    @Override
    protected String getName() {
        return "Find Available Opponents";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try{
            controller.findAvailableOpponents(panel.name, panel.getIcon());
            panel.findOpponentsBtn.setEnabled(false);
            JOptionPane.showMessageDialog(null, "You will now be found on network.");
        } catch(UnknownHostException ex){
            JOptionPane.showMessageDialog(panel, "Please verify your connection!");
        
        }
    }
}
