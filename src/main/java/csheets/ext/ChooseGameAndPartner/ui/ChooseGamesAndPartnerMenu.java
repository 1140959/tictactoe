/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author josemiranda 1140819
 */
public class ChooseGamesAndPartnerMenu extends JMenu{
    
    /**
     * Constructor
     * @param uiController UIController
     * @param panel ChooseGameAndPartnerPanel
     */
    public ChooseGamesAndPartnerMenu(UIController uiController, ChooseGameAndPartnerPanel panel) {
        super("Games");
        setMnemonic(KeyEvent.VK_G);
        add(new ChooseGameAndPartnerAction(uiController, panel));
    }
}
