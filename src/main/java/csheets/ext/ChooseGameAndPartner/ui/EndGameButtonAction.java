/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner.ui;

import csheets.ext.ChooseGameAndPartner.GamePacket;
import csheets.ext.startsharing.NetworkInstance;
import csheets.ui.ctrl.BaseAction;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author josemiranda 1140819
 */
public class EndGameButtonAction extends BaseAction{

    ChooseGameAndPartnerController controller;
    ChooseGameAndPartnerPanel panel;
            
    /**
     * Constructor
     * @param controller ChooseGameAndPartnerController
     * @param panel ChooseGameAndPartnerPanel
     */
    public EndGameButtonAction(ChooseGameAndPartnerController controller,
            ChooseGameAndPartnerPanel panel){
        this.controller=controller;
        this.panel=panel;
    }
    
    @Override
    protected String getName() {
        return "EndGameButtonAction";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (panel.getGamesList().isSelectionEmpty()) {
           JOptionPane.showMessageDialog(panel, "You must select one active game!");
        }else{
            GamePacket disconectFrom = (GamePacket) panel.getGamesModel().get(panel.getGamesList().getSelectedIndex());
//            panel.getGamesModel().removeElement(disconectFrom);
//            panel.getGamesList().setModel(panel.getGamesModel());
            controller.endGame(disconectFrom);
        }
    }
    
}
