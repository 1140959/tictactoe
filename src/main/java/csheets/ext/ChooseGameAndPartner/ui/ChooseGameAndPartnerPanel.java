/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner.ui;

import csheets.ext.ChooseGameAndPartner.Battleships;
import csheets.ext.ChooseGameAndPartner.Game;
import csheets.ext.ChooseGameAndPartner.GameName;
import csheets.ext.ChooseGameAndPartner.GamePacket;
import csheets.ext.ChooseGameAndPartner.PlayerPacket;
import csheets.ext.startsharing.NetworkInstance;
import csheets.ext.ChooseGameAndPartner.GamesObserver;
import csheets.ext.ChooseGameAndPartner.TicTacToe;
import csheets.ext.startsharing.NetworkData;
//import csheets.ext.startsharing.NetworkInstance;
//import csheets.ext.startsharing.Observer;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.TitledBorder;

/**
 *
 * @author josemiranda 1140819
 */
public class ChooseGameAndPartnerPanel extends JPanel implements GamesObserver{
    
    UIController uiController;
    ChooseGameAndPartnerController controller;
    String name;
    ImageIcon icon;
    private JList playersList;
    private JLabel availableLbl;
    private JLabel nameLabel;
    private JPanel infoPanel;
    private JPanel findPanel;
    private JPanel availablePanel;
    private JPanel connectedToPanel;
    private JPanel availableListPanel;
    private JPanel playBtnPanel;
    private JPanel iconPanel;
    private JList gamesList;
    private JButton ticttBtn;
    private JButton battleSBtn;
    private JPanel endGamePanel;
    private JButton endGameBtn;
    public JButton findOpponentsBtn;
    private JLabel connectedLbl;
    private DefaultListModel playersModel;
    private DefaultListModel gamesModel;
    private JPanel thePanel;
    ListRenderer renderer;
    
    
    ChooseGameAndPartnerPanel(UIController uiController) {
        // Configures panel
        super(new BorderLayout());
        this.setName(ChooseGameAndPartnerExtension.NAME);
        this.uiController=uiController;
        controller= new ChooseGameAndPartnerController(uiController, this);
    }

    void iniciateGames() {
        
        name = JOptionPane.showInputDialog("Insert your username: ");
        
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.showOpenDialog(null);
        File file = fileChooser.getSelectedFile();
        try {
            BufferedImage image = ImageIO.read(file);
            icon = new ImageIcon(image);
        } catch (IOException ex) {
            Logger.getLogger(ChooseGameAndPartnerPanel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("No suitable image");
        }
        
        createComponents();
    }

    private void createComponents() {
        this.setName(ChooseGameAndPartnerExtension.NAME);
        
        //JPanel with the user information: name and icon
        JPanel panel = new JPanel();
        infoPanel = new JPanel();
        infoPanel.setSize(new Dimension(Integer.MAX_VALUE, 30));
        nameLabel = new JLabel("Username: "+name);
        nameLabel.setSize(new Dimension(70, 20));
        nameLabel.setAlignmentX(LEFT_ALIGNMENT);
        infoPanel.add(nameLabel);
        panel.add(infoPanel);
        
        
        //JPanel to insert the icon
        iconPanel= new JPanel();
        iconPanel.setSize(new Dimension(Integer.MAX_VALUE, 30));
        iconPanel.setAlignmentX(RIGHT_ALIGNMENT);
        JLabel imageLabel = new JLabel();
            
        Image im = icon.getImage().getScaledInstance(25, 25, 0);
        icon.setImage(im);
        imageLabel = new JLabel(icon);
        imageLabel.setVisible(true);
       
        iconPanel.add(imageLabel);
        panel.add(iconPanel);
        
        //Btn find
        findPanel= new JPanel();
        findPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
        findOpponentsBtn = new JButton("Find opponents");
        findOpponentsBtn.addActionListener(new FindOpponentsButtonAction(controller,this));
        findOpponentsBtn.setSize(new Dimension(140, 20));
        findPanel.add(findOpponentsBtn);
        
        //Available Opponents message
        availablePanel= new JPanel();
        availablePanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 20));
        availableLbl = new JLabel("Available Opponents:");
        availableLbl.setSize(new Dimension(140, 20));
        availableLbl.setAlignmentY(TOP_ALIGNMENT);
        availableLbl.setAlignmentX(Component.LEFT_ALIGNMENT);
        availablePanel.add(availableLbl);
        
        //availableListPanel= new JPanel();
        //availableListPanel.setPreferredSize(new Dimension(Integer.MAX_VALUE, 150));
        //availableListPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 150));
        //availableListPanel.setAlignmentY(TOP_ALIGNMENT);
        playersList= new JList();
        playersList.setPreferredSize(new Dimension(Integer.MAX_VALUE, 150));
        playersList.setMaximumSize(new Dimension(Integer.MAX_VALUE, 150));// width, height
        playersList.setAlignmentY(TOP_ALIGNMENT);
        //instanceList.setAlignmentX(LEFT_ALIGNMENT);
        //availableListPanel.add(playersList);
        playersModel = new DefaultListModel();
        renderer = new ListRenderer();
        playersList.setCellRenderer(new ListRenderer());
        
        
        //Btns invite to play
        //tic-tac-toe btn
        playBtnPanel= new JPanel();
        playBtnPanel.setMaximumSize(new Dimension(340, 30));
        playBtnPanel.setAlignmentY(TOP_ALIGNMENT);
        ticttBtn = new JButton("Play Tic-Tac-Toe");
        ticttBtn.addActionListener(new PlayTicTacToeButtonAction(controller,this));
        ticttBtn.setSize(new Dimension(160, 20));
        ticttBtn.setAlignmentX(LEFT_ALIGNMENT);
        playBtnPanel.add(ticttBtn);
        //battleship btn
        battleSBtn = new JButton("Play Battleship");
        battleSBtn.addActionListener(new PlayBattleshipsButtonAction(controller,this));
        battleSBtn.setSize(new Dimension(160, 20));
        battleSBtn.setAlignmentX(RIGHT_ALIGNMENT);
        playBtnPanel.add(battleSBtn);
        
        connectedToPanel= new JPanel();
        connectedToPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 20));
        connectedLbl = new JLabel("Active Games:");
        connectedLbl.setSize(new Dimension(140, 20));
        connectedLbl.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
        connectedLbl.setAlignmentY(TOP_ALIGNMENT);
        connectedLbl.setAlignmentX(Component.LEFT_ALIGNMENT);
        connectedToPanel.add(connectedLbl);
        
        gamesList= new JList();
        gamesList.setPreferredSize(new Dimension(Integer.MAX_VALUE, 150));
        gamesList.setMaximumSize(new Dimension(Integer.MAX_VALUE, 150));// width, height
        gamesList.setAlignmentY(TOP_ALIGNMENT);
        gamesModel = new DefaultListModel();
        
        endGamePanel= new JPanel();
        endGamePanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
        endGameBtn = new JButton("End Game");
        endGameBtn.addActionListener(new EndGameButtonAction(controller, this));
        endGameBtn.setSize(new Dimension(140, 20));
        endGamePanel.add(endGameBtn);
        
        
        //Add panels to the main panel
        thePanel = new JPanel();
        thePanel.setLayout(new BoxLayout(thePanel, BoxLayout.PAGE_AXIS));
        thePanel.setPreferredSize(new Dimension(130, 500));
        thePanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));		// width, height
        thePanel.add(panel);
        thePanel.add(findPanel);
        thePanel.add(availablePanel);
        thePanel.add(playersList);
        thePanel.add(playBtnPanel);
        thePanel.add(connectedToPanel);
        thePanel.add(gamesList);
        thePanel.add(endGamePanel);
        
        
        // borders
        TitledBorder border = BorderFactory.createTitledBorder("Games");
        border.setTitleJustification(TitledBorder.CENTER);
        thePanel.setBorder(border);
        
        //add the main panel
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(thePanel, BorderLayout.NORTH);
        add(mainPanel, BorderLayout.NORTH);
    }

    /**
     * The icon of the user
     * @return ImageIcon
     */
    public ImageIcon getIcon(){
        return icon;
    }
    
    /**
     * The model which contains the available players
     * @return DefaultListModel
     */
    public DefaultListModel getInstancesModel(){
        return this.playersModel;
    }
    
    /**
     * The model which contains the active games
     * @return DefaultListModel
     */
    public DefaultListModel getGamesModel(){
        return this.gamesModel;
    }
    
    /**
     * Players list
     * @return JList
     */
    public JList getInstancesList(){
        return this.playersList;
    }
    
    /**
     * Games list
     * @return JList
     */
    public JList getGamesList(){
        return this.gamesList;
    }
    
    @Override
    public void addPlayer(PlayerPacket playerPacket){
        if (playersModel.contains(playerPacket)) {
                playersModel.removeElement(playerPacket);
                playersList.setModel(playersModel);
        }else{
            playersModel.addElement(playerPacket);
	    playersList.setModel(playersModel);
        }
        
    }
    @Override
    public void addGame(GamePacket gamePacket) {
        if (!gamesModel.contains(gamePacket)) {
            for (int i = 0; i < playersModel.size(); i++) {
                PlayerPacket pp = (PlayerPacket)playersModel.getElementAt(i);
                if (pp.getNetworkInstance().equals(gamePacket.getNetworkInstance())) {
                    playersModel.removeElement(pp);
                    playersList.setModel(playersModel);
                }
            }
            
            Game g = gamePacket.game();
            JOptionPane.showMessageDialog(this, "STARTED: "+g.toString());
            if(g instanceof TicTacToe){
                // start TicTacToe Game
                controller.startGameWith((PlayerPacket) playersList.getSelectedValue(), GameName.TICTACTOE);
                JOptionPane.showMessageDialog(this, "TicTacToe Started!");
            }
            if(g instanceof Battleships){
                //start Batteships Game
            }
            
            gamesModel.addElement(gamePacket);
            gamesList.setModel(gamesModel);
        }
    }
    @Override
    public void removeConn(NetworkInstance n) {
        for (int i = 0; i < gamesModel.getSize(); i++) {
                GamePacket gp = (GamePacket)gamesModel.getElementAt(i);
                if (gp.getNetworkInstance().equals(n)) {
                    gamesModel.removeElement(gp);
                    gamesList.setModel(gamesModel);
                }
            }
    }
    
}
