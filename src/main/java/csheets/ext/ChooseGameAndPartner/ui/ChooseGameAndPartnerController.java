/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner.ui;

import csheets.ext.ChooseGameAndPartner.GameName;
import csheets.ext.ChooseGameAndPartner.GamePacket;
import csheets.ext.ChooseGameAndPartner.GamesService;
import csheets.ext.ChooseGameAndPartner.PlayerPacket;
import csheets.ext.ChooseGameAndPartner.TicTacToe;
import csheets.ext.startsharing.NetworkInstance;
import csheets.ui.ctrl.UIController;
import java.awt.image.BufferedImage;
import java.net.UnknownHostException;
import javax.swing.ImageIcon;

/**
 *
 * @author josemiranda 1140819
 */
public class ChooseGameAndPartnerController {
    
    UIController uiController;
    ChooseGameAndPartnerPanel uiPanel;
    GamesService gservice;
    
     /**
      * Constructor
     * @param uiController
     * @param uiPanel
     */
    public ChooseGameAndPartnerController(UIController uiController, ChooseGameAndPartnerPanel uiPanel) {
        this.uiController = uiController;
        this.uiPanel = uiPanel;
    }
    
    /**
     * Creates a new GamesService and asks it to find the available opponents on the local network
     * @param name
     * @param icon
     * @throws UnknownHostException
     */
    public void findAvailableOpponents(String name, ImageIcon icon) throws UnknownHostException{
        gservice= new GamesService();
        this.gservice.findAvailableOpponents(name, icon, uiPanel);
    }

    /**
     * Asks the GamesService to start a new game
     * @param connectTo PlayerPacket
     * @param gameName GameName
     */
    public void startGameWith(PlayerPacket connectTo, GameName gameName) {
        this.gservice.startGameWith(connectTo, gameName,uiController.getActiveSpreadsheet());
    }

    /**
     * Asks the GamesService to finish the received game
     * @param disconectFrom GamePacket
     */
    public void endGame(GamePacket disconectFrom) {
        this.gservice.endGame(disconectFrom);
    }
    
    public void startTicTacToe(){
        new TicTacToe(null, null, this.uiController.getActiveSpreadsheet());
    }

    
    
}
