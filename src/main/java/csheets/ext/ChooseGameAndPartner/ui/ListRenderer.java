/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner.ui;

import csheets.ext.ChooseGameAndPartner.Player;
import csheets.ext.ChooseGameAndPartner.PlayerPacket;
import java.awt.Color;
import java.awt.Component;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;

/**
 *
 * @author josemiranda 1140819
 */
public class ListRenderer extends JLabel implements ListCellRenderer{
    
    private JList list;
    private DefaultListModel listModel;
    private Border border = BorderFactory.createLineBorder(Color.BLUE, 1);
    
    @Override
    public Component getListCellRendererComponent(JList list, Object value, 
            int index, boolean isSelected, boolean cellHasFocus) {
        
        PlayerPacket packet = (PlayerPacket)value;
        setText(packet.getPlayer().name());
        setIcon(packet.getPlayer().icon());

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else{
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        if (isSelected && cellHasFocus)
         setBorder(border);
        else
         setBorder(null);
        return this;
    }
    
}
