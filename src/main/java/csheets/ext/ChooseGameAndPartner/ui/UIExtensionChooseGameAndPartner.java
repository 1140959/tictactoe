/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;

/**
 *
 * @author josemiranda 1140819
 */
public class UIExtensionChooseGameAndPartner extends UIExtension {

    /**
     * A side bar that provides editing of comments
     */
    private ChooseGameAndPartnerPanel sideBar;
    private ChooseGamesAndPartnerMenu menu;

    /**
     * Constructor
     * @param extension ChooseGameAndPartnerExtension
     * @param uiController UIController
     */
    public UIExtensionChooseGameAndPartner(ChooseGameAndPartnerExtension extension, 
            UIController uiController) {
        super(extension, uiController);
    }

    /**
     * Returns an icon to display with the extension's name.
     *
     * @return an icon with style
     */
    @Override
    public Icon getIcon() {
        return null;
    }

    /**
     * Returns an instance of a class that implements JMenu. In this simple case
     * this class only supplies one menu option.
     *
     * @see ExampleMenu
     * @return a JMenu component
     */
    @Override
    public JMenu getMenu() {
        
        if (menu == null) {
            menu = new ChooseGamesAndPartnerMenu(uiController, sideBar);
        }
        return menu;
    }

    /**
     * Returns a side bar that provides editing of comments.
     *
     * @return a side bar
     */
    @Override
    public JComponent getSideBar() {
        if (sideBar == null) {
            sideBar = new ChooseGameAndPartnerPanel(uiController);
        }
        return sideBar;
    }

}
