/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner.ui;

import csheets.ext.ChooseGameAndPartner.GameName;
import csheets.ext.ChooseGameAndPartner.PlayerPacket;
import csheets.ext.startsharing.NetworkInstance;
import csheets.ui.ctrl.BaseAction;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author josemiranda 1140819
 */
public class PlayTicTacToeButtonAction extends BaseAction{
    ChooseGameAndPartnerController controller;
    ChooseGameAndPartnerPanel panel;
    
    /**
     * Constructor
     * @param controller ChooseGameAndPartnerController
     * @param panel ChooseGameAndPartnerPanel
     */
    public PlayTicTacToeButtonAction(ChooseGameAndPartnerController controller, 
            ChooseGameAndPartnerPanel panel){
        this.controller=controller;
        this.panel=panel;
    }

    @Override
    protected String getName() {
        return "PlayTicTacToeButtonAction";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
//        if (panel.getInstancesList().isSelectionEmpty()) {
//            JOptionPane.showMessageDialog(panel, "You must select one opponent!");
//        }else{
//            PlayerPacket playwith = (PlayerPacket) panel.getInstancesModel().get(panel.getInstancesList().getSelectedIndex());
//            panel.getInstancesModel().removeElement(playwith);
//            panel.getInstancesList().setModel(panel.getInstancesModel());
//            controller.startGameWith(playwith, GameName.TICTACTOE);
//        }
        controller.startTicTacToe();
    }
    
}
