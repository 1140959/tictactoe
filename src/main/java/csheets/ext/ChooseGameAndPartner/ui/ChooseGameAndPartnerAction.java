/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author josemiranda 1140819
 */
public class ChooseGameAndPartnerAction extends BaseAction{
    
    UIController uiController;
    ChooseGameAndPartnerPanel spanel;
            
    /**
     * Constructor
     * @param uiController UIController
     * @param panel ChooseGameAndPartnerPanel
     */
    public ChooseGameAndPartnerAction(UIController uiController, ChooseGameAndPartnerPanel panel){
        this.uiController=uiController;
        this.spanel=panel;
    }

    @Override
    protected String getName() {
        return "Start a Game";
    }
    
    protected void defineProperties() {
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.spanel.controller.gservice!=null) {
            JOptionPane.showMessageDialog(spanel, "Games Already Running!");
        } else{
            spanel.iniciateGames();
        }
    }
    
}
