/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import csheets.core.Spreadsheet;
import csheets.ext.ChooseGameAndPartner.ui.ChooseGameAndPartnerPanel;
import csheets.ext.startsharing.INetworkService;
import csheets.ext.startsharing.NetworkInstance;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import javax.swing.ImageIcon;

/**
 *
 * @author josemiranda 1140819
 */
public class GamesService {
    int port=100;
    Player player1;
    Player player2;
    GNetworkService netService;
    GamesFactory gfactory;
    
    /**
     * The service constructor
     */
    public GamesService(){
        gfactory= new GamesFactory();
    }

    /**
     * Creates a player and asks the GNetworkService to find other players
     * @param name String
     * @param icon ImageIcon
     * @param uiPanel ChooseGameAndPartnerPanel
     * @throws UnknownHostException
     */
    public void findAvailableOpponents(String name, ImageIcon icon, ChooseGameAndPartnerPanel uiPanel) throws UnknownHostException {
        //create a player
        player1= new Player(name, icon,InetAddress.getLocalHost());
        
        //get the network service
        netService = new GNetworkService();
        netService.createNetworkData(player1);
        
        //find the available instances (players) on the local network
        netService.findInstances(uiPanel);
    }

    /**
     * Asks the factory to create a new game and asks the GNetworkService to send the game data to the opponent
     * @param connectTo PlayerPacket
     * @param gameName GameName
     */
    public void startGameWith(PlayerPacket connectTo, GameName gameName,Spreadsheet sheet) {
        Game game=null;
        //Fix this method - icon
         player2=new Player(connectTo.getPlayer().name(), connectTo.getPlayer().icon(),connectTo.getIp());
         
        if (gameName.equals(GameName.BATTLESHIPS)) {
            game=gfactory.createBattleshipsGame(player1, player2);
        } else if (gameName.equals(GameName.TICTACTOE)) {
            game=gfactory.createTicTacToeGame(player1, player2,sheet);
        }
        
        
         this.netService.sendGameDataInfo(connectTo.getNetworkInstance(), game);
         this.netService.notifyNewGameCreated(game, connectTo.getNetworkInstance());
    }

    /**
     * Asks the GNetworkService to disconnect from the Cleansheet instance received
     * @param networkInstance GamePacket
     */
    public void endGame(GamePacket networkInstance) {
        this.netService.disconectFrom(networkInstance.getNetworkInstance());
    }
    

    
}
