/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import csheets.ext.ChooseGameAndPartner.ui.ChooseGameAndPartnerPanel;
import csheets.ext.startsharing.Accept;
import csheets.ext.startsharing.Broadcast;
import csheets.ext.startsharing.INetworkService;
import csheets.ext.startsharing.Listener;
import csheets.ext.startsharing.NetworkData;
import csheets.ext.startsharing.NetworkInstance;
import csheets.ext.startsharing.Observer;
import csheets.ext.startsharing.Server;
import java.awt.image.BufferedImage;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author josemiranda 1140819
 */
public class GNetworkService implements INetworkService{

    private static GNetworkService ns = null;
    Player player;
    int port=11000;
    PlayerPacket gndata;
    List<NetworkInstance> niList;
    private List<GamesObserver> obs = new ArrayList<GamesObserver>();
    private List<GServer> listConn = new ArrayList<GServer>();
    private NetworkInstance thisInstance;
    NetworkData ndata;
    
    /**
     *  Empty constructor
     */
    public GNetworkService(){
        
    }
    
    /**
     * Creates a NetworkData for the connections
     * @param p Player
     * @throws UnknownHostException
     */
    public void createNetworkData(Player p) throws UnknownHostException{
        InetAddress ip = InetAddress.getLocalHost();
        
        this.player=p;
        ndata= new NetworkData(ip, player.name(), port);
        thisInstance= new NetworkInstance(ndata);
        
    }
    
    /**
     * Iniciates the process to find the instances of Cleansheets on the local network
     * @param uiPanel ChooseGameAndPartnerPanel
     */
    public void findInstances(ChooseGameAndPartnerPanel uiPanel) {
        niList = new ArrayList<>();
        
        addObserver(uiPanel);
        listeningBroadcast();
        acceptLink();        
        broadcastInstances();
    }
    
    /**
     * add an observer to monitor UI data transfers
     * @param o GamesObserver
     */
    public void addObserver(GamesObserver o) {
        this.obs.add(o);
    }
    
     /**
     *Initialize the thread for start listening a request to the broadcast.
     */
    private void listeningBroadcast() {
        Runnable l = new Listener(ndata);
        Thread t = new Thread(l, "listening");
        t.start();
    }

    /**
    * Initialize the thread to establish the connection.
     */
    private void acceptLink() {
        Runnable a = new GAccept(this);
        Thread t = new Thread(a, "accepting");
        t.start();
    }
    
    /**
     * Initialize the thread for sendGameDataInfo broadcast.
     */
    private void broadcastInstances() {
        Runnable b = new Broadcast(ndata, this);
        Thread t = new Thread(b, "broadcasting");
        t.start();
    }
    
    /**
     * Add NetworkInstance to the NetworkInstances list
     * @param ni NetworkInstance
     */
    @Override
    public void addNetworkInstance(NetworkInstance ni) {
        if (!niList.contains(ni)) {
            this.niList.add(ni);
            connectToInstance(ni);
        }
    }
    
    /**
     * returns the NetworkData
     * @return NetworkData
     */
    public NetworkData getNetworkData(){
        return this.ndata;
    }
    
    /**
     * Increments the number of the port to be used on the next connection
     * @param port int
     */
    public void incrementPort(int port){
        port++;
        this.ndata.setPort(port);
    }

    private boolean connectToInstance(NetworkInstance n) {
        Runnable tLink = new GConnect(n, this);
        Thread t = new Thread(tLink, "link to network instance selected");
        try {
            t.start();
            t.join();
            sendPlayerInfo(n, player);
        } catch (Exception ex) {
            Logger.getLogger(GNetworkService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    /**
     * Adds the created server to the servers list
     * @param s GServer
     */
    public void addLinkUP(GServer s) {
        for (GServer link : listConn) {
            if (link.myNetworkInstance().equals(s.myNetworkInstance())) {
                return;
            }
        }
        listConn.add(s);
    }
    
    /**
     * Sends the information of the player to another instance
     * @param ni NetworkInstance
     * @param player Player
     */
    public void sendPlayerInfo(NetworkInstance ni, Player player){
        PlayerPacket ppacket = new PlayerPacket(player, this.thisInstance);
        for (GServer activeConnection : listConn) {
            if (ni.equals(activeConnection.myNetworkInstance())) {
                activeConnection.sendData(ppacket);
            }
        }
    }
    
    /**
     * Sends the information of the created Game to another instance
     * @param ni NetworkInstance
     * @param game Game
     */
    public void sendGameDataInfo(NetworkInstance ni, Game game) {
        GamePacket gpacket = new GamePacket(game, this.thisInstance);
            for (GServer activeConnection : listConn) {
                if (ni.equals(activeConnection.myNetworkInstance())) {
                    activeConnection.sendData(gpacket);
                }
            }
    }
    
    /**
     * Notifies the observer of the received data 
     * @param or Object
     */
    public void addReceivedObject(Object or) {
        if (or instanceof PlayerPacket) {
            PlayerPacket playerPacketReceived = (PlayerPacket) or;
            for (GamesObserver o : obs) {
                o.addPlayer(playerPacketReceived);
            }
        }else if (or instanceof GamePacket) {
            GamePacket gamePacketReceived = (GamePacket) or;
            for (GamesObserver o : obs) {
                o.addGame(gamePacketReceived);
            }
        }
    }
    
     /**
     * Method t return the netData name
     * @return netData machine name
     */
    public String name() {
        return ndata.name();
    }


    /**
     *Method to Disconnect an active connection.
     * @param n NetworkInstance
     * @return boolean true or false to the pretended action.
     */
    public boolean disconectFrom(NetworkInstance n) {
        GServer c = null;
        for (GServer con : listConn) {
            if (con.myNetworkInstance().equals(n)) {
                con = c;
            }
        }
        if (c != null) {
            niList.remove(c.myNetworkInstance());
            listConn.remove(c);
            c.disconnect();
            System.out.println("Disconnected from "+c.myNetworkInstance());
        }
        notifyObserversClosedConn(n);
        return true;
    }
    
    private void notifyObserversClosedConn(NetworkInstance n) {
        for (GamesObserver o : obs) {
            o.removeConn(n);
        }
    }
    
    @Override
    public void addLinkUP(Server s) {
    }

    /**
     * Notify the observers about a new game created
     * @param game Game
     * @param networkInstance NetworkInstance
     */
    public void notifyNewGameCreated(Game game, NetworkInstance networkInstance) {
        if (!this.thisInstance.equals(networkInstance)) {
            GamePacket gp = new GamePacket(game, networkInstance);
            for (GamesObserver o : obs) {
                o.addGame(gp);
            }
        }
    }

}
