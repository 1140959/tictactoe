/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import csheets.ext.startsharing.NetworkInstance;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.net.InetAddress;
import javax.swing.ImageIcon;

/**
 *
 * @author josemiranda 1140819
 */
public class PlayerPacket implements Serializable{
    
    Player player;
    NetworkInstance instance;
    
    /**
     * Constructor
     * @param player Player
     * @param ni NetworkInstance
     */
    public PlayerPacket(Player player, NetworkInstance ni) {
        this.player=player;
        this.instance=ni;
    }

    /**
     * @return the name
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * @return the ip
     */
    public InetAddress getIp() {
        return this.instance.getNetworkData().ip();
    }

    /**
     * @return the port
     */
    public int getPort() {
        return this.instance.getNetworkData().port();
    }
    
    /**
     * the player name
     * @return String
     */
    public String getName(){
        return this.instance.getNetworkData().name();
    }
    
    /**
     * the network instance
     * @return NetworkInstance
     */
    public NetworkInstance getNetworkInstance(){
        return instance;
    }

    @Override
    public String toString() {
        return this.player.toString();
    }
    
    
    
}
