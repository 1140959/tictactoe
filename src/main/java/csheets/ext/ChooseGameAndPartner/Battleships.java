/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import java.io.Serializable;

/**
 *
 * @author josemiranda 1140819
 */
public class Battleships implements Game, Serializable{
    Player player1;
    Player player2;
    
    /**
     * Constructor
     * @param player1 Player
     * @param player2 Player
     */
    public Battleships(Player player1, Player player2){
        this.player1=player1;
        this.player2=player2;
    }

    /**
     * the user player
     * @return Player
     */
    public Player getPlayer1() {
        return player1;
    }

    /**
     * the opponnent player
     * @return Player
     */
    public Player getPlayer2() {
        return player2;
    }
    
    @Override
    public String toString() {
        return "Battleships: " + this.player1 + " Vs " + this.player2;
    }

}
