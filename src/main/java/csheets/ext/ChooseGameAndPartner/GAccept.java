/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import csheets.ext.startsharing.INetworkService;
import csheets.ext.startsharing.NetworkData;
import csheets.ext.startsharing.NetworkInstance;
import csheets.ext.startsharing.Server;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author josemiranda 1140819
 */
public class GAccept implements Runnable{
    private NetworkData nd;
    private final GNetworkService net;
    private ServerSocket s_recv;
    private Socket s_share;
    private NetworkInstance n_c;
    
    /**
     * Accept connection Constructor
     * @param n network Data
     * @param ns Network Service
     */
    public GAccept(GNetworkService ns){
        
        this.net = ns;
    }
    
    @Override
    public void run() {

        try {
            this.nd= net.getNetworkData();
            s_recv = new ServerSocket(nd.port());
            

        } catch (IOException ex) {
            System.out.println("wrong port");
            System.exit(1);
        }
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(GAccept.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
            try {
                s_share = s_recv.accept();
                DataInputStream sIn = new DataInputStream(s_share.getInputStream());
                String c = sIn.readUTF();
                NetworkData co = new NetworkData(s_share.getInetAddress(), c, s_share.getPort());
                n_c = new NetworkInstance(co);
                try {
                    GServer tConnecting = new GServer(s_share, n_c, net);
                    Thread tC = new Thread(tConnecting, "connected");
                    tC.start();
                    net.addLinkUP(tConnecting);
                    
                    //prepare the next connection on a different port
                    net.incrementPort(net.getNetworkData().port());
                    s_recv = new ServerSocket(nd.port());
                    
                } catch (Exception ex) { 
                        System.out.println("failed");
                }
            } catch (IOException ex) {
                Logger.getLogger(GAccept.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
        }
    }
}
