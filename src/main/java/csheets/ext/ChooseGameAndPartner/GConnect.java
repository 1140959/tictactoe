/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import csheets.ext.startsharing.NetworkInstance;
import csheets.ext.startsharing.NetworkService;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author josemiranda 1140819
 */
public class GConnect implements Runnable{
    
    private NetworkInstance t_instance;
    private GNetworkService t_netService;
    
    /**
     * Connect constructor
     * @param instance NetworkInstance
     * @param ns GNetworkService
     */
    public GConnect(NetworkInstance instance, GNetworkService ns) {
        this.t_instance = instance;
        t_netService = ns;
    }

    @Override
    public void run() {

        Socket connectingSocket = null;
        try {
            connectingSocket = new Socket(t_instance.getNetworkData().ip(), t_instance.getNetworkData().port());
            DataOutputStream sOut = new DataOutputStream(connectingSocket.
                    getOutputStream());
            sOut.writeUTF(t_netService.name());
            GServer tConnecting;
            try {
                tConnecting = new GServer(connectingSocket, t_instance, t_netService);
                Thread tC = new Thread(tConnecting, "connect");
                tC.start();
                t_netService.addLinkUP(tConnecting);
                System.out.println("Connected successfully with " + t_instance);
            } catch (Exception ex) { 
                System.out.println("Error");
            }
        } catch (IOException ex) {
            System.out.println("Failed");
        }
    }
    
}
