/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import csheets.ext.startsharing.NetworkData;
import csheets.ext.startsharing.NetworkInstance;

/**
 *
 * @author josemiranda 1140819
 */
public interface GamesObserver {

    /**
     * Notifies the observers to  add new Player on the observer
     * @param ndata
     */
    public void addPlayer(PlayerPacket ndata);

    /**
     * Notifies the observers to add a new game
     * @param gamePacket
     */
    public void addGame(GamePacket gamePacket);

    /**
     * Notifies the observers to remove a connection
     * @param n
     */
    public void removeConn(NetworkInstance n);
}
