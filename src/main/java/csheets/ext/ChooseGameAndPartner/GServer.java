/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import csheets.ext.startsharing.INetworkService;
import csheets.ext.startsharing.NetworkInstance;
import csheets.ext.startsharing.NetworkService;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author josemiranda 1140819
 */
public class GServer implements Runnable{
    
    private NetworkInstance ni;
    private Socket s;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private GNetworkService ns;
    private List<Object> dataToSend;
    
    /**
     *Server manages send and receive packages 
     * @param s1 Socket
     * @param n NetworkInstance
     * @param nservice
     * @throws IOException test if the Sockect and instance exists
     */
    public GServer(Socket s1, NetworkInstance n, GNetworkService nservice) throws IOException{
        ni = n;
	ns= nservice;
	this.s = s1;
	out = new ObjectOutputStream(s.getOutputStream());
	out.flush();
	dataToSend = new ArrayList<Object>();
        in = new ObjectInputStream(s.getInputStream());
    }
    
    @Override
	public void run ()
	{
	try
	{
            sending();
            receiving();
        }catch (Exception ex){
            Logger.getLogger(GServer.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
   
    void sending(){
        new Thread("SendObjects"){
            public void run (){
		while (true){
                    while (dataToSend.size() > 0){
			try{
                            Object os = dataToSend.get(0);
                            out.writeObject(os);
                            out.flush();
                            System.out.println("Game data shared with the other instance");
                            dataToSend.remove(0);
                        }catch (IOException ex){
                            System.out.println("closed");
                            ns.disconectFrom(ni);
                            Logger.getLogger(GServer.class.getName()).log(Level.SEVERE, null, ex);
			}
                    }
                    try{
			Thread.sleep(500);
                    }catch (InterruptedException ex){
			Logger.getLogger(GServer.class.getName()).log(Level.SEVERE, null, ex);
                    }
		}
            }}.start();
    }   
    
    void receiving(){
        new Thread("ReceiveObjects"){
		public void run (){
                    while (true){
                    try{
			Object or = in.readObject();
			ns.addReceivedObject(or);
                        System.out.println("game data received from the other instance");
                    }catch (IOException ex){
			System.out.println("closed");
			ns.disconectFrom(ni);
			Logger.getLogger(GServer.class.getName()).log(Level.SEVERE, null, ex);
			return;
                    }catch (ClassNotFoundException ex){
			System.out.println("error");
			Logger.getLogger(GServer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try{
			Thread.sleep(500);
                    }catch (InterruptedException ex){
			Logger.getLogger(GServer.class.getName()).log(Level.SEVERE, null, ex);
                    }
		}
                }}.start();
    }
        
    /**
     *Method to disconnect active connections
     * @return Boolean with the success of not of this action
     */
    public boolean disconnect()
    {
        try
	{
            s.close();
            Thread.currentThread().interrupt();
            System.out.println("Disconnect from " + ni);
            return true;
	}
	catch (IOException ex)
	{
            Logger.getLogger(GServer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
	}
    }
    
    /**
     * Method to return User NEtworkInstances
     * @return NetworkInstance
     */
    public NetworkInstance myNetworkInstance ()
    {
	return ni;
    }
    
    /**
     *Method to Send an Object
     * @param dataToSend1 Object to send
     * @return True of false with the result of the actions
     */
    public boolean sendData (Object dataToSend1)
    {
	return dataToSend.add(dataToSend1);

    }
    


    
    
}
