/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import csheets.core.Spreadsheet;

/**
 *
 * @author josemiranda 1140819
 */
public class GamesFactory {
    
    GamesFactory factory=null;
    
    /**
     * constructor
     */
    public GamesFactory(){
    }
    
    /**
     * Creates Battleships games
     * @param p1 Player
     * @param p2 Player
     * @return Battleships
     */
    public Battleships createBattleshipsGame(Player p1, Player p2){
        Battleships game = new Battleships(p1, p2);
        return game;
    }
    
    /**
     * Creates Tic-Tac-Toe games
     * @param p1 Player
     * @param p2 Player
     * @return TicTacToe
     */
    public TicTacToe createTicTacToeGame(Player p1, Player p2,Spreadsheet sheet){
        TicTacToe game = new TicTacToe(p1, p2,sheet);
        return game;
    }
}
