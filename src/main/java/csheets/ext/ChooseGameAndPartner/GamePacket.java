/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ChooseGameAndPartner;

import csheets.ext.startsharing.NetworkInstance;
import java.io.Serializable;
import java.net.InetAddress;

/**
 *
 * @author josemiranda 1140819
 */
public class GamePacket implements Serializable{
    Game game;
    NetworkInstance instance;
    
    /**
     * Contructor
     * @param g Game
     * @param i NetworkInstance
     */
    public GamePacket(Game g, NetworkInstance i){
        this.game=g;
        this.instance=i;
    }
    
    /**
     * @return the ipAddress
     */
    public InetAddress getIpAddress() {
        return instance.getNetworkData().ip();
    }

    /**
     * @return the machineName
     */
    public String getMachineName() {
        return instance.getNetworkData().name();
    }

    /**
     * @return the port
     */
    public int getPort() {
        return instance.getNetworkData().port();
    }
    
    /**
     * @return NetworkInstance 
     */
    public NetworkInstance getNetworkInstance(){
        return instance;
    }
    
    @Override
    public String toString() {
        return this.game.toString();
    }
    
    public Game game(){
        return this.game;
    }
}
