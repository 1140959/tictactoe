/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.CommunicationsLog;


import java.net.DatagramPacket;

/**
 *
 * @author ccruz
 */
public interface CommunicationsObserver {
    
    public void sendingPacket (DatagramPacket p);
    public void receivingPacket (DatagramPacket p);

}
