package csheets.ext.CommunicationsLog.ui;

//import csheets.ext.startsharing.StartSharingController;
//import csheets.ext.share.ShareExtension;
//import csheets.ext.startsharing.NetworkInstance;
//import csheets.ext.startsharing.Observer;

//import csheets.ext.CommunicationsLog.CommunicationsLogController;
//import csheets.ext.CommunicationsLog.CommunicationsLogExtension;
import csheets.ui.ctrl.UIController;

import java.awt.Dimension;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.io.IOException;
import java.net.DatagramPacket;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

/**
 * Class to create the Communications log Panel
 *
 * @author 1111098 cferreira
 */
public class CommunicationsLogPanel extends JPanel /*implements Observer */{

   // private CommunicationsLogController commLogCtrl;
    
    private JTextArea incomingText;
    private JTextArea outgoingText;
    private JPanel logPanel;
    
    /**
     * Controller Constructor of the class
     *
     * @param uiController uiController
     */
    public CommunicationsLogPanel(UIController uiController) throws IOException {

   
       super(new GridBagLayout());
       // commLogCtrl = new CommunicationsLogController(uiController,this);
        
      //  this.setName(CommunicationsLogExtension.NAME);
        
        GridBagLayout gridbag = (GridBagLayout)getLayout();
        GridBagConstraints c = new GridBagConstraints();
 
 
        logPanel = new JPanel(new GridLayout(1,1));
        logPanel.setPreferredSize(new Dimension(200, 200));    
 
        incomingText = new JTextArea();
        incomingText.setEditable(false);
        JScrollPane incomingScrollPane = new JScrollPane(incomingText);
        incomingScrollPane.setPreferredSize(new Dimension(200, 200));
        incomingScrollPane.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        TitledBorder borderIncoming = BorderFactory.createTitledBorder("Incoming Communications");                
        borderIncoming.setTitleJustification(TitledBorder.CENTER);
        incomingScrollPane.setBorder(borderIncoming);

        outgoingText = new JTextArea();
        outgoingText.setEditable(false);
        JScrollPane outgoingScrollPane = new JScrollPane(outgoingText);
        outgoingScrollPane.setPreferredSize(new Dimension(200, 200));
        outgoingScrollPane.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        TitledBorder borderOutgoing = BorderFactory.createTitledBorder("Outgoing Communications");                
        borderOutgoing.setTitleJustification(TitledBorder.CENTER);
        outgoingScrollPane.setBorder(borderOutgoing);
 
        c.fill = GridBagConstraints.BOTH; 
        c.weightx= 1;
        c.weighty = 0.5; 
        c.gridwidth = GridBagConstraints.REMAINDER; 

        gridbag.setConstraints(incomingScrollPane, c);
        add(incomingScrollPane);
        
        gridbag.setConstraints(outgoingScrollPane, c);
        add(outgoingScrollPane);

        setPreferredSize(new Dimension(400, 400));
        setBorder(BorderFactory.createEmptyBorder(20,20,20,20));

    }
    
    public void printIncoming(String s){
      
        Date date = new Date();
        s = date.toString()+ " - " + s + "\n";
        this.incomingText.setText(this.incomingText.getText()+s );
      
    }
    
    public void printOutgoing(String s){
    
        Date date = new Date();
        s = date.toString()+ " - " + s +"\n";
        this.outgoingText.setText(this.outgoingText.getText() + s );
    }
    
    public void printIncomingDatagram (DatagramPacket p){
        
        Date date = new Date();
        String newline = date.toString()+ " - " + p.getAddress().toString() + " - " + p.getPort() + " - "+ p.getData()+"\n";
        this.incomingText.setText(this.incomingText.getText() + newline );
  
    }

    public void printOutgoignDatagram (DatagramPacket p){
    
        Date date = new Date();
        String newline = date.toString()+ " - " + p.getAddress().toString() + " - " + p.getPort() + " - "+ p.getData()+"\n";
        this.outgoingText.setText(this.outgoingText.getText() + newline );
    }
}
