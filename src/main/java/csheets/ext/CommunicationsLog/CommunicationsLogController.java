/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.CommunicationsLog;

import csheets.ext.CommunicationsLog.ui.CommunicationsLogPanel;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * author @1111098 Carlos Ferreira
 */

public class CommunicationsLogController extends FocusOwnerAction {
    
    private UIController uiController;
    private CommunicationsLogPanel sidebarPanel;
   
   //private NetworkService nwServ;
   //private List<NetworkInstance> list_ni;

    /**
     *Constructor of the class
     * @param uiController uiController
     * @param commPanel commPanel
     */
    public CommunicationsLogController(UIController uiController, CommunicationsLogPanel commPanel) {
        this.uiController = uiController;
        this.sidebarPanel = commPanel;
        
        

      //  nwServ = NetworkService.getInstance();
    }
    
    /**
     *Method to disconnect the active connection
     */
   /* public void disconnect() {
        NetworkInstance n = (NetworkInstance) sidebarPanel.getConnectModel().get(sidebarPanel.getConnectList().getSelectedIndex());
        nwServ.disconectFrom(n);
   }*/

    /**
     *Method to send the select cells of the workbook
     */
    
    public void sendpacket () {
        
        
    }
    
    public void sendData() {
       /* Cell[][] currentCells = focusOwner.getSelectedCells();
        CellsPacket cellsToSend = new CellsPacket(new HashMap<Address, String>());

        for (int i = 0; i < currentCells.length; i++) {
            for (int j = 0; j < currentCells[0].length; j++) {
                Cell currentActiveCell = currentCells[i][j];
                cellsToSend.addCell(currentActiveCell.getAddress(), currentActiveCell.getContent());
            }
        }
        List<NetworkInstance> sendTo = new ArrayList<NetworkInstance>();
        try {
            
        sendTo.add((NetworkInstance) this.sidebarPanel.getConnectModel().get(this.sidebarPanel.getConnectList().getSelectedIndex()));

        nwServ.send(cellsToSend, sendTo);
        
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Please, select an active connection to share this cells!");
        }
        */
    }
    
    /**
     *Method to start the listen of the sharePanel
     * @param cp CommunicationsLogPanel
     */
    public void startListen(CommunicationsLogPanel cp) {
     /*   this.sidebarPanel = sp;
        int port = Integer.valueOf(sidebarPanel.getListenPort());
        String name = sidebarPanel.getListenName();
        InetAddress ip = null;
        try{
            ip = InetAddress.getLocalHost();
        }catch(UnknownHostException ex){
            Logger.getLogger(StartSharingController.class.getName()).log(Level.SEVERE, null, ex);
        }

        NetworkData nd = new NetworkData(ip,name,port);
        nwServ.defineNetwork(nd);
        nwServ = NetworkService.getInstance();
        JOptionPane.showMessageDialog(null, "Share was activated\n You will be found on network!!");
        nwServ.addObserver(sidebarPanel);
        receivedDataObservers();
        nwServ.listeningBroadcast();
        nwServ.acceptLink();        
        nwServ.broadcastInstances();*/
    }

    @Override
    protected String getName() {
        return "";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }
    
    /**
     *Method to return the instances ins the list
     * @return list of instances
     */

    /**
     * Method to establish the connection
     */

    /**
     *Method to receive the observer
     */
    /*public void receivedDataObservers() {
        
            try {
            String line;
            line = "csheets.ext.startsharing.CellsPlacer";
            nwServ.addReceivedDataObserver((CellsReceiverObserver) Class.forName(line).newInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    */


}