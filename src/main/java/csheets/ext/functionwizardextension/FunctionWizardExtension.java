/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.functionwizardextension;

import csheets.ext.Extension;
import csheets.ext.functionwizard.ui.UIFunctionWizard;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author 11011_000
 */
public class FunctionWizardExtension extends Extension {
    
        /**
     * The version
     */
    public static final Long VERSION = 1L;

    /**
     * Description
     */
    public static final String DESCRIPTION = "Shows available functions";

    /**
     * The name of the extension
     */
    public static final String NAME = "Function Wizard";
    
    public FunctionWizardExtension() {
            super(VERSION, NAME, DESCRIPTION);
    }
    
    @Override
    public UIExtension getUIExtension(UIController uiController) {
        return new UIFunctionWizard(this, uiController);
    }

}
