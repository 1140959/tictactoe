/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.functionwizardextension;

import csheets.core.Value;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.functionwizard.ui.FunctionWizardUI;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author 11011_000
 */
public class FunctionWizardController {
    
    FunctionWizard wizard;
    UIController uiController;
    FunctionWizardUI ui;
    Function[] functionsList;
    String editFunc;
    
    DefaultListModel descParamModel = new DefaultListModel();
    
    public FunctionWizardController(UIController uiControl) {
        uiController=uiControl;
        wizard=new FunctionWizard();
        functionsList=wizard.getFunctions();
        openUI();
    }
    
    public Function[] getFunctions() {
        
        return functionsList;
    } 
    
    private Value getFunctionValue(Function func, List<Double> parameters) {
        return null;
    }
    
    private void openUI() {
        ui=new FunctionWizardUI();
        ui.setVisible(true);
        initialize();
        listeners();
    }
    
    private void initialize() {
        functionsList = getFunctions();
        DefaultListModel model = new DefaultListModel();
        
        for(int i=0; i<functionsList.length;i++)
            model.addElement(functionsList[i].getIdentifier());

        ui.funclist.setModel(model);
        

    }
    
    private void listeners(){

        ui.funclist.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent evt) {
                showFunction(functionsList[ui.funclist.getSelectedIndex()]);
            }
        });

        ui.applyButton.addActionListener(new ActionListener() {
 
            public void actionPerformed(ActionEvent e)
            {
                try {
                    System.out.println(editFunc);
                    uiController.getActiveCell().setContent(ui.editFunction.getText());
                    ui.dispose();
                } catch (FormulaCompilationException ex) {
                    Logger.getLogger(FunctionWizardController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });  

        ui.CancelButton.addActionListener(new ActionListener() {
 
            public void actionPerformed(ActionEvent e)
            {
                ui.dispose();
            }
        });  

        
    }
    
    private void showFunction(Function function) {
        
        editFunc = "=" + function.getIdentifier() + "(";
        descParamModel.clear();
        FunctionParameter[] parameters = wizard.getParameters(function);
        for(int i=0; i<parameters.length; i++) {
            editFunc = editFunc + parameters[i].getName();
            descParamModel.addElement(parameters[i].getName() + ": " +parameters[i].getDescription());
            if(i<parameters.length-1)
                editFunc=editFunc + "; ";
        }
        
        editFunc=editFunc + ")";
        ui.editFunction.setText(editFunc);
        ui.descriptionParameters.setModel(descParamModel);
        
        ui.editFunction.setVisible(true);
    }
    

}
