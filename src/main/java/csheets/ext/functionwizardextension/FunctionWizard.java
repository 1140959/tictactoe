/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.functionwizardextension;

import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;
import csheets.core.formula.lang.Language;

/**
 *
 * @author 11011_000
 */
public class FunctionWizard {

    private final Function[] functionList;
    private final Language lang;
    
    FunctionWizard() {
        lang=Language.getInstance();
        functionList=lang.getFunctions();
        
    }
    
    public Function[] getFunctions() {
        return functionList;
    }
    
    public FunctionParameter[] getParameters(Function function){
        return function.getParameters();
    }
    
    
}
