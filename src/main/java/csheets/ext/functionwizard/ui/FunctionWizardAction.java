/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.functionwizard.ui;

import csheets.ext.functionwizardextension.FunctionWizardController;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author 11011_000
 */
class FunctionWizardAction extends BaseAction {

    /**
     * The user interface controller
     */
    UIController uiController;


    FunctionWizardAction(UIController uiControl) {
        this.uiController = uiControl;
    }
    
    
    /**
     * Gets the name/title of the Function Wizard
     *
     * @return returns UI title
     */
    public String getName() {
        return "Function Wizard";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        new FunctionWizardController(uiController);
    }

}
