/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.functionwizard.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author 11011_000
 */
public class FunctionWizardMenu extends JMenu{
    
    public FunctionWizardMenu(UIController uiController) {
		super("Function Wizard");
		setMnemonic(KeyEvent.VK_W);

		// Adds font actions
		this.add(new FunctionWizardAction(uiController));
	}	

}
