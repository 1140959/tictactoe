package csheets.ext.manageextensions.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 * Menu of the Manage Extensions extension.
 * @author 1100588
 */
public class ManageExtensionsMenu extends JMenu {
    
    /**
     * Cria um menu para o Manage Extensions.
     * @param uic the user interface controller
     */
    public ManageExtensionsMenu(UIController uic) {
    	super("Manage Extensions");
    	setMnemonic(KeyEvent.VK_E);
	// Adds font actions
	add(new ManageExtensionsAction(uic));
    }
}
