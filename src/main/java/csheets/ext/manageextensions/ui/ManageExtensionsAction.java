/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.manageextensions.ui;

import csheets.ext.ExtensionManager;
import csheets.ui.Frame;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * An action of the Manage Extensions extension. Creates the extensions' Wizard.
 *
 * @author 1100588
 */
public class ManageExtensionsAction extends BaseAction {

    /**
     * The user interface controller
     */
    protected UIController uiController;

    private JScrollPane listPane;

    /**
     * Stores the existing extensions for use during the activation/deactivation
     * proccess.
     */
    private JCheckBox[] checkBoxList;

    /**
     * Creates an extension manager
     */
    ExtensionManager extensionManager = ExtensionManager.getInstance();

    /**
     *
     * @param controller For ManageExtension
     */
    public ManageExtensionsAction(UIController controller) {
        uiController = controller;
        createExtentionsCheckBox();
    }

    /**
     * Gets the name/title of the Extension Manager
     *
     * @return returns UI title
     */
    protected String getName() {
        return "Manage Extensions";
    }

    /**
     *
     */
    protected void defineProperties() {
    }

    /**
     *
     * @param e ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog((Frame) null, listPane, getName(), JOptionPane.QUESTION_MESSAGE);
        updateExtentionsState();
    }

    /**
     * Creates an ExtentionCheckBox
     */
    private void createExtentionsCheckBox() {
        UIExtension[] ext = uiController.getExtensions();
        checkBoxList = new JCheckBox[ext.length];

        JPanel panel = new JPanel();
        //Grid layout 0 = inifite rows, 2 = colums
        panel.setLayout(new GridLayout(0, 2));

        for (int i = 0; i < ext.length; i++) {
            JCheckBox checkBox = new JCheckBox();
            checkBox.setText(ext[i].getExtension().getName());
            if (ext[i].isEnabled()) {
                checkBox.setSelected(true);
            } else {
                checkBox.setSelected(false);
            }
            if (checkBox.getText().equals(getName())) {
                checkBox.setEnabled(false);
            }
            panel.add(checkBox);
            checkBoxList[i] = checkBox;

        }
        listPane = new JScrollPane(panel);

    }

    /**
     * Updates all Extentions State
     */
    private void updateExtentionsState() {
        for (JCheckBox jcheck : checkBoxList) {
            UIExtension ext = getExtensionUI(jcheck.getText());
            if (ext != null) {
                if (jcheck.isSelected()) {
                    ext.enable();
                } else {
                    ext.disable();
                }
            }
        }
    }

    /**
     * Get the Extension with the parameter "name"
     *
     * @param name
     * @return Extension
     */
    private UIExtension getExtensionUI(String name) {
        for (UIExtension uiExt : uiController.getExtensions()) {
            if (uiExt.getExtension().getName().equalsIgnoreCase(name)) {
                return uiExt;
            }
        }
        return null;
    }
}
