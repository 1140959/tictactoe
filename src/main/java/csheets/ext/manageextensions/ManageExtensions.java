package csheets.ext.manageextensions;

import csheets.ext.Extension;
import csheets.ext.manageextensions.ui.UIManageExtensions;
import csheets.ext.simple.ui.UIExtensionExample;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * A simple extension just to show how the extension mechanism works. An
 * extension must extend the Extension abstract class. The class that implements
 * the Extension is the "bootstrap" of the extension.
 *
 * @see Extension
 * @author 1100588
 */
public class ManageExtensions extends Extension {

    /**
     * The version
     */
    public static final Long VERSION = 1L;

    /**
     * Description
     */
    public static final String DESCRIPTION = "Manage all of your extensions";

    /**
     * The name of the extension
     */
    public static final String NAME = "Manage Extensions";

    /**
     * Creates a new Example extension.
     */
    public ManageExtensions() {
        super(VERSION, NAME, DESCRIPTION);
    }

    /**
     * Returns the user interface extension of this extension (an instance of
     * the class {@link  csheets.ext.manageextensions.ui.UIManageExtensions}). In
     * this extension example we are only extending the user interface.
     *
     * @param uiController the user interface controller
     * @return a user interface extension, or null if none is provided
     */
    public UIExtension getUIExtension(UIController uiController) {
        return new UIManageExtensions(this, uiController);
    }
}
