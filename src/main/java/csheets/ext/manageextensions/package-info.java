/**
 * Provides an extension for the management of the existent extensions.
 * This extension will have a menu on which we can Enable and Disable extensions.
 * 
 * @author 1100588
 * 
 */

package csheets.ext.manageextensions;


