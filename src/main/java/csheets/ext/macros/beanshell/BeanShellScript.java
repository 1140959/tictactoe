/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.beanshell;

import bsh.EvalError;
import bsh.Interpreter;

/**
 *
 * @author Antonio
 */
public class BeanShellScript {

    Interpreter interpreter;

    /**
     * Constructor Bean Shell Script
     */
    public BeanShellScript() {
        this.interpreter = new Interpreter();
    }

    /**
     * Adds an object
     *
     * @param name
     * @param obj
     * @throws EvalError
     */
    public void addObject(String name, Object obj) throws EvalError {
        interpreter.set(name, obj);
    }

    /**
     * Run the script against the compiler
     *
     * @param script
     * @return string with result
     * @throws EvalError
     */
    public String runScript(String script) throws EvalError {
        //removes comments from
        script = removeComments(script);
        Object result = interpreter.eval(script);
        if (result != null) {
            return result.toString();
        } else {
            return "";
        }
    }

    /**
     * Removes comments from a script
     *
     * @param script text
     * @return script without comments
     */
    private String removeComments(String script) {
        return script.replaceAll("(?m)^;.*", "");
    }
}
