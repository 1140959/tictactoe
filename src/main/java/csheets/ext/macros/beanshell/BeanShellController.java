/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.beanshell;

import bsh.EvalError;
import csheets.ui.ctrl.UIController;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Antonio
 */
public class BeanShellController {

    /**
     * interpreter instance
     */
    BeanShellScript interpreter;

    /**
     * Constructor
     */
    public BeanShellController() {
        this.interpreter = new BeanShellScript();
    }

    /**
     * Constructor
     *
     * @param uiController
     */
    public BeanShellController(UIController uiController) {
        this.interpreter = new BeanShellScript();
        try {
            interpreter.addObject("workbook", uiController.getActiveWorkbook());
        } catch (EvalError ex) {
            Logger.getLogger(BeanShellController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * loads the default file
     *
     * @return string contained in the file
     * @throws IOException
     */
    public String loadDefault() throws IOException {
        return new String(Files.readAllBytes(Paths.get("res/ext/beanshell/default.bsh")));
    }

    /**
     * Calls the interpreter to run the script
     *
     * @param script
     * @return result
     * @throws EvalError
     */
    public String runScript(String script) throws EvalError {
        return interpreter.runScript(script);
    }

}
