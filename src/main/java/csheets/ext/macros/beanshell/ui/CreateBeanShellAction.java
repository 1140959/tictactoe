/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.beanshell.ui;

import csheets.ui.Frame;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author Antonio
 */
public class CreateBeanShellAction extends BaseAction {

    protected UIController uiController;

    public CreateBeanShellAction(UIController controller) {
        uiController = controller;
    }

    @Override
    protected String getName() {
        return "Run BeanShell Script";
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        new CreateBeanShellUIDialog((Frame)null,true,uiController).setVisible(true);
    }

}
