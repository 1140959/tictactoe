// $ANTLR 3.4 csheets\\ext\\macro\\compiler\\lang.g 2016-06-07 14:41:23

package csheets.ext.macros.compiler;


import org.antlr.runtime.*;
import static org.antlr.runtime.BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
import static org.antlr.runtime.BaseRecognizer.HIDDEN;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class MacroLexer extends Lexer {
    public static final int EOF=-1;
    public static final int ABS=4;
    public static final int AMP=5;
    public static final int AOP=6;
    public static final int CELL_REF=7;
    public static final int COLON=8;
    public static final int COMMA=9;
    public static final int DIGIT=10;
    public static final int DIV=11;
    public static final int EQ=12;
    public static final int EXCL=13;
    public static final int FUNCTION=14;
    public static final int GT=15;
    public static final int GTEQ=16;
    public static final int LBRAC=17;
    public static final int LETTER=18;
    public static final int LPAR=19;
    public static final int LT=20;
    public static final int LTEQ=21;
    public static final int MINUS=22;
    public static final int MULTI=23;
    public static final int NEQ=24;
    public static final int NUMBER=25;
    public static final int PERCENT=26;
    public static final int PLUS=27;
    public static final int POWER=28;
    public static final int QUOT=29;
    public static final int RBRAC=30;
    public static final int RPAR=31;
    public static final int SEMI=32;
    public static final int STRING=33;
    public static final int WS=34;

    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public MacroLexer() {} 
    public MacroLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public MacroLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "csheets\\ext\\macro\\compiler\\lang.g"; }

    // $ANTLR start "LETTER"
    public final void mLETTER() throws RecognitionException {
        try {
            // csheets\\ext\\macro\\compiler\\lang.g:122:16: ( ( 'a' .. 'z' | 'A' .. 'Z' ) )
            // csheets\\ext\\macro\\compiler\\lang.g:
            {
            if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LETTER"

    // $ANTLR start "FUNCTION"
    public final void mFUNCTION() throws RecognitionException {
        try {
            int _type = FUNCTION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:124:10: ( ( LETTER )+ )
            // csheets\\ext\\macro\\compiler\\lang.g:125:4: ( LETTER )+
            {
            // csheets\\ext\\macro\\compiler\\lang.g:125:4: ( LETTER )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                switch ( input.LA(1) ) {
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                    {
                    alt1=1;
                    }
                    break;

                }

                switch (alt1) {
            	case 1 :
            	    // csheets\\ext\\macro\\compiler\\lang.g:
            	    {
            	    if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FUNCTION"

    // $ANTLR start "CELL_REF"
    public final void mCELL_REF() throws RecognitionException {
        try {
            int _type = CELL_REF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:130:2: ( ( ABS )? LETTER ( LETTER )? ( ABS )? ( DIGIT )+ )
            // csheets\\ext\\macro\\compiler\\lang.g:131:3: ( ABS )? LETTER ( LETTER )? ( ABS )? ( DIGIT )+
            {
            // csheets\\ext\\macro\\compiler\\lang.g:131:3: ( ABS )?
            int alt2=2;
            switch ( input.LA(1) ) {
                case '$':
                    {
                    alt2=1;
                    }
                    break;
            }

            switch (alt2) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:
                    {
                    if ( input.LA(1)=='$' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }


            mLETTER(); 


            // csheets\\ext\\macro\\compiler\\lang.g:131:19: ( LETTER )?
            int alt3=2;
            switch ( input.LA(1) ) {
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                    {
                    alt3=1;
                    }
                    break;
            }

            switch (alt3) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:
                    {
                    if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }


            // csheets\\ext\\macro\\compiler\\lang.g:132:3: ( ABS )?
            int alt4=2;
            switch ( input.LA(1) ) {
                case '$':
                    {
                    alt4=1;
                    }
                    break;
            }

            switch (alt4) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:
                    {
                    if ( input.LA(1)=='$' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }


            // csheets\\ext\\macro\\compiler\\lang.g:132:12: ( DIGIT )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                switch ( input.LA(1) ) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    {
                    alt5=1;
                    }
                    break;

                }

                switch (alt5) {
            	case 1 :
            	    // csheets\\ext\\macro\\compiler\\lang.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "CELL_REF"

    // $ANTLR start "STRING"
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:137:8: ( QUOT ( options {greedy=false; } : . )* QUOT )
            // csheets\\ext\\macro\\compiler\\lang.g:137:10: QUOT ( options {greedy=false; } : . )* QUOT
            {
            mQUOT(); 


            // csheets\\ext\\macro\\compiler\\lang.g:138:3: ( options {greedy=false; } : . )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0=='\"') ) {
                    alt6=2;
                }
                else if ( ((LA6_0 >= '\u0000' && LA6_0 <= '!')||(LA6_0 >= '#' && LA6_0 <= '\uFFFF')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // csheets\\ext\\macro\\compiler\\lang.g:138:28: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            mQUOT(); 


             setText(getText().substring(1, getText().length()-1)); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "STRING"

    // $ANTLR start "QUOT"
    public final void mQUOT() throws RecognitionException {
        try {
            int _type = QUOT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:142:5: ( '\"' )
            // csheets\\ext\\macro\\compiler\\lang.g:142:7: '\"'
            {
            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "QUOT"

    // $ANTLR start "NUMBER"
    public final void mNUMBER() throws RecognitionException {
        try {
            int _type = NUMBER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:146:7: ( ( DIGIT )+ ( COMMA ( DIGIT )+ )? )
            // csheets\\ext\\macro\\compiler\\lang.g:146:9: ( DIGIT )+ ( COMMA ( DIGIT )+ )?
            {
            // csheets\\ext\\macro\\compiler\\lang.g:146:9: ( DIGIT )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                switch ( input.LA(1) ) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    {
                    alt7=1;
                    }
                    break;

                }

                switch (alt7) {
            	case 1 :
            	    // csheets\\ext\\macro\\compiler\\lang.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);


            // csheets\\ext\\macro\\compiler\\lang.g:146:20: ( COMMA ( DIGIT )+ )?
            int alt9=2;
            switch ( input.LA(1) ) {
                case ',':
                    {
                    alt9=1;
                    }
                    break;
            }

            switch (alt9) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:146:22: COMMA ( DIGIT )+
                    {
                    mCOMMA(); 


                    // csheets\\ext\\macro\\compiler\\lang.g:146:28: ( DIGIT )+
                    int cnt8=0;
                    loop8:
                    do {
                        int alt8=2;
                        switch ( input.LA(1) ) {
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                            {
                            alt8=1;
                            }
                            break;

                        }

                        switch (alt8) {
                    	case 1 :
                    	    // csheets\\ext\\macro\\compiler\\lang.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt8 >= 1 ) break loop8;
                                EarlyExitException eee =
                                    new EarlyExitException(8, input);
                                throw eee;
                        }
                        cnt8++;
                    } while (true);


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NUMBER"

    // $ANTLR start "DIGIT"
    public final void mDIGIT() throws RecognitionException {
        try {
            // csheets\\ext\\macro\\compiler\\lang.g:150:7: ( '0' .. '9' )
            // csheets\\ext\\macro\\compiler\\lang.g:
            {
            if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DIGIT"

    // $ANTLR start "EQ"
    public final void mEQ() throws RecognitionException {
        try {
            int _type = EQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:152:5: ( '=' )
            // csheets\\ext\\macro\\compiler\\lang.g:152:7: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EQ"

    // $ANTLR start "NEQ"
    public final void mNEQ() throws RecognitionException {
        try {
            int _type = NEQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:153:6: ( '<>' )
            // csheets\\ext\\macro\\compiler\\lang.g:153:8: '<>'
            {
            match("<>"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NEQ"

    // $ANTLR start "LTEQ"
    public final void mLTEQ() throws RecognitionException {
        try {
            int _type = LTEQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:154:6: ( '<=' )
            // csheets\\ext\\macro\\compiler\\lang.g:154:8: '<='
            {
            match("<="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LTEQ"

    // $ANTLR start "GTEQ"
    public final void mGTEQ() throws RecognitionException {
        try {
            int _type = GTEQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:155:6: ( '>=' )
            // csheets\\ext\\macro\\compiler\\lang.g:155:8: '>='
            {
            match(">="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GTEQ"

    // $ANTLR start "GT"
    public final void mGT() throws RecognitionException {
        try {
            int _type = GT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:156:5: ( '>' )
            // csheets\\ext\\macro\\compiler\\lang.g:156:7: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GT"

    // $ANTLR start "LT"
    public final void mLT() throws RecognitionException {
        try {
            int _type = LT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:157:5: ( '<' )
            // csheets\\ext\\macro\\compiler\\lang.g:157:7: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LT"

    // $ANTLR start "AOP"
    public final void mAOP() throws RecognitionException {
        try {
            int _type = AOP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:160:6: ( ':=' )
            // csheets\\ext\\macro\\compiler\\lang.g:160:8: ':='
            {
            match(":="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "AOP"

    // $ANTLR start "AMP"
    public final void mAMP() throws RecognitionException {
        try {
            int _type = AMP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:163:6: ( '&' )
            // csheets\\ext\\macro\\compiler\\lang.g:163:8: '&'
            {
            match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "AMP"

    // $ANTLR start "PLUS"
    public final void mPLUS() throws RecognitionException {
        try {
            int _type = PLUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:166:6: ( '+' )
            // csheets\\ext\\macro\\compiler\\lang.g:166:8: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PLUS"

    // $ANTLR start "MINUS"
    public final void mMINUS() throws RecognitionException {
        try {
            int _type = MINUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:167:7: ( '-' )
            // csheets\\ext\\macro\\compiler\\lang.g:167:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MINUS"

    // $ANTLR start "MULTI"
    public final void mMULTI() throws RecognitionException {
        try {
            int _type = MULTI;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:168:7: ( '*' )
            // csheets\\ext\\macro\\compiler\\lang.g:168:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MULTI"

    // $ANTLR start "DIV"
    public final void mDIV() throws RecognitionException {
        try {
            int _type = DIV;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:169:6: ( '/' )
            // csheets\\ext\\macro\\compiler\\lang.g:169:8: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DIV"

    // $ANTLR start "POWER"
    public final void mPOWER() throws RecognitionException {
        try {
            int _type = POWER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:170:7: ( '^' )
            // csheets\\ext\\macro\\compiler\\lang.g:170:9: '^'
            {
            match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "POWER"

    // $ANTLR start "PERCENT"
    public final void mPERCENT() throws RecognitionException {
        try {
            int _type = PERCENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:171:9: ( '%' )
            // csheets\\ext\\macro\\compiler\\lang.g:171:11: '%'
            {
            match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PERCENT"

    // $ANTLR start "ABS"
    public final void mABS() throws RecognitionException {
        try {
            // csheets\\ext\\macro\\compiler\\lang.g:174:14: ( '$' )
            // csheets\\ext\\macro\\compiler\\lang.g:174:16: '$'
            {
            match('$'); 

            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ABS"

    // $ANTLR start "EXCL"
    public final void mEXCL() throws RecognitionException {
        try {
            // csheets\\ext\\macro\\compiler\\lang.g:175:14: ( '!' )
            // csheets\\ext\\macro\\compiler\\lang.g:175:17: '!'
            {
            match('!'); 

            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EXCL"

    // $ANTLR start "COLON"
    public final void mCOLON() throws RecognitionException {
        try {
            int _type = COLON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:176:7: ( ':' )
            // csheets\\ext\\macro\\compiler\\lang.g:176:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COLON"

    // $ANTLR start "COMMA"
    public final void mCOMMA() throws RecognitionException {
        try {
            int _type = COMMA;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:179:7: ( ',' )
            // csheets\\ext\\macro\\compiler\\lang.g:179:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COMMA"

    // $ANTLR start "SEMI"
    public final void mSEMI() throws RecognitionException {
        try {
            int _type = SEMI;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:180:6: ( ';' )
            // csheets\\ext\\macro\\compiler\\lang.g:180:8: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SEMI"

    // $ANTLR start "LPAR"
    public final void mLPAR() throws RecognitionException {
        try {
            int _type = LPAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:181:6: ( '(' )
            // csheets\\ext\\macro\\compiler\\lang.g:181:8: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LPAR"

    // $ANTLR start "RPAR"
    public final void mRPAR() throws RecognitionException {
        try {
            int _type = RPAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:182:6: ( ')' )
            // csheets\\ext\\macro\\compiler\\lang.g:182:8: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RPAR"

    // $ANTLR start "LBRAC"
    public final void mLBRAC() throws RecognitionException {
        try {
            int _type = LBRAC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:183:9: ( '{' )
            // csheets\\ext\\macro\\compiler\\lang.g:183:11: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LBRAC"

    // $ANTLR start "RBRAC"
    public final void mRBRAC() throws RecognitionException {
        try {
            int _type = RBRAC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:184:7: ( '}' )
            // csheets\\ext\\macro\\compiler\\lang.g:184:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RBRAC"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // csheets\\ext\\macro\\compiler\\lang.g:187:3: ( ( ' ' | '\\r' '\\n' | '\\n' | '\\t' ) )
            // csheets\\ext\\macro\\compiler\\lang.g:187:5: ( ' ' | '\\r' '\\n' | '\\n' | '\\t' )
            {
            // csheets\\ext\\macro\\compiler\\lang.g:187:5: ( ' ' | '\\r' '\\n' | '\\n' | '\\t' )
            int alt10=4;
            switch ( input.LA(1) ) {
            case ' ':
                {
                alt10=1;
                }
                break;
            case '\r':
                {
                alt10=2;
                }
                break;
            case '\n':
                {
                alt10=3;
                }
                break;
            case '\t':
                {
                alt10=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;

            }

            switch (alt10) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:187:7: ' '
                    {
                    match(' '); 

                    }
                    break;
                case 2 :
                    // csheets\\ext\\macro\\compiler\\lang.g:188:4: '\\r' '\\n'
                    {
                    match('\r'); 

                    match('\n'); 

                    }
                    break;
                case 3 :
                    // csheets\\ext\\macro\\compiler\\lang.g:189:4: '\\n'
                    {
                    match('\n'); 

                    }
                    break;
                case 4 :
                    // csheets\\ext\\macro\\compiler\\lang.g:190:4: '\\t'
                    {
                    match('\t'); 

                    }
                    break;

            }


            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    public void mTokens() throws RecognitionException {
        // csheets\\ext\\macro\\compiler\\lang.g:1:8: ( FUNCTION | CELL_REF | STRING | QUOT | NUMBER | EQ | NEQ | LTEQ | GTEQ | GT | LT | AOP | AMP | PLUS | MINUS | MULTI | DIV | POWER | PERCENT | COLON | COMMA | SEMI | LPAR | RPAR | LBRAC | RBRAC | WS )
        int alt11=27;
        switch ( input.LA(1) ) {
        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case 'G':
        case 'H':
        case 'I':
        case 'J':
        case 'K':
        case 'L':
        case 'M':
        case 'N':
        case 'O':
        case 'P':
        case 'Q':
        case 'R':
        case 'S':
        case 'T':
        case 'U':
        case 'V':
        case 'W':
        case 'X':
        case 'Y':
        case 'Z':
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'g':
        case 'h':
        case 'i':
        case 'j':
        case 'k':
        case 'l':
        case 'm':
        case 'n':
        case 'o':
        case 'p':
        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'x':
        case 'y':
        case 'z':
            {
            switch ( input.LA(2) ) {
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
            case 'G':
            case 'H':
            case 'I':
            case 'J':
            case 'K':
            case 'L':
            case 'M':
            case 'N':
            case 'O':
            case 'P':
            case 'Q':
            case 'R':
            case 'S':
            case 'T':
            case 'U':
            case 'V':
            case 'W':
            case 'X':
            case 'Y':
            case 'Z':
            case 'a':
            case 'b':
            case 'c':
            case 'd':
            case 'e':
            case 'f':
            case 'g':
            case 'h':
            case 'i':
            case 'j':
            case 'k':
            case 'l':
            case 'm':
            case 'n':
            case 'o':
            case 'p':
            case 'q':
            case 'r':
            case 's':
            case 't':
            case 'u':
            case 'v':
            case 'w':
            case 'x':
            case 'y':
            case 'z':
                {
                switch ( input.LA(3) ) {
                case '$':
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    {
                    alt11=2;
                    }
                    break;
                default:
                    alt11=1;
                }

                }
                break;
            case '$':
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                {
                alt11=2;
                }
                break;
            default:
                alt11=1;
            }

            }
            break;
        case '$':
            {
            alt11=2;
            }
            break;
        case '\"':
            {
            int LA11_3 = input.LA(2);

            if ( ((LA11_3 >= '\u0000' && LA11_3 <= '\uFFFF')) ) {
                alt11=3;
            }
            else {
                alt11=4;
            }
            }
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            {
            alt11=5;
            }
            break;
        case '=':
            {
            alt11=6;
            }
            break;
        case '<':
            {
            switch ( input.LA(2) ) {
            case '>':
                {
                alt11=7;
                }
                break;
            case '=':
                {
                alt11=8;
                }
                break;
            default:
                alt11=11;
            }

            }
            break;
        case '>':
            {
            switch ( input.LA(2) ) {
            case '=':
                {
                alt11=9;
                }
                break;
            default:
                alt11=10;
            }

            }
            break;
        case ':':
            {
            switch ( input.LA(2) ) {
            case '=':
                {
                alt11=12;
                }
                break;
            default:
                alt11=20;
            }

            }
            break;
        case '&':
            {
            alt11=13;
            }
            break;
        case '+':
            {
            alt11=14;
            }
            break;
        case '-':
            {
            alt11=15;
            }
            break;
        case '*':
            {
            alt11=16;
            }
            break;
        case '/':
            {
            alt11=17;
            }
            break;
        case '^':
            {
            alt11=18;
            }
            break;
        case '%':
            {
            alt11=19;
            }
            break;
        case ',':
            {
            alt11=21;
            }
            break;
        case ';':
            {
            alt11=22;
            }
            break;
        case '(':
            {
            alt11=23;
            }
            break;
        case ')':
            {
            alt11=24;
            }
            break;
        case '{':
            {
            alt11=25;
            }
            break;
        case '}':
            {
            alt11=26;
            }
            break;
        case '\t':
        case '\n':
        case '\r':
        case ' ':
            {
            alt11=27;
            }
            break;
        default:
            NoViableAltException nvae =
                new NoViableAltException("", 11, 0, input);

            throw nvae;

        }

        switch (alt11) {
            case 1 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:10: FUNCTION
                {
                mFUNCTION(); 


                }
                break;
            case 2 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:19: CELL_REF
                {
                mCELL_REF(); 


                }
                break;
            case 3 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:28: STRING
                {
                mSTRING(); 


                }
                break;
            case 4 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:35: QUOT
                {
                mQUOT(); 


                }
                break;
            case 5 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:40: NUMBER
                {
                mNUMBER(); 


                }
                break;
            case 6 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:47: EQ
                {
                mEQ(); 


                }
                break;
            case 7 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:50: NEQ
                {
                mNEQ(); 


                }
                break;
            case 8 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:54: LTEQ
                {
                mLTEQ(); 


                }
                break;
            case 9 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:59: GTEQ
                {
                mGTEQ(); 


                }
                break;
            case 10 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:64: GT
                {
                mGT(); 


                }
                break;
            case 11 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:67: LT
                {
                mLT(); 


                }
                break;
            case 12 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:70: AOP
                {
                mAOP(); 


                }
                break;
            case 13 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:74: AMP
                {
                mAMP(); 


                }
                break;
            case 14 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:78: PLUS
                {
                mPLUS(); 


                }
                break;
            case 15 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:83: MINUS
                {
                mMINUS(); 


                }
                break;
            case 16 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:89: MULTI
                {
                mMULTI(); 


                }
                break;
            case 17 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:95: DIV
                {
                mDIV(); 


                }
                break;
            case 18 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:99: POWER
                {
                mPOWER(); 


                }
                break;
            case 19 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:105: PERCENT
                {
                mPERCENT(); 


                }
                break;
            case 20 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:113: COLON
                {
                mCOLON(); 


                }
                break;
            case 21 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:119: COMMA
                {
                mCOMMA(); 


                }
                break;
            case 22 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:125: SEMI
                {
                mSEMI(); 


                }
                break;
            case 23 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:130: LPAR
                {
                mLPAR(); 


                }
                break;
            case 24 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:135: RPAR
                {
                mRPAR(); 


                }
                break;
            case 25 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:140: LBRAC
                {
                mLBRAC(); 


                }
                break;
            case 26 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:146: RBRAC
                {
                mRBRAC(); 


                }
                break;
            case 27 :
                // csheets\\ext\\macro\\compiler\\lang.g:1:152: WS
                {
                mWS(); 


                }
                break;

        }

    }


 

}