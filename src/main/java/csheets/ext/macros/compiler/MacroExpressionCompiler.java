/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.compiler;

import csheets.core.Cell;
import csheets.core.Value;
import csheets.core.formula.*;
import csheets.core.formula.compiler.ExpressionCompiler;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.macros.lang.CellReference;
import csheets.ext.macros.lang.MacroLanguage;
import csheets.ext.macros.lang.RangeReference;
import csheets.ext.macros.lang.ReferenceOperation;
import csheets.ext.macros.lang.UnknownElementException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 *
 * @author Fred
 */
public class MacroExpressionCompiler implements ExpressionCompiler {

    /**
     * The character that signals that a cell's content is a comment ('=')
     */
    public static final Character COMMENT_STARTER = ';';
    
    /**
     * Returns comment starter
     * @return char
     */
    @Override
    public char getStarter() {
        return COMMENT_STARTER;
    }
    
    /**
     * Compiles the source at the given cell
     * @param cell Cell
     * @param source String
     * @return Expression
     * @throws FormulaCompilationException Exception
     */
    @Override
    public Expression compile(Cell cell, String source) throws FormulaCompilationException {
        // Creates the lexer and parser
        ANTLRStringStream input = new ANTLRStringStream(source);

        // create the buffer of tokens between the lexer and parser 
        MacroLexer lexer = new MacroLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        MacroParser parser = new MacroParser(tokens);

        CommonTree tree = null;

        try {
            // Attempts to match an expression
            tree = (CommonTree) parser.expression().getTree();
        } catch (RecognitionException e) {
            //using same exceptions as the Formula, i didn't think it was needed to implement the macro's own exceptions
            throw new FormulaCompilationException("At (" + e.line + ";" 
                    + e.charPositionInLine + "): " 
                    + parser.getErrorMessage(e, parser.tokenNames));
        } catch (Exception e) {
            String message = "Other exception : " + e.getMessage();
            throw new FormulaCompilationException(message);
        }

        // Converts the expression and returns it
        return convert(cell, tree);
    }

    /**
     * Converts the given ANTLR AST to an expression.
     *
     * @param cell cell
     * @param node the abstract syntax tree node to convert
     * @return the result of the conversion
     * @throws csheets.core.formula.compiler.FormulaCompilationException Exception
     */
    protected Expression convert(Cell cell, Tree node) throws FormulaCompilationException {
        //Uncommented to check tree - test only
        System.out.println("Converting node '" + node.getText() + "' of tree '" + node.toStringTree() + "' with " + node.getChildCount() + " children.");
        if (node.getChildCount() == 0) {
            try {
                switch (node.getType()) {
                    case MacroLexer.NUMBER:
                        return new Literal(Value.parseNumericValue(node.getText()));
                    case MacroLexer.STRING:
                        return new Literal(Value.parseValue(node.getText(), Value.Type.BOOLEAN, Value.Type.DATE));
                    case MacroLexer.CELL_REF:
                        return new CellReference(cell.getSpreadsheet(), node.getText());

                }
            } catch (ParseException e) {
                throw new FormulaCompilationException(e);
            }
        }

        // Convert function call
        Function function = null;
        try {
            function = MacroLanguage.getInstance().getFunction(node.getText());
        } catch (UnknownElementException e) {
        }

        if (function != null) {
            List<Expression> args = new ArrayList<Expression>();
            Tree child = node.getChild(0);
            if (child != null) {
                for (int nChild = 0; nChild < node.getChildCount(); ++nChild) {
                    child = node.getChild(nChild);
                    args.add(convert(cell, child));
                }
            }
            Expression[] argArray = args.toArray(new Expression[args.size()]);
            return new FunctionCall(function, argArray);
        }

        // Convert unary operation
        if (node.getChildCount() == 1) 
        {
            return new UnaryOperation(
                    MacroLanguage.getInstance().getUnaryOperator(node.getText()),
                    convert(cell, node.getChild(0))
            );
        } else if (node.getChildCount() == 2) {
            // Convert binary operation
            BinaryOperator operator = MacroLanguage.getInstance().getBinaryOperator(node.getText());
            if (operator instanceof RangeReference) {
                return new ReferenceOperation(
                        (Reference) convert(cell, node.getChild(0)),
                        (RangeReference) operator,
                        (Reference) convert(cell, node.getChild(1))
                );
            }
           
            else {
                return new BinaryOperation(
                        convert(cell, node.getChild(0)),
                        operator,
                        convert(cell, node.getChild(1))
                );
            }
        } else // Shouldn't happen
        {
            throw new FormulaCompilationException();
        }
    }
}
