// $ANTLR 3.4 csheets\\ext\\macro\\compiler\\lang.g 2016-06-07 14:41:23

package csheets.ext.macros.compiler;


import org.antlr.runtime.*;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class MacroParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ABS", "AMP", "AOP", "CELL_REF", "COLON", "COMMA", "DIGIT", "DIV", "EQ", "EXCL", "FUNCTION", "GT", "GTEQ", "LBRAC", "LETTER", "LPAR", "LT", "LTEQ", "MINUS", "MULTI", "NEQ", "NUMBER", "PERCENT", "PLUS", "POWER", "QUOT", "RBRAC", "RPAR", "SEMI", "STRING", "WS"
    };

    public static final int EOF=-1;
    public static final int ABS=4;
    public static final int AMP=5;
    public static final int AOP=6;
    public static final int CELL_REF=7;
    public static final int COLON=8;
    public static final int COMMA=9;
    public static final int DIGIT=10;
    public static final int DIV=11;
    public static final int EQ=12;
    public static final int EXCL=13;
    public static final int FUNCTION=14;
    public static final int GT=15;
    public static final int GTEQ=16;
    public static final int LBRAC=17;
    public static final int LETTER=18;
    public static final int LPAR=19;
    public static final int LT=20;
    public static final int LTEQ=21;
    public static final int MINUS=22;
    public static final int MULTI=23;
    public static final int NEQ=24;
    public static final int NUMBER=25;
    public static final int PERCENT=26;
    public static final int PLUS=27;
    public static final int POWER=28;
    public static final int QUOT=29;
    public static final int RBRAC=30;
    public static final int RPAR=31;
    public static final int SEMI=32;
    public static final int STRING=33;
    public static final int WS=34;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public MacroParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public MacroParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return MacroParser.tokenNames; }
    public String getGrammarFileName() { return "csheets\\ext\\macro\\compiler\\lang.g"; }


    	protected void mismatch(IntStream input, int ttype, BitSet follow)
    		throws RecognitionException 
    	{
        	throw new MismatchedTokenException(ttype, input);
    	}

    	public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow)
    		throws RecognitionException 
    	{
    		throw e; 
    	}
    	
    	@Override
      	protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
        	throw new MismatchedTokenException(ttype, input);
     	}


    public static class expression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "expression"
    // csheets\\ext\\macro\\compiler\\lang.g:45:1: expression : EQ ! instruction EOF !;
    public final MacroParser.expression_return expression() throws RecognitionException {
        MacroParser.expression_return retval = new MacroParser.expression_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token EQ1=null;
        Token EOF3=null;
        MacroParser.instruction_return instruction2 =null;


        Object EQ1_tree=null;
        Object EOF3_tree=null;

        try {
            // csheets\\ext\\macro\\compiler\\lang.g:46:9: ( EQ ! instruction EOF !)
            // csheets\\ext\\macro\\compiler\\lang.g:46:11: EQ ! instruction EOF !
            {
            root_0 = (Object)adaptor.nil();


            EQ1=(Token)match(input,EQ,FOLLOW_EQ_in_expression87); 

            pushFollow(FOLLOW_instruction_in_expression90);
            instruction2=instruction();

            state._fsp--;

            adaptor.addChild(root_0, instruction2.getTree());

            EOF3=(Token)match(input,EOF,FOLLOW_EOF_in_expression92); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expression"


    public static class instruction_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "instruction"
    // csheets\\ext\\macro\\compiler\\lang.g:50:1: instruction : ( comparison | block | assign );
    public final MacroParser.instruction_return instruction() throws RecognitionException {
        MacroParser.instruction_return retval = new MacroParser.instruction_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        MacroParser.comparison_return comparison4 =null;

        MacroParser.block_return block5 =null;

        MacroParser.assign_return assign6 =null;



        try {
            // csheets\\ext\\macro\\compiler\\lang.g:51:2: ( comparison | block | assign )
            int alt1=3;
            switch ( input.LA(1) ) {
            case FUNCTION:
            case LPAR:
            case MINUS:
            case NUMBER:
            case STRING:
                {
                alt1=1;
                }
                break;
            case CELL_REF:
                {
                switch ( input.LA(2) ) {
                case AOP:
                    {
                    alt1=3;
                    }
                    break;
                case EOF:
                case AMP:
                case COLON:
                case DIV:
                case EQ:
                case GT:
                case GTEQ:
                case LT:
                case LTEQ:
                case MINUS:
                case MULTI:
                case NEQ:
                case PERCENT:
                case PLUS:
                case POWER:
                case RBRAC:
                case RPAR:
                case SEMI:
                    {
                    alt1=1;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 2, input);

                    throw nvae;

                }

                }
                break;
            case LBRAC:
                {
                alt1=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;

            }

            switch (alt1) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:51:4: comparison
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_comparison_in_instruction113);
                    comparison4=comparison();

                    state._fsp--;

                    adaptor.addChild(root_0, comparison4.getTree());

                    }
                    break;
                case 2 :
                    // csheets\\ext\\macro\\compiler\\lang.g:52:11: block
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_block_in_instruction125);
                    block5=block();

                    state._fsp--;

                    adaptor.addChild(root_0, block5.getTree());

                    }
                    break;
                case 3 :
                    // csheets\\ext\\macro\\compiler\\lang.g:53:11: assign
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_assign_in_instruction137);
                    assign6=assign();

                    state._fsp--;

                    adaptor.addChild(root_0, assign6.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "instruction"


    public static class assign_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "assign"
    // csheets\\ext\\macro\\compiler\\lang.g:56:1: assign : CELL_REF AOP ^ instruction ;
    public final MacroParser.assign_return assign() throws RecognitionException {
        MacroParser.assign_return retval = new MacroParser.assign_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token CELL_REF7=null;
        Token AOP8=null;
        MacroParser.instruction_return instruction9 =null;


        Object CELL_REF7_tree=null;
        Object AOP8_tree=null;

        try {
            // csheets\\ext\\macro\\compiler\\lang.g:57:9: ( CELL_REF AOP ^ instruction )
            // csheets\\ext\\macro\\compiler\\lang.g:57:11: CELL_REF AOP ^ instruction
            {
            root_0 = (Object)adaptor.nil();


            CELL_REF7=(Token)match(input,CELL_REF,FOLLOW_CELL_REF_in_assign162); 
            CELL_REF7_tree = 
            (Object)adaptor.create(CELL_REF7)
            ;
            adaptor.addChild(root_0, CELL_REF7_tree);


            AOP8=(Token)match(input,AOP,FOLLOW_AOP_in_assign164); 
            AOP8_tree = 
            (Object)adaptor.create(AOP8)
            ;
            root_0 = (Object)adaptor.becomeRoot(AOP8_tree, root_0);


            pushFollow(FOLLOW_instruction_in_assign167);
            instruction9=instruction();

            state._fsp--;

            adaptor.addChild(root_0, instruction9.getTree());

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "assign"


    public static class block_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "block"
    // csheets\\ext\\macro\\compiler\\lang.g:60:1: block : LBRAC ! instruction ( SEMI ^ instruction )* RBRAC !;
    public final MacroParser.block_return block() throws RecognitionException {
        MacroParser.block_return retval = new MacroParser.block_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token LBRAC10=null;
        Token SEMI12=null;
        Token RBRAC14=null;
        MacroParser.instruction_return instruction11 =null;

        MacroParser.instruction_return instruction13 =null;


        Object LBRAC10_tree=null;
        Object SEMI12_tree=null;
        Object RBRAC14_tree=null;

        try {
            // csheets\\ext\\macro\\compiler\\lang.g:61:9: ( LBRAC ! instruction ( SEMI ^ instruction )* RBRAC !)
            // csheets\\ext\\macro\\compiler\\lang.g:61:11: LBRAC ! instruction ( SEMI ^ instruction )* RBRAC !
            {
            root_0 = (Object)adaptor.nil();


            LBRAC10=(Token)match(input,LBRAC,FOLLOW_LBRAC_in_block193); 

            pushFollow(FOLLOW_instruction_in_block196);
            instruction11=instruction();

            state._fsp--;

            adaptor.addChild(root_0, instruction11.getTree());

            // csheets\\ext\\macro\\compiler\\lang.g:61:30: ( SEMI ^ instruction )*
            loop2:
            do {
                int alt2=2;
                switch ( input.LA(1) ) {
                case SEMI:
                    {
                    alt2=1;
                    }
                    break;

                }

                switch (alt2) {
            	case 1 :
            	    // csheets\\ext\\macro\\compiler\\lang.g:61:31: SEMI ^ instruction
            	    {
            	    SEMI12=(Token)match(input,SEMI,FOLLOW_SEMI_in_block199); 
            	    SEMI12_tree = 
            	    (Object)adaptor.create(SEMI12)
            	    ;
            	    root_0 = (Object)adaptor.becomeRoot(SEMI12_tree, root_0);


            	    pushFollow(FOLLOW_instruction_in_block202);
            	    instruction13=instruction();

            	    state._fsp--;

            	    adaptor.addChild(root_0, instruction13.getTree());

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            RBRAC14=(Token)match(input,RBRAC,FOLLOW_RBRAC_in_block206); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "block"


    public static class comparison_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "comparison"
    // csheets\\ext\\macro\\compiler\\lang.g:64:1: comparison : concatenation ( ( EQ ^| NEQ ^| GT ^| LT ^| LTEQ ^| GTEQ ^) concatenation )? ;
    public final MacroParser.comparison_return comparison() throws RecognitionException {
        MacroParser.comparison_return retval = new MacroParser.comparison_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token EQ16=null;
        Token NEQ17=null;
        Token GT18=null;
        Token LT19=null;
        Token LTEQ20=null;
        Token GTEQ21=null;
        MacroParser.concatenation_return concatenation15 =null;

        MacroParser.concatenation_return concatenation22 =null;


        Object EQ16_tree=null;
        Object NEQ17_tree=null;
        Object GT18_tree=null;
        Object LT19_tree=null;
        Object LTEQ20_tree=null;
        Object GTEQ21_tree=null;

        try {
            // csheets\\ext\\macro\\compiler\\lang.g:65:2: ( concatenation ( ( EQ ^| NEQ ^| GT ^| LT ^| LTEQ ^| GTEQ ^) concatenation )? )
            // csheets\\ext\\macro\\compiler\\lang.g:65:4: concatenation ( ( EQ ^| NEQ ^| GT ^| LT ^| LTEQ ^| GTEQ ^) concatenation )?
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_concatenation_in_comparison218);
            concatenation15=concatenation();

            state._fsp--;

            adaptor.addChild(root_0, concatenation15.getTree());

            // csheets\\ext\\macro\\compiler\\lang.g:66:17: ( ( EQ ^| NEQ ^| GT ^| LT ^| LTEQ ^| GTEQ ^) concatenation )?
            int alt4=2;
            switch ( input.LA(1) ) {
                case EQ:
                case GT:
                case GTEQ:
                case LT:
                case LTEQ:
                case NEQ:
                    {
                    alt4=1;
                    }
                    break;
            }

            switch (alt4) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:66:19: ( EQ ^| NEQ ^| GT ^| LT ^| LTEQ ^| GTEQ ^) concatenation
                    {
                    // csheets\\ext\\macro\\compiler\\lang.g:66:19: ( EQ ^| NEQ ^| GT ^| LT ^| LTEQ ^| GTEQ ^)
                    int alt3=6;
                    switch ( input.LA(1) ) {
                    case EQ:
                        {
                        alt3=1;
                        }
                        break;
                    case NEQ:
                        {
                        alt3=2;
                        }
                        break;
                    case GT:
                        {
                        alt3=3;
                        }
                        break;
                    case LT:
                        {
                        alt3=4;
                        }
                        break;
                    case LTEQ:
                        {
                        alt3=5;
                        }
                        break;
                    case GTEQ:
                        {
                        alt3=6;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 3, 0, input);

                        throw nvae;

                    }

                    switch (alt3) {
                        case 1 :
                            // csheets\\ext\\macro\\compiler\\lang.g:66:21: EQ ^
                            {
                            EQ16=(Token)match(input,EQ,FOLLOW_EQ_in_comparison240); 
                            EQ16_tree = 
                            (Object)adaptor.create(EQ16)
                            ;
                            root_0 = (Object)adaptor.becomeRoot(EQ16_tree, root_0);


                            }
                            break;
                        case 2 :
                            // csheets\\ext\\macro\\compiler\\lang.g:66:27: NEQ ^
                            {
                            NEQ17=(Token)match(input,NEQ,FOLLOW_NEQ_in_comparison245); 
                            NEQ17_tree = 
                            (Object)adaptor.create(NEQ17)
                            ;
                            root_0 = (Object)adaptor.becomeRoot(NEQ17_tree, root_0);


                            }
                            break;
                        case 3 :
                            // csheets\\ext\\macro\\compiler\\lang.g:66:34: GT ^
                            {
                            GT18=(Token)match(input,GT,FOLLOW_GT_in_comparison250); 
                            GT18_tree = 
                            (Object)adaptor.create(GT18)
                            ;
                            root_0 = (Object)adaptor.becomeRoot(GT18_tree, root_0);


                            }
                            break;
                        case 4 :
                            // csheets\\ext\\macro\\compiler\\lang.g:66:40: LT ^
                            {
                            LT19=(Token)match(input,LT,FOLLOW_LT_in_comparison255); 
                            LT19_tree = 
                            (Object)adaptor.create(LT19)
                            ;
                            root_0 = (Object)adaptor.becomeRoot(LT19_tree, root_0);


                            }
                            break;
                        case 5 :
                            // csheets\\ext\\macro\\compiler\\lang.g:66:46: LTEQ ^
                            {
                            LTEQ20=(Token)match(input,LTEQ,FOLLOW_LTEQ_in_comparison260); 
                            LTEQ20_tree = 
                            (Object)adaptor.create(LTEQ20)
                            ;
                            root_0 = (Object)adaptor.becomeRoot(LTEQ20_tree, root_0);


                            }
                            break;
                        case 6 :
                            // csheets\\ext\\macro\\compiler\\lang.g:66:54: GTEQ ^
                            {
                            GTEQ21=(Token)match(input,GTEQ,FOLLOW_GTEQ_in_comparison265); 
                            GTEQ21_tree = 
                            (Object)adaptor.create(GTEQ21)
                            ;
                            root_0 = (Object)adaptor.becomeRoot(GTEQ21_tree, root_0);


                            }
                            break;

                    }


                    pushFollow(FOLLOW_concatenation_in_comparison270);
                    concatenation22=concatenation();

                    state._fsp--;

                    adaptor.addChild(root_0, concatenation22.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "comparison"


    public static class concatenation_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "concatenation"
    // csheets\\ext\\macro\\compiler\\lang.g:69:1: concatenation : arithmetic_lowest ( AMP ^ arithmetic_lowest )* ;
    public final MacroParser.concatenation_return concatenation() throws RecognitionException {
        MacroParser.concatenation_return retval = new MacroParser.concatenation_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token AMP24=null;
        MacroParser.arithmetic_lowest_return arithmetic_lowest23 =null;

        MacroParser.arithmetic_lowest_return arithmetic_lowest25 =null;


        Object AMP24_tree=null;

        try {
            // csheets\\ext\\macro\\compiler\\lang.g:70:2: ( arithmetic_lowest ( AMP ^ arithmetic_lowest )* )
            // csheets\\ext\\macro\\compiler\\lang.g:70:4: arithmetic_lowest ( AMP ^ arithmetic_lowest )*
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_arithmetic_lowest_in_concatenation284);
            arithmetic_lowest23=arithmetic_lowest();

            state._fsp--;

            adaptor.addChild(root_0, arithmetic_lowest23.getTree());

            // csheets\\ext\\macro\\compiler\\lang.g:71:3: ( AMP ^ arithmetic_lowest )*
            loop5:
            do {
                int alt5=2;
                switch ( input.LA(1) ) {
                case AMP:
                    {
                    alt5=1;
                    }
                    break;

                }

                switch (alt5) {
            	case 1 :
            	    // csheets\\ext\\macro\\compiler\\lang.g:71:5: AMP ^ arithmetic_lowest
            	    {
            	    AMP24=(Token)match(input,AMP,FOLLOW_AMP_in_concatenation290); 
            	    AMP24_tree = 
            	    (Object)adaptor.create(AMP24)
            	    ;
            	    root_0 = (Object)adaptor.becomeRoot(AMP24_tree, root_0);


            	    pushFollow(FOLLOW_arithmetic_lowest_in_concatenation293);
            	    arithmetic_lowest25=arithmetic_lowest();

            	    state._fsp--;

            	    adaptor.addChild(root_0, arithmetic_lowest25.getTree());

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "concatenation"


    public static class arithmetic_lowest_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "arithmetic_lowest"
    // csheets\\ext\\macro\\compiler\\lang.g:74:1: arithmetic_lowest : arithmetic_low ( ( PLUS ^| MINUS ^) arithmetic_low )* ;
    public final MacroParser.arithmetic_lowest_return arithmetic_lowest() throws RecognitionException {
        MacroParser.arithmetic_lowest_return retval = new MacroParser.arithmetic_lowest_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token PLUS27=null;
        Token MINUS28=null;
        MacroParser.arithmetic_low_return arithmetic_low26 =null;

        MacroParser.arithmetic_low_return arithmetic_low29 =null;


        Object PLUS27_tree=null;
        Object MINUS28_tree=null;

        try {
            // csheets\\ext\\macro\\compiler\\lang.g:75:2: ( arithmetic_low ( ( PLUS ^| MINUS ^) arithmetic_low )* )
            // csheets\\ext\\macro\\compiler\\lang.g:75:4: arithmetic_low ( ( PLUS ^| MINUS ^) arithmetic_low )*
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_arithmetic_low_in_arithmetic_lowest307);
            arithmetic_low26=arithmetic_low();

            state._fsp--;

            adaptor.addChild(root_0, arithmetic_low26.getTree());

            // csheets\\ext\\macro\\compiler\\lang.g:76:3: ( ( PLUS ^| MINUS ^) arithmetic_low )*
            loop7:
            do {
                int alt7=2;
                switch ( input.LA(1) ) {
                case MINUS:
                case PLUS:
                    {
                    alt7=1;
                    }
                    break;

                }

                switch (alt7) {
            	case 1 :
            	    // csheets\\ext\\macro\\compiler\\lang.g:76:5: ( PLUS ^| MINUS ^) arithmetic_low
            	    {
            	    // csheets\\ext\\macro\\compiler\\lang.g:76:5: ( PLUS ^| MINUS ^)
            	    int alt6=2;
            	    switch ( input.LA(1) ) {
            	    case PLUS:
            	        {
            	        alt6=1;
            	        }
            	        break;
            	    case MINUS:
            	        {
            	        alt6=2;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 6, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt6) {
            	        case 1 :
            	            // csheets\\ext\\macro\\compiler\\lang.g:76:7: PLUS ^
            	            {
            	            PLUS27=(Token)match(input,PLUS,FOLLOW_PLUS_in_arithmetic_lowest315); 
            	            PLUS27_tree = 
            	            (Object)adaptor.create(PLUS27)
            	            ;
            	            root_0 = (Object)adaptor.becomeRoot(PLUS27_tree, root_0);


            	            }
            	            break;
            	        case 2 :
            	            // csheets\\ext\\macro\\compiler\\lang.g:76:15: MINUS ^
            	            {
            	            MINUS28=(Token)match(input,MINUS,FOLLOW_MINUS_in_arithmetic_lowest320); 
            	            MINUS28_tree = 
            	            (Object)adaptor.create(MINUS28)
            	            ;
            	            root_0 = (Object)adaptor.becomeRoot(MINUS28_tree, root_0);


            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_arithmetic_low_in_arithmetic_lowest325);
            	    arithmetic_low29=arithmetic_low();

            	    state._fsp--;

            	    adaptor.addChild(root_0, arithmetic_low29.getTree());

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "arithmetic_lowest"


    public static class arithmetic_low_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "arithmetic_low"
    // csheets\\ext\\macro\\compiler\\lang.g:79:1: arithmetic_low : arithmetic_medium ( ( MULTI ^| DIV ^) arithmetic_medium )* ;
    public final MacroParser.arithmetic_low_return arithmetic_low() throws RecognitionException {
        MacroParser.arithmetic_low_return retval = new MacroParser.arithmetic_low_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token MULTI31=null;
        Token DIV32=null;
        MacroParser.arithmetic_medium_return arithmetic_medium30 =null;

        MacroParser.arithmetic_medium_return arithmetic_medium33 =null;


        Object MULTI31_tree=null;
        Object DIV32_tree=null;

        try {
            // csheets\\ext\\macro\\compiler\\lang.g:80:2: ( arithmetic_medium ( ( MULTI ^| DIV ^) arithmetic_medium )* )
            // csheets\\ext\\macro\\compiler\\lang.g:80:4: arithmetic_medium ( ( MULTI ^| DIV ^) arithmetic_medium )*
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_arithmetic_medium_in_arithmetic_low339);
            arithmetic_medium30=arithmetic_medium();

            state._fsp--;

            adaptor.addChild(root_0, arithmetic_medium30.getTree());

            // csheets\\ext\\macro\\compiler\\lang.g:81:3: ( ( MULTI ^| DIV ^) arithmetic_medium )*
            loop9:
            do {
                int alt9=2;
                switch ( input.LA(1) ) {
                case DIV:
                case MULTI:
                    {
                    alt9=1;
                    }
                    break;

                }

                switch (alt9) {
            	case 1 :
            	    // csheets\\ext\\macro\\compiler\\lang.g:81:5: ( MULTI ^| DIV ^) arithmetic_medium
            	    {
            	    // csheets\\ext\\macro\\compiler\\lang.g:81:5: ( MULTI ^| DIV ^)
            	    int alt8=2;
            	    switch ( input.LA(1) ) {
            	    case MULTI:
            	        {
            	        alt8=1;
            	        }
            	        break;
            	    case DIV:
            	        {
            	        alt8=2;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 8, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt8) {
            	        case 1 :
            	            // csheets\\ext\\macro\\compiler\\lang.g:81:7: MULTI ^
            	            {
            	            MULTI31=(Token)match(input,MULTI,FOLLOW_MULTI_in_arithmetic_low347); 
            	            MULTI31_tree = 
            	            (Object)adaptor.create(MULTI31)
            	            ;
            	            root_0 = (Object)adaptor.becomeRoot(MULTI31_tree, root_0);


            	            }
            	            break;
            	        case 2 :
            	            // csheets\\ext\\macro\\compiler\\lang.g:81:16: DIV ^
            	            {
            	            DIV32=(Token)match(input,DIV,FOLLOW_DIV_in_arithmetic_low352); 
            	            DIV32_tree = 
            	            (Object)adaptor.create(DIV32)
            	            ;
            	            root_0 = (Object)adaptor.becomeRoot(DIV32_tree, root_0);


            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_arithmetic_medium_in_arithmetic_low357);
            	    arithmetic_medium33=arithmetic_medium();

            	    state._fsp--;

            	    adaptor.addChild(root_0, arithmetic_medium33.getTree());

            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "arithmetic_low"


    public static class arithmetic_medium_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "arithmetic_medium"
    // csheets\\ext\\macro\\compiler\\lang.g:84:1: arithmetic_medium : arithmetic_high ( POWER ^ arithmetic_high )? ;
    public final MacroParser.arithmetic_medium_return arithmetic_medium() throws RecognitionException {
        MacroParser.arithmetic_medium_return retval = new MacroParser.arithmetic_medium_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token POWER35=null;
        MacroParser.arithmetic_high_return arithmetic_high34 =null;

        MacroParser.arithmetic_high_return arithmetic_high36 =null;


        Object POWER35_tree=null;

        try {
            // csheets\\ext\\macro\\compiler\\lang.g:85:2: ( arithmetic_high ( POWER ^ arithmetic_high )? )
            // csheets\\ext\\macro\\compiler\\lang.g:85:4: arithmetic_high ( POWER ^ arithmetic_high )?
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_arithmetic_high_in_arithmetic_medium371);
            arithmetic_high34=arithmetic_high();

            state._fsp--;

            adaptor.addChild(root_0, arithmetic_high34.getTree());

            // csheets\\ext\\macro\\compiler\\lang.g:86:3: ( POWER ^ arithmetic_high )?
            int alt10=2;
            switch ( input.LA(1) ) {
                case POWER:
                    {
                    alt10=1;
                    }
                    break;
            }

            switch (alt10) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:86:5: POWER ^ arithmetic_high
                    {
                    POWER35=(Token)match(input,POWER,FOLLOW_POWER_in_arithmetic_medium377); 
                    POWER35_tree = 
                    (Object)adaptor.create(POWER35)
                    ;
                    root_0 = (Object)adaptor.becomeRoot(POWER35_tree, root_0);


                    pushFollow(FOLLOW_arithmetic_high_in_arithmetic_medium380);
                    arithmetic_high36=arithmetic_high();

                    state._fsp--;

                    adaptor.addChild(root_0, arithmetic_high36.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "arithmetic_medium"


    public static class arithmetic_high_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "arithmetic_high"
    // csheets\\ext\\macro\\compiler\\lang.g:89:1: arithmetic_high : arithmetic_highest ( PERCENT ^)? ;
    public final MacroParser.arithmetic_high_return arithmetic_high() throws RecognitionException {
        MacroParser.arithmetic_high_return retval = new MacroParser.arithmetic_high_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token PERCENT38=null;
        MacroParser.arithmetic_highest_return arithmetic_highest37 =null;


        Object PERCENT38_tree=null;

        try {
            // csheets\\ext\\macro\\compiler\\lang.g:90:2: ( arithmetic_highest ( PERCENT ^)? )
            // csheets\\ext\\macro\\compiler\\lang.g:90:4: arithmetic_highest ( PERCENT ^)?
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_arithmetic_highest_in_arithmetic_high394);
            arithmetic_highest37=arithmetic_highest();

            state._fsp--;

            adaptor.addChild(root_0, arithmetic_highest37.getTree());

            // csheets\\ext\\macro\\compiler\\lang.g:90:23: ( PERCENT ^)?
            int alt11=2;
            switch ( input.LA(1) ) {
                case PERCENT:
                    {
                    alt11=1;
                    }
                    break;
            }

            switch (alt11) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:90:25: PERCENT ^
                    {
                    PERCENT38=(Token)match(input,PERCENT,FOLLOW_PERCENT_in_arithmetic_high398); 
                    PERCENT38_tree = 
                    (Object)adaptor.create(PERCENT38)
                    ;
                    root_0 = (Object)adaptor.becomeRoot(PERCENT38_tree, root_0);


                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "arithmetic_high"


    public static class arithmetic_highest_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "arithmetic_highest"
    // csheets\\ext\\macro\\compiler\\lang.g:93:1: arithmetic_highest : ( MINUS ^)? atom ;
    public final MacroParser.arithmetic_highest_return arithmetic_highest() throws RecognitionException {
        MacroParser.arithmetic_highest_return retval = new MacroParser.arithmetic_highest_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token MINUS39=null;
        MacroParser.atom_return atom40 =null;


        Object MINUS39_tree=null;

        try {
            // csheets\\ext\\macro\\compiler\\lang.g:94:2: ( ( MINUS ^)? atom )
            // csheets\\ext\\macro\\compiler\\lang.g:94:4: ( MINUS ^)? atom
            {
            root_0 = (Object)adaptor.nil();


            // csheets\\ext\\macro\\compiler\\lang.g:94:4: ( MINUS ^)?
            int alt12=2;
            switch ( input.LA(1) ) {
                case MINUS:
                    {
                    alt12=1;
                    }
                    break;
            }

            switch (alt12) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:94:6: MINUS ^
                    {
                    MINUS39=(Token)match(input,MINUS,FOLLOW_MINUS_in_arithmetic_highest415); 
                    MINUS39_tree = 
                    (Object)adaptor.create(MINUS39)
                    ;
                    root_0 = (Object)adaptor.becomeRoot(MINUS39_tree, root_0);


                    }
                    break;

            }


            pushFollow(FOLLOW_atom_in_arithmetic_highest421);
            atom40=atom();

            state._fsp--;

            adaptor.addChild(root_0, atom40.getTree());

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "arithmetic_highest"


    public static class atom_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "atom"
    // csheets\\ext\\macro\\compiler\\lang.g:97:1: atom : ( function_call | reference | literal | LPAR ! comparison RPAR !);
    public final MacroParser.atom_return atom() throws RecognitionException {
        MacroParser.atom_return retval = new MacroParser.atom_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token LPAR44=null;
        Token RPAR46=null;
        MacroParser.function_call_return function_call41 =null;

        MacroParser.reference_return reference42 =null;

        MacroParser.literal_return literal43 =null;

        MacroParser.comparison_return comparison45 =null;


        Object LPAR44_tree=null;
        Object RPAR46_tree=null;

        try {
            // csheets\\ext\\macro\\compiler\\lang.g:98:2: ( function_call | reference | literal | LPAR ! comparison RPAR !)
            int alt13=4;
            switch ( input.LA(1) ) {
            case FUNCTION:
                {
                alt13=1;
                }
                break;
            case CELL_REF:
                {
                alt13=2;
                }
                break;
            case NUMBER:
            case STRING:
                {
                alt13=3;
                }
                break;
            case LPAR:
                {
                alt13=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;

            }

            switch (alt13) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:98:4: function_call
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_function_call_in_atom432);
                    function_call41=function_call();

                    state._fsp--;

                    adaptor.addChild(root_0, function_call41.getTree());

                    }
                    break;
                case 2 :
                    // csheets\\ext\\macro\\compiler\\lang.g:99:17: reference
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_reference_in_atom450);
                    reference42=reference();

                    state._fsp--;

                    adaptor.addChild(root_0, reference42.getTree());

                    }
                    break;
                case 3 :
                    // csheets\\ext\\macro\\compiler\\lang.g:100:4: literal
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_literal_in_atom455);
                    literal43=literal();

                    state._fsp--;

                    adaptor.addChild(root_0, literal43.getTree());

                    }
                    break;
                case 4 :
                    // csheets\\ext\\macro\\compiler\\lang.g:101:4: LPAR ! comparison RPAR !
                    {
                    root_0 = (Object)adaptor.nil();


                    LPAR44=(Token)match(input,LPAR,FOLLOW_LPAR_in_atom460); 

                    pushFollow(FOLLOW_comparison_in_atom463);
                    comparison45=comparison();

                    state._fsp--;

                    adaptor.addChild(root_0, comparison45.getTree());

                    RPAR46=(Token)match(input,RPAR,FOLLOW_RPAR_in_atom465); 

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "atom"


    public static class function_call_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "function_call"
    // csheets\\ext\\macro\\compiler\\lang.g:105:1: function_call : FUNCTION ^ ( LPAR !| LBRAC !) ( instruction ( SEMI ! instruction )* )? ( RPAR !| RBRAC !) ;
    public final MacroParser.function_call_return function_call() throws RecognitionException {
        MacroParser.function_call_return retval = new MacroParser.function_call_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token FUNCTION47=null;
        Token LPAR48=null;
        Token LBRAC49=null;
        Token SEMI51=null;
        Token RPAR53=null;
        Token RBRAC54=null;
        MacroParser.instruction_return instruction50 =null;

        MacroParser.instruction_return instruction52 =null;


        Object FUNCTION47_tree=null;
        Object LPAR48_tree=null;
        Object LBRAC49_tree=null;
        Object SEMI51_tree=null;
        Object RPAR53_tree=null;
        Object RBRAC54_tree=null;

        try {
            // csheets\\ext\\macro\\compiler\\lang.g:106:2: ( FUNCTION ^ ( LPAR !| LBRAC !) ( instruction ( SEMI ! instruction )* )? ( RPAR !| RBRAC !) )
            // csheets\\ext\\macro\\compiler\\lang.g:106:4: FUNCTION ^ ( LPAR !| LBRAC !) ( instruction ( SEMI ! instruction )* )? ( RPAR !| RBRAC !)
            {
            root_0 = (Object)adaptor.nil();


            FUNCTION47=(Token)match(input,FUNCTION,FOLLOW_FUNCTION_in_function_call479); 
            FUNCTION47_tree = 
            (Object)adaptor.create(FUNCTION47)
            ;
            root_0 = (Object)adaptor.becomeRoot(FUNCTION47_tree, root_0);


            // csheets\\ext\\macro\\compiler\\lang.g:106:14: ( LPAR !| LBRAC !)
            int alt14=2;
            switch ( input.LA(1) ) {
            case LPAR:
                {
                alt14=1;
                }
                break;
            case LBRAC:
                {
                alt14=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;

            }

            switch (alt14) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:106:16: LPAR !
                    {
                    LPAR48=(Token)match(input,LPAR,FOLLOW_LPAR_in_function_call484); 

                    }
                    break;
                case 2 :
                    // csheets\\ext\\macro\\compiler\\lang.g:106:24: LBRAC !
                    {
                    LBRAC49=(Token)match(input,LBRAC,FOLLOW_LBRAC_in_function_call489); 

                    }
                    break;

            }


            // csheets\\ext\\macro\\compiler\\lang.g:107:3: ( instruction ( SEMI ! instruction )* )?
            int alt16=2;
            switch ( input.LA(1) ) {
                case CELL_REF:
                case FUNCTION:
                case LBRAC:
                case LPAR:
                case MINUS:
                case NUMBER:
                case STRING:
                    {
                    alt16=1;
                    }
                    break;
            }

            switch (alt16) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:107:5: instruction ( SEMI ! instruction )*
                    {
                    pushFollow(FOLLOW_instruction_in_function_call499);
                    instruction50=instruction();

                    state._fsp--;

                    adaptor.addChild(root_0, instruction50.getTree());

                    // csheets\\ext\\macro\\compiler\\lang.g:107:17: ( SEMI ! instruction )*
                    loop15:
                    do {
                        int alt15=2;
                        switch ( input.LA(1) ) {
                        case SEMI:
                            {
                            alt15=1;
                            }
                            break;

                        }

                        switch (alt15) {
                    	case 1 :
                    	    // csheets\\ext\\macro\\compiler\\lang.g:107:19: SEMI ! instruction
                    	    {
                    	    SEMI51=(Token)match(input,SEMI,FOLLOW_SEMI_in_function_call503); 

                    	    pushFollow(FOLLOW_instruction_in_function_call506);
                    	    instruction52=instruction();

                    	    state._fsp--;

                    	    adaptor.addChild(root_0, instruction52.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop15;
                        }
                    } while (true);


                    }
                    break;

            }


            // csheets\\ext\\macro\\compiler\\lang.g:108:3: ( RPAR !| RBRAC !)
            int alt17=2;
            switch ( input.LA(1) ) {
            case RPAR:
                {
                alt17=1;
                }
                break;
            case RBRAC:
                {
                alt17=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;

            }

            switch (alt17) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:108:5: RPAR !
                    {
                    RPAR53=(Token)match(input,RPAR,FOLLOW_RPAR_in_function_call518); 

                    }
                    break;
                case 2 :
                    // csheets\\ext\\macro\\compiler\\lang.g:108:13: RBRAC !
                    {
                    RBRAC54=(Token)match(input,RBRAC,FOLLOW_RBRAC_in_function_call523); 

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "function_call"


    public static class reference_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "reference"
    // csheets\\ext\\macro\\compiler\\lang.g:111:1: reference : CELL_REF ( ( COLON ^) CELL_REF )? ;
    public final MacroParser.reference_return reference() throws RecognitionException {
        MacroParser.reference_return retval = new MacroParser.reference_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token CELL_REF55=null;
        Token COLON56=null;
        Token CELL_REF57=null;

        Object CELL_REF55_tree=null;
        Object COLON56_tree=null;
        Object CELL_REF57_tree=null;

        try {
            // csheets\\ext\\macro\\compiler\\lang.g:112:2: ( CELL_REF ( ( COLON ^) CELL_REF )? )
            // csheets\\ext\\macro\\compiler\\lang.g:112:4: CELL_REF ( ( COLON ^) CELL_REF )?
            {
            root_0 = (Object)adaptor.nil();


            CELL_REF55=(Token)match(input,CELL_REF,FOLLOW_CELL_REF_in_reference537); 
            CELL_REF55_tree = 
            (Object)adaptor.create(CELL_REF55)
            ;
            adaptor.addChild(root_0, CELL_REF55_tree);


            // csheets\\ext\\macro\\compiler\\lang.g:113:3: ( ( COLON ^) CELL_REF )?
            int alt18=2;
            switch ( input.LA(1) ) {
                case COLON:
                    {
                    alt18=1;
                    }
                    break;
            }

            switch (alt18) {
                case 1 :
                    // csheets\\ext\\macro\\compiler\\lang.g:113:5: ( COLON ^) CELL_REF
                    {
                    // csheets\\ext\\macro\\compiler\\lang.g:113:5: ( COLON ^)
                    // csheets\\ext\\macro\\compiler\\lang.g:113:7: COLON ^
                    {
                    COLON56=(Token)match(input,COLON,FOLLOW_COLON_in_reference545); 
                    COLON56_tree = 
                    (Object)adaptor.create(COLON56)
                    ;
                    root_0 = (Object)adaptor.becomeRoot(COLON56_tree, root_0);


                    }


                    CELL_REF57=(Token)match(input,CELL_REF,FOLLOW_CELL_REF_in_reference550); 
                    CELL_REF57_tree = 
                    (Object)adaptor.create(CELL_REF57)
                    ;
                    adaptor.addChild(root_0, CELL_REF57_tree);


                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "reference"


    public static class literal_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "literal"
    // csheets\\ext\\macro\\compiler\\lang.g:116:1: literal : ( NUMBER | STRING );
    public final MacroParser.literal_return literal() throws RecognitionException {
        MacroParser.literal_return retval = new MacroParser.literal_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token set58=null;

        Object set58_tree=null;

        try {
            // csheets\\ext\\macro\\compiler\\lang.g:117:2: ( NUMBER | STRING )
            // csheets\\ext\\macro\\compiler\\lang.g:
            {
            root_0 = (Object)adaptor.nil();


            set58=(Token)input.LT(1);

            if ( input.LA(1)==NUMBER||input.LA(1)==STRING ) {
                input.consume();
                adaptor.addChild(root_0, 
                (Object)adaptor.create(set58)
                );
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        		reportError(e);
        		throw e; 
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "literal"

    // Delegated rules


 

    public static final BitSet FOLLOW_EQ_in_expression87 = new BitSet(new long[]{0x00000002024A4080L});
    public static final BitSet FOLLOW_instruction_in_expression90 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_expression92 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_comparison_in_instruction113 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_block_in_instruction125 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_assign_in_instruction137 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CELL_REF_in_assign162 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_AOP_in_assign164 = new BitSet(new long[]{0x00000002024A4080L});
    public static final BitSet FOLLOW_instruction_in_assign167 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LBRAC_in_block193 = new BitSet(new long[]{0x00000002024A4080L});
    public static final BitSet FOLLOW_instruction_in_block196 = new BitSet(new long[]{0x0000000140000000L});
    public static final BitSet FOLLOW_SEMI_in_block199 = new BitSet(new long[]{0x00000002024A4080L});
    public static final BitSet FOLLOW_instruction_in_block202 = new BitSet(new long[]{0x0000000140000000L});
    public static final BitSet FOLLOW_RBRAC_in_block206 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_concatenation_in_comparison218 = new BitSet(new long[]{0x0000000001319002L});
    public static final BitSet FOLLOW_EQ_in_comparison240 = new BitSet(new long[]{0x0000000202484080L});
    public static final BitSet FOLLOW_NEQ_in_comparison245 = new BitSet(new long[]{0x0000000202484080L});
    public static final BitSet FOLLOW_GT_in_comparison250 = new BitSet(new long[]{0x0000000202484080L});
    public static final BitSet FOLLOW_LT_in_comparison255 = new BitSet(new long[]{0x0000000202484080L});
    public static final BitSet FOLLOW_LTEQ_in_comparison260 = new BitSet(new long[]{0x0000000202484080L});
    public static final BitSet FOLLOW_GTEQ_in_comparison265 = new BitSet(new long[]{0x0000000202484080L});
    public static final BitSet FOLLOW_concatenation_in_comparison270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_arithmetic_lowest_in_concatenation284 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_AMP_in_concatenation290 = new BitSet(new long[]{0x0000000202484080L});
    public static final BitSet FOLLOW_arithmetic_lowest_in_concatenation293 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_arithmetic_low_in_arithmetic_lowest307 = new BitSet(new long[]{0x0000000008400002L});
    public static final BitSet FOLLOW_PLUS_in_arithmetic_lowest315 = new BitSet(new long[]{0x0000000202484080L});
    public static final BitSet FOLLOW_MINUS_in_arithmetic_lowest320 = new BitSet(new long[]{0x0000000202484080L});
    public static final BitSet FOLLOW_arithmetic_low_in_arithmetic_lowest325 = new BitSet(new long[]{0x0000000008400002L});
    public static final BitSet FOLLOW_arithmetic_medium_in_arithmetic_low339 = new BitSet(new long[]{0x0000000000800802L});
    public static final BitSet FOLLOW_MULTI_in_arithmetic_low347 = new BitSet(new long[]{0x0000000202484080L});
    public static final BitSet FOLLOW_DIV_in_arithmetic_low352 = new BitSet(new long[]{0x0000000202484080L});
    public static final BitSet FOLLOW_arithmetic_medium_in_arithmetic_low357 = new BitSet(new long[]{0x0000000000800802L});
    public static final BitSet FOLLOW_arithmetic_high_in_arithmetic_medium371 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_POWER_in_arithmetic_medium377 = new BitSet(new long[]{0x0000000202484080L});
    public static final BitSet FOLLOW_arithmetic_high_in_arithmetic_medium380 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_arithmetic_highest_in_arithmetic_high394 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_PERCENT_in_arithmetic_high398 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MINUS_in_arithmetic_highest415 = new BitSet(new long[]{0x0000000202084080L});
    public static final BitSet FOLLOW_atom_in_arithmetic_highest421 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_function_call_in_atom432 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_reference_in_atom450 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_literal_in_atom455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LPAR_in_atom460 = new BitSet(new long[]{0x0000000202484080L});
    public static final BitSet FOLLOW_comparison_in_atom463 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_RPAR_in_atom465 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FUNCTION_in_function_call479 = new BitSet(new long[]{0x00000000000A0000L});
    public static final BitSet FOLLOW_LPAR_in_function_call484 = new BitSet(new long[]{0x00000002C24A4080L});
    public static final BitSet FOLLOW_LBRAC_in_function_call489 = new BitSet(new long[]{0x00000002C24A4080L});
    public static final BitSet FOLLOW_instruction_in_function_call499 = new BitSet(new long[]{0x00000001C0000000L});
    public static final BitSet FOLLOW_SEMI_in_function_call503 = new BitSet(new long[]{0x00000002024A4080L});
    public static final BitSet FOLLOW_instruction_in_function_call506 = new BitSet(new long[]{0x00000001C0000000L});
    public static final BitSet FOLLOW_RPAR_in_function_call518 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RBRAC_in_function_call523 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CELL_REF_in_reference537 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_COLON_in_reference545 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_CELL_REF_in_reference550 = new BitSet(new long[]{0x0000000000000002L});

}