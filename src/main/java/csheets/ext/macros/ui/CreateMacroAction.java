/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.ui;

import csheets.ui.Frame;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author Fred
 */
public class CreateMacroAction extends BaseAction {

    protected UIController uiController;

    /**
     * Default constructor
     * @param controller  UIController
     */
    public CreateMacroAction(UIController controller) {
        uiController = controller;
    }

    /**
     * returns ui name
     * @return String
     */
    @Override
    protected String getName() {
        return "Create Macro";
    }

    /**
     * Action performed
     * @param ae actionEvent
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        new CreateMacroUIDialog((Frame)null,true,uiController).setVisible(true);
    }

}
