/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.ui;

import csheets.ext.macros.beanshell.ui.CreateBeanShellAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author Fred
 */
public class MacroMenu extends JMenu {

    /**
     * Default constructor
     *
     * @param uiController UIController
     */
    public MacroMenu(UIController uiController) {
        super("Macros");
        setMnemonic(KeyEvent.VK_M);
        // Adds font actions
        add(new CreateMacroAction(uiController));
        add(new CreateBeanShellAction(uiController));
    }
}
