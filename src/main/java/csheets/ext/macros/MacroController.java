/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros;

import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.formula.compiler.FormulaCompilationException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fred
 */
public class MacroController {

    String macroName;
    List<String> instructionList;

    /**
     * Constructor
     */
    public MacroController() {
        instructionList = new ArrayList<>();
    }

    /**
     * Sets the name for the macro
     *
     * @param str String
     */
    public void setMacroName(String str) {
        macroName = str;
    }

    /**
     * Adds an instruction to the macro
     *
     * @param str String
     */
    public void addInstruction(String str) {
        instructionList.add(str);
    }

    /**
     * Creates and executes the macro
     *
     * @param cell Cell
     * @return String
     * @throws FormulaCompilationException Exception
     * @throws IllegalValueTypeException Exception
     */
    public String executeMacro(Cell cell) throws FormulaCompilationException, IllegalValueTypeException {
        Macro macro = new Macro(macroName, instructionList, cell.getSpreadsheet().getWorkbook());
        if (macro.isValid()) {
            return macro.execute(cell);
        } else {
            throw new IllegalStateException("Please specify the name and at least one instruction.");
        }
    }
}
