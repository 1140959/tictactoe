/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros;

import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.Workbook;
import csheets.core.formula.Expression;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.macros.compiler.MacroExpressionCompiler;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fred
 */
public class Macro {

    private final String name;
    private final List<String> instructions;
    private final Workbook workbook;

    /**
     * Constructor for Macro
     *
     * @param name Macro's name
     * @param instructions List String
     * @param book Workbook
     */
    public Macro(String name, List instructions, Workbook book) {
        this.instructions = new ArrayList<>(instructions);
        this.name = name;
        workbook = book;
    }

    /**
     * Validates the Macro
     *
     * @return true if macro is valid
     */
    public boolean isValid() {
        if ((!name.isEmpty()) && (!instructions.isEmpty())) {
            //TODO: validate if instructions in list are valid
            return true;
        }
        return false;
    }

    /**
     * Execute the Macro.
     *
     * @param cell Cell
     * @return String
     * @throws csheets.core.formula.compiler.FormulaCompilationException Exception
     * @throws csheets.core.IllegalValueTypeException Exception
     */
    public String execute(Cell cell) throws FormulaCompilationException, IllegalValueTypeException {
        MacroExpressionCompiler compiler = new MacroExpressionCompiler();
        String value = null;
        for (int i = 0; i < instructions.size(); i++) {
            //both this responsabilities should be passed to the grammer.
            String instruction = instructions.get(i);
            if (instruction.charAt(0) != ';' && instruction.charAt(0) != '\n') {
                String str = new String(instruction);
                if (str.charAt(0) != '=') {
                    str = "=" + str;
                }
                Expression exp = compiler.compile(cell, str);
                //Expression exp = compiler.compile(cell, instruction);
                Value val = exp.evaluate();
                if (i == instructions.size() - 1) {
                    //cell.setContent(valor.toString());
                    value = val.toString();
                }
            }
        }
        return value;
    }
}
