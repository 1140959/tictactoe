/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.lang;

import csheets.core.formula.lang.*;
import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;

/**
 * 
 * 
 * @author José Lopes (1080696)
 * 
 */
public class Block implements BinaryOperator{
    

    Block() {};
    
    @Override
    public Value applyTo(Expression leftOperand, Expression rightOperand) throws IllegalValueTypeException {
        System.out.println("Block leftOperand: " + leftOperand.toString());
        System.out.println("Block rightOperand: " + rightOperand.toString());
        
        leftOperand.evaluate();
        return rightOperand.evaluate();
    }

    @Override
    public String getIdentifier() {
        return ";";
    }

    @Override
    public Value.Type getOperandValueType() {
        return Value.Type.UNDEFINED;
    }
    
}
