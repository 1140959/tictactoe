package csheets.ext.macros.lang;

import csheets.core.formula.lang.*;
import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;
import csheets.core.formula.compiler.FormulaCompilationException;

/**
 *
 * @author 1090698@isep.ipp.pt IN PROGRESS...NEEDS TO FIX CYCLE BUG
 */
public class Assign implements BinaryOperator {

    int i;

    /**
     * Creates a new Assign.
     */
    public Assign() {
    }

    /**
     * @param leftOperand is the element to the left of :=
     * @param rightOperand is the element to the right of :=
     */
    @Override
    public Value applyTo(Expression leftOperand, Expression rightOperand) throws IllegalValueTypeException {
        System.out.println("Assign leftOperand: " + leftOperand.toString());
        System.out.println("Assign rightOperand: " + rightOperand.toString());
        
        Value val;

        // validate if leftOperand is a CellReference and allows only one single run
        if (leftOperand instanceof CellReference && i < 1) {

            CellReference cellRefer = (CellReference) leftOperand;

            Cell cell = cellRefer.getCell();

            try {
                i++;
                // in var val, is placed the result of the rightOperand operator
                val = rightOperand.evaluate();
                String valStr = String.valueOf(val);
                cell.setContent(valStr);
            } catch (FormulaCompilationException e) {
                return new Value(e);
            }
            return val;
        }

        return new Value(new IllegalArgumentException());
    }

    /**
     * get the String identifier
     *
     * @return String Identifier
     */
    @Override
    public String getIdentifier() {
        //couter added to prevent cicles
        i = 0;
        return ":=";
    }

    /**
     * get the operand Value Type
     *
     * @return Value.Type
     */
    @Override
    public Value.Type getOperandValueType() {
        return Value.Type.UNDEFINED;
    }

}
