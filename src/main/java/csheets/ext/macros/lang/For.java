/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.lang;

import csheets.core.formula.lang.*;
import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;

/**
 *
 * @author José Pedro Lopes (1080696) IN PROGRESS... NEEDS FUNCTIONAL TESTING
 */
public class For implements Function {
    
    public For() { 
    }

    public static final FunctionParameter[] parameters = new FunctionParameter[]{
        new FunctionParameter(Value.Type.NUMERIC, "Initialization", false,
        "Counter initialization"),
        new FunctionParameter(Value.Type.BOOLEAN, "End of loop", false,
        "The condition where the loop will stop"),
        new FunctionParameter(Value.Type.UNDEFINED, "Instructions", true,
        "Block of instructions inside the loop")
    };

    @Override
    public String getIdentifier() {
        return "FOR";
    }

    @Override
    public Value applyTo(Expression[] args) throws IllegalValueTypeException {
        // Added to check arguments - test only
        int j = 0;
        for (Expression e : args) {
            System.out.println("args[" + j + "] " + e.toString() + "\n");
            j++;
        }

        args[0].evaluate();
        //args[1].evaluate(); done in the while cycle
        while (args[1].evaluate().toBoolean()) {
//            args[2].evaluate();
//            args[3].evaluate();
            for (int argPos = 2; argPos < args.length; argPos++) {
                (args[argPos]).evaluate();
            }
        }

        return new Value();
    }

    @Override
    public FunctionParameter[] getParameters() {
        return parameters;
    }

    @Override
    public boolean isVarArg() {
        return true;
    }

}
