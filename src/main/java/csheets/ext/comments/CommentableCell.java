package csheets.ext.comments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import csheets.core.Cell;
import csheets.ext.CellExtension;


/**
 * An extension of a cell in a spreadsheet, with support for comments.
 * @author Alexandre Braganca
 * @author Einar Pehrson
 */
public class CommentableCell extends CellExtension {

	/** The unique version identifier used for serialization */
	private static final long serialVersionUID = 1L;

	/** The cell's user-specified comment */
    //private String userComment;
    
    /** The list of comments that could be stored into a cell*/
    private List<Comment> lstOfComments = new ArrayList<>();

	/** The listeners registered to receive events from the comentable cell */
	private transient List<CommentableCellListener> listeners
		= new ArrayList<>();

	/**
	 * Creates a commentable cell extension for the given cell.
	 * @param cell the cell to extend
	 */
	CommentableCell(Cell cell) {
		super(cell, CommentsExtension.NAME);
	}


/*
 * DATA UPDATES
 */


//	public void contentChanged(Cell cell) {
//	}


/*
 * COMMENT ACCESSORS
 */

	/**
	 * Get the cell's user comments.
	 * @return The user supplied comment for the cell or <code>null</code> if no user
	 supplied comment exists.
	*/
	public String getUserComment() {
		String str = new String();
        if(lstOfComments == null){
            return null;
        }  
        for(Comment c : lstOfComments){
            str += c.toString();
            str+="\n";
        }
        return str;
	}

	/**
	 * Returns whether the cell has one or more comments.
	 * @return true if the cell has a comment
	 */
	public boolean hasComment() {
        //true if it has at least one comment
		return (lstOfComments!=null &&!lstOfComments.isEmpty());
	}

/*
 * COMMENT MODIFIERS
 */

	/**
	 * Sets the list of user-specified comments for the cell.
	 * @param comment the user-specified comment
     * @throws java.lang.Exception
	 */
	public void setUserComment(Comment comment) throws Exception {
		if (comment == null){
            throw new Exception("comment can not be null");
        }
        this.lstOfComments.add(comment);
		// Notifies listeners
		fireCommentsChanged();
	}


/*
 * EVENT LISTENING SUPPORT
 */

	/**
	 * Registers the given listener on the cell.
	 * @param listener the listener to be added
	 */
	public void addCommentableCellListener(CommentableCellListener listener) {
		listeners.add(listener);
	}

	/**
	 * Removes the given listener from the cell.
	 * @param listener the listener to be removed
	 */
	public void removeCommentableCellListener(CommentableCellListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Notifies all registered listeners that the cell's comments changed.
	 */
	protected void fireCommentsChanged() {
		for (CommentableCellListener listener : listeners)
			listener.commentChanged(this);
	}

	/**
	 * Customizes serialization, by recreating the listener list.
	 * @param stream the object input stream from which the object is to be read
	 * @throws IOException If any of the usual Input/Output related exceptions occur
	 * @throws ClassNotFoundException If the class of a serialized object cannot be found.
	 */
	private void readObject(java.io.ObjectInputStream stream)
			throws java.io.IOException, ClassNotFoundException {
	    stream.defaultReadObject();
		listeners = new ArrayList<>();
	}
    
     /**
     * Method for returning a formated string to pass by argument in the setToolTip method.
     * @return string formated in html
     */
    public String toStringForToolTip() {
        String str = "<html>";
        if (lstOfComments==null || lstOfComments.isEmpty()) {
            return null;
        }
        for (Comment comment : lstOfComments) {
            str += comment.toStringForToolTip();
        }
        str += "</html>";
        return str;
    }
}
