/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.comments;

import java.io.Serializable;

/**
 * Class that will be the comment with the user who created it.
 * @author 1000841@isep.ipp.pt <Egidio Caldeira>.
 */
public class Comment implements Serializable{
    
    /**
     * Name of the user who created a comment
     */
    private String strUserName;
    
    /**
     * String to store the comment that has been written
     */
    private String strComment;

    /**
     * constructor to create a new comment with the user and comment.
     * @param strUserName
     * @param strComment 
     */
    public Comment(String strUserName, String strComment) {
        this.strUserName = strUserName;
        this.strComment = strComment;
    }

    /**
     * method to return the user who created the comment and the comment
     * @return 
     */
    @Override
    public String toString() {
        return strUserName + ":\n" + strComment + "\n";
    }
    
    public String toStringForToolTip(){
        return "<p><b>" + strUserName + "</b></p>" + "<p>" + strComment +"</p>"; 
    }    
    
}
