/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class responsible for inform the thread about the listening process for connection.
 * This class implements the Runnable interface.
 * @author dmonteiro
 */
public class Listener implements Runnable{

    private DatagramSocket listenSock;
    private NetworkData net;
    private DatagramPacket dataSendp;

    private String receivedData;
    private byte[] dataSend = new byte[300];
    private byte[] dataReceived = new byte[300];
    private static final int PAUSE = 5;
    
    /**
     * Listener broadcast request constructor
     * @param n NetworkData
     */
    public Listener(NetworkData n){
        this.net = n;
    }
    
    @Override
    public void run(){
        
        InetAddress ipToSend;
        int portToSend;
    
        try{
            listenSock = new DatagramSocket(9999); //broadcast port
        } 
        catch(SocketException ex){
            Logger.getLogger(NetworkService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        while(true){
            
            try{
            DatagramPacket dataReceivep = new DatagramPacket(dataReceived,dataReceived.length);
            listenSock.setSoTimeout(1000 * PAUSE);
            listenSock.receive(dataReceivep);
            
            ipToSend = dataReceivep.getAddress();
            portToSend = dataReceivep.getPort();
            
            receivedData = "-" + this.net.name() + "-"  + this.net.port() + "-";
            
            dataSend = new byte[receivedData.getBytes().length];
            dataSend = receivedData.getBytes();
            dataSendp = new DatagramPacket(dataSend,dataSend.length,ipToSend,portToSend);
            
            listenSock.send(dataSendp);
            
            } catch (SocketTimeoutException ex) {
                //timeout ignore
            } catch (IOException ex) {
                Logger.getLogger(NetworkService.class.getName()).
                        log(Level.SEVERE, null, ex);
                break;
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(NetworkService.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
        }
        listenSock.close();
    }
    
    
}
