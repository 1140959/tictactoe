/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.ext.startsharing.automaticupdate.AutomaticUpdateController;
import csheets.ext.startsharing.ui.SharingPanel;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Rui-Think
 */
public class StartSharingController extends FocusOwnerAction {

    private UIController uiController;
    private AutomaticUpdateController autoUpdate;
    private SharingPanel sidebarPanel;
    private NetworkService nwServ;
    private List<NetworkInstance> list_ni;

    /**
     *Constructor of the class
     * @param uiController uiController
     * @param sharingPanel sharePanel
     */
    public StartSharingController(UIController uiController, SharingPanel sharingPanel) {
        this.uiController = uiController;
        this.sidebarPanel = sharingPanel;
        nwServ = NetworkService.getInstance();
    }
    
    /**
     *Method to disconnect the active connection
     */
    public void disconnect() {
        NetworkInstance n = (NetworkInstance) sidebarPanel.getConnectModel().get(sidebarPanel.getConnectList().getSelectedIndex());
        nwServ.disconectFrom(n);
    }

    /**
     *Method to send the select cells of the workbook
     */
    public void sendData() {
        Cell[][] currentCells = focusOwner.getSelectedCells();
        CellsPacket cellsToSend = new CellsPacket(new HashMap<Address, String>());

        for (int i = 0; i < currentCells.length; i++) {
            for (int j = 0; j < currentCells[0].length; j++) {
                Cell currentActiveCell = currentCells[i][j];
                cellsToSend.addCell(currentActiveCell.getAddress(), currentActiveCell.getContent());
            }
        }
        List<NetworkInstance> sendTo = new ArrayList<NetworkInstance>();
        try {
            
        sendTo.add((NetworkInstance) this.sidebarPanel.getConnectModel().get(this.sidebarPanel.getConnectList().getSelectedIndex()));

        nwServ.send(cellsToSend, sendTo);
        
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Please, select an active connection to share this cells!");
        }
        
    }
    
    /**
     *Method to start the listen of the sharePanel
     * @param sp SharePanel
     */
    public void startListen(SharingPanel sp) {
        this.sidebarPanel = sp;
        int port = Integer.valueOf(sidebarPanel.getListenPort());
        String name = sidebarPanel.getListenName();
        InetAddress ip = null;
        try{
            ip = InetAddress.getLocalHost();
        }catch(UnknownHostException ex){
            Logger.getLogger(StartSharingController.class.getName()).log(Level.SEVERE, null, ex);
        }

        NetworkData nd = new NetworkData(ip,name,port);
        nwServ.defineNetwork(nd);
        nwServ = NetworkService.getInstance();
        JOptionPane.showMessageDialog(null, "Share was activated\n You will be found on network!!");
        nwServ.addObserver(sidebarPanel);
        receivedDataObservers();
        nwServ.listeningBroadcast();
        nwServ.acceptLink();        
        nwServ.broadcastInstances();
    }

    @Override
    protected String getName() {
        return "";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }
    
    /**
     *Method to return the instances ins the list
     * @return list of instances
     */
    public List<NetworkInstance> getInstances(){
        return list_ni; 
    }
    
    /**
     * Method to establish the connection
     */
    public void connect(){
        NetworkInstance connectTo = (NetworkInstance) sidebarPanel.getInstancesModel().get(sidebarPanel.getInstancesList().getSelectedIndex());
        nwServ.connectToInstance(connectTo);
        this.autoUpdate = new AutomaticUpdateController(uiController,sidebarPanel);
        
    }
    
    /**
     *Method to receive the observer
     */
    public void receivedDataObservers() {
        
            try {
            String line;
            String line2;
            line = "csheets.ext.startsharing.CellsPlacer";
            nwServ.addReceivedDataObserver((CellsReceiverObserver) Class.forName(line).newInstance());
            line2 = "csheets.ext.startsharing.automaticupdate.AutomaticUpdateCellsPlacer";
            nwServ.addReceivedDataObserver(((CellsReceiverObserver) Class.forName(line2).newInstance()));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    


}