/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing;

import csheets.core.Address;
import java.io.Serializable;
import java.util.Map;

/**
 * Class containing the collection of cells to share
 * @author Rui-Think
 */
public class CellsPacket implements Serializable {
    
    private static final long serialVersionUID = 7010464744129096272L;

    private Map<Address,String> cellsToShare;
    /*
    * Cells collection constructor
    */
    public CellsPacket(Map<Address, String> cellsToShare) {
        this.cellsToShare = cellsToShare;
    }

    /*
    * Return the Map containing the cells collection
    */
    public Map<Address, String> getCellsToShare() {
        return cellsToShare;
    }
    
    /*
    * Add a ceel to the collection
    */
    public void addCell(Address cellAdress,String cellContent){
        cellsToShare.put(cellAdress, cellContent);
    }
    
}
