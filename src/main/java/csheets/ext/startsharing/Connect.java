/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Class responsible to inform the thread about the connection process between 2 points.
 * This class implements the Runnable interface.
 * @author dmonteiro
 */
public class Connect implements Runnable{
    
    private NetworkInstance t_instance;
    private NetworkService t_netService;
    
    /**
     * Connect constructor
     * @param instance NetworkInstance
     */
    public Connect(NetworkInstance instance) {
        this.t_instance = instance;
        t_netService = NetworkService.getInstance();
    }

    @Override
    public void run() {

        Socket connectingSocket = null;
        try {
            connectingSocket = new Socket(t_instance.getNetworkData().ip(), t_instance.getNetworkData().port());
            DataOutputStream sOut = new DataOutputStream(connectingSocket.
                    getOutputStream());
            sOut.writeUTF(t_netService.name());
            Server tConnecting;
            try {
                tConnecting = new Server(connectingSocket, t_instance);
                Thread tC = new Thread(tConnecting, "connect");
                tC.start();
                t_netService.addLinkUP(tConnecting);
            } catch (Exception ex) { 
                System.out.println("Error");
            }
        } catch (IOException ex) {
            System.out.println("Failed");
        }
    }
}
