/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class responsible for inform the thread about the time to send the network broadcast.
 * This class implements the Runnable interface.
 * @author dmonteiro
 */
public class Broadcast implements Runnable {

    private final NetworkData dataToSend;
    private NetworkData dataToReceive;
    private NetworkInstance ni;
    private DatagramSocket ds = null;
    private INetworkService ns;
    private InetAddress broadcastAddress;
    private static final int PAUSE = 10;
    
    /**
     * Send network broadcast constructor
     * @param d Network Data
     * @param s Network Service
     */
    public Broadcast(NetworkData d, INetworkService s){
        this.dataToSend = d;
        this.ns = s;
    }
    
    //All this code was already done by us to RCOMP discipline. We just adapt to his project.
    @Override
    public void run() {
        try{
            this.broadcastAddress = InetAddress.getByName("255.255.255.255");
            ds = new DatagramSocket();
            ds.setBroadcast(true);
        }
        catch(Exception ex){
          Logger.getLogger(Broadcast.class.getName()).log(Level.SEVERE, null, ex);     
        }
        
        while(true){
            
            byte[] sendData = new byte[300];
            String name = dataToSend.name();
            sendData = name.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData,name.length(),broadcastAddress,9999);
            try{
                ds.send(sendPacket);
            }
            catch(IOException ex){
                Logger.getLogger(Broadcast.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            byte[] receiveData = new byte[300];
            DatagramPacket receivePacket = new DatagramPacket(receiveData,receiveData.length);
            try{
                
                ds.setSoTimeout(1000 * PAUSE);
                ds.receive(receivePacket);
                
                String dataReceived = new String(receivePacket.getData(),0,receivePacket.getLength());
                
                String split[] = dataReceived.split("-");
                
                dataToReceive = new NetworkData(receivePacket.getAddress(),split[1], Integer.parseInt(split[2]));
                
                ni = new NetworkInstance(dataToReceive);
                
                ns.addNetworkInstance(ni);
            } catch (SocketTimeoutException ex) {
                System.out.println("Error");
            } catch (IOException ex) {
                Logger.getLogger(Broadcast.class.getName()).
                        log(Level.SEVERE, null, ex);
                break;
            }
             try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Broadcast.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
        }
        ds.close();
    }
}
