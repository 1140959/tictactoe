/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing;

/**
 *
 * @author dmonteiro
 */
public interface Observer {
    
    public void addInstance(NetworkInstance n);
    
    public void addConn(NetworkInstance n);
    
    public void removeConn(NetworkInstance n);
}

