/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ui.ctrl.FocusOwnerAction;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class responsible to get all cells for the CellsPacket 
 * @author dmonteiro
 */
public class CellsPlacer extends FocusOwnerAction implements CellsReceiverObserver {

    private NetworkService netS;
    
    /**
     *Method to get the data com a CellsPakets
     */
    @Override
    public void getData() {
        netS = NetworkService.getInstance();
        
        CellsPacket receivedCells = new CellsPacket(new HashMap<Address, String>());

        List<Object> cellsObjects = netS.getReceivedObjectFromType(receivedCells);

        if (!cellsObjects.isEmpty() && cellsObjects.get(0) instanceof CellsPacket) {
            receivedCells = (CellsPacket) cellsObjects.get(0);

            for (Map.Entry<Address, String> cell : receivedCells.getCellsToShare().entrySet()) {

                try {
                    focusOwner.getSpreadsheet().getCell(cell.getKey()).setContent(cell.getValue());
                } catch (FormulaCompilationException ex) {
                    Logger.getLogger(CellsPlacer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }    
    }
    
    @Override
    protected String getName() {
        return "";
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
    }
    
    
    
}
