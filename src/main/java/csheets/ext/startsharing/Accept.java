/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class responsible for inform the thread about the time to accept the connection.
 * This class implements the Runnable interface.
 * @author dmonteiro
 */
public class Accept implements Runnable {

    private NetworkData nd;
    private final INetworkService net;
    private ServerSocket s_recv;
    private Socket s_share;
    private NetworkInstance n_c;
    
    /**
     * Accept connection Constructor
     * @param n network Data
     * @param ns Network Service
     */
    public Accept(NetworkData n,INetworkService ns){
        this.nd = n;
        this.net = ns;
    }
    
    @Override
    public void run() {

        try {
            s_recv = new ServerSocket(nd.port());

        } catch (IOException ex) {
            System.out.println("wrong port");
            System.exit(1);
        }
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(NetworkService.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
            try {
                s_share = s_recv.accept();
                DataInputStream sIn = new DataInputStream(s_share.getInputStream());
                String c = sIn.readUTF();
                NetworkData co = new NetworkData(s_share.getInetAddress(), c, s_share.getLocalPort());
                n_c = new NetworkInstance(co);
                try {
                    Server tConnecting = new Server(s_share, n_c);
                    Thread tC = new Thread(tConnecting, "connected");
                    tC.start();
                    net.addLinkUP(tConnecting);
                } catch (Exception ex) { 
                        System.out.println("failed");
                }
            } catch (IOException ex) {
                Logger.getLogger(Accept.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
        }
    }
 } 

