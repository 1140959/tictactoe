/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.Objects;

/**
 * Class responsible to keep the network information from a cleanSheet on the Network
 * @author dmonteiro
 */
public class NetworkData implements Serializable{
    
    private InetAddress ipAddress;
    private String machineName;
    private int port;
    
    /*
    * Network information constructor
    */
    public NetworkData(InetAddress ip,String m,int p){
        this.ipAddress = ip;
        this.machineName = m;
        this.port = p;
    }
    
    /**
     * Method to return the Port defined by the UI
     * @return a integer with the port
     */
    public int port(){
        return this.port;
    }
    
    /**
     * Method to return th ip of the machine
     * @return ip of the machine
     */
    public InetAddress ip(){
        return this.ipAddress;
    }
    
    /**
     * Method to return the name of the machine
     * @return the machine name
     */
    public String name(){
        return this.machineName;
    }
    
    public void setPort(int p){
        this.port=p;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NetworkData other = (NetworkData) obj;
        if (!(this.ipAddress.getHostAddress().equals(other.ip().getHostAddress())) || !(this.machineName.equals(other.name()))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.ipAddress);
        hash = 83 * hash + Objects.hashCode(this.machineName);
        hash = 83 * hash + this.port;
        return hash;
    }
    
    
    
}
