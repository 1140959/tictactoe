/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing.automaticupdate;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.ext.style.StylableCell;
import java.awt.Color;
import java.awt.Font;
import java.io.Serializable;
import java.text.Format;
import javax.swing.border.Border;

/**
 * Class responsible for setting up the packet to send and receive within the instances of cleansheets in form of a automatic update.
 * @author dmonteiro
 */
public class AutomaticUpdateObject implements Serializable {
    
    private static final long serialVersionUID = 7010464744129096272L;

    private Address address;
    private String cellContent;
    private Format format;
    private Font font;
    private int horizontalAlignment;
    private int verticalAlignment;
    private Color backgroundColor;
    private Border border;

    /**
     * Constructor of the class with cell and style attributes.
     * @param address Address
     * @param cellContent String
     * @param format Format
     * @param font Font
     * @param horizontalAlignment Integer
     * @param verticalAlignment Integer
     * @param backgroundColor Color
     * @param border Border
     */
    public AutomaticUpdateObject(Address address, String cellContent,Format format, Font font,int horizontalAlignment, int verticalAlignment,Color backgroundColor, Border border) {
	this.address = address;
	this.cellContent = cellContent;
	this.format = format;
	this.font = font;
	this.horizontalAlignment = horizontalAlignment;
	this.verticalAlignment = verticalAlignment;
	this.backgroundColor = backgroundColor;
	this.border = border;
    }
   
        /**
         * Method to change the address of the cell
         * @param a Address
	 */
	public void changeAddress(Address a) {
		address = a;
	}

	/**
	 * @return the address of the cell
	 */
	public Address getAddress() {
		return address;
	}
        
        	/**
	 * @return the style background color of the cell
	 */
	public Color getBackgroundColor() {
		return backgroundColor;
	}
        
        /**
	 * @return the style border of the cell
	 */
	public Border getBorder() {
		return border;
	}

	/**
	 * @return the cellContent
	 */
	public String getCellContent() {
		return cellContent;
	}

	/**
	 * @return the style format of the cell
	 */
	public Format getFormat() {
		return format;
	}

	/**
	 * @return the style font of the cell
	 */
	public Font getFont() {
		return font;
	}

	/**
	 * @return the style horizontal alignment of the cell
	 */
	public int getHorizontalAlignment() {
		return horizontalAlignment;
	}

        /**
	 * @return the style vertical alignment of the cell
	 */
	public int getVerticalAlignment() {
		return verticalAlignment;
	} 

}