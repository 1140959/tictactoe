/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing.automaticupdate;

import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.startsharing.CellsReceiverObserver;
import csheets.ext.startsharing.NetworkService;
import csheets.ext.style.StylableCell;
import csheets.ext.style.StyleExtension;
import csheets.ui.ctrl.FocusOwnerAction;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class that places the automatic update cells in the other instances of cleansheets.
 * @author dmonteiro
 */
public class AutomaticUpdateCellsPlacer extends FocusOwnerAction implements CellsReceiverObserver {

    private NetworkService netService;
    private AutomaticUpdateObject auo;
    private StylableCell styleToApply;

    /**
     * Method to get the received cells from automatic update and place them in the cleansheets instance.
     */
    @Override
    public void getData() {
        
        netService = NetworkService.getInstance();
        
        auo = new AutomaticUpdateObject(null, DEFAULT, null, null, 0, 0,Color.BLACK, null);
        
        List<Object> receivedObjs = netService.getReceivedObjectFromType(auo);
        
        auo = (AutomaticUpdateObject) receivedObjs.get(0);
        
        try{
            styleToApply = (StylableCell) focusOwner.getSpreadsheet().getCell(auo.getAddress()).getExtension(StyleExtension.NAME);
            styleToApply.setBackgroundColor(auo.getBackgroundColor());
            styleToApply.setBorder(auo.getBorder());
            styleToApply.setFont(auo.getFont());
            styleToApply.setFormat(auo.getFormat());
            styleToApply.setHorizontalAlignment(auo.getHorizontalAlignment());
            styleToApply.setVerticalAlignment(auo.getVerticalAlignment());
            focusOwner.getSpreadsheet().getCell(auo.getAddress()).setContent(auo.getCellContent());
                 
        } catch (FormulaCompilationException ex) {
            Logger.getLogger(AutomaticUpdateCellsPlacer.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }

    /**
     * Not used
     * @return an empty string
     */
    @Override
    protected String getName() {
        return "";
    }
    
    /**
     * Not used
     * @param ae ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        
    }
    
    
}
