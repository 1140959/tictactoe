/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing.automaticupdate;


import csheets.ext.startsharing.NetworkService;
import csheets.ext.startsharing.ui.SharingPanel;
import csheets.ui.ctrl.UIController;

/**
 * Controller class to the use case of automatic updates, which consists in, 
 * automatic update the other instances of cleansheets in the network, with the changes applied by the user in this instance
 * @author dmonteiro
 */
public class AutomaticUpdateController{
    
    private UIController uicontroller;
    private SharingPanel sharingPanel;
    private NetworkService netService;
    private AutomaticUpdateListener aul;
    
    
    /**
     * Constructor of the controller
     * @param ui UIController
     * @param sp SharingPanel
     */
    public AutomaticUpdateController(UIController ui,SharingPanel sp){
           this.uicontroller = ui;
           this.sharingPanel = sp;
           netService = NetworkService.getInstance();
           aul = new AutomaticUpdateListener(netService,sharingPanel);
           uicontroller.addSelectionListener(aul);          
    }
    
    
    /**
     * Set the automatic updates to a certain Sharing Panel
     * @param sharingPanel SharingPanel
     */
    public void addSharingPanel(SharingPanel sharingPanel) {
        this.sharingPanel = sharingPanel;
    }
    
}
