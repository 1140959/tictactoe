/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing.automaticupdate;

import csheets.core.Cell;
import csheets.ext.startsharing.NetworkInstance;
import csheets.ext.startsharing.NetworkService;
import csheets.ext.startsharing.ui.SharingPanel;
import csheets.ext.style.StylableCell;
import csheets.ext.style.StyleExtension;
import csheets.ui.ctrl.SelectionEvent;
import csheets.ui.ctrl.SelectionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class to listen the connection of cleansheets instances to check when it sends cells from one instance to another to make automatic updates
 * @author dmonteiro
 */
public class AutomaticUpdateListener implements SelectionListener{
    
    private NetworkService netServ;
    private SharingPanel sharingpanel;
    private Cell cell;
    private StylableCell style;
    /**
     * Constructor of the class AutomaticUpdateListener
     * @param n NetworkService
     * @param sp SharingPanel
     */
    public AutomaticUpdateListener(NetworkService n,SharingPanel sp){
        this.netServ = n; 
        this.sharingpanel = sp;
    }
    
    
    /**
     * Method to check if the user has passed to the next cell. If he has, get the previous cell and style, and update the other cleansheets instance.
     */
    @Override
    public void selectionChanged(SelectionEvent event) {
        if (netServ==null) {
            netServ = netServ.getInstance();
        }
        try{
            if(sharingpanel.getCheckBox().isSelected()){
                cell = event.getPreviousCell();
                style = (StylableCell) cell.getExtension(StyleExtension.NAME);
                AutomaticUpdateObject cellToSend = new AutomaticUpdateObject(cell.getAddress(), cell.getContent(), style.getFormat(), style.getFont(), style.getHorizontalAlignment(), style.getVerticalAlignment(), style.getBackgroundColor(), style.getBorder());
                List<NetworkInstance> connectedInstance = new ArrayList<NetworkInstance>();
                for(int i=0;i< sharingpanel.getConnectModel().getSize();i++){
                    connectedInstance.add((NetworkInstance) sharingpanel.getConnectModel().get(i));
                }
                netServ.send(cellToSend, connectedInstance);
            }
        }
        catch(Exception e){
            Logger.getLogger(AutomaticUpdateListener.class.getName()).log(Level.SEVERE, null, e);     
        }
    }
}
