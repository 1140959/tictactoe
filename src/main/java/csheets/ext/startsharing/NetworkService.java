/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class responsible for the service provided to all the network connection
 * process. This class use the singleton method to get a unique instance.
 *
 * @author dmonteiro
 */
public class NetworkService implements INetworkService{

    private static NetworkService ns = null;
    private static NetworkData netData;
    private List<NetworkInstance> listInst = new ArrayList<NetworkInstance>();
    private List<Server> listConn = new ArrayList<Server>();
    private List<Object> receivedObjects = new ArrayList<Object>();
    private List<Observer> obs = new ArrayList<Observer>();
    private List<CellsReceiverObserver> obsReceived = new ArrayList<CellsReceiverObserver>();

    /*
    * Simple Network service constructor
     */
    protected NetworkService() {
    }

    /**
     * This method define the network
     *
     * @param d defined Network
     */
    public static void defineNetwork(NetworkData d) {
        netData = d;
    }

    //singleton like EAPLI
    /**
     * This method get a instance in the network
     *
     * @return the network instance
     */
    public static NetworkService getInstance() {
        if (ns == null && netData != null) {
            synchronized (NetworkService.class) {
                if (ns == null) {
                    ns = new NetworkService();
                }
            }
        }
        return ns;
    }

    /**
     * Initialize the thread for send broadcast.
     */
    public void broadcastInstances() {
        Runnable b = new Broadcast(this.netData, this);
        Thread t = new Thread(b, "broadcasting");
        t.start();
    }



    /**
     *Initialize the thread for start listening a request to the broadcast.
     */

    public void listeningBroadcast() {
        Runnable l = new Listener(this.netData);
        Thread t = new Thread(l, "listening");
        t.start();
    }

    /**
    * Initialize the thread to establish the connection.
     */
    public void acceptLink() {
        Runnable a = new Accept(this.netData, this);
        Thread t = new Thread(a, "accepting");
        t.start();
    }



    /**
     *Add a NetworkInstance to the NetworkInstance list
     * @param ni Network Instance
     */

    @Override
    public void addNetworkInstance(NetworkInstance ni) {

        if (!listInst.contains(ni)) {
            this.listInst.add(ni);
            notifyObserversofInstance(ni);
        }
    }

    /**
     *method to connect to another instance
     * @param n Network Instance
     * @return Boolean True or False to the connection intent
     */
    public boolean connectToInstance(NetworkInstance n) {
        Runnable tLink = new Connect(n);
        Thread t = new Thread(tLink, "link to network instance selected");

        try {
            t.start();
        } catch (Exception ex) {
            Logger.getLogger(NetworkService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        notifyObserversOfConnected(n);
        return true;
    }

    /**
     *Method to add the link to the ListConn List of Servers
     * @param s Server to be added
     */
    @Override
    public void addLinkUP(Server s) {

        for (Server link : listConn) {
            if (link.myNetworkInstance().equals(s.myNetworkInstance())) {
                return;
            }
        }
        listConn.add(s);
        notifyObserversOfConnected(s.myNetworkInstance());
    }

    /**
     *Method to add an Generic Object into the the list of received Objects 
     * @param o Object added
     */
    public void addReceivedObject(Object o) {
        receivedObjects.add(o);
        notifyObserversOfDataReceived();
    }

    /**
     * Method to send Objects into a List of Network Instances
     * @param o the object to send
     * @param l the list of Network Instances
     * @return boolean true of false 
     */
    public boolean send(Object o, List<NetworkInstance> l) {
        try {
            for (NetworkInstance sendTo : l) {
                for (Server activeConnection : listConn) {
                    if (sendTo.equals(activeConnection.myNetworkInstance())) {
                        activeConnection.sendData(o);
                    }
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    /**
     *Method to receive the object from the sender, add to a list meanwhile UI uses it, and in the end
     * removes it from the list for the next object gets the place.
     * @param o Object to send to the List
     * @return the List of objects.
     */
    public List<Object> getReceivedObjectFromType(Object o) {
        List<Object> objectsToReturn = new ArrayList<Object>();
        for (Object receivedObject : receivedObjects) {
            if (o.getClass().getName().equals(receivedObject.getClass().getName())) {
                objectsToReturn.add(receivedObject);
            }
        }
        receivedObjects.removeAll(objectsToReturn);

        return objectsToReturn;
    }

    /**
     *Method to Disconnect an active connection.
     * @param n NetworkInstance
     * @return boolean true or false to the pretended action.
     */
    public boolean disconectFrom(NetworkInstance n) {
        Server c = null;
        for (Server con : listConn) {
            if (con.myNetworkInstance().equals(n)) {
                con = c;
            }
        }
        if (c != null) {
            listInst.remove(c.myNetworkInstance());
            listConn.remove(c);
            c.disconnect();
        }
        notifyObserversClosedConn(n);
        return true;
    }

    private void notifyObserversofInstance(NetworkInstance n) {
        for (Observer o : obs) {
            o.addInstance(n);
        }
    }

    private void notifyObserversOfConnected(NetworkInstance n) {
        for (Observer o : obs) {
            o.addConn(n);
        }
    }

    private void notifyObserversClosedConn(NetworkInstance n) {
        for (Observer o : obs) {
            o.removeConn(n);
        }
    }

    /**
     * add an observer to monitor UI data transfers
     * @param o Observer
     */
    public void addObserver(Observer o) {
        this.obs.add(o);
    }

    private void notifyObserversOfDataReceived() {
        for (CellsReceiverObserver o : obsReceived) {
            try {
                o.getData();
            } catch (Exception e) {
            }
        }
    }

    /**
     *Add a receiver data observer
     * @param r cell receiver observer
     */
    public void addReceivedDataObserver(CellsReceiverObserver r) {
        this.obsReceived.add(r);
    }

    /**
     * Method to return a list of network instances
     * @return list of network instances
     */
    public List<NetworkInstance> getNetInstances() {
        return this.listInst;
    }

    /**
     * Method t return the netData name
     * @return netData machine name
     */
    public String name() {
        return netData.name();
    }

}
