/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class representative of a instance in the network
 * @author dmonteiro
 */
public class NetworkInstance implements Serializable{
    
    private NetworkData networkData;
    
    /**
    * Network intance constructor
     * @param networkData networkData
    */
    public NetworkInstance(NetworkData networkData) {
        this.networkData = networkData;
    }
    
    /**
     *Method to return network data
     * @return return the network data
     */
    public NetworkData getNetworkData() {
        return networkData;
    }
    
    public String toString() {
        return networkData.name() + " -> " + networkData.ip().toString() ;
    }
    
    @Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final NetworkInstance other = (NetworkInstance) obj;
		if (this.networkData != other.networkData && (this.networkData == null || !this.networkData.equals(other.networkData))) {
			return false;
		}
		return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.networkData);
        return hash;
    }
    
}
