/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing.ui;

import csheets.ext.startsharing.NetworkData;
import csheets.ext.startsharing.NetworkInstance;
import csheets.ext.startsharing.StartSharingController;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 * Class tho Start the Listening Actions and extends the abstract class BaseAction
 * @author LMendes
 */
public class StartListenAction extends BaseAction{
    
       protected UIController uiController;
       private StartSharingController sharingCtrl;
       private SharingPanel sideBar;

    /**
     *Constructor of the class
     * @param uiController uiController 
     * @param sharingController sharingController
     * @param sp SharingPanel
     */
    public StartListenAction(UIController uiController,
            StartSharingController sharingController,SharingPanel sp) {
        this.uiController = uiController;
        this.sharingCtrl = sharingController;
        this.sideBar = sp;
    }

    @Override
    protected String getName() {
        return "Start broadcasting";
    }
    
    public void addSharingPanel(SharingPanel sp){
        this.sideBar = sp;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        sharingCtrl.startListen(this.sideBar);
    }
    
    
    
   
}
