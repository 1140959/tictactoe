/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing.ui;

import csheets.ext.Extension;
import csheets.ext.deptree.DependencyTreeExtension;
import csheets.ext.deptree.DependentsTree;
import csheets.ext.deptree.PrecedentsTree;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.CellDecorator;
import csheets.ui.ext.TableDecorator;
import csheets.ui.ext.UIExtension;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.border.TitledBorder;

/**
 * Class to add the extentions to the app and extends the abstract class UIExtension
 * @author LMendes
 */
public class UIExtensionShare extends UIExtension {
   /** The icon to display with the extension's name */
	private Icon icon;

	/** The menu of the extension */
	private ShareMenu menu;
        
        private JComponent sideBar;

    /**
     *Constructor of the class
     * @param extension extension to use
     * @param uiController uiController
     */
    public UIExtensionShare(Extension extension, UIController uiController) {
		super(extension, uiController);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Returns an icon to display with the extension's name.
	 * @return an icon with style
	 */
	public Icon getIcon() {
		return null;
	}

	/**
	 * Returns an instance of a class that implements JMenu.
	 * In this simple case this class only supplies one menu option.
	 * 
	 * @return a JMenu component
	 */
	public JMenu getMenu() {
		if (menu == null)
			menu = new ShareMenu(uiController);
		return menu;
	}
	
	/**
	 * Returns a cell decorator that visualizes the data added by the extension.
	 * @return a cell decorator, or null if the extension does not provide one
	 */
	public CellDecorator getCellDecorator() {
		return null;
	}

	/**
	 * Returns a table decorator that visualizes the data added by the extension.
	 * @return a table decorator, or null if the extension does not provide one
	 */
	public TableDecorator getTableDecorator() {
		return null;
	}	
	
	/**
	 * Returns a toolbar that gives access to extension-specific
	 * functionality.
	 * @return a JToolBar component, or null if the extension does not provide one
	 */
	public JToolBar getToolBar() {
		return null;
	}

	/**
	 * Returns a panel to share cell
	 * @return a panel 
	 */
	public JComponent getSideBar() {
		 if (sideBar == null) {
            sideBar = new SharingPanel(uiController);
        }

        return sideBar;
    }
	
}
