/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing.ui;

import csheets.ext.startsharing.StartSharingController;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 * Class to the Send Button Action
 * @author LMendes
 */
public class SendButtonAction extends BaseAction{
    protected UIController uiController;
       private StartSharingController sharingCtrl;

    /**
     * Contructor of the class
     * @param uiController uiController
     * @param sharingController sharingController
     */
    public SendButtonAction(UIController uiController,
            StartSharingController sharingController) {
        this.uiController = uiController;
        this.sharingCtrl = sharingController;
    }

    @Override
    protected String getName() {
        return "Send button";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       sharingCtrl.sendData();
    }
   
}
