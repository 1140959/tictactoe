/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing.ui;

import csheets.ext.startsharing.StartSharingController;
import csheets.ext.share.ShareExtension;
import csheets.ext.startsharing.NetworkInstance;
import csheets.ext.startsharing.Observer;
import csheets.ext.startsharing.automaticupdate.AutomaticUpdateController;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

/**
 * Class to create the Sharing panel
 *
 * @author LMendes
 */
public class SharingPanel extends JPanel implements Observer {

    private StartSharingController sharingCtrl;
    private JCheckBox automaticUpdate;
    private AutomaticUpdateController autoUpdate;
    private final JList instanceList;
    private final JLabel connectedLbl;
    private final JLabel listenConection;
    private final JList connectionsList;
    private final JLabel availableLbl;
    private final JButton startlistenBtn;
    private final JButton discBtn;
    private final JButton connectBtn;
    private final JButton sendBtn;
    private final JTextField portText;
    private final JPanel portPanel;
    private final JLabel portLabel;
    private final JTextField connectionText;
    private final JLabel connectionLabel;
    private final DefaultListModel instancesModel;
    private final DefaultListModel connectedModel;

    /**
     * Controller Constructor of the class
     *
     * @param uiController uiController
     */
    public SharingPanel(UIController uiController) {

        super(new BorderLayout());
        this.sharingCtrl = new StartSharingController(uiController, this);
        this.autoUpdate = new AutomaticUpdateController(uiController,this);
        this.setName(ShareExtension.NAME);
        connectBtn = new JButton("Connect");
        connectBtn.addActionListener(new ConButtonAction(uiController, sharingCtrl));
        sendBtn = new JButton("Send Cells");
        sendBtn.addActionListener(new SendButtonAction(uiController, sharingCtrl));
        discBtn = new JButton("Disconnect");
        discBtn.addActionListener(new DiscButtonAction(uiController, sharingCtrl));
        startlistenBtn = new JButton("Start");
        instanceList = new JList();
        startlistenBtn.addActionListener(new StartListenAction(uiController, sharingCtrl,this));
        connectionsList = new JList();
        availableLbl = new JLabel("Available Network CleanSheets:");
        availableLbl.setPreferredSize(new Dimension(20, 20));
        connectedLbl = new JLabel("Connected to:");
        connectedLbl.setPreferredSize(new Dimension(20, 20));
        listenConection = new JLabel("Start share:");
        listenConection.setPreferredSize(new Dimension(20, 20));
        instancesModel = new DefaultListModel();
        connectedModel = new DefaultListModel();
        connectBtn.setPreferredSize(new Dimension(70, 20));
        connectBtn.setMaximumSize(new Dimension(100, 20));
        sendBtn.setPreferredSize(new Dimension(70, 20));
        sendBtn.setMaximumSize(new Dimension(100, 20));
        discBtn.setPreferredSize(new Dimension(70, 20));
        discBtn.setMaximumSize(new Dimension(100, 20));
        startlistenBtn.setPreferredSize(new Dimension(70, 20));
        startlistenBtn.setMaximumSize(new Dimension(100, 20));
        instanceList.setPreferredSize(new Dimension(Integer.MAX_VALUE, 150));
        instanceList.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));// width, height
        connectionsList.setPreferredSize(new Dimension(Integer.MAX_VALUE, 150));
        connectionsList.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));// width, height
        availableLbl.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
        connectedLbl.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
        listenConection.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
        automaticUpdate = new JCheckBox("Auto Cell Update", false);
	automaticUpdate.setAlignmentX(Component.RIGHT_ALIGNMENT);
	automaticUpdate.setAlignmentY(Component.BOTTOM_ALIGNMENT);

        availableLbl.setAlignmentX(Component.TOP_ALIGNMENT);
        connectedLbl.setAlignmentX(Component.TOP_ALIGNMENT);
        sendBtn.setAlignmentX(Component.LEFT_ALIGNMENT);
        listenConection.setAlignmentX(Component.TOP_ALIGNMENT);

        portPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

        portLabel = new JLabel("Port");
        portLabel.setPreferredSize(new Dimension(30, 20));
        portPanel.add(portLabel);
        portText = new JTextField();
        portText.setPreferredSize(new Dimension(60, 25));
        portText.setEditable(true);
        portText.setText("12345");
        portPanel.add(this.portText);

        connectionLabel = new JLabel("Name:");
        connectionLabel.setPreferredSize(new Dimension(70, 20));
        portPanel.add(connectionLabel);
        connectionText = new JTextField();
        connectionText.setPreferredSize(new Dimension(70, 20));
        connectionText.setEditable(true);
        connectionText.setText("MyName");
        portPanel.add(connectionText);

        portPanel.add(startlistenBtn);

        JPanel thePanel = new JPanel();
        thePanel.setLayout(new BoxLayout(thePanel, BoxLayout.PAGE_AXIS));
        thePanel.setPreferredSize(new Dimension(130, 500));
        thePanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));		// width, height
        thePanel.add(listenConection);
        thePanel.add(portPanel);
        thePanel.add(availableLbl);
        thePanel.add(instanceList);
        thePanel.add(connectBtn);
        thePanel.add(connectedLbl);
        thePanel.add(connectionsList);
        thePanel.add(automaticUpdate);
        thePanel.add(sendBtn);
        thePanel.add(discBtn);

        // borders
        TitledBorder border = BorderFactory.createTitledBorder("Sharing");
        border.setTitleJustification(TitledBorder.CENTER);
        thePanel.setBorder(border);

        //panels
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(thePanel, BorderLayout.NORTH);
        add(panel, BorderLayout.NORTH);

    }
    
    public JCheckBox getCheckBox() {
	return automaticUpdate;
    }

    public String getListenPort() {
        return portText.getText();
    }

    public String getListenName() {
        return connectionText.getText();
    }
    
    public JList getInstancesList(){
        return this.instanceList;
    }
    
    public DefaultListModel getInstancesModel(){
        return this.instancesModel;
    }
    
    public JList getConnectList(){
        return this.connectionsList;
    }
    
    public DefaultListModel getConnectModel(){
        return this.connectedModel;
    }

    @Override
    public void addInstance(NetworkInstance n) {
        if (instancesModel.contains(n)) {

			instancesModel.removeElement(n);
			instanceList.setModel(instancesModel);
		}
        else{
            instancesModel.addElement(n);
	    instanceList.setModel(instancesModel);
        }
    }

    @Override
    public void addConn(NetworkInstance n) {
        if (!connectedModel.contains(n)) {

			instancesModel.removeElement(n);
			instanceList.setModel(instancesModel);
			connectedModel.addElement(n);
			connectionsList.setModel(connectedModel);
		}
    }

    @Override
    public void removeConn(NetworkInstance n) {
		if (connectedModel.contains(n)) {
                    instancesModel.removeElement(n);
                    instanceList.setModel(instancesModel);
                    connectedModel.removeElement(n);
                    connectionsList.setModel(connectedModel);
		}
    }  
}
