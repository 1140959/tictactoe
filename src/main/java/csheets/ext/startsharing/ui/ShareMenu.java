/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.startsharing.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *Classe to create the Share Menu
 * @author LMendes
 */
public class ShareMenu extends JMenu {

    /**
     * Constructor of the class
     * @param uiController uiController
     */
    public ShareMenu(UIController uiController) {
        super("Share");
        setMnemonic(KeyEvent.VK_S);
        
        add(new SharingAction(uiController));

    }
}
