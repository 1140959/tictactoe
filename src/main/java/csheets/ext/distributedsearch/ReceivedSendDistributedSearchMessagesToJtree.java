/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.distributedsearch;

import java.util.Enumeration;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
/**
 *
 * @author Paulo Silva - 1960554@isep.ipp.pt
 */
public class ReceivedSendDistributedSearchMessagesToJtree  extends JTree {
    
    
        public ReceivedSendDistributedSearchMessagesToJtree() {
        super(new DefaultMutableTreeNode("Received Distributed Search of Workbook"));
    }
        
    /**
     * Add path to Jtree message
     * @param path tree add path
     */
    public void addDistributedSearchMessageToJTree(String path) {
        DefaultTreeModel model = (DefaultTreeModel) this.getModel();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();

        String[] allPaths = path.split("/");
        DefaultMutableTreeNode rootnode = (DefaultMutableTreeNode) root;
        //the node existe?
        for (String actualPath : allPaths) {
            if (!actualPath.equals("") && !actualPath.equals("..")) {
                Enumeration<DefaultMutableTreeNode> en = rootnode.children();
                boolean exists = false;
                while (en.hasMoreElements()) {
                    DefaultMutableTreeNode node = en.nextElement();
                    if (node.getUserObject().equals(actualPath)) {
                        rootnode = node;
                        exists = true;
                        break;
                    }
                }
                //if not, add new one
                if (exists != true) {
                    DefaultMutableTreeNode tmpNode = new DefaultMutableTreeNode(actualPath);
                    rootnode.add(tmpNode);
                    model.reload(rootnode);
                    rootnode = tmpNode;
                }
            }
        }
    }
}
