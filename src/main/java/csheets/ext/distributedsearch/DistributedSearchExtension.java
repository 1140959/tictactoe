/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.distributedsearch;

import csheets.ext.Extension;
import csheets.ext.distributedsearch.ui.UIDistributedSearchExtension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Paulo Silva 1960554@isep.ipp.pt
 */
public class DistributedSearchExtension extends Extension {

    /**
     * The version
     */
    public static final Long VERSION = 1L;

    /**
     * Description
     */
    public static final String DESCRIPTION = "Distribute your workbook on your network!";

    /**
     * The name of the extension
     */
    public static final String NAME = "Distribute";

    /**
     * Creates a new Example extension.
     */
    public DistributedSearchExtension() {
        super(VERSION, NAME, DESCRIPTION);
    }

    /**
     * Returns the user interface extension of this extension (an instance of
     * the class {@link  csheets.ext.simple.ui.UIExtensionExample}). In this
     * extension example we are only extending the user interface.
     *
     * @param uiController the user interface controller
     * @return a user interface extension, or null if none is provided
     */
    public UIExtension getUIExtension(UIController uiController) {
        return new UIDistributedSearchExtension(this, uiController);
    }

}
