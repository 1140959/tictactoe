/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.distributedsearch;

import java.net.InetAddress;
import java.util.Objects;

/**
 *
 * @author Paulo Silva - 1960554@isep.ipp.pt
 */
public class DistributedSearchNetworkData {

    private final InetAddress ipAddress;
    private final String nickname;
    private final int port;

    public DistributedSearchNetworkData() {
        this.nickname = "";
        this.ipAddress = null;
        this.port = 0;

    }

    public DistributedSearchNetworkData(InetAddress ip, String m, int p) {
        this.ipAddress = ip;
        this.nickname = m;
        this.port = p;
    }
    
    
    
    /**
     * Method to return the Port connection
     *
     * @return a integer with the getPort
     */
    public int getPort() {
        return this.port;
    }

    /**
     * Method to return th getIpAddress of the machine
     *
     * @return getIpAddress of the machine
     */
    public InetAddress getIpAddress() {
        return this.ipAddress;
    }

    /**
     * Method to return the nickname
     *
     * @return the nickname
     */
    public String getNickname() {
        return this.nickname;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.ipAddress);
        hash = 43 * hash + Objects.hashCode(this.nickname);
        hash = 43 * hash + this.port;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DistributedSearchNetworkData other = (DistributedSearchNetworkData) obj;
        if (this.port != other.port) {
            return false;
        }
        if (!Objects.equals(this.nickname, other.nickname)) {
            return false;
        }
        if (!Objects.equals(this.ipAddress, other.ipAddress)) {
            return false;
        }
        return true;
    }

    

}
