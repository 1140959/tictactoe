/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.distributedsearch;

import csheets.ext.distributedsearch.ui.DistributedSearchMessagePopUp;
import csheets.ui.ctrl.UIController;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import javax.swing.JOptionPane;

/**
 *
 * @author Paulo Silva - 1960554@isep.ipp.pt
 */
public class DistributedSearchMessageConnection extends DistributedSearchMessageThread {

    protected DistributedSearchNetworkData ChatNwData;
    protected final int TIMEWAITGETMSG = 50000;
    protected DatagramSocket socket;
    private UIController uiController;
    private String autoReplay;
    private int replayCount;

    /**
     * Constructor to start Socket to receive data
     *
     * @param nickname nickname
     * @param port port number
     * @throws SocketException exception
     */
    public DistributedSearchMessageConnection(String nickname, int port, UIController uiController) throws SocketException {
        this.replayCount=0;
        this.socket = new DatagramSocket(port);

        try {
            this.ChatNwData = new DistributedSearchNetworkData(InetAddress.getLocalHost(), nickname, port);
        } catch (UnknownHostException ex) {
            JOptionPane.showMessageDialog(null, "An error occured.\nCall the technical support!", "Error!!",
                    JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * Receive a message
     *
     * @return boolean
     */
    @Override
    public boolean receiveMessageFrom() {
        Object object;
        try {
            byte[] buffer = new byte[5000];
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            socket.receive(packet);
            ByteArrayInputStream baos = new ByteArrayInputStream(buffer);
            ObjectInputStream ois = new ObjectInputStream(baos);
            object = (Object) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            return false;
        }
        if (object != null) {
            if (object instanceof DistributedSearchMessage) {
                DistributedSearchMessage message = (DistributedSearchMessage) object;
                String response = "It was masde a Search for workbook " + message.getMsgText() + " in this machine";
                response = response + " and my active workboor is " + DistributedSearchService.getInstance(uiController).getMyUiController().getActiveWorkbookName();
                autoReplay = "The active workbook the other instance is " + DistributedSearchService.getInstance(uiController).getMyUiController().getActiveWorkbookName();
                DistributedSearchMessage replayMessage = new DistributedSearchMessage(autoReplay);
                new DistributedSearchMessagePopUp(message.getNickName(), response, TIMEWAITGETMSG);
                DistributedSearchService.getInstance(uiController).addDistributedSearchMessage(message);
                DistributedSearchService.getInstance(uiController).getReceivedSendDistributedSearchMessages().addDistributedSearchMessageToJTree(message.getIpAddress().getHostAddress() + "/" + message.getNickName() + ":" + message.getMsgText());
                if(this.replayCount==0){
                sendReplyMessageTo(((DistributedSearchMessage) object).getIpAddress(), ((DistributedSearchMessage) object).getPortNumber(), replayMessage);
                }
                this.replayCount=1;
            }
        }
        return true;
    }

    /**
     * Send a message
     *
     * @param ipAddress the Ip address
     * @param port the port
     * @param message the message
     * @return true/false
     */
    public boolean sendMessageTo(InetAddress ipAddress, int port, DistributedSearchMessage message) {
        message.setNickName(this.ChatNwData.getNickname());
        message.setIpAddress(this.ChatNwData.getIpAddress());
        message.setPortNumber(this.ChatNwData.getPort());

        try {
            DatagramSocket socketSend = new DatagramSocket();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(message);
            oos.flush();

            byte[] Buf = baos.toByteArray();

            DatagramPacket packet = new DatagramPacket(Buf, Buf.length, ipAddress, port);
            socketSend.send(packet);
            DistributedSearchService.getInstance(uiController).getReceivedSendDistributedSearchMessages().addDistributedSearchMessageToJTree(ipAddress.getHostAddress() + "/" + message.getNickName() + ":" + message.getMsgText());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "An error occured.\nCall the technical support!", "Error!!",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    /**
     * Send a message
     *
     * @param ipAddress the Ip address
     * @param port the port
     * @param message the message
     * @return true/false
     */
    public boolean sendReplyMessageTo(InetAddress ipAddress, int port, DistributedSearchMessage message) {
        message.setNickName(this.ChatNwData.getNickname());
        message.setIpAddress(this.ChatNwData.getIpAddress());
        message.setPortNumber(this.ChatNwData.getPort());

        try {
            DatagramSocket socketSend = new DatagramSocket();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(message);
            oos.flush();

            byte[] Buf = baos.toByteArray();

            DatagramPacket packet = new DatagramPacket(Buf, Buf.length, ipAddress, port);
            socketSend.send(packet);
            DistributedSearchService.getInstance(uiController).getReceivedSendDistributedSearchMessages().addDistributedSearchMessageToJTree(ipAddress.getHostAddress() + "/" + message.getNickName() + ":" + message.getMsgText());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "An error occured.\nCall the technical support!", "Error!!",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    /**
     * Get my nickname
     *
     * @return nickname
     */
    public String getMyNickName() {
        return ChatNwData.getNickname();
    }

    /**
     * My getIpAddress address
     *
     * @return getIpAddress Address
     */
    public String getMyIpAddress() {
        return ChatNwData.getIpAddress().getHostAddress();
    }

    /**
     * My getPort number
     *
     * @return getPort
     */
    public int getMyPort() {
        return ChatNwData.getPort();
    }

}
