/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.distributedsearch;


import csheets.ui.ctrl.UIController;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Silva - 1960554@isep.ipp.pt
 */
public class DistributedSearchService {
    
    private static DistributedSearchService instance = null;
    protected DistributedSearchMessageConnection connection = null;
    protected List<DistributedSearchMessage> listMessages = new ArrayList<>();
    private ReceivedSendDistributedSearchMessagesToJtree receivedSendMessages;
    private static final int DISTRIBUTEDPORT = 40500;
   private UIController uiController;

    public DistributedSearchService(UIController uiController) {
        this.uiController=uiController;
    }

    
        public static DistributedSearchService getInstance(UIController uiController) {
        if (instance == null) {
             instance = new DistributedSearchService(uiController);
         
        }
        return instance;
    }

        public UIController getMyUiController()
        {
        return this.uiController;
               
        }
        
        /**
     * Start socket connection
     *
     * @param nickname nickname
     * @throws SocketException exception
     */
    public void startConnection(String nickname) throws SocketException {
        this.connection = new DistributedSearchMessageConnection(nickname, DISTRIBUTEDPORT,uiController);
        this.connection.start();
    }
    
    public DistributedSearchMessageConnection getConnection() {
        return connection;
    }

    /**
     * Add message to List
     *
     * @param message Message
     */
    public void addDistributedSearchMessage(DistributedSearchMessage message) {

        this.listMessages.add(message);
    }

    /**
     * Send a message
     *
     * @param ip Ip adrress
     * @param portNumber port
     * @param message Mesaage
     * @return true/false
     * @throws SocketException exception
     */
    public boolean sendDistributedSearchMessage(InetAddress ip, int portNumber, DistributedSearchMessage message) throws SocketException {
        return this.connection.sendMessageTo(ip, portNumber, message);
    }

    /**
     * Chat port default
     *
     * @return port
     */
    public int DistributedSearchPort() {
        return DISTRIBUTEDPORT;
    }

    
    /**
     * My ip address
     *
     * @return ip
     */
    public String getMyIP() {

        return connection.getMyIpAddress();
    }

    /**
     * Return the tree of messages
     *
     * @return the tree
     */
    public ReceivedSendDistributedSearchMessagesToJtree getReceivedSendDistributedSearchMessages() {
        return this.receivedSendMessages;
    }

    /**
     * Define a tree
     *
     * @param jTree the tree
     */
    public void setReceivedDistributedSearchMessages(ReceivedSendDistributedSearchMessagesToJtree jTree) {
        this.receivedSendMessages = jTree;
    }
        
        
}
