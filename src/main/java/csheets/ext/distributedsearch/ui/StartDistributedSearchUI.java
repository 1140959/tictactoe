/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.distributedsearch.ui;
import csheets.ext.distributedsearch.DistributedSearchService;
import csheets.ui.ctrl.UIController;
import java.net.InetAddress;
import java.net.UnknownHostException;
import static java.util.Objects.isNull;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
/**
 *
 * @author Paulo Silva - 1960554@isep.ipp.pt
 */
public class StartDistributedSearchUI {
    
    
    protected StartDistributedSearchController startDistributedSearchController;
    protected UIController uiController;

      private JFrame jframe;
      
      
    public StartDistributedSearchUI(UIController uiController) {
        this.uiController=uiController;
        this.startDistributedSearchController = new StartDistributedSearchController(uiController);

    }

    
    
    protected void run() {
        
        
        String IpAddress, myHost;
        InetAddress iAddress;
        try {
            iAddress = InetAddress.getLocalHost();
            IpAddress = iAddress.getHostAddress();
        } catch (UnknownHostException ex) {
            IpAddress = "Ip Address not found";
        }
        try {
            iAddress = InetAddress.getLocalHost();
            myHost = iAddress.getHostName();
        } catch (UnknownHostException ex) {
            myHost = "Host not found!!";
        }
        
        
        
        String nickname;
        nickname = myHost;
                //JOptionPane.showInputDialog("Insert nickname:");
        if (isNull(nickname) || nickname.isEmpty()) {
            return;
        }
        nickname = nickname.trim();
        if (nickname.compareTo("") == 0) {
            JOptionPane.showMessageDialog(jframe,
                    "Invalid Nickname.", jframe.
                    getTitle(),
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        try {
            if (startDistributedSearchController.startDistributedSearch(nickname)) {
                JOptionPane.showMessageDialog(jframe,
                        "Success start Distributed Search at port " + DistributedSearchService.getInstance(uiController).DistributedSearchPort()
                        + "!\nDistributed Search service is now running.");
                
            }
        } catch (Exception ex) {
            Logger.getLogger(StartDistributedSearchUI.class.getName()).
                    log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(jframe,
                    "Some error occured!\n" + ex.getMessage(), jframe.
                    getTitle(),
                    JOptionPane.ERROR_MESSAGE);
        }
    }

}
