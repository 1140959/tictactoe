/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.distributedsearch.ui;

import csheets.ext.distributedsearch.DistributedSearchService;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;


/**
 *
 * @author Paulo Silva - 1960554@isep.ipp.pt
 */
public class StartDistributedSearchAction extends BaseAction{
     protected UIController uiController;

    /**
     * Creates a new action.
     *
     * @param uiController the user interface controller
     */
    public StartDistributedSearchAction(UIController uiController) {
        this.uiController = uiController;
    }

    @Override
    protected String getName() {
        return "Start";
    }

@Override
	public void actionPerformed(ActionEvent ae) {
		if (DistributedSearchService.getInstance(uiController).getConnection() != null) {
                    int port=DistributedSearchService.getInstance(uiController).DistributedSearchPort();
			JOptionPane.
				showMessageDialog(null, "Search Already Started on port "+port+"!", "Start Search", JOptionPane.WARNING_MESSAGE);

		} else {
			StartDistributedSearchUI uictr = new StartDistributedSearchUI(uiController);
			uictr.run();
		}
	}

    
}
