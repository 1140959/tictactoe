/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.distributedsearch.ui;

import csheets.CleanSheets;
import csheets.ext.distributedsearch.DistributedSearchService;
import csheets.ext.distributedsearch.DistributedSearchMessage;
import csheets.ext.distributedsearch.ReceivedSendDistributedSearchMessagesToJtree;
import csheets.ui.ctrl.UIController;
import java.net.InetAddress;

/**
 *
 * @author Paulo Silva - 1960554@isep.ipp.pt
 */
public class StartDistributedSearchController {

    /**
     * The user interface controller.
     */
    private final UIController uiController;

    /**
     * Class constructor.
     *
     * @param uiController user interface controller
     */
    public StartDistributedSearchController(UIController uiController) {
        this.uiController = uiController;
    }

    /**
     * start chat service
     *
     * @param nickname chat nickname
     * @return true/false
     * @throws Exception exception
     */
    public boolean startDistributedSearch(String nickname) throws Exception {
        DistributedSearchService.getInstance(uiController).
                startConnection(nickname);
        return true;
    }

    /**
     * Get my IP address
     *
     * @return ip address
     */
    public String getMyIpAddress() {
        return DistributedSearchService.getInstance(uiController).getMyIP();
    }

   

    /**
     * Send a message
     *
     * @param ipAddress ip address
     * @param portNumber port number
     * @param textMessage The message
     * @return true/false
     * @throws Exception exception
     */
    public boolean sendMessage(String ipAddress, int portNumber, String textMessage) throws Exception {
        DistributedSearchMessage message = new DistributedSearchMessage(textMessage);
        return DistributedSearchService.getInstance(uiController).sendDistributedSearchMessage(InetAddress.
                getByName(ipAddress), portNumber, message);
    }

    /**
     * Define Tree Messages
     *
     * @param jTree The message tree
     */
    public void setReceivedMessages(ReceivedSendDistributedSearchMessagesToJtree jTree) {
        DistributedSearchService.getInstance(uiController).setReceivedDistributedSearchMessages(jTree);

    }

    public ReceivedSendDistributedSearchMessagesToJtree getReceivedMessages() {
        return DistributedSearchService.getInstance(uiController).getReceivedSendDistributedSearchMessages();
    }

    /**
     * Get chat port
     *
     * @return port
     */
    public int getDistributedSearchPort() {
        return DistributedSearchService.getInstance(uiController).DistributedSearchPort();
    }
}
