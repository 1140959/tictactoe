/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.distributedsearch.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
/**
 *
 * @author Paulo Silva - 1960554@isep.ipp.pt
 */
public class SendDistributedSearchAction  extends BaseAction{
      /**
     * The user interface controller
     */
    protected UIController uiController;

    /**
     * Creates a new action.
     *
     * @param uiController the user interface controller
     */
    public SendDistributedSearchAction(UIController uiController) {
        this.uiController = uiController;
    }

    @Override
    protected String getName() {
        return "Distributes Search Sorkbook";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SendDistributedSearchMessageUI sm = new SendDistributedSearchMessageUI(uiController);
        sm.run();
    }

    
}
