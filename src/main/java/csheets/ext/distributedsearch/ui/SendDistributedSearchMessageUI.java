/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.distributedsearch.ui;

import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
/**
 *
 * @author Paulo Silva - 1960554@isep.ipp.pt
 */
public class SendDistributedSearchMessageUI  {
      protected StartDistributedSearchController distController;
    private JFrame jframe;
    private JTextField ipText;
    private JTextArea messageText;
    private JButton sendButton;

public SendDistributedSearchMessageUI(UIController uiController) {
        this.distController = new StartDistributedSearchController(uiController);

    }



    /**
     * Send message dialog
     */
    protected void run() {
        jframe = new JFrame();
        this.jframe.setTitle("Distributed Search Workbook");
        this.jframe.setSize(350, 300);
        this.jframe.setResizable(false);
        this.jframe.setLayout(new BorderLayout());

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - this.jframe.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - this.jframe.getHeight()) / 2);
        this.jframe.setLocation(x, y);

        JPanel titlePanel = new JPanel(new FlowLayout());
        JLabel titleLabel = new JLabel("Workbook to Search");
        titlePanel.add(titleLabel);
        this.jframe.add(titlePanel, BorderLayout.PAGE_START);

        JPanel infoPanel = new JPanel();
        infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));

        JPanel sendToPanel = new JPanel(new GridLayout(2, 1));
        JLabel sendtoLabel = new JLabel("Distributed Search To");
        sendToPanel.add(sendtoLabel);
        JPanel ipportPanel = new JPanel(new FlowLayout());
        JLabel ipLabel = new JLabel("IP");
        ipportPanel.add(ipLabel);
        this.ipText = new JTextField();
        this.ipText.setPreferredSize(new Dimension(200, 25));
        this.ipText.setEditable(true);
        ipportPanel.add(this.ipText);
        sendToPanel.add(ipportPanel);
        infoPanel.add(sendToPanel);

        JPanel messagePanel = new JPanel();
        messagePanel.setLayout(new BoxLayout(messagePanel, BoxLayout.Y_AXIS));
        JPanel messageTitlePanel = new JPanel(new GridLayout(1, 1));
        JLabel messageLabel = new JLabel("Workboot to search");
        messageTitlePanel.add(messageLabel);
        messagePanel.add(messageTitlePanel);
        this.messageText = new JTextArea(5, 5);
        this.messageText.setEditable(true);
        JScrollPane scroll = new JScrollPane(this.messageText);
        messagePanel.add(scroll);
        infoPanel.add(messagePanel, BorderLayout.CENTER);

        this.jframe.add(infoPanel);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        this.sendButton = new JButton("Start Search");
        buttonPanel.add(this.sendButton);
        this.jframe.add(buttonPanel, BorderLayout.PAGE_END);

        this.jframe.setVisible(true);

        this.sendButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                String ipAddress = "";
                int port = distController.getDistributedSearchPort();
                String message;
                ipAddress = ipText.getText();
                if (ipAddress.compareTo("") == 0) {
                    JOptionPane.showMessageDialog(jframe, "Invalid IP Address!", jframe.
                            getTitle(),
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                message = messageText.getText();
                if (message.compareTo("") == 0) {
                    JOptionPane.showMessageDialog(jframe, "No message to send!!", jframe.
                            getTitle(),
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                try {
                    distController.sendMessage(ipAddress, port, message);
                    jframe.dispose();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(jframe, "Search not send!!!\nAn error occured.\n Probably you or destination not started the search service! "
                           ,jframe.getTitle(),
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        });
    }

}
