/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.distributedsearch.ui;

import csheets.ext.distributedsearch.DistributedSearchExtension;
import csheets.ext.distributedsearch.DistributedSearchService;
import csheets.ext.distributedsearch.ReceivedSendDistributedSearchMessagesToJtree;
import csheets.ui.ctrl.UIController;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
/**
 *
 * @author Paulo Silva - 1960554@isep.ipp.pt
 */
public class DistributedSearchPanel extends JPanel {

    JLabel myIpLabel = new JLabel();
    JLabel myHostName = new JLabel();

    public DistributedSearchPanel(UIController uiController) {
        this.setName(DistributedSearchExtension.NAME);
        this.setLayout(new GridLayout(2, 2));
        final StartDistributedSearchController distController = new StartDistributedSearchController(uiController);
        distController.setReceivedMessages(new ReceivedSendDistributedSearchMessagesToJtree());
        distController.getReceivedMessages().addTreeSelectionListener((TreeSelectionEvent e) -> {
            JTree tree = (JTree) e.getSource();
            DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
            if (selectedNode != null && selectedNode.isLeaf() && selectedNode.getParent() != null) {
                String ipAddress = selectedNode.getParent().toString();
                String messageText = JOptionPane.showInputDialog(null,
                        "Search:", "Search Workbook ", JOptionPane.INFORMATION_MESSAGE);
                if (messageText != null && messageText.compareTo("") != 0) {
                    try {
                        distController.sendMessage(ipAddress, DistributedSearchService.getInstance(uiController).DistributedSearchPort(), messageText);
                        //If success, nothing to say
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, "Error!!!\nSearch not send", "Searc Workbook fail",
                                JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No workbook to search!", "Distributed Search fail",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        JScrollPane pane = new JScrollPane(distController.getReceivedMessages());
        this.add(pane);
        String IpAddress, myHost;
        InetAddress iAddress;
        try {
            iAddress = InetAddress.getLocalHost();
            IpAddress = iAddress.getHostAddress();
        } catch (UnknownHostException ex) {
            IpAddress = "Ip Address not found";
        }
        try {
            iAddress = InetAddress.getLocalHost();
            myHost = iAddress.getHostName();
        } catch (UnknownHostException ex) {
            myHost = "Host not found!!";
        }
        JPanel commentPanel = new JPanel();
        commentPanel.setLayout(new BoxLayout(commentPanel, BoxLayout.PAGE_AXIS));
        commentPanel.setPreferredSize(new Dimension(130, 336));
        commentPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));		// width, height
        myHostName.setText("Host: " + myHost);
        myIpLabel.setText("My ip: " + IpAddress);
        myHostName.setPreferredSize(new Dimension(20, 50));
        myHostName.setLocation(50, 50);
        myIpLabel.setPreferredSize(new Dimension(20, 50));
        myIpLabel.setLocation(80, 50);
        commentPanel.add(myHostName);
        commentPanel.add(myIpLabel);
        this.add(commentPanel);

    }
}

