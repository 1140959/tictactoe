/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.distributedsearch.ui;
import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author Paulo Silva - 1960554
 */
public class DistributedSearchMenu extends JMenu{
    /**
     * Constructor of the class
     * @param uiController uiController
     */
    public DistributedSearchMenu(UIController uiController) {
        super("DistributedSearch");
        setMnemonic(KeyEvent.VK_S);

        // Adds font actions
        add(new StartDistributedSearchAction(uiController));
        add(new SendDistributedSearchAction(uiController));
    }
    
    
    
}
