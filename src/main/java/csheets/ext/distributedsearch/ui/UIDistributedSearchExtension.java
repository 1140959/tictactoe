/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.distributedsearch.ui;

import csheets.ext.distributedsearch.DistributedSearchExtension;
import csheets.ext.contacts.ContactsExtension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JMenu;
/**
 *
 * @author Paulo Silva 1960554@isep.ipp.pt
 */
public class UIDistributedSearchExtension  extends UIExtension {
   
    /**
     * A menu that provides Funcion Wizard
     */
    private DistributedSearchMenu menu;

    /**
     * The icon to display with the extension's name
     */
    //private Icon icon;

    private DistributedSearchPanel sideBar;

    public UIDistributedSearchExtension(DistributedSearchExtension extension, UIController uiController) {
        super(extension, uiController);
    }

    /**
     * Returns an instance of a class that implements JMenu. In this simple case
     * this class only supplies one menu option.
     *
     * @return a JMenu component
     */
    @Override
    public JMenu getMenu() {
        if (menu == null) {
            menu = new DistributedSearchMenu(uiController);
        }
        return menu;
    }

    /**
     * Returns a side bar that gives access to extension-specific functionality.
     *
     * @return a component, or null if the extension does not provide one
     */
    @Override
    public JComponent getSideBar() {
        if (sideBar == null) {
            sideBar = new DistributedSearchPanel(uiController);
        }
        return sideBar;
    }
    
//     @Override
//    public Icon getIcon() {
//        if (icon == null) {
//            icon = new ImageIcon(DistributedSearchExtension.class.getResource("res/img/chat_icon.gif"));
//        }
//        return icon;
//    }

}


