/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importexporttext;

import csheets.core.Cell;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ui.ctrl.UIController;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class responsible to execute the import text process
 * @author Pedro
 */
public class ImportTThread implements Runnable{
    
    private IETInfo importTextInfo;
    private UIController uiController;
    private Cell[][] rangeCell;
    
    /*
     *Constructor
     * @param importTextInfo - the information needed to export process using a thread
     * @param uiController - the user interface controller
     */
    public ImportTThread(IETInfo importTextInfo, UIController uiController) {
        this.importTextInfo = importTextInfo;
        this.uiController = uiController;
 //        this.rangeCell = rangeCell;
    }

    /*
    * Method responsible to execute the import text process
    */
    @Override
    public void run() {
        String splitBy = importTextInfo.ColumnSeparator();
        BufferedReader bufferReader = null;
        String line = "";
        String headerOption = importTextInfo.headerOption();

        try {

            bufferReader = new BufferedReader(new FileReader(importTextInfo.File()));
//            int row = 0;
//            int nextColumn = rangeCell[0][0].getAddress().getColumn();
//            int nextRow = rangeCell[0][0].getAddress().getRow();
//            Cell firstCell = uiController.getActiveCell();
            int nextColumn = uiController.getActiveCell().getAddress().getColumn();
            int nextRow = uiController.getActiveCell().getAddress().getRow();
            Cell firstCell = uiController.getActiveCell();
            Cell nextCell;
            if(headerOption.equals("No"))
                        nextRow++;
            while ((line = bufferReader.readLine()) != null ) {

                String splitedLine[] = line.split(splitBy);
                
                for (int i = 0; i < splitedLine.length ; i++) {

//                    rangeCell[row][i].setContent(splitedLine[i]);
                    uiController.getActiveCell().setContent(splitedLine[i]);
                    nextColumn++;

                    nextCell = uiController.getActiveSpreadsheet().getCell(nextColumn, nextRow);
                    uiController.setActiveCell(nextCell);

                }
//                row++;
                nextColumn = firstCell.getAddress().getColumn();
                nextRow++;
                nextCell = uiController.getActiveSpreadsheet().getCell(nextColumn, nextRow);
                uiController.setActiveCell(nextCell);

            }
            bufferReader.close();
            Thread.currentThread().wait(10000);
            Thread.currentThread().interrupt();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImportTThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ImportTThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FormulaCompilationException ex) {
            Logger.getLogger(ImportTThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(ImportTThread.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (bufferReader != null) {
                try {

                    bufferReader.close();
//                    Thread.currentThread().wait(10000);
                    Thread.currentThread().interrupt();

                } catch (IOException ex) {
                    Logger.getLogger(ImportTThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
}
