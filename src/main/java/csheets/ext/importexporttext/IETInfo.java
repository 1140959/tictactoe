/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importexporttext;

import java.io.File;

/*
 * This class contains all the information about
 * import or export text operation. Works like a value object.
 * @author Pedro
 */
public class IETInfo {
    
    /*
     * Constructor.
     *
     * @param columnSeparator is the char that will be used as a column
     * separator
     * @param importExportFile is the file to be imported or exported.
     * @param headerOption is the selected option if the header is
     * included or not.
     */
    private String columnSeparator;
    private String headerOption;
    private File file;
    
    public IETInfo(String columnSeparator, File importExportFile, String headerOption) {

        this.columnSeparator = columnSeparator;
        this.file = importExportFile;
        this.headerOption = headerOption;
    }
    
    /*
     * Method that returns the column separator choosed by user
     *
     * @return selected column separator
     */
    public String ColumnSeparator() {
        return columnSeparator;
    }
    
    /**
     * Method that returns the file chose by user
     *
     * @return selected file
     */
    public File File() {
        return file;
    }
    
    /*
     * Method that returns the header option
     *
     * @return selected header option
     */
    public String headerOption() {
        return headerOption;
    }
    
}
