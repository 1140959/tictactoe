/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importexporttext;

import csheets.core.Cell;
import csheets.ui.ctrl.FocusOwnerAction;
import java.awt.event.ActionEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.util.SortedSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class responsible to execute the export text process
 * @author Pedro
 */
public class ExportTThread implements Runnable {
    
    private IETInfo exportTextInfo;
    private Cell[][] cellSelected;
     private SortedSet<Cell> cellRange;
    
    /*
     *Constructor
     * @param exportTextInfo the information needed to export process using a thread
     * @param cellRange cells to export
     */
    public ExportTThread(IETInfo exportTextInfo, SortedSet<Cell> cellRange, Cell[][]sel) {
        this.exportTextInfo = exportTextInfo;
        this.cellRange = cellRange;
        this.cellSelected = sel;
    }
    
    /*
    * Method responsible to execute the export text process
    */
    @Override
    public void run() {

        FileWriter writer = null;
        SortedSet<Cell> tmpCellRange = this.cellRange;

        int columns = (tmpCellRange.last().getAddress().getColumn()) - (tmpCellRange.first().getAddress().getColumn()) + 1;
        int lines = (tmpCellRange.last().getAddress().getRow()) - (tmpCellRange.first().getAddress().getRow());

        try {

            writer = new FileWriter(exportTextInfo.File());
            
            for (int i = 0; i <= lines ; i++) {
                for (int j = 0; j <= columns; j++) {
                    
                    writer.append(tmpCellRange.first().getContent());
                    writer.append(exportTextInfo.ColumnSeparator());
                    writer.append('\n');
                    tmpCellRange.remove(tmpCellRange.first());
                }
            }

            writer.flush();
            writer.close();
            Thread.currentThread().interrupt();
            
        } catch (IOException ex) {
            Logger.getLogger(ExportTThread.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(ExportTThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
