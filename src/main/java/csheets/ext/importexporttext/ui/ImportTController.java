/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importexporttext.ui;

import csheets.ext.importexporttext.IETInfo;
import csheets.ext.importexporttext.ImportTThread;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.io.File;

/**
 *
 * @author Pedro
 */
public class ImportTController extends FocusOwnerAction {
    
    UIController uiController;
    private IETInfo importTextInfo;
    private File newFile;
    private String columnSeparator;
    private String headerOption;
    
    /**
     * Constructor
     * 
     * @param columnSeparator selected column separator
     * @param fileLocation selected file location.
     * @param headerOption selected header include option
     * @param uiController the UIController
     */
    public ImportTController(String columnSeparator, String fileLocation, String headerOption, UIController uiController) {

        this.newFile = new File(fileLocation);
        this.columnSeparator = columnSeparator;
        this.headerOption = headerOption;
        this.uiController = uiController;
        this.importTextInfo = new IETInfo(this.columnSeparator, this.newFile, this.headerOption);
     }
     
    /**
    * Method responsible to execute the import process 
    */
    public void ImportTextProcess() {
        
//        Cell[][] cellRange = focusOwner.getSelectedCells();
        Runnable tImportText = new ImportTThread(this.importTextInfo, this.uiController);
        Thread threadImport = new Thread(tImportText, "import text");
        
        threadImport.start();
    }

    @Override
    protected String getName() {
        return "";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }
}
