/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importexporttext.ui;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.ext.importexporttext.ExportTThread;
import csheets.ext.importexporttext.IETInfo;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.SortedSet;

/**
 *
 * @author Pedro
 */
public class ExportTController extends FocusOwnerAction {
    
    UIController uiController;
    private IETInfo exportTextInfo;
    private File newFile;
    private String columnSeparator;
    private String headerOption;
    private Cell [][] cellSelected;
    private SortedSet<Cell> cellRange;
    
    /**
     * Constructor
     * 
     * @param columnSeparator selected column separator
     * @param fileLocation selected file location
     * @param headerOption selected header include option
     * @param uiController the UIController UI user interface
     */
    public ExportTController(String columnSeparator, String fileLocation, String headerOption, UIController uiController) {

        this.newFile = new File(fileLocation);
        this.columnSeparator = columnSeparator;
        this.headerOption = headerOption;
        this.uiController = uiController;
        this.exportTextInfo = new IETInfo(this.columnSeparator, this.newFile, this.headerOption);
//        this.cellSelected = this.focusOwner.getSelectedCells();
    }
    
    /**
    * Method responsible to execute the export process 
    */
    public void ExportTextProcess() {

        Address address = uiController.getActiveCell().getAddress();
        cellRange = this.getCellRange(address);
        
        Runnable threadExpText = new ExportTThread(this.exportTextInfo, this.cellRange, this.cellSelected);
        Thread threadExp = new Thread(threadExpText, "exporting text");
        threadExp.start();
    }

    /**
     * Method to determinate the cell range needed to  fill all the file content.
     * It uses two methods to find the first blanc column and line. With the 
     * returned adresses it creates e new cell which will be the last cell. 
     * With the UIController's method getCells() its possible to get the cell 
     * range.
     *
     * @param firstCellAdress active cell 
     * @return the cell range
     */
    private SortedSet<Cell> getCellRange(Address beginCellAd) {

        int lastCol = this.lastColumn(beginCellAd).getColumn();
        int lastLine = this.lastLine(beginCellAd).getRow();

        Address lastCellAd = new Address(lastCol, lastLine);

        cellRange = uiController.getActiveSpreadsheet().getCells(beginCellAd, lastCellAd);

        return cellRange;
    }

    /*
    * Method responsible to get the last column not empty
    */
    private Address lastColumn(Address beginCellAd) {

        Address cellAd = beginCellAd;
        Cell nextCell;
        boolean search = true;
        int i = 1;
        int column = beginCellAd.getColumn();
        int nextColumn = beginCellAd.getColumn() + i;
        int line = beginCellAd.getRow();
        nextCell = uiController.getActiveSpreadsheet().getCell(nextColumn, line);

        while (search) {

            if (!(nextCell.getContent().isEmpty())) {
                i++;
                nextColumn = column + i;
                nextCell = uiController.getActiveSpreadsheet().getCell(nextColumn, line);

            } else {
                search = false;
                nextColumn = nextColumn - 1;
                cellAd = new Address(nextColumn, line);
            }
        }       
        return cellAd;
    }

    /*
    * Method responsible to get the last line not empty
    */
    private Address lastLine(Address beginCellAd) {

        Address cellAd = beginCellAd;
        Cell nextCell;
        boolean search = true;
        int i = 1;
        int column = beginCellAd.getColumn();
        int line = beginCellAd.getRow();
        int nextLine = beginCellAd.getRow() + i;
        nextCell = uiController.getActiveSpreadsheet().getCell(column, nextLine);

        while (search) {

            if (!(nextCell.getContent().isEmpty())) {
                i++;
                nextLine = line + i;
                nextCell = uiController.getActiveSpreadsheet().getCell(column, nextLine);

            } else {
                search = false;
                nextLine = nextLine - 1;
                cellAd = new Address(column, nextLine);
            }
        }
        return cellAd;
    }
    
    @Override
    protected String getName() {
        return "";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }
}
