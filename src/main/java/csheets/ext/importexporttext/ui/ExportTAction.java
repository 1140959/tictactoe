/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importexporttext.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author Pedro
 */
public class ExportTAction extends BaseAction {
    
    /* 
     * The user interface controller 
     */
    protected UIController uiController;
    
    ExportTAction(UIController uiController) {

        this.uiController = uiController;
    }

    @Override
    protected String getName() {
        return "Export from Text File";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ExportTUI exportUI = new ExportTUI(uiController);
        exportUI.setVisible(true);
    }
    
}

