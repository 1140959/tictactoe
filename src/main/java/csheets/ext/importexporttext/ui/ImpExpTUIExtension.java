/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importexporttext.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.JMenu;

/**
 *
 * @author Pedro
 */
public class ImpExpTUIExtension extends UIExtension {
    
    /**
     * The menu of the extension
     */
    private ImpExpTMenu menu;
    
    public ImpExpTUIExtension(Extension extension, UIController uiController) {
        super(extension, uiController);
    }
    
    public JMenu getMenu() {
	if (menu == null) {
            menu = new ImpExpTMenu(uiController);
	}
	return menu;
    }
}

