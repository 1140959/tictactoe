/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importexporttext.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author Pedro
 */
public class ImpExpTMenu extends JMenu {
    
    /**
     * Constructor of the class
     * @param uiController uiController
     */
    public ImpExpTMenu(UIController uiController) {
        super("Import / Export Text");
        setMnemonic(KeyEvent.VK_I);
        // Adds font actions
        add(new ImportTAction(uiController));
        add(new ExportTAction(uiController));
    }
}
