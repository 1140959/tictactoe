/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importexporttext.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author Pedro
 */
public class ImportTAction extends BaseAction{
    
    /**
     * The user interface controller
     */
    protected UIController uiController;

    ImportTAction(UIController uiController) {
        this.uiController = uiController;
    }

    @Override
    protected String getName() {
       return "Import to Text File";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ImportTUI importUI = new ImportTUI(uiController);
        importUI.setVisible(true);
    }
    
}
