/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importexporttext;

import csheets.ext.Extension;
import csheets.ext.importexporttext.ui.ImpExpTUIExtension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * Class responsible to create a import and export text extension
 * @author Pedro
 */
public class IETExtension extends Extension {

    /**
     * The version
     */
    public static final Long VERSION = 1L;

    /**
     * Description
     */
    public static final String DESCRIPTION = "Import and Export your data";

    /*
    * Name 
    */
    public static final String NAME = "Import/Export Text";

    /*
    * Constructor
    */
    public IETExtension() {
        super(VERSION, NAME, DESCRIPTION);
    }

    @Override
    public UIExtension getUIExtension(UIController uiController) {
        return new ImpExpTUIExtension(this, uiController);
    }
}
