package csheets.ext;

import java.util.Enumeration;
import java.util.List;
import javax.swing.*;

/**
 * The controller for choosing extensions for loading
 *
 * @author Tiago Gabriel 1140775
 */
public class ChoosingExtensionsController {

    /**
     * All available extension. Loaded from a file of properties
     */
    private final List<Extension> availableExtensions;

    /**
     * The extensions chosen by the user to load
     */
    private final List<Extension> extensionsToLoad;

    /**
     * Model of the JList of all available extensions
     */
    private DefaultListModel<Extension> allExtensionsModel;

    /**
     * Model of the JList for chosen exceptions
     */
    private DefaultListModel<Extension> chosenExtensionsModel;

    /**
     * Default constructor
     *
     * @param availableExtensions All available extensions loaded from the files
     * @param extensionsToLoad List to be filled
     */
    public ChoosingExtensionsController(List<Extension> availableExtensions, List<Extension> extensionsToLoad) {
        this.availableExtensions = availableExtensions;
        this.extensionsToLoad = extensionsToLoad;
    }

    /**
     * Set the models of JLists
     *
     * @param allExtensions JList of all extensions
     * @param chosenExtensions JList of all chosen extensions
     */
    protected void setModel(ListModel<Extension> allExtensions, ListModel<Extension> chosenExtensions) {
        this.allExtensionsModel = (DefaultListModel<Extension>) allExtensions;
        this.chosenExtensionsModel = (DefaultListModel<Extension>) chosenExtensions;
        refreshListModel();
    }

    /**
     * Refreshes the model of JList of the frame
     */
    private void refreshListModel() {
        for (Extension e : this.availableExtensions) {
            allExtensionsModel.addElement(e);
        }
    }

    /**
     * Selects a given extension
     *
     * @param index Index of the chosen extension
     */
    void selectExtension(int index) {
        if(index == -1) return;
        Extension remove = this.allExtensionsModel.remove(index);
        if (remove != null) {
            this.chosenExtensionsModel.addElement(remove);
        }
    }

    /**
     * Deselects a given extension from the selected extensions
     *
     * @param index Index of the extension
     */
    void deselectExtension(int index) {
        if(index == -1) return;
        int size = this.chosenExtensionsModel.size();
        if (size == 0) {
            return;
        }
        for (int i = 0; i < size; i++) {
            if (i == index) {
                Extension ext = this.chosenExtensionsModel.remove(i);
                this.allExtensionsModel.addElement(ext);
            }
        }
    }

    /**
     * Selects all the extension
     */
    void selectAll() {
        int size = this.allExtensionsModel.size();
        Extension ext;
        while (size != 0) {
            ext = this.allExtensionsModel.remove(0); //removes the first
            this.chosenExtensionsModel.addElement(ext);
            size--;
        }
    }

    /**
     * Deselects all of the extensions
     */
    void deselectAll() {
        int size = this.chosenExtensionsModel.size();
        Extension ext;
        while (size != 0) {
            ext = this.chosenExtensionsModel.remove(0);
            this.allExtensionsModel.addElement(ext);
            size--;
        }
    }

    /**
     * Loads the extensions to the final list to be load by Extensions Manager
     *
     * @return True if there are extensions to load, false otherwise
     */
    boolean loadExtensions() {
        Enumeration<Extension> chosenElements = chosenExtensionsModel.elements();

        while (chosenElements.hasMoreElements()) {
            Extension ext = chosenElements.nextElement();
            this.extensionsToLoad.add(ext);
        }
        return true;
    }

}
