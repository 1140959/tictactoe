/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;

import csheets.ext.contacts.ui.panels.EventPopupPanel;
import csheets.persistence.RepositoryEntity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Calendar;

/**
 * Agenda's Events
 *
 *  @author Created by Sérgio Silva (1141074)
 */
public class Event implements RepositoryEntity<Event,Long> {

    /**
     * This class repository id;
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    /**
     * This event's description
     */
    private String description;

    /**
     * This event's timestamp
     */
    private Calendar timestamp;

    public Event() {
    }

    /**
     * The complete constructor
     *
     * @param description Description of this event
     * @param timestamp Timestamp
     */
    public Event(String description, Calendar timestamp) {
        this.description = description;
        this.timestamp = timestamp;
    }

    /**
     * Gets the information for edition of the panel
     *
     * @param thePanel My panel
     */
    public void retrieveInformation(EventPopupPanel thePanel) {
        thePanel.setDescription(description);
        thePanel.setDatePicker(timestamp);
    }

    /**
     * Edit this event
     *
     * @param newEvent This event
     * @return Edited event
     */
    public Event editThisEvent(Event newEvent) {
        this.description = newEvent.description;
        this.timestamp = newEvent.timestamp;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        Event event = (Event) o;
        return (this.description.equals(event.description) && this.timestamp.equals(event.timestamp));
    }

    @Override
    public int hashCode() {
        int result = description.hashCode();
        result = 31 * result + timestamp.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("Description: %s \n Date: %d/%d/%d",
                this.description,
                timestamp.get(Calendar.DATE),
                timestamp.get(Calendar.MONTH) + 1,
                timestamp.get(Calendar.YEAR));
    }

    /**
     * Gets the id of this entity on the repository
     *
     * @return The primary key
     */
    @Override
    public Long id() {
        return id;
    }

    /**
     * Checks if this entity and the other are the same in the repository
     *
     * @param otherEntity Entity to Compare
     * @return True or false
     */
    @Override
    public boolean sameHas(Event otherEntity) {
        return this.id==otherEntity.id;
    }

    /**
     * Sets the entity's id
     *
     * @param id Primary key created
     */
    @Override
    public void setID(Long id) {
        this.id=id;
    }
}
