/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;


/**
 * A service for contacts use case
 * @author Tiago Gabriel 1140775
 * @param <T> The type of object to apply the service
 * @param <C> The information of the object to create
 */
public interface ManagingContactsService<T,C> {
    
    /** Creates a new entity
     * @param theInfo information for the creation
     * @return  Object created
     * @throws java.lang.Exception*/
    T create(C theInfo) throws Exception;
    
    /** Edits a given entity
     * @param toEdit Object to edit
     * @param newInfo New info
     * @return An object
     * @throws java.lang.Exception
     */
    T edit(T toEdit, C newInfo) throws Exception;
    
    /** Removes a given entity
     * @param toRemove Object to remove
     * @return true if successfully removed */
    boolean remove(T toRemove);
}
