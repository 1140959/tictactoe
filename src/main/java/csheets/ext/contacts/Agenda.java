/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;

import csheets.persistence.RepositoryEntity;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * The contact's agenda
 *
 * @author Tiago Gabriel 1140775
 */
@Entity
public class Agenda implements RepositoryEntity<Agenda,Long> {

    /**
     * This class repository id;
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    /**
     * The events related to my agenda
     */
    @OneToMany(cascade = CascadeType.ALL)
    private List<Event> myEvents;

    public Agenda() {
    }

    /**
     * Complete constructor
     *
     * @param myEvents Iterable of events
     */
    public Agenda(List<Event> myEvents) {
        this.myEvents = myEvents;
    }

    /**
     * Returns all events
     *
     * @return Iterable of events
     */
    public Iterable<Event> getMyEvents() {
        return myEvents;
    }

    /**
     * Saves a new event to this agenda
     *
     * @param newEvent new Event to add
     * @return true if successfully added
     */
    public boolean save(Event newEvent) {
        return this.myEvents.add(newEvent);
    }

    /**
     * Removes an Event from the list
     * @param event Event to remove
     * @return true or false
     */
    public boolean remove(Event event) {
        return this.myEvents.remove(event);
    }

    public void removeALL() {
        this.myEvents = new LinkedList<>();
    }

    /**
     * Gets the id of this entity on the repository
     *
     * @return The primary key
     */
    @Override
    public Long id() {
        return id;
    }

    /**
     * Checks if this entity and the other are the same in the repository
     *
     * @param otherEntity Entity to Compare
     * @return True or false
     */
    @Override
    public boolean sameHas(Agenda otherEntity) {
        return this.id==otherEntity.id;
    }

    /**
     * Sets the entity's id
     *
     * @param id Primary key created
     */
    @Override
    public void setID(Long id) {
            this.id=id;
    }
}
