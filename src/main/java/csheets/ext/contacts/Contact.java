/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;

import csheets.ext.address.AddressContact;
import csheets.ext.address.AddressType;
import csheets.ext.contacts.ui.ContactDTO;
import csheets.ext.contacts.ui.panels.CompanyContactPopupPanel;
import csheets.ext.contacts.ui.panels.PersonalContactPopupPanel;
import csheets.ext.managephoneemail.PhoneBook;
import csheets.ext.notes.Note;
import csheets.persistence.RepositoryEntity;

import javax.persistence.*;
import java.io.IOException;
import java.util.List;

/**
 * This is the Domain class for Contacts
 *
 * @author Tiago Gabriel 1140775
 */
@Entity
@Table(name="CONTACT")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "CONTACTTYPE")
public abstract class Contact implements RepositoryEntity<Contact, Long> {

    /**
     * This class repository id;
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    /**
     * This contact's agenda
     */
    private Agenda myAgenda;

    /**
     * This contact's address
     */
    private AddressContact myPrimaryAddress;

    /**
     * This contact's address
     */
    private AddressContact mySecondaryAddress;

    /**
     * This contact's list notes
     */
    @OneToMany(cascade = CascadeType.ALL)
    private List<Note> listNote;
    
    private PhoneBook phoneBook;

    /**
     * Empty Contructor
     */
    public Contact() {
    }

    /**
     * The Contact's constructor
     *
     * @param myAgenda This is the Agenda
     */
    public Contact(Agenda myAgenda) {
        this.myAgenda = myAgenda;
    }

    /**
     * @param myAgenda The Agenda
     * @param myPrimaryAddress The primary Address
     */
    public Contact(Agenda myAgenda, AddressContact myPrimaryAddress) {
        this.myAgenda = myAgenda;
        this.myPrimaryAddress = myPrimaryAddress;
    }

    /**
     * @param myAgenda The Agenda
     * @param myPrimaryAddress The primary Address
     * @param mySecondaryAddress The secondary Address
     */
    public Contact(Agenda myAgenda, AddressContact myPrimaryAddress, AddressContact mySecondaryAddress) {
        this.myAgenda = myAgenda;
        this.myPrimaryAddress = myPrimaryAddress;
        this.mySecondaryAddress = mySecondaryAddress;
    }

    /**
     * This contact's agenda
     *
     * @return The contact's agenda
     */
    public Agenda myAgenda() {
        return myAgenda;
    }

    /**
     * This contact's Adress
     *
     * @return The contact's primary address
     */
    public AddressContact myPrimaryAdress() {
        return myPrimaryAddress;
    }

    /**
     * This contact's Adress
     *
     * @return The contact's secondary address
     */
    public AddressContact mySecondaryAddress() {
        return mySecondaryAddress;
    }

    public List<Note> notes() {
        return listNote;
    }

    public void alterList(List<Note> notelist){
        this.listNote=notelist;
    }
    
    /**
     * Retrieves the information to the popup panel
     *
     * @param panel The popup panel for retrieving the information about the contacts
     * @throws Exception if it cant get the info.
     */
    public abstract void retrieveInformation(PersonalContactPopupPanel panel) throws Exception;
    public abstract void retrieveInformation(CompanyContactPopupPanel panel) throws Exception;

    /**
     * Procedes to the edition of this contact
     *
     * @param newInfo New informations
     * @return New edited contact
     */
    abstract Contact editThisContact(ContactDTO newInfo) throws IOException;
    public abstract void retrieveName(List<CompanyContact> contactNames);
    public abstract void retrieveNameString(List<String> contactNames);
    
    /**
     * 
     * @param addr Address
     * @param type Type of address
     */
    public void setAnAddress(AddressContact addr, AddressType type) {
        if (type == AddressType.PRIMARY) {
            myPrimaryAddress = addr;
        }
        if (type == AddressType.SECONDARY) {
            mySecondaryAddress = addr;
        }
    }

    public abstract String getName();
    
    public abstract PhoneBook getPhoneBook();
    public abstract void setPhoneBook(PhoneBook book);

}
