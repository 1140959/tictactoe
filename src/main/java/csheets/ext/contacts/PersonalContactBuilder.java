/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;

import csheets.ext.contacts.ui.panels.ImageCopy;

import java.io.File;
import java.util.LinkedList;

/**
 * Created by Sérgio Silva (1141074) on 04/06/2016.
 */
public class PersonalContactBuilder {

    private String firstName;
    private String lastName;
    private Photograph myPhoto;
    private CompanyContact company;
    private Profession profession;

    public PersonalContactBuilder setFirstName(String firstName) throws Exception {
        if (firstName != null && firstName.length() > 2) {
            this.firstName = firstName;
            return this;
        }
        throw new Exception("First name is empty!\nPlease insert a first name with at least 3 characters.");
    }

    public PersonalContactBuilder setLastName(String lastName) throws Exception {
        if (lastName != null && lastName.length() > 2) {
            this.lastName = lastName;
            return this;
        }
        throw new Exception("Last name is empty!\nPlease insert a last name with at least 3 characters.");
    }

    public PersonalContactBuilder setCompany(CompanyContact company) throws Exception {
        //if (company != null) {
            this.company = company;
            return this;
        //}
        //throw new Exception("Company is empty!\nPlease insert a Company.");
    }

    public PersonalContactBuilder setProfession(Profession profession) throws Exception {
        //if (profession != null) {
            this.profession = profession;
            return this;
        //}
        //throw new Exception("Company is empty!\nPlease insert a Company.");
    }

    public PersonalContactBuilder setMyPhoto(File foto) throws Exception {
        if (foto != null && foto.getName() != null) {
            File temp = File.createTempFile("file", ".tmp" );
            String absolutePath = temp.getAbsolutePath();
            String filePath = absolutePath.
                    substring(0,absolutePath.lastIndexOf(File.separator));
            String path = filePath + "/csheets/";
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdir();
            }
            filePath = path + firstName + lastName + "/";
            dir = new File(filePath);
            if (!dir.exists()) {
                dir.mkdir();
            }
            String photo = filePath + foto.getName();
            File newFile = new File(photo);
            new ImageCopy().CopyFile(foto, newFile);
            this.myPhoto = new Photograph(photo);
            return this;
        }
        throw new Exception("No photo uploaded!\nPlease select a photo.");
    }

    public PersonalContact createPersonalContact() {
        if (firstName != null && lastName != null && myPhoto != null) {
            return new PersonalContact(firstName, lastName, myPhoto,company,profession, new Agenda(new LinkedList<>()));
        }
        return null;
    }
}
