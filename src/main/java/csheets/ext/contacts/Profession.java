/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;

import csheets.persistence.RepositoryEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Sérgio Silva (1141074) on 07/06/2016.
 */
@Entity
public class Profession implements RepositoryEntity<Profession,Long>{

    /**
     * This class repository id;
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    private String professionName;

    public Profession() {
    }

    public Profession(String professionName) {
        this.professionName=professionName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Profession)) return false;

        Profession that = (Profession) o;

        return professionName != null ? professionName.equals(that.professionName) : that.professionName == null;

    }

    @Override
    public int hashCode() {
        return professionName != null ? professionName.hashCode() : 0;
    }

    @Override
    public String toString() {
        return professionName;
    }

    /**
     * Gets the id of this entity on the repository
     *
     * @return The primary key
     */
    @Override
    public Long id() {
        return this.id;
    }

    /**
     * Checks if this entity and the other are the same in the repository
     *
     * @param otherEntity Entity to Compare
     * @return True or false
     */
    @Override
    public boolean sameHas(Profession otherEntity) {
        return this.id() == otherEntity.id();
    }

    /**
     * Sets the entity's id
     *
     * @param id Primary key created
     */
    @Override
    public void setID(Long id) {
        this.id = id;
    }
}
