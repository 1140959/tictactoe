/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts.ui;

import csheets.ext.contacts.*;
import csheets.ext.contacts.ui.panels.CompanyContactPopupPanel;
import csheets.ext.contacts.ui.panels.PersonalContactPopupPanel;
import csheets.persistence.ContactRepository;
import csheets.persistence.PersistenceContext;

import javax.swing.*;
import java.io.File;
import java.util.Calendar;
import java.util.Objects;

/**
 * Created by Sérgio Silva (1141074) on 04/06/2016.
 */

/*
this is the unique controller for the Contacts/Agenda extension.
 */
public class ContactController {

    /**
     * The Controller for this extension
     */
    private final ContactsExtensionUI theUIExtension;

    /**
     * The selected Contact
     */
    private Contact selectedContact;

    /**
     * The selected Event
     */
    private Event selectedEvent;



    /**
     * The controller extension
     *
     * @param theUIExtension The main UI extension
     */
    public ContactController(ContactsExtensionUI theUIExtension) {
        this.theUIExtension = theUIExtension;
        this.selectedContact = theUIExtension.getTabContacts().getSelectedContact();
        this.selectedEvent = theUIExtension.getTabAgenda().getSelectedEvent();
    }

    /**
     * Validate if the logged contact is an existing contact
     *
     * @return A contact validated
     */
    public Contact validateContact() {
        final String loggedUser = SystemUser.getInstance().myName();
        ContactRepository contacts = PersistenceContext.repositories().contacts();
        Contact theSystemUser = contacts.findBySystemUser(loggedUser);
        if (theSystemUser != null) {
            setMainUserContact(theSystemUser);
            return theSystemUser;
        }
        return null;
    }

    /**
     * Sets the main user contact
     *
     * @param theContact
     */
    private void setMainUserContact(Contact theContact) {
        this.theUIExtension.setUserContact(theContact);
    }

    /**
     * Gets the system name
     *
     * @return First and last name if exists
     */
    public String[] getSystemName() {
        final String loggedUser = SystemUser.getInstance().myName();
        return loggedUser.split(" ");
    }

    /**
     * Creates a new personal contact.. If it's valid, saves it to the
     * repository
     *
     * @param firstName Contact's first name
     * @param lastName Contact's Last Name
     * @param photo The absolute path of the photo
     * @return A new Contact
     * @throws Exception
     */
    public Contact createPersonalContact(String firstName, String lastName, File photo, CompanyContact cc, Profession prof) throws Exception {
        ContactDTO contDto = new ContactDTO(firstName, lastName, photo, cc, prof, true);
        ManagingContactsService<Contact, ContactDTO> contServ = new ContactService();
        Contact contact = contServ.create(contDto);
        ContactRepository contacts = PersistenceContext.repositories().contacts();
        return contacts.save(contact);
    }

    public Contact createCompanyContact(String companyName) throws Exception {
        ContactDTO contDto = new ContactDTO(companyName, false);
        ManagingContactsService<Contact, ContactDTO> contServ = new ContactService();
        final Contact contact = contServ.create(contDto);
        ContactRepository contacts = PersistenceContext.repositories().contacts();
        return contacts.save(contact);
    }

    /**
     * Edits a personal contact
     * @param contactPanel Panel popup
     * @param firstName Contact first names
     * @param lastName Contact last name
     * @param photo The photograph
     * @throws Exception 
     */
    public void editPersonalContact(PersonalContactPopupPanel contactPanel, String firstName, String lastName, File photo, CompanyContact cc, Profession prof) throws Exception {
        ContactDTO contDto = new ContactDTO(firstName, lastName, photo,cc,prof,true);
        ManagingContactsService<Contact, ContactDTO> contServ = new ContactService();
        contServ.edit(selectedContact, contDto);
    }

    public void editCompanyContact(CompanyContactPopupPanel ccpl, String name) throws Exception {
	ContactDTO contDto = new ContactDTO(name, false);
	ManagingContactsService<Contact,ContactDTO> contServ = new ContactService();
	contServ.edit(selectedContact, contDto);
    }

    /**
     * Removes the selected personal contact
     */
    public void removePersonalContact() {
        ManagingContactsService<Contact, ContactDTO> contServ = new ContactService();
        contServ.remove(selectedContact);
        DefaultListModel<Contact> model = (DefaultListModel) theUIExtension.getTabContacts().getPanelList().getModel();
        model.removeElement(selectedContact);
        if (Objects.equals(selectedContact, theUIExtension.getMainUserContact())) {
            theUIExtension.setUserContact(null);
        }
        selectedContact = null;
    }

    public void removeCompanyContact() {
    }

    


    /**
     * Create a new Event
     *
     * @param description Event's description
     * @param timestamp Event´s time stamp
     * @return An event created
     * @throws Exception
     */
    public Event createEvent(String description, Calendar timestamp) throws Exception {
        final EventDTO eventDto = new EventDTO(description, timestamp);
        final EventService evServ = new EventService();
        final Event event = evServ.create(eventDto);
        return event;
    }

    /**
     * Saves an event
     * @param ev Event to save
     * @return true if successfully saved
     */
    public boolean saveEvent(Event ev) {
        if (!this.selectedContact.myAgenda().save(ev)) {
            return false;
        }
        if (Objects.equals(selectedContact, theUIExtension.getMainUserContact())) {
            schedule(ev);
        }
        this.theUIExtension.getTabAgenda().refreshPanelList();
        return true;
    }

    private void schedule(Event ev) {

    }

    /**
     * Edits a given event
     * @param description Event's description
     * @param timestamp The event's timestamp
     * @throws Exception 
     */
    public void editEvent(String description, Calendar timestamp) throws Exception {
        final EventDTO dto = new EventDTO(description, timestamp);
        final EventService evServ = new EventService();
        evServ.edit(selectedEvent, dto);
        this.theUIExtension.getTabAgenda().refreshPanelList();
    }

    /**
     * Removes an event from the agenda
     */
    public void removeEvent() {
        this.selectedContact.myAgenda().remove(selectedEvent);
        this.theUIExtension.getTabAgenda().refreshPanelList();
    }

    public Iterable<Contact> allContact() {
        return PersistenceContext.repositories().contacts().all();
    }
}
