/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts.ui;

/**
 * This class is a singleton for the system user of the local app
 * @author Tiago Gabriel 1140775
 */
public class SystemUser {
    
    /** Instantiation of this system user */
    private static final SystemUser thisUser = new SystemUser();
    
    /** System user name */
    private String myName;
    
    /** private constructor */
    private SystemUser(){
        //get the logged user works on both Windows/Linux (needs testing on Linux)
        myName = System.getenv("USERNAME");
    }
    
    /** The singleton instance of this user
     * @return the system user instance */
    public static SystemUser getInstance(){
        return thisUser;
    }
   
    /** Gets the system user's name
     * @return The user's name */
    public String myName(){
        return this.myName;
    }

}
