/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.ui;

import csheets.ext.contacts.CompanyContact;
import csheets.ext.contacts.Profession;

import java.io.File;

/**
 * The contacts DTO
 *
 * Created by Sérgio Silva (1141074) on 10/06/2016.
 */
public class ContactDTO{

    private String firstName;

    private String lastName;

    private CompanyContact companyName;

    private String companyNameText;

    private Profession profession;

    private File photo;

    private boolean isPersonal;


    /**
     * @param firstName
     * @param lastName
     * @param photo
     * @param cc
     * @param prof
     * @param isPersonal
     */
    public ContactDTO(String firstName, String lastName, File photo, CompanyContact cc, Profession prof ,boolean isPersonal) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.companyName = cc;
        this.profession = prof;
        this.photo = photo;
        this.isPersonal = isPersonal;
    }

    /**
     * @param companyNameText
     * @param isPersonal
     */
    public ContactDTO(String companyNameText, boolean isPersonal) {
        this.companyNameText = companyNameText;
        this.isPersonal = isPersonal;
    }


    public boolean isPersonalContact(){
        return this.isPersonal;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public File getPhoto() {
        return photo;
    }

    public String getCompanyName() {
        return this.companyNameText;
    }

    public CompanyContact getCompany() {
        return this.companyName;
    }

    public Profession getProfession() {
        return profession;
    }
}
