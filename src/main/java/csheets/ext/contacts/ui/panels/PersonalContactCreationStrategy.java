/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package csheets.ext.contacts.ui.panels;

import csheets.ext.contacts.ui.ContactsExtensionUI;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author 1141074@isep.ipp.pt - Sérgio Silva
 */
public class PersonalContactCreationStrategy implements PopupStrategy{

    /** 
     * The panel to drawn in the frame
     */
    private final PersonalContactPopupPanel thePanel;
    private final ContactsExtensionUI mainUI;

    /**
     * Constructor for the strategy
     * @param mainUI
     * @param thePanel The panel to present
     */
    public PersonalContactCreationStrategy(ContactsExtensionUI mainUI, PersonalContactPopupPanel thePanel) {
        this.thePanel = thePanel;
        this.mainUI = mainUI;
    }
    
    @Override
    public void drawFrame() {
        thePanel.setEditOFF();
        thePanel.setRemoveOFF();
        JFrame fr = new JFrame();  
        fr.setTitle("Create New Contact");
        fr.setLayout(new FlowLayout(FlowLayout.CENTER));
        fr.getContentPane().add(thePanel);
        fr.pack();
        fr.setLocationRelativeTo(null);
        thePanel.setMyFrame(fr);
        fr.setVisible(true);
    }
    
}
