/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts.ui.panels;

import csheets.ext.contacts.*;
import csheets.ext.contacts.ui.ContactController;
import csheets.ext.contacts.ui.ContactsExtensionUI;
import csheets.ui.ctrl.UIController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 *
 * @author Tiago Gabriel 1140775
 */
public class AgendaPanel extends GeneralContactsPanel<Event> {

    /**
     * The contact's Agenda
     */
    private Agenda theAgenda;

    /**
     * The event selected
     */
    private Event selectedEvent;

    /**
     *
     * @param ui
     * @param uiController
     */
    public AgendaPanel(ContactsExtensionUI ui, UIController uiController) {
        super(ui, uiController, "My Agenda");
        Contact selectedContact = mainUI.getTabContacts().getSelectedContact();
        btnPrsContact.setText("Add new Event");
        btnPrsContact.addActionListener(new CreateAction(this));
        theList.addMouseListener(new ClickingAction(ui));
        if (selectedContact instanceof CompanyContact) {
            btnPrsContact.setEnabled(false);
            btnPrsContact.setToolTipText("Company Events View is read only mode!");
        }
    }

    /**
     * Refreshes the panel list
     */
    public void refreshPanelList() {
        if (theAgenda == null) {
            return;
        }
        final DefaultListModel<Event> model = new DefaultListModel<>();
        theList.removeAll();
        for (Event e : theAgenda.getMyEvents()) {
            model.addElement(e);
        }
        theList.setModel(model);
    }

    /**
     * Sets the contact's agenda
     *
     * @param theAgenda Contact's agenda
     */
    protected void setTheAgenda(Agenda theAgenda) {
        this.theAgenda = theAgenda;
    }

    /**
     * Returns the current agenda
     *
     * @return agenda
     */
    public Agenda getAgenda() {
        return this.theAgenda;
    }

    /**
     * Gets the event selected
     *
     * @return Event
     */
    public Event getSelectedEvent() {
        return selectedEvent;
    }

    /**
     * All the user interactions with this panel
     */
    class CreateAction implements ActionListener {

        private AgendaPanel thePanel;

        public CreateAction(AgendaPanel panel) {
            this.thePanel = panel;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            Contact selectedContact = mainUI.getTabContacts().getSelectedContact();
            if (selectedContact != null) {
                PopupContext pop = new PopupContext(new EventCreationStrategy(
                        new EventPopupPanel(new ContactController(mainUI))));

                try {
                    pop.executePopupWindow();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please select a contact on the contact's tab!",
                        "ATENTION", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    //Class for a Clicking Action
    private class ClickingAction extends MouseAdapter {

        ContactsExtensionUI extensionUI;

        private ClickingAction(ContactsExtensionUI ui) {
            this.extensionUI = ui;
        }

        @Override
        public void mouseClicked(MouseEvent event) {
            //if it's one click         
            if (SwingUtilities.isLeftMouseButton(event) && event.getClickCount() == 1) {
                oneClickAction();
            }
            //if it's two click or more
            if (SwingUtilities.isLeftMouseButton(event) && event.getClickCount() >= 2) {
                try {
                    twoClickAction();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        private void oneClickAction() {
            final Event selectEvent = theList.getSelectedValue();
            if (selectEvent != null) {
                Contact selectedContact = this.extensionUI.getTabContacts().getSelectedContact();
                this.extensionUI.getTabAgenda().setTheAgenda(selectedContact.myAgenda());
                this.extensionUI.getTabAgenda().selectedEvent = selectEvent;
            }
        }

        private void twoClickAction() throws Exception {
            final Event selectEvent = theList.getSelectedValue();
            Contact selectedContact = this.extensionUI.getTabContacts().getSelectedContact();
            if (selectEvent != null && selectedContact instanceof PersonalContact) {
                PopupContext pop = new PopupContext(new EventManagementStrategy(
                        new EventPopupPanel(new ContactController(extensionUI)), selectEvent));
                pop.executePopupWindow();
            }
        }
    }

}
