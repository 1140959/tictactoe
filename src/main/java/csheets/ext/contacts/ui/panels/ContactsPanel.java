/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts.ui.panels;

import csheets.ext.contacts.CompanyContact;
import csheets.ext.contacts.Contact;
import csheets.ext.contacts.PersonalContact;
import csheets.ext.contacts.ui.ContactController;
import csheets.ext.contacts.ui.ContactsExtensionUI;
import csheets.ui.ctrl.UIController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 *
 * @author 1141074@isep.ipp.pt - Sérgio Silva
 */
public class ContactsPanel extends GeneralContactsPanel<Contact> {

    /**
     * The contact selected
     */
    private Contact selectedContact;

    /**
     * Complete constructor
     *
     * @param ui The contacts Extension UI
     * @param uiController The main window controller
     */
    public ContactsPanel(ContactsExtensionUI ui, UIController uiController) {
	super(ui, uiController, "Contacts");
	btnPrsContact.setText("Personal");
	btnPrsContact.addActionListener(new CreateAction(this));
	btnCpnContact.addActionListener(new CreateAction(this));
	theList.addMouseListener(new ClickingAction(ui));
    }

    /**
     * Gets the selected contact
     *
     * @return Selected Contact
     */
    public Contact getSelectedContact() {
        return selectedContact;
    }

    /**
     * Sets a selected contact
     *
     * @param selectedContact
     */
    public void setSelectedContact(Contact selectedContact) {
        this.selectedContact = selectedContact;
    }

    /**
     * All the user interactions with this panel
     */
    class CreateAction implements ActionListener {

	private final ContactsPanel thePanel;

	private CreateAction(ContactsPanel panel) {
	    this.thePanel = panel;
	}

	@Override
	public void actionPerformed(ActionEvent ae) {

	    if (ae.getSource() == btnPrsContact) {
		PopupContext pop = new PopupContext(new PersonalContactCreationStrategy(mainUI,
			new PersonalContactPopupPanel(new ContactController(mainUI), thePanel)));
			try {
				pop.executePopupWindow();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
		PopupContext pop = new PopupContext(new CompanyContactCreationStrategy(mainUI,
			new CompanyContactPopupPanel(new ContactController(mainUI), thePanel)));
			try {
				pop.executePopupWindow();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
    }

    //Class for a Clicking Action
    private class ClickingAction extends MouseAdapter {

	private ContactsExtensionUI extensionUI;

	private ClickingAction(ContactsExtensionUI ui) {
	    this.extensionUI = ui;
	}

	@Override
	public void mouseClicked(MouseEvent event) {
	    //if it's one click         
	    if (SwingUtilities.isLeftMouseButton(event) && event.getClickCount() == 1) {
		oneClickAction();
	    }
	    //if it's two click or more
	    if (SwingUtilities.isLeftMouseButton(event) && event.getClickCount() >= 2) {
			try {
				twoClickAction();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void oneClickAction() {
	    final Contact selectedContact = theList.getSelectedValue();
        this.extensionUI.getTabContacts().setLbl(selectedContact);
        if (selectedContact != null) {
            this.extensionUI.getTabContacts().setSelectedContact(selectedContact);
		this.extensionUI.getTabAgenda().setTheAgenda(selectedContact.myAgenda());
		this.extensionUI.getTabAgenda().refreshPanelList();
            if (selectedContact instanceof CompanyContact) {
                this.extensionUI.getTabAgenda().btnPrsContact.setEnabled(false);
                this.extensionUI.getTabAgenda().btnPrsContact.setText("Read-only");
            } else {
                this.extensionUI.getTabAgenda().btnPrsContact.setEnabled(true);
                this.extensionUI.getTabAgenda().btnPrsContact.setText("Add new Event");
            }
        }
    }

	private void twoClickAction() throws Exception {
	    oneClickAction();
	    final Contact selectedContact = theList.getSelectedValue();
	    if (selectedContact != null && selectedContact instanceof PersonalContact) {
		PopupContext pop = new PopupContext(new PersonalContactManagementStrategy(
			new PersonalContactPopupPanel(new ContactController(mainUI), extensionUI.getTabContacts()), selectedContact, extensionUI.getMainUserContact()));
		pop.executePopupWindow();
	    } else {
			PopupContext pop = new PopupContext(new CompanyContactManagementStrategy(
					new CompanyContactPopupPanel(new ContactController(mainUI), extensionUI.getTabContacts()), selectedContact, extensionUI.getMainUserContact()));
			try {
				pop.executePopupWindow();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
    }

}
