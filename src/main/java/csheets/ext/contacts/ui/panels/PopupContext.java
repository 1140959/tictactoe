/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.ui.panels;

/**
 * The contexte of the popup
 *
 * @author Tiago Gabriel 1140775
 */
public class PopupContext {

    /**
     * The strategy to be defined
     */
    private final PopupStrategy strategy;

    /**
     * Valid Constructor
     * @param strategy Strategy to be used
     */
    public PopupContext(PopupStrategy strategy) {
        this.strategy = strategy;
    }

    /**
     * Executes the popup window with the given strategy
     */
    public void executePopupWindow() throws Exception {
        strategy.drawFrame();
    }
}
