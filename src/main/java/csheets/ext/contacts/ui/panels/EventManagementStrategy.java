/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.ui.panels;

import csheets.ext.contacts.Event;

import javax.swing.*;
import java.awt.*;

/**
 * Popup for edition or removing an event
 *
 * @author Tiago Gabriel 1140775
 */
public class EventManagementStrategy implements PopupStrategy {

    /**
     * The panel to drawn in the frame
     */
    private final EventPopupPanel thePanel;

    /**
     * The event to edit/remove
     */
    private final Event theEvent;

    /**
     * Complete Constructor
     *
     * @param thePanel the panel to present
     */
    public EventManagementStrategy(EventPopupPanel thePanel, Event e) {
        this.thePanel = thePanel;
        this.theEvent = e;
    }

    @Override
    public void drawFrame() {
        thePanel.setSubmitOFF();
        theEvent.retrieveInformation(thePanel);
        JFrame fr = new JFrame();
        fr.setTitle("Manage this Event");
        fr.setLayout(new FlowLayout(FlowLayout.CENTER));
        fr.getContentPane().add(thePanel);
        fr.pack();
        fr.setLocationRelativeTo(null);
        thePanel.setMyFrame(fr);
        fr.setVisible(true);
    }

}
