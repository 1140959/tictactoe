/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.ui.panels;

import csheets.ext.contacts.Contact;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author 1141074@isep.ipp.pt - Sérgio Silva
 */
public class CompanyContactManagementStrategy implements PopupStrategy {

    /**
     * Contact to Manage
     */
    private final Contact toManage;

    /**
     * The panel to drawn in the frame
     */
    private final CompanyContactPopupPanel thePanel;

    /**
     * Selected contact
     */
    private final Contact userContact;

    /**
     * Complete Constructor
     *
     * @param thePanel the panel to present
     * @param toManage Contact to manage
     * @param userContact
     */
    public CompanyContactManagementStrategy(CompanyContactPopupPanel thePanel, Contact toManage, Contact userContact) {
        this.thePanel = thePanel;
        this.toManage = toManage;
        this.userContact = userContact;
    }

    @Override
    public void drawFrame() throws Exception {
        thePanel.setSubmitOFF();
        toManage.retrieveInformation(thePanel);
        JFrame fr = new JFrame();
        fr.setTitle("Manage this Contact");
        fr.setLayout(new FlowLayout(FlowLayout.CENTER));
        fr.getContentPane().add(thePanel);
        fr.pack();
        fr.setLocationRelativeTo(null);
        thePanel.setMyFrame(fr);
        fr.setVisible(true);
    }

}
