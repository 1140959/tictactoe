/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.ui.panels;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.ui.ContactsExtensionUI;
import csheets.ui.ctrl.UIController;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * Represents a tabbed panel of the sidebar
 *
 * @author Tiago Gabriel 1140775
 * @param <T> The type of element kept on the panel
 */
public abstract class GeneralContactsPanel<T> extends JPanel {

    /**
     * Main Contact Extension UI
     */
    protected ContactsExtensionUI mainUI;

    /**
     * The list to present to user
     */
    protected JList<T> theList;

    /**
     * Create Button
     */
    protected JButton btnPrsContact = new JButton();
    protected JButton btnCpnContact = null;
    protected JLabel jbl;

    /**
     * The constructor
     *
     * @param ui The extension's UI
     * @param uiController The application UI controller
     * @param panelName Panel Name
     */
    protected GeneralContactsPanel(ContactsExtensionUI ui, UIController uiController, String panelName) {
        //set panel's Layout
        super(new BorderLayout());
        setName(panelName);
        this.mainUI = ui;

        // Adds 
        TitledBorder border = BorderFactory.createTitledBorder("List:");
        border.setTitleJustification(TitledBorder.LEFT);
        this.setPreferredSize(new Dimension(200, 450));
        this.setSize(new Dimension(200, 450));
        setBorder(border);

        // Adds panel for button
        JPanel southPanel = new JPanel(new GridLayout(3, 2));
        southPanel.setPreferredSize(new Dimension(200, 100));
        JPanel but3 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        jbl = new JLabel();
        jbl.setPreferredSize(new Dimension(120, 25));
        jbl.setText("No selected contact!");

        //Buttons Dimension
        Dimension dim = new Dimension(120, 25);
        JPanel but1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        btnPrsContact.setPreferredSize(dim);
        but1.add(btnPrsContact);
        southPanel.add(but1);
	if(this instanceof ContactsPanel) {
	    btnCpnContact = new JButton("Company");
	    JPanel but2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
	    btnCpnContact.setPreferredSize(dim);
	    but2.add(btnCpnContact);
	    southPanel.add(but2);
        but3.add(jbl);
        southPanel.add(but3);
    }


        // The users list
        theList = new JList<>(new DefaultListModel<>()); //model created - just needs to make get
        theList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        theList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        theList.setPreferredSize(new Dimension(200, 450));
        theList.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        theList.setBorder(new LineBorder(Color.BLACK));

        this.add(theList, BorderLayout.NORTH);
        this.add(southPanel, BorderLayout.SOUTH);
    }

    /**
     * Return the list model of JList
     *
     * @return The list Model
     */
    public JList<T> getPanelList() {
        return theList;
    }

    public void setLbl(Contact lbl) {
        this.jbl.setText(lbl.getName());
    }
}
