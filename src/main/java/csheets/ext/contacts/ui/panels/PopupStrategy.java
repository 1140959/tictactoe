/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts.ui.panels;

/**
 * Strategy for popup menus
 * @author Tiago Gabriel
 */
public interface PopupStrategy {
    
    void drawFrame() throws Exception;
}
