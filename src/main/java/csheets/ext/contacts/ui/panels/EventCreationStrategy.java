/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts.ui.panels;

import javax.swing.*;
import java.awt.*;

/**
 * Strategy for create event popup
 *
 * @author Tiago Gabriel 1140775
 */
public class EventCreationStrategy implements PopupStrategy {

    /**
     * The panel to drawn in the frame
     */
    private final EventPopupPanel thePanel;

    /**
     * Complete Constructor
     *
     * @param thePanel the panel to present
     */
    public EventCreationStrategy(EventPopupPanel thePanel) {
        this.thePanel = thePanel;
    }

    @Override
    public void drawFrame() {
        thePanel.setEditOFF();
        thePanel.setRemoveOFF();
        JFrame fr = new JFrame();
        fr.setTitle("Create New Event");
        fr.setLayout(new FlowLayout(FlowLayout.CENTER));
        fr.getContentPane().add(thePanel);
        fr.pack();
        fr.setLocationRelativeTo(null);
        thePanel.setMyFrame(fr);
        fr.setVisible(true);
    }

}
