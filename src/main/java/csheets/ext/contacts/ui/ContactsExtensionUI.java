/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts.ui;

import csheets.ext.Extension;
import csheets.ext.contacts.Contact;
import csheets.ext.contacts.ContactsExtension;
import csheets.ext.contacts.ui.panels.AgendaPanel;
import csheets.ext.contacts.ui.panels.ContactsPanel;
import csheets.ext.contacts.ui.panels.GeneralContactsPanel;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

import javax.swing.*;

/**
 * UI for contacts Extension UI
 *
 * @author Tiago Gabriel 1140775
 */
public class ContactsExtensionUI extends UIExtension {

    /**
     * Main user contact
     */
    private Contact mainContact;

    /**
     * The icon to display with the extension's name
     */
    private Icon icon;

    /**
     * A panel in which contacts and agendas will be displayed
     */
    private JComponent sideBar;

    /**
     * Tab of all contacts
     */
    private GeneralContactsPanel tabContacts;

    /**
     * Tab of all events of the given agenda
     */
    private GeneralContactsPanel tabAgenda;

    /**
     * Complete constructor
     *
     * @param extension The main extension
     * @param uiController The Window controller
     */
    public ContactsExtensionUI(Extension extension, UIController uiController) {
        super(extension, uiController);
    }

    @Override
    public Icon getIcon() {
        if (icon == null) {
            icon = new ImageIcon(ContactsExtension.class.getResource("res/img/contact.gif"));
        }
        return icon;
    }

    @Override
    public JComponent getSideBar() {

        if (sideBar == null) {
            JTabbedPane tabs = new JTabbedPane(JTabbedPane.TOP);

            tabContacts = new ContactsPanel(this, uiController);
            tabs.addTab(tabContacts.getName(), tabContacts);

            tabAgenda = new AgendaPanel(this, uiController);
            tabs.addTab(tabAgenda.getName(), tabAgenda);

            sideBar = tabs;
        }

        return sideBar;
    }

    /**
     * Return the contact of this local user
     *
     * @return Contact
     */
    public Contact getMainUserContact() {
        return this.mainContact;
    }

    /**
     * Sets the computer's user contact
     *
     * @param c The contact to set
     */
    protected void setUserContact(Contact c) {
        this.mainContact = c;
    }

    /**
     * Returns the contacts panel
     *
     * @return Tab of contacts
     */
    public ContactsPanel getTabContacts() {
        return (ContactsPanel) tabContacts;
    }

    /**
     * Returns the agenda panel
     *
     * @return Tab of Agenda
     */
    public AgendaPanel getTabAgenda() {
        return (AgendaPanel) tabAgenda;
    }

}
