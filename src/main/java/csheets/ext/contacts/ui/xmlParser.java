/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts.ui;

import csheets.ext.contacts.Profession;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by Sérgio Silva (1141074) on 10/06/2016.
 */
public class xmlParser {

    static List<Profession> theProfessionList;

    public xmlParser() {
    }

    public static List<Profession> run() {

        SAXBuilder builder = new SAXBuilder();


        try {
            File xmlFile = new File("professions.xml");
            theProfessionList = new LinkedList<>();
            Profession p = new Profession(null);

            //to have a null option so the Personal Contact don't need to have a profession
            theProfessionList.add(p);
            Document document = (Document) builder.build(xmlFile);
            Element rootNode = document.getRootElement();
            List list = rootNode.getChildren("profession");

            for (int i = 0; i < list.size(); i++) {
                Element node = (Element) list.get(i);
                p = new Profession(node.getChildText("name"));
                theProfessionList.add(p);

            }

        } catch (IOException io) {
            System.out.println(io.getMessage());
        } catch (JDOMException jdomex) {
            System.out.println(jdomex.getMessage());
        }
        return theProfessionList;
    }

}
