/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.ui;

import java.util.Calendar;

/**
 * The Event DTO
 *
 * @author Tiago Gabriel 1140775
 */
public class EventDTO {

    /**
     * The description of the event
     */
    private final String description;

    /**
     * The event's time stamp
     */
    private final Calendar timeStamp;

    public EventDTO(String description, Calendar timeStamp) {
        this.description = description;
        this.timeStamp = timeStamp;
    }

    public Calendar getTimeStamp() {
        return timeStamp;
    }

    public String getDescription() {
        return description;
    }

}
