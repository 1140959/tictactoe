/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;

import csheets.ext.contacts.ui.ContactDTO;
import csheets.persistence.PersistenceContext;



public class ContactService implements ManagingContactsService<Contact, ContactDTO> {

    @Override
    public Contact create(ContactDTO theInfo) throws Exception {
        if (theInfo.isPersonalContact()) {
            PersonalContactBuilder personalContactBuilder = new PersonalContactBuilder();
            personalContactBuilder.setFirstName(theInfo.getFirstName()).setLastName(theInfo.getLastName()).setMyPhoto(theInfo.getPhoto()).setCompany(theInfo.getCompany()).setProfession(theInfo.getProfession());
            return personalContactBuilder.createPersonalContact();
        } else {
	    CompanyContactBuilder companyContactBuilder = new CompanyContactBuilder();
	    companyContactBuilder.setCompanyName(theInfo.getCompanyName());
	    return companyContactBuilder.createCompanyContact();
	}
    }

    @Override
    public Contact edit(Contact toEdit, ContactDTO newInfo) throws Exception {
        if (toEdit instanceof PersonalContact) {
            Contact edited = toEdit.editThisContact(newInfo);
            return PersistenceContext.repositories().contacts().save(edited);
        } else {
            Contact edited = toEdit.editThisContact(newInfo);
            return PersistenceContext.repositories().contacts().save(edited);
        }
    }

    @Override
    public boolean remove(Contact toRemove) {
        PersistenceContext.repositories().contacts().deleteById(toRemove.id());
        return true;
    }

    

}
