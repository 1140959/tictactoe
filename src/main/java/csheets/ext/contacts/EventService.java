/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts;

import csheets.ext.contacts.ui.EventDTO;

import java.util.Calendar;
import java.util.Timer;

/**
 * Implementation of the service for managing actions on events
 *
 * @author Tiago Gabriel 1140775
 */
public class EventService implements ManagingContactsService<Event, EventDTO> {

    Timer timer;

    @Override
    public Event create(EventDTO theInfo) throws Exception {
        //The service will make sure the information is valid
        if (theInfo.getDescription() == null || theInfo.getDescription().isEmpty() || theInfo.getDescription().length() < 3) {
            throw new Exception("Description not valid\nPlease insert a description with at least 3 characters.");
        }
        if (theInfo.getTimeStamp() == null) {
            throw new Exception("Please select a due date");
        }

        final int dueDay = theInfo.getTimeStamp().get(Calendar.DATE);
        final int today = Calendar.getInstance().get(Calendar.DATE);

        if (dueDay < today) {
            throw new Exception("Due date not valid! Date must be greater or equal to today!");
        }
        
        return new Event(theInfo.getDescription(), theInfo.getTimeStamp());
    }

    @Override
    public Event edit(Event toEdit, EventDTO newInfo) throws Exception {
        Event create = create(newInfo);
        return toEdit.editThisEvent(create);
    }

    @Override
    public boolean remove(Event toRemove) {
        return false;
    }
}
