/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;

import csheets.persistence.RepositoryEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.File;

/**
 * 
 * @author Tiago Gabriel 1140775
 */
@Entity
public class Photograph implements RepositoryEntity<Photograph,Long> {

    /**
     * This class repository id;
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    /*
    String that holds the photo name
     */
    private String photoName = null;

    public Photograph() {
    }

    public Photograph(String photoName) {
        this.photoName = photoName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Photograph)) return false;

        Photograph that = (Photograph) o;

        return photoName != null ? photoName.equals(that.photoName) : that.photoName == null;
    }

    @Override
    public int hashCode() {
        return photoName != null ? photoName.hashCode() : 0;
    }

    protected File getMyPhoto(){
        return new File(photoName);
    }

    /**
     * Gets the id of this entity on the repository
     *
     * @return The primary key
     */
    @Override
    public Long id() {
        return this.id;
    }

    /**
     * Checks if this entity and the other are the same in the repository
     *
     * @param otherEntity Entity to Compare
     * @return True or false
     */
    @Override
    public boolean sameHas(Photograph otherEntity) {
        return this.id() == otherEntity.id();
    }

    /**
     * Sets the entity's id
     *
     * @param id Primary key created
     */
    @Override
    public void setID(Long id) {
        this.id = id;
    }
}
