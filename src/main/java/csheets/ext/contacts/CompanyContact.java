/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;

import csheets.ext.contacts.ui.ContactDTO;
import csheets.ext.contacts.ui.panels.CompanyContactPopupPanel;
import csheets.ext.contacts.ui.panels.PersonalContactPopupPanel;
import csheets.ext.managephoneemail.CompanyPhoneBook;
import csheets.ext.managephoneemail.PhoneBook;
import csheets.persistence.RepositoryEntity;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Specific type of contact. Company Contact
 *
 * @author Created by Sérgio Silva (1141074)
 */
@Entity
@DiscriminatorValue("COMPANY")
public class CompanyContact extends Contact implements RepositoryEntity<Contact,Long> {

    /**
     * The Company's name
     */
    private String companyName;

    @OneToMany(cascade = CascadeType.ALL)
    private List<PersonalContact> contactList;
    private PhoneBook phoneBook;

    /**
     * Empty Contructor
     */
    public CompanyContact() {
    }

    /**
     * The contructor for a company contact
     *
     * @param companyName This is the company name
     * @param myAgenda This is the agenda
     */
    public CompanyContact(String companyName, Agenda myAgenda) {
        super(myAgenda);
        this.companyName = companyName;
        this.contactList = new ArrayList<>();
        this.phoneBook= new CompanyPhoneBook(null, null, null, null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyContact)) {
            return false;
        }

        CompanyContact that = (CompanyContact) o;

        return companyName.equals(that.companyName);

    }

    @Override
    public int hashCode() {
        return companyName.hashCode();
    }

    @Override
    public Long id() {
        return id;
    }

    @Override
    public boolean sameHas(Contact otherEntity) {
        return (this.id == otherEntity.id);
    }

    @Override
    public void setID(Long id) {
        this.id = id;
    }

    @Override
    public void retrieveInformation(PersonalContactPopupPanel panel) throws Exception {

    }

    @Override
    public void retrieveInformation(CompanyContactPopupPanel panel) throws Exception {
        panel.setCompanyName(getCompany());
    }

    public CompanyContact getCompany() {
        return this;
    }

    public String getCompanyName() {
        return companyName;
    }

    @Override
    Contact editThisContact(ContactDTO newInfo) {
        if (!newInfo.isPersonalContact()) {
            companyName = newInfo.getCompanyName();
        }
        return this;
    }

    @Override
    public void retrieveName(List<CompanyContact> contactNames) {
        contactNames.add(this);
    }

    @Override
    public void retrieveNameString(List<String> contactNames) {
        contactNames.add(this.companyName);
    }

    @Override
    public String getName() {
        return toString();
    }

    @Override
    public String toString() {
        return "[C] : " + companyName;
    }

    @Override
    public PhoneBook getPhoneBook() {
        return this.phoneBook;
    }

    @Override
    public void setPhoneBook(PhoneBook book) {
        this.phoneBook=book;
    }

}

