/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;

import javax.swing.*;
import java.util.TimerTask;

/**
 *
 * @author Tiago Gabriel 1140775
 */
public class EventPopupTask extends TimerTask {

    private final Event ev;

    public EventPopupTask(Event ev) {
        this.ev = ev;
    }

    @Override
    public void run() {
        JOptionPane.showInternalMessageDialog(null,"Event " + ev.toString() + " arrived!");
    }

}
