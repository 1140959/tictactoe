/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;

import java.util.LinkedList;

/**
 *
 * This is the builder for creating Company Contacts
 * Created by Sérgio Silva (1141074) on 04/06/2016.
 */

public class CompanyContactBuilder {
    /*
    *This is the name of the Company
     */
    private String companyName;

    public CompanyContactBuilder setCompanyName(String companyName) {
        if (companyName!=null && companyName.length()>=2) this.companyName = companyName;
        return this;
    }

    public CompanyContact createCompanyContact() {
        if (companyName!=null) return new CompanyContact(companyName, new Agenda(new LinkedList<Event>() {
        }));
        return null;
    }
}