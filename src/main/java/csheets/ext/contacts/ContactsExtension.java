/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;

import csheets.ext.Extension;
import csheets.ext.contacts.ui.ContactsExtensionUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

import java.util.Calendar;
import java.util.Timer;

/**
 * Extension for contacts edition
 *
 * @author Tiago Gabriel 1140775
 */
public class ContactsExtension extends Extension {

    /**
     * The Timer
     */
    private static Timer timer = new Timer();

    /**
     * The version
     */
    public static final Long VERSION = 1L;

    /**
     * Description
     */
    public static final String DESCRIPTION = "Manage your contacts";
    /**
     * The extension name
     */
    private static final String NAME = "Contacts";

    /**
     * Default Constructor
     */
    public ContactsExtension() {
        super(VERSION, NAME, DESCRIPTION);
    }

    /**
     * Returns the user interface extension of this extension
     *
     * @param uiController the user interface controller
     * @return a user interface extension, or null if none is provided
     */
    @Override
    public UIExtension getUIExtension(UIController uiController) {
        return new ContactsExtensionUI(this, uiController);
    }

    public void schedule(Event ev, Calendar cl) {
        timer.schedule(new EventPopupTask(ev), cl.getTime());
    }
}
