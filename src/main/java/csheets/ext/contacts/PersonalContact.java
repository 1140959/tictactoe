/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

package csheets.ext.contacts;

import csheets.ext.contacts.ui.ContactDTO;
import csheets.ext.contacts.ui.panels.CompanyContactPopupPanel;
import csheets.ext.contacts.ui.panels.ImageCopy;
import csheets.ext.contacts.ui.panels.PersonalContactPopupPanel;
import csheets.ext.managephoneemail.PersonalPhoneBook;
import csheets.ext.managephoneemail.PhoneBook;
import csheets.persistence.RepositoryEntity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * It's a specific type of contact
 *
 * @author Tiago Gabriel 1140775
 */
@Entity
@DiscriminatorValue("PERSONAL")
public class PersonalContact extends Contact implements RepositoryEntity<Contact,Long> {

    /**
     * The contact's first name
     */
    private String firstName;
    /**
     * The contact's last name
     */
    private String lastName;
    /**
     * The contact's photograph
     */
    private Photograph myPhoto;

    /**
     * The contact's Company
     */
    private CompanyContact MyCompany;

    /**
     * The contact's Company
     */
    private Profession MyProfession;
    
    private PhoneBook phoneBook;

    /**
     * Empty Contructor
     */
    public PersonalContact() {
    }

    /**
     * @param firstName First name of personal contact
     * @param lastName Last name of personal contact
     * @param myPhoto The photo for personal contact
     * @param myCompany The Company of the personal contact
     * @param myProfession The Profession for the personal contact
     * @param agenda Agenda for the personal contact
     */
    public PersonalContact(String firstName, String lastName, Photograph myPhoto, CompanyContact myCompany, Profession myProfession, Agenda agenda) {
        super(agenda);
        this.firstName = firstName;
        this.lastName = lastName;
        this.myPhoto = myPhoto;
        MyCompany = myCompany;
        MyProfession = myProfession;
        phoneBook = new PersonalPhoneBook(null, null, null, null, null, null);
    }

    /**
     * Checks if this contact has this two names
     *
     * @param firstName The first Name
     * @param lastName The second Name
     * @return True if it does, false otherwise
     */
    public boolean hasCompleteName(String firstName, String lastName) {
        return (this.firstName.equals(firstName) && this.lastName.equals(lastName));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonalContact)) {
            return false;
        }

        PersonalContact that = (PersonalContact) o;

        if (!firstName.equals(that.firstName)) {
            return false;
        }
        return lastName.equals(that.lastName);

    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "[P] : " + firstName + " " + lastName;
    }

    @Override
    public Long id() {
        return this.id;
    }

    @Override
    public boolean sameHas(Contact otherEntity) {
        return (Objects.equals(this.id, otherEntity.id));
    }

    @Override
    public void setID(Long id) {
        this.id = id;
    }

    @Override
    public void retrieveInformation(PersonalContactPopupPanel panel) throws Exception {
        panel.setFirstName(firstName);
        panel.setLastName(lastName);
        panel.setPhoto(myPhoto.getMyPhoto());
        if (MyCompany!=null)
            panel.setCompanyName(MyCompany);
        if (MyProfession!=null)
            panel.setProfession(MyProfession);

    }

    public CompanyContact getMyCompany() {
        return MyCompany;
    }

    @Override
    public void retrieveInformation(CompanyContactPopupPanel panel) throws Exception {
        panel.setCompanyName(MyCompany);
    }

    @Override
    protected Contact editThisContact(ContactDTO newInfo) throws IOException {
        if (newInfo.isPersonalContact()) {
            firstName = newInfo.getFirstName();
            lastName = newInfo.getLastName();
            MyCompany = newInfo.getCompany();
            MyProfession = newInfo.getProfession();
            if (newInfo.getPhoto() != null && newInfo.getPhoto().getName() != null) {
                String path = "src/main/resources/csheets/ext/contacts/res/img/contacts/" + firstName + lastName + "/";
                File dir = new File(path);
                if (!dir.exists()) {
                    dir.mkdir();
                }
                String photo = path + newInfo.getPhoto().getName();
                File newFile = new File(photo);
                new ImageCopy().CopyFile(newInfo.getPhoto(), newFile);
                this.myPhoto = new Photograph(path + newInfo.getPhoto().getName());
            }
        }
        return this;
    }

    @Override
    public void retrieveName(List<CompanyContact> contactNames) {

    }

    @Override
    public void retrieveNameString(List<String> contactNames) {
        contactNames.add(this.firstName+" "+this.lastName);
    }

    @Override
    public String getName() {
        return this.toString();
    }

    @Override
    public PhoneBook getPhoneBook() {
        return this.phoneBook;
    }

    @Override
    public void setPhoneBook(PhoneBook book) {
        this.phoneBook = book;
    }

}
