/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chat;

import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LEMendes
 */
public class ChatService {

    private static ChatService instance = null;
    protected ChatMessageConnection connection = null;
    protected List<Message> listMessages = new ArrayList<>();
    private ReceivedSendMessagesToJtree receivedSendMessages;
    private static final int CHATPORT = 10500;

    private ChatService() {
    }

    /**
     * Singleton instance.
     *
     * @return the singleton instance
     */
    public static ChatService getInstance() {
        if (instance == null) {

            instance = new ChatService();
        }
        return instance;
    }

    /**
     * Start socket connection
     *
     * @param nickname nickname
     * @throws SocketException exception
     */
    public void startConnection(String nickname) throws SocketException {
        this.connection = new ChatMessageConnection(nickname, CHATPORT);
        this.connection.start();
    }

    /**
     * Get the connection
     *
     * @return connection
     */
    public ChatMessageConnection getConnection() {
        return connection;
    }

    /**
     * Add message to List
     *
     * @param message Message
     */
    public void addMessage(Message message) {

        this.listMessages.add(message);
    }

    /**
     * Send a message
     *
     * @param ip Ip adrress
     * @param portNumber port
     * @param message Mesaage
     * @return true/false
     * @throws SocketException exception
     */
    public boolean sendMessage(InetAddress ip, int portNumber, Message message) throws SocketException {
        return this.connection.sendMessageTo(ip, portNumber, message);
    }

    /**
     * Chat port default
     *
     * @return port
     */
    public int chatPort() {
        return CHATPORT;
    }

    
    /**
     * My ip address
     *
     * @return ip
     */
    public String getMyIP() {

        return connection.getMyIpAddress();
    }

    /**
     * Return the tree of messages
     *
     * @return the tree
     */
    public ReceivedSendMessagesToJtree getReceivedSendMessages() {
        return this.receivedSendMessages;
    }

    /**
     * Define a tree
     *
     * @param jTree the tree
     */
    public void setReceivedMessages(ReceivedSendMessagesToJtree jTree) {
        this.receivedSendMessages = jTree;
    }
}
