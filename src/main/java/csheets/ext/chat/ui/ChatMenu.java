/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chat.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author LEMendes
 */
public class ChatMenu extends JMenu {
    	/**
	 * Creates a new JMenu and adds new actions to it
	 *
	 * @param uiController workbook controller 
	 */
	public ChatMenu(UIController uiController) {
		super("Chat");
		setMnemonic(KeyEvent.VK_C);
		// Adds actions
		add(new StartChatAction(uiController));
		add(new SendMessageAction(uiController));
	}

}
