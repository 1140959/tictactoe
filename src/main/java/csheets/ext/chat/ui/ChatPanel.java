/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chat.ui;

import csheets.ext.chat.ChatExtension;
import csheets.ext.chat.ChatService;
import csheets.ext.chat.ReceivedSendMessagesToJtree;
import csheets.ui.ctrl.UIController;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

/**
 * Sidebar panel
 * @author LEMendes
 */
public class ChatPanel extends JPanel {

    JLabel myIpLabel = new JLabel();
    JLabel myHostName = new JLabel();

    public ChatPanel(UIController uiController) {
        this.setName(ChatExtension.NAME);
        this.setLayout(new GridLayout(2, 2));
        final StartChatController chatController = new StartChatController(uiController);
        chatController.setReceivedMessages(new ReceivedSendMessagesToJtree());
        chatController.getReceivedMessages().addTreeSelectionListener((TreeSelectionEvent e) -> {
            JTree tree = (JTree) e.getSource();
            DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
            if (selectedNode != null && selectedNode.isLeaf() && selectedNode.getParent() != null) {
                String ipAddress = selectedNode.getParent().toString();
                String messageText = JOptionPane.showInputDialog(null,
                        "Message:", "Send Message ", JOptionPane.INFORMATION_MESSAGE);
                if (messageText != null && messageText.compareTo("") != 0) {
                    try {
                        chatController.sendMessage(ipAddress, ChatService.getInstance().chatPort(), messageText);
                        //If success, nothing to say
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, "Error!!!\nMessage not send", "Send Message fail",
                                JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No message to send!", "Send Message fail",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        JScrollPane pane = new JScrollPane(chatController.getReceivedMessages());
        this.add(pane);
        String IpAddress, myHost;
        InetAddress iAddress;
        try {
            iAddress = InetAddress.getLocalHost();
            IpAddress = iAddress.getHostAddress();
        } catch (UnknownHostException ex) {
            IpAddress = "Ip Address not found";
        }
        try {
            iAddress = InetAddress.getLocalHost();
            myHost = iAddress.getHostName();
        } catch (UnknownHostException ex) {
            myHost = "Host not found!!";
        }
        JPanel commentPanel = new JPanel();
        commentPanel.setLayout(new BoxLayout(commentPanel, BoxLayout.PAGE_AXIS));
        commentPanel.setPreferredSize(new Dimension(130, 336));
        commentPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));		// width, height
        myHostName.setText("Host: " + myHost);
        myIpLabel.setText("My ip: " + IpAddress);
        myHostName.setPreferredSize(new Dimension(20, 50));
        myHostName.setLocation(50, 50);
        myIpLabel.setPreferredSize(new Dimension(20, 50));
        myIpLabel.setLocation(80, 50);
        commentPanel.add(myHostName);
        commentPanel.add(myIpLabel);
        this.add(commentPanel);

    }

}
