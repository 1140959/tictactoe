/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chat.ui;

import csheets.ext.chat.ChatService;
import csheets.ui.ctrl.UIController;
import static java.util.Objects.isNull;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author LEMendes
 */
public class StartChatUI {

    protected StartChatController startChatController;
    protected UIController uiController;

    private JFrame jframe;

    public StartChatUI(UIController uiController) {
        this.uiController=uiController;
        this.startChatController = new StartChatController(uiController);

    }

    protected void run() {
        String nickname;
        nickname = JOptionPane.showInputDialog("Insert nickname:");
        if (isNull(nickname) || nickname.isEmpty()) {
            return;
        }
        nickname = nickname.trim();
        if (nickname.compareTo("") == 0) {
            JOptionPane.showMessageDialog(jframe,
                    "Invalid Nickname.", jframe.
                    getTitle(),
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        try {
            if (startChatController.startChat(nickname)) {
                JOptionPane.showMessageDialog(jframe,
                        "Success start chat at port " + ChatService.getInstance().chatPort()
                        + "!\nChat service is now running.");
                
            }
        } catch (Exception ex) {
            Logger.getLogger(StartChatUI.class.getName()).
                    log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(jframe,
                    "Some error occured!\n" + ex.getMessage(), jframe.
                    getTitle(),
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
