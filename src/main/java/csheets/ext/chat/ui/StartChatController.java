/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chat.ui;

import csheets.ext.chat.ChatService;
import csheets.ext.chat.Message;
import csheets.ext.chat.ReceivedSendMessagesToJtree;
import csheets.ui.ctrl.UIController;
import java.net.InetAddress;

/**
 *
 * @author LEMendes
 */
public class StartChatController {

    /**
     * The user interface controller.
     */
    private final UIController uiController;

    /**
     * Class constructor.
     *
     * @param uiController user interface controller
     */
    public StartChatController(UIController uiController) {
        this.uiController = uiController;
    }

    /**
     * start chat service
     *
     * @param nickname chat nickname
     * @return true/false
     * @throws Exception exception
     */
    public boolean startChat(String nickname) throws Exception {
        ChatService.getInstance().
                startConnection(nickname);
        return true;
    }

    /**
     * Get my IP address
     *
     * @return ip address
     */
    public String getMyIpAddress() {
        return ChatService.getInstance().getMyIP();
    }

    /**
     * Send a message
     *
     * @param ipAddress ip address
     * @param portNumber port number
     * @param textMessage The message
     * @return true/false
     * @throws Exception exception
     */
    public boolean sendMessage(String ipAddress, int portNumber, String textMessage) throws Exception {
        Message message = new Message(textMessage);
        return ChatService.getInstance().sendMessage(InetAddress.
                getByName(ipAddress), portNumber, message);
    }

    /**
     * Define Tree Messages
     * @param jTree The message tree
     */
    public void setReceivedMessages(ReceivedSendMessagesToJtree jTree) {
        ChatService.getInstance().setReceivedMessages(jTree);
    }

    public ReceivedSendMessagesToJtree getReceivedMessages() {
        return ChatService.getInstance().getReceivedSendMessages();
    }

    /**
     * Get chat port
     *
     * @return port
     */
    public int getChatPort() {
        return ChatService.getInstance().chatPort();
    }
}
