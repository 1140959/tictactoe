/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chat.ui;

import csheets.ext.chat.ChatService;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author LEMendes
 */
public class StartChatAction extends BaseAction {

    /**
     * The user interface controller
     */
    protected UIController uiController;

    /**
     * Creates a new action.
     *
     * @param uiController the user interface controller
     */
    public StartChatAction(UIController uiController) {
        this.uiController = uiController;
    }

    @Override
    protected String getName() {
        return "Start";
    }

@Override
	public void actionPerformed(ActionEvent ae) {
		if (ChatService.getInstance().getConnection() != null) {
                    int port=ChatService.getInstance().chatPort();
			JOptionPane.
				showMessageDialog(null, "Chat Already Started on port "+port+"!", "Start Chat", JOptionPane.WARNING_MESSAGE);

		} else {
			StartChatUI uictr = new StartChatUI(uiController);
			uictr.run();
		}
	}

}
