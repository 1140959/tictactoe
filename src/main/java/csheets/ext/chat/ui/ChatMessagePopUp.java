/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chat.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

/**
 * Popup dialog
 * @author LMendes
 */
public class ChatMessagePopUp extends JDialog {

    private final String trackHeader;
    private final String message;
    private final int waitTime;

    /**
     * Contructor
     * @param Nick nickname
     * @param message the received message
     * @param wait time to shoe popup
     */
    public ChatMessagePopUp(String Nick, String message, int wait) {
        trackHeader = "New message from " + Nick;
        this.message = message;
        waitTime = wait;
        initComponents();
    }

    private void initComponents() {
        Dimension scrSize = Toolkit.getDefaultToolkit().getScreenSize();// size of the screen
        setLocation(scrSize.width / 2, scrSize.height / 2);
        setSize(300, 200);
        setLayout(null);
        setUndecorated(true);
        setLayout(new GridBagLayout());
        this.setTitle(trackHeader);
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 1.0f;
        constraints.weighty = 1.0f;
        constraints.insets = new Insets(5, 5, 5, 5);
        constraints.fill = GridBagConstraints.BOTH;

        JLabel msg = new JLabel(message);
        msg.setOpaque(false);
        add(msg, constraints);

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setVisible(true);
        setAlwaysOnTop(true);

        new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(waitTime * 1000);
                    dispose();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        ;
    }


.start();
}
}
