/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chat;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LEMendes
 */
public abstract class ChatMessageThread extends Thread {

    public ChatMessageThread() {

    }

    /**
     * run a thread
     */
    @Override
    public final void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                if (!this.receiveMessageFrom()) {
                    break;
                }
            } catch (Exception ex) {
                Logger.getLogger(ChatMessageThread.class.getName()).
                        log(Level.SEVERE, null, ex);
                this.disconnect();
                break;
            }
        }
    }

    public abstract boolean receiveMessageFrom();

    /**
     * Interrupt thread
     *
     * @return true/false
     */
    public boolean disconnect() {
        if (!Thread.currentThread().isInterrupted()) {
            this.interrupt();
            return true;
        }
        return false;
    }

}
