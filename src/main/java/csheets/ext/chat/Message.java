/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chat;

import java.io.Serializable;
import java.net.InetAddress;

/**
 * Packet send and receive by service chat
 * @author LEMendes
 */
public class Message implements Serializable {

    private String nickName;
    private InetAddress IpAddress;
    private int portNumber;
    private String msgText;
    
    /**
     * Contructor
     * @param messageText the message
     */
    public Message(String messageText) {
        this.nickName = "";
        this.IpAddress = null;
        this.portNumber = 0;
        this.msgText = messageText;
    }

    /**
     * Set nickname
     * @param nickName the nickname
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    
    /**
     * Set ip address
     * @param IpAddress Internet Protocol (IP) address
     */
    public void setIpAddress(InetAddress IpAddress) {
        this.IpAddress = IpAddress;
    }

    /**
     * Set port number
     * @param portNumber port
     */
    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    /**
     * Get nickname
     * @return nickname
     */
    public String getNickName() {
        return nickName;
    }

    /**
     * Get message
     * @return message
     */
    public String getMsgText() {
        return msgText;
    }

    /**
     * Get ip address
     * @return Internet Protocol (IP) address
     */
    public InetAddress getIpAddress() {
        return IpAddress;
    }

    /**
     * Get port number
     * @return port
     */
    public int getPortNumber() {
        return portNumber;
    }

    @Override
    public String toString() {
        return this.nickName+"("+this.getIpAddress().getHostAddress()
                +":"+this.getPortNumber()+")" + ": " + this.msgText;
    }

}
