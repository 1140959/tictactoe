/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chat;

import java.util.Enumeration;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * @author LMendes
 */
public class ReceivedSendMessagesToJtree extends JTree {

    /**
     * Contructor
     */
    public ReceivedSendMessagesToJtree() {
        super(new DefaultMutableTreeNode("Received and sent Messages"));
    }

    /**
     * Add path to Jtree message
     * @param path tree add path
     */
    public void addMessageToJTree(String path) {
        DefaultTreeModel model = (DefaultTreeModel) this.getModel();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();

        String[] allPaths = path.split("/");
        DefaultMutableTreeNode rootnode = (DefaultMutableTreeNode) root;
        //the node existe?
        for (String actualPath : allPaths) {
            if (!actualPath.equals("") && !actualPath.equals("..")) {
                Enumeration<DefaultMutableTreeNode> en = rootnode.children();
                boolean exists = false;
                while (en.hasMoreElements()) {
                    DefaultMutableTreeNode node = en.nextElement();
                    if (node.getUserObject().equals(actualPath)) {
                        rootnode = node;
                        exists = true;
                        break;
                    }
                }
                //if not, add new one
                if (exists != true) {
                    DefaultMutableTreeNode tmpNode = new DefaultMutableTreeNode(actualPath);
                    rootnode.add(tmpNode);
                    model.reload(rootnode);
                    rootnode = tmpNode;
                }
            }
        }
    }
}
