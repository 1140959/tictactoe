package csheets.ext.address;

/**
 * A service for addresses use case
 * @author Joel Ferreira (1140777)
 * @param <T> The type of object to apply the service
 * @param <C> The information of the object to create
 */
public interface ManagingAddressService<T,C> {
    
     /** Creates a new entity
     * @param theInfo information for the creation
     * @return  Object created
     */
    T create(C theInfo) throws UnsupportedOperationException;
    
    /** Edits a given entity
     * @param toEdit Object to edit
     * @param newInfo New info
     * @return An object
     */
    T edit(T toEdit, C newInfo) throws UnsupportedOperationException;
    
    /** Removes a given entity
     * @param toRemove Object to remove
     * @return true if successfully removed */
    boolean remove(T toRemove);

}
