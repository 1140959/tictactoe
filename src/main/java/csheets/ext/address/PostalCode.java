/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.address;

/**
 *
 * @author JFerreira
 */
public class PostalCode {

    private String code;
    private String location;
    private String valdiation;

    PostalCode(String postalCode, String local, String regex) {
        this.code = postalCode;
        this.location = local;
        this.valdiation = regex;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public String getLocation(){
        return this.location;
    }
      
    @Override
    public String toString() {
        return "PostalCode: " + code + "Local: " + location;
    }
}
