
package csheets.ext.address;

import csheets.ext.address.ui.panels.CreateAddressPopupPanel;
import csheets.ext.notes.Note;
import csheets.persistence.RepositoryEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Contact Address
 * @author Joel Ferreira (1140777)
 */
@Entity
public class AddressContact implements RepositoryEntity<AddressContact,Long> {

    /**
     * This class repository id;
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

     /**
     * address street
     */
    private String street;
    
    /**
     * address town
     */
    private String town;
    
    /**
     * address postalCode
     */
    private String postalCode;
    
     /**
     * address city
     */
    private String city;
    
    /**
     * address country
     */
    private String country;  
    
    /**
     * 
     */
    private AddressType type;


    public AddressContact() {
    }

    /**
     * The complete constructor
     *
     * @param street street of address
     * @param town town of address
     * @param postalCode postalCode of address
     * @param city city of address
     * @param country country of address
     * @param type the type of address
     */
    public AddressContact(String street, String town, String postalCode, String city, String country, AddressType type) {
        this.street = street;
        this.town = town;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
        this.type = type;
    }
    
     /**
     * Gets the information for edition of the panel
     *
     * @param thePanel My panel
     */
    public void retrieveInformation(CreateAddressPopupPanel thePanel) {
        thePanel.setStreet(street);
        thePanel.setTown(town);
        thePanel.setCity(city);
        thePanel.setPostalCode(postalCode);
        thePanel.setCountry(country);
        thePanel.setType(type);
    }
    
     /**
     * Edit this event
     *
     * @param newAddress This address
     * @return edited address
     */
    public AddressContact editThisAddress(AddressContact newAddress) {
        this.street = newAddress.street;
        this.town = newAddress.town;
        this.postalCode = newAddress.postalCode;
        this.city = newAddress.city;
        this.country = newAddress.country;
        this.type = newAddress.type;
        return this;
    }
    
     /**
     * 
     * toString to string of the address to show on lists
     *
     * @return a string with the address info
     */
    @Override
    public String toString() {
        return type + ":" + street + " - " + town + ", " + postalCode + " - " + city + ", " + country;
    }
    
    /**
     * 
     * @return  the type of address
     */
    public AddressType getType() {
        return this.type;
    }

    /**
     * Gets the id of this entity on the repository
     *
     * @return The primary key
     */
    @Override
    public Long id() {
        return this.id;
    }

    /**
     * Checks if this entity and the other are the same in the repository
     *
     * @param otherEntity Entity to Compare
     * @return True or false
     */
    @Override
    public boolean sameHas(AddressContact otherEntity) {
        return this.id==otherEntity.id;
    }

    /**
     * Sets the entity's id
     *
     * @param id Primary key created
     */
    @Override
    public void setID(Long id) {
        this.id=id;
    }
}
