/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.address.ui.panels;

import csheets.ext.address.ui.AddressEditionUI;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Joel Ferreira (1140777)
 * @param <T> generic type
 * 
 * */

public class GeneralAddressPanel<T> extends JPanel {
     /**
     * Main Contact Extension UI
     */
    protected AddressEditionUI mainUI;

    /**
     * The list to present to user
     */
    protected JList<T> theList;

    /**
     * Create Button
     */
    protected JButton showBtn = new JButton();

    /**
     * The constructor
     *
     * @param ui The extension's UI
     * @param uiController The application UI controller
     * @param panelName Panel Name
     */
    protected GeneralAddressPanel(AddressEditionUI ui, UIController uiController, String panelName) {
        //set panel's Layout
        super(new BorderLayout());
        setName(panelName);
        this.mainUI = ui;

        // Adds 
        TitledBorder border = BorderFactory.createTitledBorder("List of all contacts:");
        border.setTitleJustification(TitledBorder.LEFT);
        this.setPreferredSize(new Dimension(200, 600));
        setBorder(border);

        // Adds panel for button
        JPanel southPanel = new JPanel(new GridLayout(2, 1));
        southPanel.setPreferredSize(new Dimension(150, 60));

        //Buttons Dimension
        Dimension dim = new Dimension(120, 25);
        JPanel but1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        showBtn.setPreferredSize(dim);
        but1.add(showBtn);
        southPanel.add(but1);

        // The users list
        theList = new JList<>(new DefaultListModel<>()); //model created - just needs to make get
        theList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        theList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        theList.setPreferredSize(new Dimension(300, 490));
        theList.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        theList.setBorder(new LineBorder(Color.BLACK));

        this.add(theList, BorderLayout.NORTH);
        this.add(southPanel, BorderLayout.SOUTH);
    }

    /**
     * Return the list model of JList
     *
     * @return The list Model
     */
    public JList<T> getPanelList() {
        return theList;
    }

}
