/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.address.ui.panels;

import csheets.ext.address.AddressContact;
import csheets.ext.address.ui.AddressDTO;
import csheets.ext.address.ui.AddressEditionUI;
import csheets.ext.contacts.Contact;
import csheets.persistence.PersistenceContext;
import csheets.ui.ctrl.UIController;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author Joel Ferreira (1140777)
 */
public class AddressEditionController extends GeneralAddressPanel<Contact>{
    
     /**
     * The contact selected
     */
    private Contact selectedContact;
    
     /**
     * The address selected
     */
    private AddressDTO selectedAddress;
    
    /**
     * 
     */
    private final AddressEditionController theController;
    
    /**
     * Complete constructor
     *
     * @param ui The contacts Extension UI
     * @param uiController The main window controller
     */
    public AddressEditionController(AddressEditionUI ui, UIController uiController) {
        super(ui, uiController, "Address");
        showBtn.setText("Show all Contacts");
        showBtn.addActionListener(new CreateAction(this));
        theList.addMouseListener(new ClickingAction(ui));
        theController = this;
    }
    
     /**
     * Sets a selected contact
     *
     * @param selectedContact current selected contact
     */
    public void setSelectedContact(Contact selectedContact) {
	this.selectedContact = selectedContact;
    }

    /**
     * 
     *
     * @return Selected Contact
     */
    public Contact getSelectedContact() {
	return selectedContact;
    }

    /**
     * constructs a DTO for an address
     * @param selectedContact
     * @param selectedValue 
     */
    public void setSelectedAddress(Contact selectedContact, AddressContact selectedValue) {
        
        
    }
    
      /**
     * All the user interactions with the button
     */
    class CreateAction implements ActionListener {
	private final AddressEditionController thePanel;
        
	private CreateAction(AddressEditionController panel) {
	    this.thePanel = panel;
	}
    
	@Override
	public void actionPerformed(ActionEvent ae) {
	    if (ae.getSource() == showBtn) {
               DefaultListModel<Contact> model = (DefaultListModel<Contact>) thePanel.theList.getModel();
               model.removeAllElements();
               Iterable<Contact> all = PersistenceContext.repositories().contacts().all();
                for (Contact contact : all) {
                    model.addElement(contact);
                }
	    } 
	}
    }
    
     /**
     * All the user interactions with the list
     */
    class ClickingAction extends MouseAdapter {
        
        private final AddressEditionUI extensionUI;
       
        private ClickingAction(AddressEditionUI ui) {
            this.extensionUI = ui;
        }

        @Override
        public void mouseClicked(MouseEvent me) {
            if (SwingUtilities.isLeftMouseButton(me) && me.getClickCount() == 1) {
                oneClickAction();
            }
            if (SwingUtilities.isLeftMouseButton(me) && me.getClickCount() >= 2) {
                twoClickAction();
            }
        }

        private void oneClickAction() {
	    selectedContact = theList.getSelectedValue();

	    if (selectedContact != null) {
		this.extensionUI.getTabContacts().setSelectedContact(selectedContact);
	    }
	}
    
        private void twoClickAction() {
	    oneClickAction();
	    selectedContact = theList.getSelectedValue();
	    if (selectedContact != null) {
                AddressPopupPanel app = new AddressPopupPanel(theController, selectedContact);
                JFrame fr = new JFrame();
                fr.setLayout(new FlowLayout(FlowLayout.CENTER));
                fr.add(app);
                app.setMyFrame(fr);
                fr.pack();
                fr.setLocationRelativeTo(null);
                fr.setVisible(true);
	    }
	}
    }
}
