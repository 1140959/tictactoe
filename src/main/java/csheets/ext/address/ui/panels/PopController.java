package csheets.ext.address.ui.panels;

import csheets.ext.address.AddressContact;
import csheets.ext.address.AddressService;
import csheets.ext.address.AddressType;
import csheets.ext.address.ImportPostalCodes;
import csheets.ext.address.PostalCode;
import csheets.ext.address.ui.AddressDTO;
import csheets.ext.contacts.Contact;
import csheets.ext.contacts.ContactService;
import java.util.List;

/**
 *
 * @author Joel Ferreira (1140777)
 */
public class PopController {

    /**
     * The selected Contact
     */
    private final Contact selectedContact;

    /**
     *
     * @param selected the selected contact on the list
     */
    public PopController(Contact selected) {
        this.selectedContact = selected;
    }

    /**
     *
     * @return boolean (true or false)
     */
    public boolean validate() {
        return !(selectedContact.myPrimaryAdress() != null && selectedContact.mySecondaryAddress() != null);
    }

    /**
     *
     * @param addr the addressDTO
     * @throws java.lang.Exception
     */
    public void addAddress(AddressDTO addr) throws Exception {
        AddressService ads = new AddressService();
        AddressContact nAdd = ads.create(addr);
        ContactService cs = new ContactService();

        if (addr.getType() == AddressType.PRIMARY) {
            selectedContact.setAnAddress(nAdd, AddressType.PRIMARY);
        }

        if (addr.getType() == AddressType.SECONDARY) {
            selectedContact.setAnAddress(nAdd, AddressType.SECONDARY);
        }
    }

    /**
     *
     * @param addr the address of the contact
     * @throws Exception to validate that the ENUM is respected
     */
    public void removeAddress(AddressContact addr) {
        if (addr != null) {
            if (addr.getType() == AddressType.PRIMARY) {
                selectedContact.setAnAddress(null, AddressType.PRIMARY);
            }

            if (addr.getType() == AddressType.SECONDARY) {
                selectedContact.setAnAddress(null, AddressType.SECONDARY);
            }
        }
    }

    /**
     *
     * @param street the street
     * @param town the town
     * @param postalCode the postal code
     * @param city the city
     * @param country the country
     * @throws Exception to validate the DTO creation
     */
    public AddressDTO generateDTO(String street, String town, String postalCode, String city, String country, Contact selectedcontact, AddressType type) throws Exception {
        AddressDTO addr = new AddressDTO(street, town, postalCode, city, country, selectedcontact, type);
        return addr;
    }

    /**
     *
     * @param country
     * @param pCode
     * @return
     */
    public boolean validatePostalCode(String country, String pCode) {
        List<PostalCode> postCode = null;
        ImportPostalCodes file = new ImportPostalCodes();
        if (file.readFile()) {
            postCode = file.getImportedData();
            System.out.println(postCode);
        }

        for (PostalCode postalCode : postCode) {
            if (country.equalsIgnoreCase("portugal")) {
                if (pCode.equals(postCode)) {
                    return true;
                }
            } else {
                return true;
            }
        }
        return false;
    }

}
