package csheets.ext.address.ui;

import csheets.ext.address.ui.panels.AddressEditionController;
import csheets.ext.address.ui.panels.GeneralAddressPanel;
import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;

/**
 * UI for contacts Extension UI
 *
 * @author Joel Ferreira (1140777)
 */
public class AddressEditionUI extends UIExtension {

    /**
     * A panel in which contacts will be displayed
     */
    private JComponent sideBar;

    /**
     * Tab of all contacts
     */
    private GeneralAddressPanel tabContacts;

    /**
     * Complete constructor
     *
     * @param extension The main extension
     * @param uiController The Window controller
     */
    public AddressEditionUI(Extension extension, UIController uiController) {
        super(extension, uiController);
    }

    @Override
    public JComponent getSideBar() {

        if (sideBar == null) {
            JTabbedPane tabs = new JTabbedPane(JTabbedPane.TOP);
            tabs.setName(extension.getName());
            tabContacts = new AddressEditionController(this, uiController);
            tabs.addTab(tabContacts.getName(), tabContacts);
            sideBar = tabs;
        }

        return sideBar;
    }

    /**
     * Returns the address panel
     *
     * @return Tab of address
     */
    public AddressEditionController getTabContacts() {
        return (AddressEditionController) tabContacts;
    }
}
