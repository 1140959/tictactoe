
package csheets.ext.address.ui;

import csheets.ext.address.AddressType;
import csheets.ext.contacts.Contact;

/**
 *
 * @author Joel Ferreira (1140777)
 */
public class AddressDTO {
    
      /**
     * address street
     */
    private final String street;
    
    /**
     * address town
     */
    private final String town;
    
    /**
     * address postalCode
     */
    private final String postalCode;
    
     /**
     * address city
     */
    private final String city;
    
    /**
     * address country
     */
    private final String country;
    
     /**
     * address country
     */
    private final Contact contact;
    
    /**
     * type of address
     */
    private final AddressType type;
    
    /**
     * Address DTO 
     * @param street the street
     * @param town the town
     * @param postalCode the postalCode
     * @param city the city
     * @param country the country
     * @param contact the contact
     * @param type the type
     */
    public  AddressDTO(String street, String town, String postalCode, String city, String country, Contact contact, AddressType type) {
        this.street = street;
        this.town = town;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
        this.contact = contact;
        this.type = type;
    }

    /**
     * 
     * @return the street 
     */
    public String getStreet() {
        return street;
    }

    /**
     * 
     * @return the town
     */
    public String getTown() {
        return town;
    }
    
    /**
     * 
     * @return the postal code
     */
    public String getPostalCode() {
        return postalCode;
    }
    
    /**
     * 
     * @return the city
     */
    public String getCity() {
        return city;
    }
    
    /**
     * 
     * @return the country
     */
    public String getCountry() {
        return country;
    }
    
    /**
     * 
     * @return the contact that stores the address
     */
    public Contact getContact() {
        return contact;
    }
    
    /**
     * 
     * @return the type of address
     */
    public AddressType getType() {
        return type;
    }
}
