/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.address.ui;
import csheets.ext.contacts.Contact;

/**
 *
 * @author Joel Ferreira (1140777)
 */
public class AddressEditionExtension {
    
    /**
    * The Controller for this extension
    */
    private final AddressEditionUI theUIExtension;

    /**
     * The selected Contact
     */
    private final Contact selectedContact;
    
    /**
     * The controller extension
     *
     * @param theUIExtension The main UI extension
     */
    public AddressEditionExtension(AddressEditionUI theUIExtension) {
        this.theUIExtension = theUIExtension;
        this.selectedContact = theUIExtension.getTabContacts().getSelectedContact();
    }
    
    /**
     *
     * @return the selected contact
     */
    public Contact returnSelectedContact() {
        return selectedContact;
    }
    
}
