
package csheets.ext.address;

import csheets.ext.address.ui.AddressDTO;

/**
 * Implementation of the service for managing actions on addresses
 * @author Joel Ferreira (1140777)
 */
public class AddressService implements ManagingAddressService<AddressContact, AddressDTO> {

    @Override
    public AddressContact create(AddressDTO theInfo) {
        if (theInfo.getStreet() == null || theInfo.getTown().isEmpty() || theInfo.getPostalCode().isEmpty() || theInfo.getCity().isEmpty() || theInfo.getCountry().isEmpty()) {
            throw new UnsupportedOperationException("All field are required!.");
        }
        return new AddressContact(theInfo.getStreet(), theInfo.getTown(), theInfo.getPostalCode(), theInfo.getCity(), theInfo.getCountry(), theInfo.getType());
    }

    @Override
    public boolean remove(AddressContact toRemove) {
        return false;
    }

    @Override
    public AddressContact edit(AddressContact toEdit, AddressDTO newInfo) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
