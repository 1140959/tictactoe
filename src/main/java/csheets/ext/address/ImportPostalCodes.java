package csheets.ext.address;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Joel Ferreira (1140777)
 */
public class ImportPostalCodes {

    /**
     * XML location
     */
    private static final String XML_PATH = "res\\PostalCodes.xml";

    /**
     * List to store all postalCodes
     */
    List<PostalCode> postalCodes;

    /**
     * constructor
     */
    public ImportPostalCodes() {
        this.postalCodes = new ArrayList<>();
    }

    /**
     *
     * @return a boolean
     */
    public boolean readFile() {

        try {
            File fXmlFile = new File(XML_PATH);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("Country");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String local = eElement.getAttribute("local");
                    String postalCode = eElement.getAttribute("postalCode");
                    String regex = eElement.getElementsByTagName("regex").item(0).getTextContent();
                    PostalCode code = new PostalCode(postalCode, local, regex);
                    postalCodes.add(code);
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
        }
        return true;
    }

    /**
     * 
     * @return the full list with postal codes
     */
    public List<PostalCode> getImportedData() {
        return postalCodes;
    }

}
