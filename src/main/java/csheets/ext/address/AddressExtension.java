/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.address;

import csheets.ext.Extension;
import csheets.ext.address.ui.AddressEditionUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Joel Ferreira (1140777)
 */
public class AddressExtension extends Extension {

    /**
     * The version
     */
    public static final Long VERSION = 1L;

    /**
     * Description
     */
    public static final String DESCRIPTION = "Manage your contact addresses";
    /**
     * The extension name
     */
    private static final String NAME = "Addresses";

    /**
     * Default Constructor
     */
    public AddressExtension() {
        super(VERSION, NAME, DESCRIPTION);
    }

    /**
     * 
     * @param uiController the controller of the ui
     * @return a new UI
     */
    @Override
    public UIExtension getUIExtension(UIController uiController) {
        return new AddressEditionUI(this, uiController);
    }

}
