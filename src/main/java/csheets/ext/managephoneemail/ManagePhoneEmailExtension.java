package csheets.ext.managephoneemail;

import csheets.ext.Extension;
import csheets.ext.managephoneemail.ui.UIManagePhoneEmail;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import java.util.List;

/**
 *
 * @author David
 */
public class ManagePhoneEmailExtension extends Extension {

    /**
     * The version
     */
    public static final Long VERSION = 1L;

    /**
     * Description
     */
    public static final String DESCRIPTION = "Manage phones and emails of your contacts";

    private final static String NAME = "Manage Phone and Email";
    private List<CountryCode> countryCodes;

    public ManagePhoneEmailExtension() {
        super(VERSION, NAME, DESCRIPTION);
    }

    @Override
    public UIExtension getUIExtension(UIController uiController) {
        return new UIManagePhoneEmail(this, uiController);
    }

}
