package csheets.ext.managephoneemail.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.JMenu;

/**
 *
 * @author David
 */
public class UIManagePhoneEmail extends UIExtension {
    
    private ManagePhoneEmailMenu menu;
    
    public UIManagePhoneEmail(Extension extension, UIController uiController) {
        super(extension, uiController);
    }

    @Override
    public JMenu getMenu() {
        if(menu == null)
            menu = new ManagePhoneEmailMenu();
        return menu;
    }
    
}
