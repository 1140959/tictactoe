package csheets.ext.managephoneemail.ui;

import csheets.ext.managephoneemail.ui.panels.ManagePhoneEmailWindow;
import csheets.ui.ctrl.BaseAction;
import java.awt.event.ActionEvent;

/**
 *
 * @author David
 */
public class ManagePhoneEmailAction extends BaseAction {

    public ManagePhoneEmailAction() {
        
    }
    
    @Override
    protected String getName() {
        return "Phone and Email";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        new ManagePhoneEmailWindow();
    }

 
    
}
