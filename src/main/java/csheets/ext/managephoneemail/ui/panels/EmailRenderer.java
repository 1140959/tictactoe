package csheets.ext.managephoneemail.ui.panels;

import csheets.ext.managephoneemail.Email;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author David
 */
public class EmailRenderer extends JLabel implements ListCellRenderer<Email> {

    @Override
    public Component getListCellRendererComponent(JList<? extends Email> list, Email value, int index, boolean isSelected, boolean cellHasFocus) {
        setText(value.getAddress());
        setOpaque(true);
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        return this;
    }
}
