package csheets.ext.managephoneemail.ui.panels;

import csheets.ext.managephoneemail.PhoneNumber;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author David
 */
public class PhoneRenderer extends JLabel implements ListCellRenderer<PhoneNumber> {

    @Override
    public Component getListCellRendererComponent(JList<? extends PhoneNumber> list, PhoneNumber value, int index, boolean isSelected, boolean cellHasFocus) {
        setText(value.getCountry().getAbbrev() + " " + value.getCountry().getCode() + " " + value.getNumber());
        setOpaque(true);
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        return this;
    }

}
