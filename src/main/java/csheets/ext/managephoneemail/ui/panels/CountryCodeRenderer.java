package csheets.ext.managephoneemail.ui.panels;

import csheets.ext.managephoneemail.CountryCode;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author David
 */
public class CountryCodeRenderer extends JLabel implements ListCellRenderer<CountryCode> {

    @Override
    public Component getListCellRendererComponent(JList<? extends CountryCode> list, CountryCode value, int index, boolean isSelected, boolean cellHasFocus) {
        setText(value.getAbbrev() + " " + value.getCode());
        setOpaque(true);
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        return this;
    }
}
