package csheets.ext.managephoneemail.ui;

import javax.swing.JMenu;

/**
 *
 * @author David
 */
public class ManagePhoneEmailMenu extends JMenu{
    
    public ManagePhoneEmailMenu() {
        super("Manage Phone and Email");
        this.add(new ManagePhoneEmailAction());
    }
    
}
