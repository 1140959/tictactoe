package csheets.ext.managephoneemail;

/**
 *
 * @author David
 */
public class CountryCodeBuilder {

    private String code;
    private String abbrev;
    private String country;
    private String validation;

    public CountryCodeBuilder setCode(String code) throws Exception {
        if (code.isEmpty()) {
            throw new Exception("Code can not be empty !");
        }
        this.code = code;
        return this;
    }

    public CountryCodeBuilder setAbbrev(String abbrev) throws Exception {
        if (abbrev.isEmpty()) {
            throw new Exception("Abbrev can not be empty !");
        }
        this.abbrev = abbrev;
        return this;
    }

    public CountryCodeBuilder setCountry(String country) throws Exception {
        if (country.isEmpty()) {
            throw new Exception("Country can not be empty !");
        }
        this.country = country;
        return this;
    }

    public CountryCodeBuilder setValidation(String validation) throws Exception {
        if (validation.isEmpty()) {
            throw new Exception("Validation can not be empty !");
        }
        this.validation = validation;
        return this;
    }

    public CountryCode createCountryCode() {
        if (validation == null || country == null || code == null || abbrev == null) {
            return null;
        }
        return new CountryCode(code, abbrev, country, validation);
    }
}
