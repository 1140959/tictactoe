package csheets.ext.managephoneemail;

import csheets.persistence.RepositoryEntity;
import org.eclipse.persistence.jpa.config.Cascade;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author David
 */
@Entity
public class CompanyPhoneBook extends PhoneBook implements RepositoryEntity<PhoneBook,Long> {
    /**
     * This class repository id;
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    private PhoneNumber mainPhone;
    @OneToMany(cascade = CascadeType.ALL)
    private List<PhoneNumber> secondaryPhones;

    public CompanyPhoneBook(PhoneNumber mainPhone, List<PhoneNumber> secondaryPhones, Email mainEmail, List<Email> secondaryEmails) {
        super(mainEmail, secondaryEmails);
        this.mainPhone = mainPhone;
        this.secondaryPhones = secondaryPhones;
    }

    public CompanyPhoneBook() {
    }

    public PhoneNumber getMainPhone() {
        return mainPhone;
    }

    public void setMainPhone(PhoneNumber mainPhone) {
        this.mainPhone = mainPhone;
    }

    public List<PhoneNumber> getSecondaryPhones() {
        return secondaryPhones;
    }

    public void setSecondaryPhones(List<PhoneNumber> secondaryPhones) {
        this.secondaryPhones = secondaryPhones;
    }

    @Override
    public String toString() {
        return "CompanyPhoneBook{" + "mainPhone=" + mainPhone + ", secondaryPhones=" + secondaryPhones + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.mainPhone);
        hash = 41 * hash + Objects.hashCode(this.secondaryPhones);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CompanyPhoneBook other = (CompanyPhoneBook) obj;
        if (!Objects.equals(this.mainPhone, other.mainPhone)) {
            return false;
        }
        if (!Objects.equals(this.secondaryPhones, other.secondaryPhones)) {
            return false;
        }
        return true;
    }


    /**
     * Gets the id of this entity on the repository
     *
     * @return The primary key
     */
    @Override
    public Long id() {
        return id;
    }

    /**
     * Checks if this entity and the other are the same in the repository
     *
     * @param otherEntity Entity to Compare
     * @return True or false
     */
    @Override
    public boolean sameHas(PhoneBook otherEntity) {
        return this.id==otherEntity.id;
    }

    /**
     * Sets the entity's id
     *
     * @param id Primary key created
     */
    @Override
    public void setID(Long id) {
        this.id=id;
    }
}
