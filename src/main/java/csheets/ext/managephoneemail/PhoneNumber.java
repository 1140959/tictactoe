package csheets.ext.managephoneemail;

import csheets.persistence.RepositoryEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

/**
 *
 * @author David
 */
@Entity
public class PhoneNumber implements RepositoryEntity<PhoneBook,Long> {
    /**
     * This class repository id;
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    /**
     * Gets the id of this entity on the repository
     *
     * @return The primary key
     */
    @Override
    public Long id() {
        return id;
    }

    /**
     * Checks if this entity and the other are the same in the repository
     *
     * @param otherEntity Entity to Compare
     * @return True or false
     */
    @Override
    public boolean sameHas(PhoneBook otherEntity) {
        return this.id==otherEntity.id;
    }

    /**
     * Sets the entity's id
     *
     * @param id Primary key created
     */
    @Override
    public void setID(Long id) {
        this.id=id;
    }

    public enum Type {

        Main, Secondary, Home, Work, Celular1, Celular2;
    }

    private CountryCode country;
    private String number;

    public PhoneNumber() {
    }

    public PhoneNumber(CountryCode country, String number) {
        this.country = country;
        this.number = number;
    }

    public CountryCode getCountry() {
        return country;
    }

    public void setCountry(CountryCode country) {
        this.country = country;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.country);
        hash = 97 * hash + Objects.hashCode(this.number);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PhoneNumber other = (PhoneNumber) obj;
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.number, other.number)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PhoneNumber{" + "country=" + country + ", number=" + number + '}';
    }

}
