package csheets.ext.managephoneemail;

import csheets.persistence.RepositoryEntity;

import javax.persistence.*;
import java.util.List;

/**
 *
 * @author David
 */
@Entity
public abstract class PhoneBook implements RepositoryEntity<PhoneBook,Long> {

    /**
     * This class repository id;
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    private Email mainEmail;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Email> secondaryEmails;

    public PhoneBook() {
    }

    public PhoneBook(Email mainEmail, List<Email> secondaryEmails) {
        this.mainEmail = mainEmail;
        this.secondaryEmails = secondaryEmails;
    }

    public Email getMainEmail() {
        return mainEmail;
    }

    public void setMainEmail(Email mainEmail) {
        this.mainEmail = mainEmail;
    }

    public List<Email> getSecondaryEmails() {
        return secondaryEmails;
    }

    public void setSecondaryEmails(List<Email> secondaryEmails) {
        this.secondaryEmails = secondaryEmails;
    }

}
