package csheets.ext.managephoneemail;

/**
 *
 * @author David
 */
public class EmailBuilder {

    private String address;

    public EmailBuilder setAddress(String address) {
        if (address.isEmpty()) {
//            return null;
        }
        this.address=address;
        return this;
    }

    public Email createEmail() {
        if (address == null) {
            return null;
        }
        return new Email(address);
    }

}
