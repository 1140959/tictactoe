package csheets.ext.managephoneemail;

import csheets.ext.contacts.CompanyContact;
import csheets.ext.contacts.Contact;
import csheets.ext.contacts.PersonalContact;
import csheets.persistence.ContactRepository;
import csheets.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author David
 */
public class ManagePhoneEmailController {

    private Contact selected;
    private List<Contact> contacts;
    private List<String> contactNames;
    private List<CountryCode> countryCodes;
    private boolean set;

    public ManagePhoneEmailController() {
        set = false;
        // selected = new CompanyContact(null, null);
        contacts = new ArrayList<>();
        contactNames = new ArrayList<>();
        loadContacts();

        CountryCodeFileLoader file = new CountryCodeFileLoader();
        if (file.readFile()) {
            countryCodes = file.getImportedData();
            //System.out.println(countryCodes);
        }
    }

    private void loadContacts() {
        ContactRepository repo = PersistenceContext.repositories().contacts();
        Iterator<Contact> iter = repo.all().iterator();
        while (iter.hasNext()) {
            Contact c = iter.next();
            c.retrieveNameString(contactNames);
            contacts.add(c);
        }

//        Contact c = new CompanyContact("Company", null);
//        contacts.add(c);
//        c.retrieveNameString(contactNames);
//         c = new PersonalContact("Paulo", "Person", null, null, null, null);
//        contacts.add(c);
//        c.retrieveNameString(contactNames);
    }

    public CountryCode[] getPhonePrefixes() {
        return countryCodes.toArray(new CountryCode[countryCodes.size()]);
    }

    public List<Email> getSecondaryEmailList() {
        List<Email> lst = selected.getPhoneBook().getSecondaryEmails();
        return lst == null ? new ArrayList<>() : lst;
    }

    public List<PhoneNumber> getSecondaryPhoneList() {
        CompanyPhoneBook book = (CompanyPhoneBook) selected.getPhoneBook();
        List<PhoneNumber> lst = book.getSecondaryPhones();
        return lst == null ? new ArrayList<>() : lst;
    }

    public void setActiveContact(int index) {
        selected = (CompanyContact) contacts.get(index);
    }

    public String getMainEmail() {
        CompanyContact contact = (CompanyContact) selected;
        return contact.getPhoneBook().getMainEmail().getAddress();
    }

    public String getMainPhone() {
        CompanyPhoneBook book = (CompanyPhoneBook) selected.getPhoneBook();
        return book.getMainPhone()== null ? "" : book.getMainPhone().getNumber();
    }

    public Email buildEmail(String secondary_email) {
        return new EmailBuilder().setAddress(secondary_email).createEmail();
    }

    public PhoneNumber buildPhoneNumber(String secondary_phone, CountryCode code) {
        return new PhoneNumberBuilder().setCountryCode(code).setNumber(secondary_phone).createPhoneNumber();
    }

    public void saveCompanyData(PhoneNumber main_phone, Email main_email, List<PhoneNumber> secondary_phones, List<Email> secondary_emails) {
        CompanyContact contact = (CompanyContact) selected;
        CompanyPhoneBook book = (CompanyPhoneBook) selected.getPhoneBook();
        if (main_phone != null) {
            book.setMainPhone(main_phone);
        }
        if (main_email != null) {
            book.setMainEmail(main_email);
        }
        
        book.setSecondaryEmails(secondary_emails);
        book.setSecondaryPhones(secondary_phones);
    }

    public String[] retrieveContactNames() {
        return contactNames.toArray(new String[contactNames.size()]);
    }

    public boolean isCompanyContact(int index) {
        set = true;
        selected = contacts.get(index);
        return contacts.get(index) instanceof CompanyContact;
    }

    public boolean isPersonalContact(int index) {
        set = true;
        selected = contacts.get(index);
        return contacts.get(index) instanceof PersonalContact;
    }

    public boolean isSet() {
        return set;
    }

    public String getCelular1() {
         PersonalPhoneBook book = (PersonalPhoneBook) selected.getPhoneBook();
         return book.getCelular1().getNumber();
    }

    public String getCelular2() {
        PersonalPhoneBook book = (PersonalPhoneBook) selected.getPhoneBook();
         return book.getCalular2().getNumber();
    }

    public String getWorkPhone() {
        PersonalPhoneBook book = (PersonalPhoneBook) selected.getPhoneBook();
         return book.getWorkNumber().getNumber();
    }

    public String getHomePhone() {
        PersonalPhoneBook book = (PersonalPhoneBook) selected.getPhoneBook();
         return book.getHomeNumber().getNumber();
    }

    public void savePersonalData(PhoneNumber celular1, PhoneNumber celular2, PhoneNumber home, PhoneNumber work,Email email ,List<Email> listEmails) {
        PersonalPhoneBook book = new PersonalPhoneBook(work, home, celular1, celular2, email, listEmails);
        selected.setPhoneBook(book);
    }

}
