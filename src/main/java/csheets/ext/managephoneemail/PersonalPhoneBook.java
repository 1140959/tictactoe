package csheets.ext.managephoneemail;

import csheets.persistence.RepositoryEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;
import java.util.Objects;


/**
 *
 * @author David
 */
@Entity
public class PersonalPhoneBook extends PhoneBook implements RepositoryEntity<PhoneBook,Long> {
    /**
     * This class repository id;
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    private PhoneNumber workNumber;
    private PhoneNumber homeNumber;
    private PhoneNumber celular1;
    private PhoneNumber calular2;

    public PersonalPhoneBook() {
    }

    public PersonalPhoneBook(PhoneNumber workNumber, PhoneNumber homeNumber, PhoneNumber celular1, PhoneNumber calular2, Email mainEmail, List<Email> secondaryEmails) {
        super(mainEmail, secondaryEmails);
        this.workNumber = workNumber;
        this.homeNumber = homeNumber;
        this.celular1 = celular1;
        this.calular2 = calular2;
    }

    public PhoneNumber getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(PhoneNumber workNumber) {
        this.workNumber = workNumber;
    }

    public PhoneNumber getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(PhoneNumber homeNumber) {
        this.homeNumber = homeNumber;
    }

    public PhoneNumber getCelular1() {
        return celular1;
    }

    public void setCelular1(PhoneNumber celular1) {
        this.celular1 = celular1;
    }

    public PhoneNumber getCalular2() {
        return calular2;
    }

    public void setCalular2(PhoneNumber calular2) {
        this.calular2 = calular2;
    }

    @Override
    public String toString() {
        return "PersonalPhoneBook{" + "workNumber=" + workNumber + ", homeNumber=" + homeNumber + ", celular1=" + celular1 + ", calular2=" + calular2 + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.workNumber);
        hash = 83 * hash + Objects.hashCode(this.homeNumber);
        hash = 83 * hash + Objects.hashCode(this.celular1);
        hash = 83 * hash + Objects.hashCode(this.calular2);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PersonalPhoneBook other = (PersonalPhoneBook) obj;
        if (!Objects.equals(this.workNumber, other.workNumber)) {
            return false;
        }
        if (!Objects.equals(this.homeNumber, other.homeNumber)) {
            return false;
        }
        if (!Objects.equals(this.celular1, other.celular1)) {
            return false;
        }
        if (!Objects.equals(this.calular2, other.calular2)) {
            return false;
        }
        return true;
    }


    /**
     * Gets the id of this entity on the repository
     *
     * @return The primary key
     */
    @Override
    public Long id() {
        return this.id;
    }

    /**
     * Checks if this entity and the other are the same in the repository
     *
     * @param otherEntity Entity to Compare
     * @return True or false
     */
    @Override
    public boolean sameHas(PhoneBook otherEntity) {
        return this.id==otherEntity.id;
    }

    /**
     * Sets the entity's id
     *
     * @param id Primary key created
     */
    @Override
    public void setID(Long id) {
        this.id=id;
    }
}
