package csheets.ext.managephoneemail;

/**
 *
 * @author David
 */
public class PhoneNumberBuilder {
    
    private CountryCode country;
    private String number;
    
    public PhoneNumberBuilder setCountryCode(CountryCode code){
        this.country = code;
        return this;
    }
    
    public PhoneNumberBuilder setNumber(String number) {
        if(number.isEmpty())
            return null;
        this.number=number;
        return this;
    }
    
    public PhoneNumber createPhoneNumber(){
        if(number == null)
            return null;
        return new PhoneNumber(country, number);
    }
    
    
}
