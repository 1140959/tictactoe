package csheets.ext.managephoneemail;

import csheets.persistence.RepositoryEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

/**
 *
 * @author David
 */
@Entity
public class Email implements RepositoryEntity<Email,Long> {

    /**
     * This class repository id;
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    public static final String REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    /**
     *
     */
    public Email() {
    }

    /**
     * Gets the id of this entity on the repository
     *
     * @return The primary key
     */
    @Override
    public Long id() {
        return id;
    }

    /**
     * Checks if this entity and the other are the same in the repository
     *
     * @param otherEntity Entity to Compare
     * @return True or false
     */
    @Override
    public boolean sameHas(Email otherEntity) {
        return this.id==otherEntity.id;
    }

    /**
     * Sets the entity's id
     *
     * @param id Primary key created
     */
    @Override
    public void setID(Long id) {
        this.id=id;
    }

    public enum Type {

        Main, Secondary;
    }

    private String address;

    public Email(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.address);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Email other = (Email) obj;
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Email{" + "address=" + address + '}';
    }

}
