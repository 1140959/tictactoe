package csheets.ext.managephoneemail;

import csheets.persistence.RepositoryEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

/**
 *
 * @author David
 */
@Entity
public class CountryCode implements RepositoryEntity<CountryCode,Long> {
    /**
     * This class repository id;
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    /**
     * Gets the id of this entity on the repository
     *
     * @return The primary key
     */
    @Override
    public Long id() {
        return id;
    }

    /**
     * Checks if this entity and the other are the same in the repository
     *
     * @param otherEntity Entity to Compare
     * @return True or false
     */
    @Override
    public boolean sameHas(CountryCode otherEntity) {
        return this.id == otherEntity.id;
    }

    /**
     * Sets the entity's id
     *
     * @param id Primary key created
     */
    @Override
    public void setID(Long id) {
        this.id=id;
    }

    private String code;
    private String abbrev;
    private String country;
    private String validation;

    public CountryCode() {
    }

    /**
     * @param code
     * @param abbrev
     * @param country
     * @param validation
     */
    public CountryCode(String code, String abbrev, String country, String validation) {
        this.code = code;
        this.country = country;
        this.validation = validation;
        this.abbrev = abbrev;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "CountryCode{" + "code=" + code + ", abbrev=" + abbrev + ", country=" + country + ", validation=" + validation + '}';
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getValidation() {
        return validation;
    }

    public void setValidation(String validation) {
        this.validation = validation;
    }

    public String getAbbrev() {
        return abbrev;
    }

    public void setAbbrev(String abbrev) {
        this.abbrev = abbrev;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.code);
        hash = 79 * hash + Objects.hashCode(this.abbrev);
        hash = 79 * hash + Objects.hashCode(this.country);
        hash = 79 * hash + Objects.hashCode(this.validation);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CountryCode other = (CountryCode) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.abbrev, other.abbrev)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.validation, other.validation)) {
            return false;
        }
        return true;
    }

}
