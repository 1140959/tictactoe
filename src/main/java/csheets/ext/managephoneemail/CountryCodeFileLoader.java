package csheets.ext.managephoneemail;

import csheets.CleanSheets;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.io.InputStream;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

/**
 *
 * @author David
 */
public class CountryCodeFileLoader {

    private static final String XML_PATH = "CountryCodes.xml";
    private static final String XSD_PATH = "CountryCodes.xsd";

    List<CountryCode> countrycodes;

    public CountryCodeFileLoader() {
        this.countrycodes = new ArrayList<>();
    }

    public boolean readFile() {
        //CHECK IF FILE STRUCTURE IS VALID
        if (!validateAgainstXSD()) {
            return false;
        }

        try {
            InputStream fXmlFile = CleanSheets.class.getResourceAsStream("res/" + XML_PATH);
            //File fXmlFile = new File(XML_PATH);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("Country");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String name = eElement.getAttribute("name");
                    String abbrev = eElement.getAttribute("abbrev");
                    String prefix = eElement.getElementsByTagName("prefix").item(0).getTextContent();
                    String regex = eElement.getElementsByTagName("regex").item(0).getTextContent();

                    CountryCode code = new CountryCodeBuilder().setAbbrev(abbrev).setCode(prefix).setCountry(name).setValidation(regex).createCountryCode();
                    countrycodes.add(code);
                }
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return true;
    }

    private boolean validateAgainstXSD() {
        try {
            InputStream xml = CleanSheets.class.getResourceAsStream("res/" + XML_PATH);
            InputStream xsd = CleanSheets.class.getResourceAsStream("res/" + XSD_PATH);

            SchemaFactory factory
                    = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource(xsd));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(xml));
            return true;
        } catch (Exception ex) {
            System.out.println(ex.toString());
            return false;
        }
    }

    public List<CountryCode> getImportedData() {
        return countrycodes;
    }

}
