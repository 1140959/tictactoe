/*
 * Copyright (c) 2005 Einar Pehrson <einar@pehrson.nu>.
 *
 * This file is part of
 * CleanSheets - a spreadsheet application for the Java platform.
 *
 * CleanSheets is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CleanSheets is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CleanSheets; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package csheets.ext;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;

import csheets.CleanSheets;
import csheets.ext.comments.CommentsExtension;
import csheets.ext.style.StyleExtension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.ldap.ManageReferralControl;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

/**
 * The class that manages extensions to the CleanSheets application.
 *
 * @author Einar Pehrson
 */
public class ExtensionManager {

    /**
     * The singleton instance
     */
    private static final ExtensionManager instance = new ExtensionManager();

    /**
     * The name of the file in which mandatory extension properties are stored
     */
    private static final String MANDATORY_PROPERTIES_FILENAME = "mandatoryextensions.props";

    /**
     * The name of the file in which other extension properties are stored
     */
    private static final String PROPERTIES_FILENAME = "extensions.props";

    /**
     * The extensions that have been loaded
     */
    private SortedMap<String, Extension> extensionMap
            = new TreeMap<>();

    /**
     * The class loader used to load extensions
     */
    private Loader loader = new Loader();

    /**
     * Creates the extension manager.
     */
    private ExtensionManager() {
        List<Extension> mandatoryExtensions = loadingOfExtensions(MANDATORY_PROPERTIES_FILENAME);
        for (Extension e : mandatoryExtensions) {
            extensionMap.put(e.getName(), e);
        }
    }

    private List<Extension> loadingOfExtensions(String extensions) {
        List<Extension> list = new ArrayList<>();
        // Loads default extension properties
        Properties extProps = new Properties();
        InputStream stream = CleanSheets.class.getResourceAsStream("res/" + extensions);
        if (stream != null) {
            try {
                extProps.load(stream);
            } catch (IOException e) {
                System.err.println("Could not load default extension properties from: "
                        + extensions);
            } finally {
                try {
                    if (stream != null) {
                        stream.close();
                    }
                } catch (IOException e) {
                }
            }
        }
        // Loads user extension properties
        File userExtPropsFile = new File(extensions);
        if (userExtPropsFile.exists()) {
            stream = null;
        }
        try {
            stream = new FileInputStream(userExtPropsFile);
            extProps.load(stream);
        } catch (IOException e) {
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (IOException e) {
            }
        }
        // Loads extensions
        Extension loaded = null;
        for (Map.Entry<Object, Object> entry : extProps.entrySet()) {
            // Resolves class path
            String classPathProp = (String) entry.getValue();
            URL classPath = null;
            if (classPathProp.length() > 0) {
                // Looks for resource
                classPath = ExtensionManager.class.getResource(classPathProp);
                if (classPath == null) {
                    // Looks for file
                    File classPathFile = new File(classPathProp);
                    if (classPathFile.exists()) {
                        try {
                            classPath = classPathFile.toURL();
                        } catch (MalformedURLException e) {
                        }
                    }
                }
            }
            // Loads class
            String className = (String) entry.getKey();
            if (classPath == null) {
                loaded = load(className);
            } else {
                loaded = load(className, classPath);
            }
            list.add(loaded);
        }
        return list;
    }

    public void chooseExtensionsToLoadWindow(CleanSheets app) {
        //Lists for the extensions choosing
        List<Extension> allExtensions = loadingOfExtensions(PROPERTIES_FILENAME);
        List<Extension> toLoadExtensions = new LinkedList<>();

        //The frame of loading extensions
        ExtensionsFrame frame = new ExtensionsFrame(new ChoosingExtensionsController(allExtensions, toLoadExtensions));
        //Create a thread for running the frame
        Thread t = new Thread(new MyFrameRunning(frame, app));
        t.start(); //starts the thread
        try {
            t.join(); //The current thread waits for the thread of frame
        } catch (InterruptedException ex) {
            System.out.println(ex.getMessage());
        }

        //refresh the map
        Iterator<Extension> it = toLoadExtensions.iterator();
        while (it.hasNext()) {
            Extension next = it.next();
            extensionMap.put(next.getName(), next);
        }
    }

    /**
     * The class that manages the thread of choosing extensions
     */
    class MyFrameRunning implements Runnable {

        private final ExtensionsFrame frame;
        private final CleanSheets app;

        public MyFrameRunning(ExtensionsFrame frame, CleanSheets app) {
            this.frame = frame;
            this.app = app;
            //adds a window listenner to the frame
            frame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent we) {
                    frame.setVisible(false);
                    synchronized (instance) {
                        instance.notify();
                    }
                }
            });
        }

        @Override
        public void run() {
            //runs the thread
            frame.setVisible(true);
            while (frame.isVisible()) {
                try {
                    //app.wait();
                    Thread.sleep(1500); //The current thread sleeps for a second and a half
                } catch (InterruptedException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }

    /**
     * Returns the singleton instance.
     *
     * @return the singleton instance
     */
    public static ExtensionManager getInstance() {
        return instance;
    }

    /**
     * Returns the extensions that have been loaded.
     *
     * @return the extensions that have been loaded
     */
    public Extension[] getExtensions() {
        Collection<Extension> extensions = extensionMap.values();
        return extensions.toArray(new Extension[extensions.size()]);
    }

    /**
     * Returns the extension with the given name.
     *
     * @param name name
     * @return the extension with the given name or null if none was found
     */
    public Extension getExtension(String name) {
        return extensionMap.get(name);
    }

    /**
     * Adds the given url to the class path, and loads the extension with the
     * given class name.
     *
     * @param className the complete class name of a class that extends the
     * abstract Extension class
     * @param url the URL of the JAR-file or directory that contains the class
     * @return the extension that was loaded, or null if none was found.
     */
    public Extension load(String className, URL url) {
        loader.addURL(url);
        try {
            Class extensionClass = Class.forName(className, true, loader);
            return load(extensionClass);
        } catch (Exception e) {
            System.err.println("Failed to load extension class " + className + ".");
            return null;
        }
    }

    /**
     * Loads the extension with the given class name.
     *
     * @param className the complete class name of a class that extends the
     * abstract Extension class
     * @return the extension that was loaded, or null if none was found.
     */
    public Extension load(String className) {
        try {
            Class extensionClass = Class.forName(className);
            return load(extensionClass);
        } catch (Exception e) {
            System.err.println("Failed to load extension class " + className + ".");
            return null;
        }
    }

    /**
     * Instantiates the given extension class.
     *
     * @param extensionClass a class that extends the abstract Extension class
     * @return the extension that was loaded, or null if none was found.
     */
    public Extension load(Class extensionClass) {
        try {
            Extension extension = (Extension) extensionClass.newInstance();
            //extensionMap.put(extension.getName(), extension);
            return extension;
        } catch (IllegalAccessException iae) {
            System.err.println("Could not access extension " + extensionClass.getName() + ".");
            return null;
        } catch (InstantiationException ie) {
            System.err.println("Could not load extension from " + extensionClass.getName() + ".");
            ie.printStackTrace();
            return null;
        }
    }

    /**
     * Returns the class loader used to load extensions.
     *
     * @return the class loader used to load extensions
     */
    public ClassLoader getLoader() {
        return loader;
    }

    /**
     * The class loader used to load extensions.
     */
    public static class Loader extends URLClassLoader {

        /**
         * Creates a new extension loader.
         */
        public Loader() {
            super(new URL[]{}, Loader.class.getClassLoader());
        }

        /**
         * Appends the specified URL to the list of URLs to search for classes
         * and resources.
         *
         * @param url the URL to be added to the search path of URL:s
         */
        protected void addURL(URL url) {
            super.addURL(url);
        }
    }
}
