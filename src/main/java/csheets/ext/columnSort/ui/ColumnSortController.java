package csheets.ext.columnSort.ui;

import csheets.core.Cell;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.columnSort.Sort;
import csheets.ui.ctrl.UIController;

/**
 * An action of the column sort extension that exemplifies how to interact with the spreadsheet.
 * @author 1140445
 */
public class ColumnSortController {
    
    /**
     * The user interface controller
     */
    private UIController uiController;
 
    /**
     * Sort class
     */
    private Sort sort;
 
    /**
     * List of cells of the column
     */
    private Cell[] cellList;
 
    /**
     * The column index
     */
    private int column_index;
 
    /**
     * The cells' content list
     */
    private String[] cellsContent;
 
    /**
     * Creates the sort class.
     *
     * @param uiController the user interface controller
     */
    public ColumnSortController(UIController uiController) {
        this.uiController = uiController;
        this.sort = new Sort();
    }
 
    /**
     * Updates the column index
     */
    public void getColumn() {
        this.column_index = this.uiController.getActiveCell().getAddress().
            getColumn();
    }
 
    /**
     * Updates the cell List
     */
    public void getCellList() {
        this.cellList = this.uiController.getActiveSpreadsheet().
            getColumn(this.column_index);
    }
 
    /**
     * Updates the cells content after the descending sort
     */
    public void descendingSort() {
        this.cellsContent = this.sort.descendSort(this.cellList);
    }
 
    /**
     * Updates the cells content after the ascending sort
     *
     * @throws FormulaCompilationException exception 
     */
    public void ascendingSort() throws FormulaCompilationException {
        this.cellsContent = this.sort.ascendSort(this.cellList);
    }
 
    /**
     * Updates the spreadsheet with the sorted cells list
     *
     * @throws FormulaCompilationException exception
     */
    public void sortSpreadsheet() throws FormulaCompilationException {
        this.sort.
            sortSpreadsheet(this.uiController.getActiveSpreadsheet(), this.column_index, this.cellsContent);
    }

}
