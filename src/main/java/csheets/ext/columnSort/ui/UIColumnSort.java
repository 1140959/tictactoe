package csheets.ext.columnSort.ui;

import csheets.ext.simple.ui.*;
import javax.swing.JMenu;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * This class implements the UI interface extension for the simple extension.
 * A UI interface extension must extend the UIExtension abstract class.
 * @see UIExtension
 * @author 1140445
 */
public class UIColumnSort extends UIExtension {
 
    /**
     * The menu of the extension
     */
    private ColumnSortMenu menu;
 
    public UIColumnSort(Extension extension, UIController uiController) {
        super(extension, uiController);
    }
 
    /**
     * Returns an instance of a class that implements JMenu. In this simple case
     * this class only supplies one menu option.
     *
     * @see ExampleMenu
     * @return a JMenu component
     */
    public JMenu getMenu() {
        if (menu == null) {
            menu = new ColumnSortMenu(uiController);
        }
        return menu;
    }
}
