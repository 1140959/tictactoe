/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.columnSort.ui;

import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1140445
 */
public class DescendingSortColunsAction extends BaseAction {
 
    /**
     * The SortCellsController
     */
    private ColumnSortController controller;
 
    /**
     * Creates a new SortCellsController
     *
     * @param uiController
     */
    DescendingSortColunsAction(UIController uiController) {
        this.controller = new ColumnSortController(uiController);
    }
 
    /**
     * Returns the name of the menu item
     *
     * @return Name of the menu item
     */
    @Override
    protected String getName() {
        return "Descending Sort";
    }
 
    /**
     * Action for the descending sort
     *
     * @param e Action Event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        controller.getColumn();
        controller.getCellList();
        try {
            controller.descendingSort();
            controller.sortSpreadsheet();
        } catch (FormulaCompilationException ex) {
            Logger.getLogger(AscendingSortColunsAction.class.getName()).
                log(Level.SEVERE, null, ex);
        }
    }
 
}