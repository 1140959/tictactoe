package csheets.ext.columnSort.ui;


import javax.swing.JMenu;
import csheets.ui.ctrl.UIController;

/**
 * Representes the UI extension menu of the column sort extension.
 * @author 1140445
 */
public class ColumnSortMenu extends JMenu {

    /**
     * Changes the name of the menu and adds the menu items
     *
     * @param uiController controller
     */
    public ColumnSortMenu(UIController uiController) {
        super("Column Sort");
 
        // Adds font actions
       add(new AscendingSortColunsAction(uiController));
 
       add(new DescendingSortColunsAction(uiController));
    }
}
