/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.columnSort;
 
import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.formula.compiler.FormulaCompilationException;
 
/**
 * Class Sort responsible for sorting the Cells 
 *
 * @author 1140445
 */
public class Sort {
    
    public Sort() {
    }
 
    /**
     * Method responsible for the descending sort
     *
     * @param cellList List of cells in the selected column
     * @return The content of the cells
     */
    public String[] descendSort(Cell[] cellList) {
        String temp;
        String[] cellsContent = new String[cellList.length];
        String[] new_cellsContent = new String[cellList.length];
        int cont = 0;
 
        for (int i = 0; i < cellList.length; i++) {
            cellsContent[i] = cellList[i].getContent();
        }
 
        for (int i = 0; i < cellList.length; i++) {
            for (int j = i + 1; j < cellList.length; j++) {
                if (cellsContent[i].compareToIgnoreCase(cellsContent[j]) < 0) {
                    temp = cellsContent[j];
                    cellsContent[j] = cellsContent[i];
                    cellsContent[i] = temp;
                }
            }
        }
 
        for (int i = 0; i < cellList.length; i++) {
            if (!"".equalsIgnoreCase(cellsContent[i])) {
                new_cellsContent[cont] = cellsContent[i];
                cont++;
            }
        }
 
        return new_cellsContent;
    }
 
    /**
     * Method responsible for the ascending sort
     *
     * @param cellList List of cells in the selected column
     * @return The content of the cells
     * @throws FormulaCompilationException exception
     */
    public String[] ascendSort(Cell[] cellList) throws FormulaCompilationException {
        String temp;
        String[] cellsContent = new String[cellList.length];
        String[] new_cellsContent = new String[cellList.length];
        int cont = 0;
 
        for (int i = 0; i < cellList.length; i++) {
            Cell c = cellList[i];
            cellsContent[i] = c.getContent();
        }
 
        for (int i = 0; i < cellList.length; i++) {
            for (int j = i + 1; j < cellList.length; j++) {
                if (cellsContent[i].compareToIgnoreCase(cellsContent[j]) > 0) {
                    temp = cellsContent[j];
                    cellsContent[j] = cellsContent[i];
                    cellsContent[i] = temp;
                }
            }
        }
 
        for (int i = 0; i < cellList.length; i++) {
            if (!"".equalsIgnoreCase(cellsContent[i])) {
                new_cellsContent[cont] = cellsContent[i];
                cont++;
            }
        }
 
        return new_cellsContent;
    }

 
    /**
     * Method responsible for sorting the spreadsheet according to the the sort
     * method
     *
     * @param activeSpreadsheet The active Spreadsheet
     * @param column The column to sort
     * @param cellsContent The new content of the cells
     * @throws FormulaCompilationException exception
     */
    public void sortSpreadsheet(Spreadsheet activeSpreadsheet, int column,
                                String[] cellsContent) throws FormulaCompilationException {
 
        for (int i = 0; i < cellsContent.length; i++) {
            if (cellsContent[i] != null) {
                activeSpreadsheet.getCell(column, i).setContent(cellsContent[i]);
            } else {
                activeSpreadsheet.getCell(column, i).
                    setContent("");
            }
 
        }
    }
}