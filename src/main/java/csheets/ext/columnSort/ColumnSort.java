package csheets.ext.columnSort;

import csheets.ext.Extension;
import csheets.ext.columnSort.ui.UIColumnSort;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * @author 1140445
 */
public class ColumnSort extends Extension {

    /**
     * The version
     */
    public static final Long VERSION = 1L;

    /**
     * Description
     */
    public static final String DESCRIPTION = "Sort the columns of your spreadsheet";

    /**
     * The name of the extension
     */
    public static final String NAME = "Column Sort";

    /**
     * Creates a new Column Sort extension.
     */
    public ColumnSort() {
        super(VERSION,NAME,DESCRIPTION);
    }

    /**
     * Returns the user interface extension of this extension (an instance of
     * the class {@link  csheets.ext.columnSort.ui.UIColumnSort}). In this
     * extension example we are only extending the user interface.
     *
     * @param uiController the user interface controller
     * @return a user interface extension, or null if none is provided
     */
    @Override
    public UIExtension getUIExtension(UIController uiController) {
        return new UIColumnSort(this, uiController);
    }
}
