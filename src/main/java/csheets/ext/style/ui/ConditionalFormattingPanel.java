package csheets.ext.style.ui;

import csheets.core.CellImpl;
import csheets.core.IllegalValueTypeException;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.logging.*;
import javax.swing.*;

/**
 *
 * @author 1090698@isep.ipp.pt
 */
public class ConditionalFormattingPanel extends JPanel {

    private ConditionalFormattingController controller;

    JTextField textFieldFormula = new JTextField();

    /**
     * Create panel for sidebar. Show buttons and options for Conditional
     * Formatting.
     *
     * @param uiController
     */
    public ConditionalFormattingPanel(final UIController uiController) {
        super(new BorderLayout());
        setName("Conditional Formatting of Cells");

        controller = new ConditionalFormattingController(uiController);
        Dimension preferredSize = new Dimension(150, 25);

        /* Create Labels */
        JLabel labelFormula = new JLabel();

        /* Create Panels */
        JPanel panelCondForm = new JPanel();
        JPanel panelButtons = new JPanel();
        JPanel panelSubmit = new JPanel();
        panelCondForm.setLayout(new FlowLayout());
        panelButtons.setLayout(new FlowLayout());

        /* Create Buttons */
        JButton buttonBTrue = new JButton();
        JButton buttonBFalse = new JButton();
        JButton buttonCTrue = new JButton();
        JButton buttonCFalse = new JButton();
        JButton buttonReset = new JButton();
        JButton buttonApply = new JButton();


        /* Labels */
        labelFormula.setText("Insert formula:");
        labelFormula.setAlignmentX(CENTER_ALIGNMENT);

        /* Text Fields */
        textFieldFormula.setPreferredSize(preferredSize);
        textFieldFormula.setAlignmentX(CENTER_ALIGNMENT);

        /* Buttons */
        buttonBTrue.setText("Background TRUE");
        buttonBFalse.setText("Background FALSE");
        buttonCTrue.setText("Color TRUE");
        buttonCFalse.setText("Color FALSE");
        buttonReset.setText("Remove Style");
        buttonApply.setText("Apply Style");


        /* Add elements in panels */
        panelCondForm.add(labelFormula);
        panelCondForm.add(textFieldFormula);
        panelButtons.add(buttonBTrue);
        panelButtons.add(buttonBFalse);
        panelButtons.add(buttonCTrue);
        panelButtons.add(buttonCFalse);
        panelSubmit.add(buttonReset);
        panelSubmit.add(buttonApply);

        /* Add panels in Panel */
        add(panelSubmit, BorderLayout.SOUTH);
        add(panelButtons, BorderLayout.CENTER);
        add(panelCondForm, BorderLayout.NORTH);


        /* Action button Background True */
        buttonBTrue.addActionListener((ActionEvent e) -> {
            Color colorT = JColorChooser.showDialog(this, "Select True Color", null);
            controller.setBackgroundTrue(colorT);
            buttonBTrue.setBackground(colorT);
        });

        /* Action button Background False */
        buttonBFalse.addActionListener((ActionEvent e) -> {
            Color colorF = JColorChooser.showDialog(this, "Select False Color", null);
            controller.setBackgroundFalse(colorF);
            buttonBFalse.setBackground(colorF);
        });

        /* Action button Color True */
        buttonCTrue.addActionListener((ActionEvent e) -> {
            Color colorT = JColorChooser.showDialog(this, "Select True Color", null);
            controller.setColorTrue(colorT);
            buttonCTrue.setBackground(colorT);
        });

        /* Action button Color False */
        buttonCFalse.addActionListener((ActionEvent e) -> {
            Color colorF = JColorChooser.showDialog(this, "Select False Color", null);
            controller.setColorFalse(colorF);
            buttonCFalse.setBackground(colorF);
        });

        /* Action button Reset */
        buttonReset.addActionListener((ActionEvent e) -> {
            controller.removeStyle();
        });

        /* Action button Apply */
        buttonApply.addActionListener((ActionEvent e) -> {
            try {
                controller.applyStyles(textFieldFormula.getText(), (CellImpl) uiController.getActiveCell());
            } catch (IllegalValueTypeException ex) {
                Logger.getLogger(ConditionalFormattingPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

    }

}
