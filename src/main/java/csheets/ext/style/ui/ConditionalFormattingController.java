package csheets.ext.style.ui;

import csheets.core.CellImpl;
import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.compiler.ExcelExpressionCompiler;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.style.*;
import csheets.ui.ctrl.UIController;
import java.awt.Color;

/**
 *
 * @author 1090698@isep.ipp.pt
 */
public class ConditionalFormattingController {

    private final UIController uiController;

    Color colorTrue;
    Color colorFalse;
    Color backgroundTrue;
    Color backgroundFalse;

    /**
     * Constructor Controller
     *
     * @param uiController
     */
    public ConditionalFormattingController(UIController uiController) {
        this.uiController = uiController;
    }

    /**
     * Return result of the inserted expression
     *
     * @param string
     * @param cell
     * @return Value
     */
    public Value getExpressionResult(String string, CellImpl cell) {
        Value value = new Value();
        ExcelExpressionCompiler excelExpComp = new ExcelExpressionCompiler();
        Expression exp = null;

        try {
            exp = excelExpComp.compile(cell, string);
        } catch (FormulaCompilationException ex) {
            return new Value(ex);
        }

        try {
            value = exp.evaluate();
        } catch (IllegalValueTypeException ex) {
            return new Value(ex);
        }

        return value;
    }

    /**
     * Apply diferents styles in selected cell. If true apply style True, else
     * aply style False
     *
     * @param expression
     * @param celli
     * @throws IllegalValueTypeException
     */
    public void applyStyles(String expression, CellImpl celli) throws IllegalValueTypeException {
        Value value = getExpressionResult(expression, celli);
        StylableCell stylableCell = (StylableCell) uiController.getActiveCell().getExtension(StyleExtension.NAME);
        boolean result = value.toBoolean();

        if (result == true) {
            stylableCell.setBackgroundColor(backgroundTrue);
            stylableCell.setForegroundColor(colorTrue);
        } else {
            stylableCell.setBackgroundColor(backgroundFalse);
            stylableCell.setForegroundColor(colorFalse);
        }
    }

    /**
     * Remove style of the selected cell.
     */
    public void removeStyle() {
        StylableCell stylableCell = (StylableCell) uiController.getActiveCell().getExtension(StyleExtension.NAME);
        stylableCell.resetStyle();
    }

    /**
     * Define background color for True result.
     *
     * @param colorT
     */
    public void setBackgroundTrue(Color colorT) {
        this.backgroundTrue = colorT;
    }

    /**
     * Define background color for False result.
     *
     * @param colorF
     */
    public void setBackgroundFalse(Color colorF) {
        this.backgroundFalse = colorF;
    }

    /**
     * Define color for True result.
     *
     * @param colorT
     */
    public void setColorTrue(Color colorT) {
        this.colorTrue = colorT;
    }

    /**
     * Define color for False result.
     *
     * @param colorF
     */
    public void setColorFalse(Color colorF) {
        this.colorFalse = colorF;
    }

}
