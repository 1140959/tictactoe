/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.filesharing;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Random;

/**
 *
 * @author Ricardo Rio 1050126
 */
public class FileSharingBroadCastSender implements Runnable {

    private static int ESPERA = 20;
    private int numPort = 50505;
    private int sendPort = 50506;
    private String ipContact = "255.255.255.255"; // broadcast
    private FileList localFileList;
    private DatagramSocket datagramSocket;

    public FileSharingBroadCastSender(FileList filelist) {
        localFileList = filelist;
        try {
            this.datagramSocket = new DatagramSocket(sendPort);
        } catch (SocketException ex) {
            System.out.println(ex.toString());
        }

    }

    @Override
    public void run() {
        //System.out.println("SENDER UP");
        String localIP = "";
        try {

            String x[] = Inet4Address.getLocalHost().toString().split("/");
            localIP = x[1];
        } catch (Exception ex) {
            System.out.println("Sender (Erro Nao foi possivel ver o IP local): " + ex.toString());

        }

        while (true) {

            try {
                datagramSocket.setBroadcast(true);
                while (true) {
                    Random randomGenerator = new Random();

                    String msg = "CSLAPR4:SendingMyFiles"; // + ":" + ficheiros.getLocalFileCount();
                    DatagramPacket dp = new DatagramPacket(msg.getBytes(),
                            msg.length(),
                            InetAddress.getByName(ipContact),
                            numPort);
                    //System.out.println("SendingInitSendFiles: (" + msg + ")");

                    datagramSocket.send(dp); // send
                    int count = 0;
                    for (FileInfo f : localFileList.getLocalFiles()) {

                        msg = "CSLAPR4:" + f.getName() + ":" + f.getSize();
                        System.out.println("Sender: Enviar->" + msg);
                        dp = new DatagramPacket(msg.getBytes(),
                                msg.length(),
                                InetAddress.getByName(ipContact),
                                numPort);

                        count++;
                        datagramSocket.send(dp);
                    }
                    // System.out.println("Ficheiros enviados: " + count);
                    Thread.sleep(ESPERA * 1000);
                }
            } catch (Exception ex) {
                System.err.println("Sender: (Erro)- " + ex.toString());
            }

        }
    }

}
