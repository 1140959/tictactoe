/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.filesharing;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Ricardo Rio 1050126
 */
public class FileSharingNetworkController {
    JTable networkTable,downloadTable;
    
    public FileSharingNetworkController(JTable table1,JTable table2){
        networkTable = table1;
        downloadTable = table2;
        FileSharingAssembler sharingFiles = new  FileSharingAssembler(this);
        sharingFiles.startSharing();
    }
    
    public void updateNetwork(FileList fx){
        
        DefaultTableModel model = (DefaultTableModel) networkTable.getModel();
        
        int rowCount = model.getRowCount();
        //Remove rows one by one from the end of the table
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        for (FileInfo x : fx.getNetWorkFiles()) {
                if(x.getMachine().length()>0) 
                    model.addRow(new Object[]{x.getName(), "" + x.getSize()});

        }
        
        
    }
}
