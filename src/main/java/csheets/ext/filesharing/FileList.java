/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.filesharing;

import java.util.ArrayList;
import java.util.Calendar;


/**
 * Saves a list os a Class FileInfo
 * this class controls the list.
 * 
 * @author Ricardo Rio 1050126
 */
public class FileList {
    private final int MAX_FILE_TIME = 30000;
    private ArrayList<FileInfo> lista;
    private final Object lock = new Object();

    public FileList() {
        lista = new ArrayList<>();
    }

    
    
    /**
     * Adds a file to the list
     * @param machine Machine name or IP 
     * @param file File name
     * @param size Size of File 
     */
    public  void addFile(String machine, String file,long size) {
        synchronized (lock) {
            if (exist(machine,file,size) == false) {
                lista.add(new FileInfo(machine, file, size, Calendar.getInstance().getTimeInMillis()));
            } else {
                updateTimeStamp(machine,file,size);
            }
        }
    }

    /** 
     * Checks if the file already is on the list
     * @param machine Machine name or IP 
     * @param file File name
     * @param size Size of File 
     * @return boolean 
     */
    public synchronized boolean exist(String machine, String file,long size) {
        synchronized (lock) {
            FileInfo fileInfo = new FileInfo(machine, file, size,0);

            for (FileInfo lista1 : lista) {
                if (fileInfo.equals(lista1)) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Gets a list of network files
     * @return ArrayList Returns a list of network Files
     */
    public ArrayList<FileInfo> getNetWorkFiles() {
        synchronized (lock) {
            ArrayList<FileInfo> networkList = new ArrayList();

            for (FileInfo x : lista) {
                if(x.getMachine().length()>0) 
                    networkList.add(x);
            }
            return networkList;
        }
    }

    /**
     * Gets a list of local files
     * @return ArrayList Returns a list of local Files
     */
    public ArrayList<FileInfo> getLocalFiles() {
        synchronized (lock) {
            ArrayList<FileInfo> localList = new ArrayList<>();

            for (FileInfo x : lista) {
                if (x.getMachine().length() == 0) {
                    localList.add(x);
                }
            }

            return localList;
        }
    }

    /** 
     * Updates Timestamp on a file.
     * @param machine Machine name or IP 
     * @param file File name
     * @param size Size of File 
     */
    public void updateTimeStamp(String machine, String file, long size) {
        synchronized (lock) {
            for (FileInfo x : lista) {
                if (x.getMachine().compareTo(machine) == 0 && x.getName().compareTo(file) == 0  && x.getSize() == size) {
                    x.setTime(Calendar.getInstance().getTimeInMillis());
                }
            }
        }
    }

    
    /**
     * Cleans files with a timestamp larger then MAX_FILE_TIME
     */
    public void cleanOld() {
        synchronized (lock) {
            long lastData = Calendar.getInstance().getTimeInMillis();
            ArrayList newlista = new ArrayList<>();
            for (FileInfo x : lista) {
                if(x.getMachine().length()==0){
                    newlista.add(x);
                    
                } else if (lastData - x.getTime() < MAX_FILE_TIME){ 
                    newlista.add(x);
                }
                
            }
            lista=new ArrayList<>(newlista);
        }
    }
    
}
