/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.filesharing.ui;

import csheets.ext.filesharing.FileSharingController;
import csheets.ext.filesharing.FileSharingExtension;
import csheets.ext.filesharing.FileSharingNetworkController;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Ricardo Rio 1050126
 */
public class FileSharingPanel extends JPanel {

    /**
     * test to see if it works
     *
     */
    private JTextArea commentField = new JTextArea();
    private JTable table = new JTable();
    private final JButton chooseShareBtn;
    private JTable tableNetwork,tableDownloads;
    private JLabel titulo1,titulo2;
    //private int counter=0;
    /**
     * The assertion controller
     */
    private FileSharingController controller;

    public FileSharingPanel(UIController uiController) {
        // Configures panel
        super(new BorderLayout());

        controller = new FileSharingController(uiController);
        this.setName(FileSharingExtension.NAME);
                // uiController.addSelectionListener(this);

        chooseShareBtn = new JButton("Start Sharing Files");

        chooseShareBtn.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                 FileSharingNetworkController fsnc = new FileSharingNetworkController(tableNetwork,tableDownloads);
                 chooseShareBtn.setEnabled(false);
                 chooseShareBtn.setText("Sharing Started");
            }    
            
        });

        // table = new JTable(dataModel);
        JScrollPane scrollpane = new JScrollPane(table);
        
        String[] columnNames = {"FileName", "Size"};
        Object[][] data
                = {
                    {"Not", "started"}
                    
                    };
        DefaultTableModel model = new DefaultTableModel(data, columnNames);
        tableNetwork = new JTable(model) {
            //  Returning the Class of each column will allow different
            //  renderers to be used based on Class
            public Class getColumnClass(int column) {
                return getValueAt(0, column).getClass();
            }
        };

        String[] columnNames2 = {"FileName", "Size"};
        Object[][] data2
                = {
                    {"No files to", "donwload"},
                    };
        DefaultTableModel model2 = new DefaultTableModel(data2, columnNames2);
        tableDownloads = new JTable(model2) {
            //  Returning the Class of each column will allow different
            //  renderers to be used based on Class
            public Class getColumnClass(int column) {
                return getValueAt(0, column).getClass();
            }
        };

        
        setLayout(new GridLayout(2,1));
        JPanel fileSharingPanel = new JPanel();
        titulo1 = new JLabel("Network Files");
        fileSharingPanel.setLayout(new BoxLayout(fileSharingPanel, BoxLayout.PAGE_AXIS));
        fileSharingPanel.setPreferredSize(new Dimension(130, 336));
        fileSharingPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));		// width, height
        
        fileSharingPanel.add(chooseShareBtn);
        fileSharingPanel.add(titulo1);
        JScrollPane js=new JScrollPane(tableNetwork);
        js.setVisible(true);
        
        fileSharingPanel.add(js);
        add(fileSharingPanel);
        
        
        JPanel fileSharingPanel2 = new JPanel();
        titulo2 = new JLabel("to Download");
        
        fileSharingPanel2.setLayout(new BoxLayout(fileSharingPanel2, BoxLayout.PAGE_AXIS));
        fileSharingPanel2.setPreferredSize(new Dimension(130, 336));
        fileSharingPanel2.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));		// width, height
        // fileSharingPanel2.add(chooseShareBtn, BorderLayout.EAST);
        fileSharingPanel2.add(titulo2);
        JScrollPane js2=new JScrollPane(tableDownloads);
        js2.setVisible(true);
        
        
        fileSharingPanel2.add(js2);
        add(fileSharingPanel2);
        
    }

    TableModel dataModel = new AbstractTableModel() {
        public int getColumnCount() {
            return 2;
        }

        public int getRowCount() {
            return 10;
        }

        public Object getValueAt(int row, int col) {
            return new Integer(row * col);
        }

    };


}
