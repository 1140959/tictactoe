/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.filesharing.ui;

import csheets.ext.filesharing.FileSharingController;
import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author Ricardo Rio 1050126
 */
public class FileSharingMenu extends JMenu {

    public FileSharingMenu(UIController uiController) {
		super("File Sharing");
		setMnemonic(KeyEvent.VK_E);

		// Adds action 
		add(new FileSharingController(uiController));
	}	

    
}
