/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.filesharing.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;

/**
 *
 * @author Ricardo Rio 1050126
 */
public class FileSharingUI extends UIExtension {

    private JComponent jSideBar;
    
    private Icon icon;

    /** The menu of the extension */
	private FileSharingMenu menu;

    
    public FileSharingUI(Extension extension, UIController uiController) {
        super(extension, uiController);
        
    }
    public Icon getIcon(){
       return icon;
   }

    public JMenu getMenu() {
		if (menu == null)
			menu = new FileSharingMenu(uiController);
		return menu;
	}
	
    /**
     * Returns a side bar that provides editing of text note.
     *
     * @return a side bar
     */
    public JComponent getSideBar() {
        
        if (jSideBar == null) {
            
            jSideBar = new FileSharingPanel(uiController);
        }
        return jSideBar;
        
    }
}
