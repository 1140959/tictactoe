package csheets.ext.filesharing;

import java.util.Objects;
/**
 * The FileInfo Saves information on a file, name, machine and size.
 * there is an extra variable to see if the file was or was not updated.
 *
 * @author Ricardo Rio 1050126
 */

public class FileInfo {

    private String machine;
    private String name;
    private long size;
    private long time;
    
    public FileInfo(String machine, String name, long size, long time) {
        this.machine = machine;
        this.name = name;
        this.size = size;
        this.time = time;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileInfo other = (FileInfo) obj;
        if (!Objects.equals(this.machine, other.machine)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.size, other.size)) {
            return false;
        }
        return true;
    }

    /**
     *  Gets Machine Name or IP
     * @return String Returns the machines name or IP frome where the file came / Empty if local
     */
    public String getMachine() {
        return this.machine;

    }

    /** 
     *  Gets File Name 
     * @return String Returns the file name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets File Size
     * @return Long Returns file size
     */
    public long getSize() {
        return this.size;
    }

    /**
     * Gets last time it was timestamped
     * @return Long returns files time stamp
     */
    public long getTime(){
        return this.time;
    }
            
    /**
     * Sets the file size
     * @param size The file size
     */
    public void setSize(long size) {
        this.size = size;
    }
    /** 
     * Sets machines or IPs name  
     * @param machine The machines name or IP from where the file is located / empty if local
     */
    public void setMachine(String machine) {
        this.machine = machine;
    }

    /**
     * Sets file name
     * @param name The file name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Sets Timestamp to file
     * @param time Files last Time stamp
     */    
    public void setTime(long time){
        this.time = time;
    }
}
