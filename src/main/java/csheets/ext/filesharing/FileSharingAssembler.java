/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.filesharing;

import javax.swing.JFileChooser;


/**
 *
 * @author Ricardo Rio 1050126
 */
public class FileSharingAssembler {
    private FileSharingNetworkController crtl;
    private FileList theList, downloadList;
    
    private String shareDirectory,downloadDirectory;
    
    public FileSharingAssembler(FileSharingNetworkController crtl) {
        this.crtl = crtl;
        
    }
    
    public void startSharing(){
        shareDirectory = chooseSharedFiles("Choose Share Folder");
        downloadDirectory = chooseSharedFiles("Choose Download Folder");
        theList = new FileList();
        FileNameLoader readDir = new FileNameLoader(theList,shareDirectory);
        readDir.readLocalFiles();
        new Thread(new FileSharingTableUpdater(theList,this)).start();
       
    }
    
    
    public void updateNetworkTable(FileList list){
        crtl.updateNetwork(theList);
    }

    public String chooseSharedFiles(String titleText) {

        JFileChooser fc = new JFileChooser(".");

        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle(titleText);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            return chooser.getSelectedFile().toString();
        } else {
            System.out.println("No Selection ");
        }
        return chooser.getSelectedFile().toString();
    }


}

