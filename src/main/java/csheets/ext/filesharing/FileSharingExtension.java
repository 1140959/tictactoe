/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.filesharing;

import csheets.ext.Extension;
import csheets.ext.filesharing.ui.FileSharingUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Ricardo Rio 1050126
 */
public class FileSharingExtension extends Extension {

    /**
     * The version
     */
    public static final Long VERSION = 1L;

    /**
     * Description
     */
    public static final String DESCRIPTION = "Sharing files between CleanSheet";

    /**
     * The extension name
     */
    public static final String NAME = "File Sharing";

    /**
     * Default Constructor
     */
    public FileSharingExtension() {
        super(VERSION, NAME, DESCRIPTION);
    }

    public UIExtension getUIExtension(UIController uiController) {
        return new FileSharingUI(this, uiController);
    }

}
