/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.filesharing;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;

/**
 *
 * @author Ricardo Rio 1050126
 */
public class FileSharingBroadCastReceiver implements Runnable {

    private int numPorto = 50505;
    private FileList fileList;
    private DatagramSocket datagramSocket;
    private String ipContact = "255.255.255.255";
    private String localIP;
    
    public FileSharingBroadCastReceiver(FileList ficheiros) {
        this.fileList = ficheiros;

        try {
            this.datagramSocket = new DatagramSocket(numPorto);
        } catch (SocketException ex) {
            System.out.println(ex.toString());
        }

        try {
            
            String x[] = Inet4Address.getLocalHost().toString().split("/");
            localIP = x[1];
        } catch (Exception ex) {
            System.out.println("Sender (Erro Nao foi possivel ver o IP local): " + ex.toString());

        }

    }

    @Override
    public void run() {
        try {
            datagramSocket.setBroadcast(true);
            while (true) {
                byte[] msgF = new byte[50];
                DatagramPacket dp = new DatagramPacket(msgF, msgF.length,
                        InetAddress.getByName(ipContact),
                        numPorto);
                try {

                    datagramSocket.receive(dp);

                    
                    InetAddress x = dp.getAddress();
                        
                    String ipFile = x.toString();
                    System.out.println("Recebeu pedido do ip:" + ipFile);
                    if (ipFile.compareTo("/"+localIP) != 0 && ipFile.compareTo("/127.0.0.1")!=0) {
                        
                    
                    String strRecebido = new String(dp.getData()).trim();
                    if (strRecebido.compareTo("CSLAPR4:SendingMyFiles") != 0) {
                        String[] fileData = strRecebido.split(":");
                        if (fileData.length == 3) {
                            if (fileData[0].compareTo("CSLAPR4") == 0) {
                               
                               fileList.addFile(ipFile, fileData[1], Long.parseLong(fileData[2]));
                               //System.out.print("Read: " + fileData[1] + " - " + fileData[2]);
                            }
                        }
                    } 
                    }
                } catch (SocketTimeoutException e) {
                    

                }

            }

        } catch (Exception ex) {
            System.err.println("Receiver: (Erro)- " + ex.toString());
        }

    }
}
