/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.filesharing;

/**
 *
 * @author Ricardo Rio 1050126
 */
public class FileSharingTableUpdater implements Runnable {
    private FileList theList;
    private FileSharingAssembler assemble;
    
    public FileSharingTableUpdater(FileList list,FileSharingAssembler assemble){
        this.theList = list;
        this.assemble = assemble;
     
        
        
        
        
        
        
    }
    
     @Override
    public void run() {
        new Thread(new FileSharingBroadCastReceiver(theList)).start();
        new Thread(new FileSharingBroadCastSender(theList)).start();
       
        while(true){
            try{
                Thread.sleep(30000);
            } catch (Exception ex) {
                System.out.print("Error: " + ex.getMessage());
            } 
            this.assemble.updateNetworkTable(theList);
        }
    
    }
    
    
}
