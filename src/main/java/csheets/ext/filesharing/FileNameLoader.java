/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.filesharing;

import java.io.File;

/**
 *
 * @author Ricardo Rio 1050126
 */
public class FileNameLoader {
    private FileList list;
    private String directory;
    
    public FileNameLoader(FileList list,String directory){
        this.list = list;
        this.directory = directory;
    }
    
    /**
     * Reads Files from a directory
     * @return int number of files in the directory
     */
    public int readLocalFiles() {

            int count = 0;
            File d;
            d = new File(directory);

            File[] filesList = d.listFiles();
            for (File f : filesList) {
                if (f.isFile()) {
                    if (list.exist("", f.getName(),f.length()) == false) {
                        list.addFile("", f.getName(),f.length());
                        count++;
                    } else {
                        list.updateTimeStamp("", f.getName(), f.length());
                    }
                }
            }

            return count;
    }

    
}
