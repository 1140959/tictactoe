/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.notes;

import csheets.ext.contacts.Contact;
import java.time.LocalDateTime;

/**
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 * @param <T>
 * @param <C>
 */
public interface ManagingNoteService<T, C> {

    /**
     * Creates a new entity
     *
     * @param theInfo information for the creation
     * @return Object created
     * @throws java.lang.Exception
     */
    T create(C theInfo) throws Exception;

    /**
     * Edits a given entity
     *
     * @param description
     * @param date
     * @param newNote New info
     * @return An object
     * @throws java.lang.Exception
     */
    T edit(String description,T newNote) throws Exception;

    /**
     * Removes a given entity
     *
     * @param toRemove Object to remove
     * @param contact
     * @return true if successfully removed
     * @throws java.lang.Exception
     */
    boolean remove(T toRemove, Contact contact) throws Exception;

    boolean addNewHistory(T newNote);

    boolean addHistorytoListNote(T noteContact, T newNote);
}
