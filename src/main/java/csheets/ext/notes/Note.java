/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.notes;

import csheets.ext.contacts.Profession;
import csheets.persistence.RepositoryEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 */
@Entity
public class Note implements RepositoryEntity<Note,Long> {
    /**
     * This class repository id;
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id = 1L;
    /**
     * This note's text
     */
    private String text;
    /**
     * This note's title
     */
    private String title;

    /**
     * This note's timestamp
     */
    private LocalDateTime timestamp;

    /**
     * History a text note
     */
    @OneToMany(cascade = CascadeType.ALL)
    private List<HistoryNote> histNote;

    public Note() {
    }

    /**
     * Constuctor with receive a string text and a LocalDateTime
     *
     * @param text
     * @param timestamp
     */
    public Note(String text, LocalDateTime timestamp) {
        this.text = text;
        this.timestamp = timestamp;
    }

    /**
     * Constuctor with receive a string text and a LocalDateTime
     *
     * @param text description note
     * @param title title note
     * @param timestamp date create the text note
     */
    public Note(String text, String title, LocalDateTime timestamp) {
        this.text = text;
        this.title = title;
        this.timestamp = timestamp;
        this.histNote = new ArrayList<>();
    }

    public void alterNote(String description,String newTitle) {
        this.text = description;
        this.title = newTitle;
    }

    /**
     *
     * @return list the history
     */
    public List<HistoryNote> getListHistory() {
        return this.histNote;
    }

    public boolean addNewElement(HistoryNote hist) {
        return this.histNote.add(hist);
    }

    public String getText() {
        return this.text;
    }

    public String getTitle() {
        return this.title;
    }

    public LocalDateTime getTime() {
        return this.timestamp;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.text);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Note other = (Note) obj;
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Titulo:" + this.title + " Descricao: " + this.text
                + " Time: " + this.timestamp.getHour() + ":" + this.timestamp.getMinute();
    }

    public String toStringTitle() {
        return "Titulo:" + this.title;
    }

    @Override
    public Long id() {
        return this.id;
    }

    @Override
    public boolean sameHas(Note otherEntity) {
        return this.id()==otherEntity.id();
    }

    @Override
    public void setID(Long id) {
        this.id = id;
    }
}
