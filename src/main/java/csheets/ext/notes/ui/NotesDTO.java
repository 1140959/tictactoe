/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.notes.ui;

import java.time.LocalDateTime;

/**
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 */
public class NotesDTO {

    /**
     * Content is the note
     */
    private final String text;
    /**
     * This timestamp text Note
     */
    private final LocalDateTime timestamp;

    /**
     * Constructor
     * @param text
     * @param timestamp
     */
    public NotesDTO(String text, LocalDateTime timestamp) {
        this.text = text;
        this.timestamp = timestamp;
    }

    public LocalDateTime getTimeStamp() {
        return this.timestamp;
    }

    public String getText() {
        return this.text;
    }
    
}
