/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.notes.ui.panels;


/**
 * The contexte of the popup
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 */
public class PopupContext {

    /**
     * The strategy to be defined
     */
    private final PopupStrategy strategy;

    /**
     * Valid Constructor
     * @param strategy Strategy to be used
     */
    public PopupContext(PopupStrategy strategy) {
        this.strategy = strategy;
    }

    /**
     * Executes the popup window with the given strategy
     */
    public void executePopupWindow() {
        strategy.drawFrame();
    }
}
