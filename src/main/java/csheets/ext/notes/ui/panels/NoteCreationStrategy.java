/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.notes.ui.panels;

import csheets.ext.notes.ui.NoteExtensionUI;
import java.awt.FlowLayout;
import javax.swing.JFrame;

/**
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 */
public class NoteCreationStrategy implements PopupStrategy {

    /**
     * The panel to drawn in the frame
     */
    private final NotePopupPanel thePanel;

    private final NoteExtensionUI mainUI;

    /**
     * Complete Constructor
     *
     * @param thePanel the panel to present
     * @param ui the uiExtension
     */
    public NoteCreationStrategy(NoteExtensionUI ui, NotePopupPanel thePanel) {
        this.thePanel = thePanel;
        this.mainUI = ui;
    }

    @Override
    public void drawFrame() {
        thePanel.setEditOFF();
        thePanel.setRemoveOFF();
        JFrame fr = new JFrame();
        fr.setTitle("Create New Contact");
        fr.setLayout(new FlowLayout(FlowLayout.CENTER));
        fr.getContentPane().add(thePanel);
        fr.pack();
        fr.setLocationRelativeTo(null);
        thePanel.setMyFrame(fr);
        fr.setVisible(true);
    }
}
