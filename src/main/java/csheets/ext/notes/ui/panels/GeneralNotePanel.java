package csheets.ext.notes.ui.panels;

import csheets.ext.contacts.Contact;
import csheets.ext.notes.Note;
import csheets.ext.notes.ui.NoteExtensionUI;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 * @param <T>
 */
public class GeneralNotePanel<T> extends JPanel {

    protected Contact contact;

    protected NoteExtensionUI uiExtension;
    /**
     * The list with text note
     */
    protected JList<T> jlist;
    /**
     * Button for create new text note
     */
    protected JButton createNote = new JButton();

    /**
     * Button for view the history the text note
     */
    protected JButton viewHistory = new JButton();

    protected JLabel userContact = new JLabel();

    /**
     *
     * @param ui
     * @param uiController
     * @param panelName Panel Name
     */
    public GeneralNotePanel(NoteExtensionUI ui, UIController uiController, String panelName) {
        super(new BorderLayout());
        setName(panelName);
        this.uiExtension = ui;

        TitledBorder border = BorderFactory.createTitledBorder("");
        border.setTitleJustification(TitledBorder.LEFT);
        this.setPreferredSize(new Dimension(200, 600));
        setBorder(border);

        // Adds panel for button
        JPanel panel = new JPanel(new GridLayout(2, 1));
        panel.setPreferredSize(new Dimension(150, 60));

        Dimension dim = new Dimension(120, 25);
        JPanel btncreate = new JPanel(new FlowLayout(FlowLayout.LEFT));
        createNote.setPreferredSize(dim);
        btncreate.add(createNote);
        panel.add(btncreate);
        JPanel btnview = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        viewHistory.setPreferredSize(dim);
        btnview.add(viewHistory);
        panel.add(btnview);

        // The text notes list
        jlist = new JList<>(new DefaultListModel<T>());
        jlist.setBorder(BorderFactory.createTitledBorder("List Note"));
        jlist.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        jlist.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        jlist.setPreferredSize(new Dimension(300, 490));
        jlist.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));

        userContact.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        
        contact = ui.getUserContact();
        List<Note> noteList= new ArrayList<>();
        contact.alterList(noteList);

        this.add(userContact, BorderLayout.NORTH);
        this.add(jlist, BorderLayout.CENTER);
        this.add(panel, BorderLayout.SOUTH);
    }

    public JList<T> getList() {
        return this.jlist;
    }
}
