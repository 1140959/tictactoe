package csheets.ext.notes.ui.panels;


/**
 * Strategy for popup menus
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 */
public interface PopupStrategy {
    
    void drawFrame();
}
