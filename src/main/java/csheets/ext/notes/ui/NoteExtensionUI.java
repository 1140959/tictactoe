/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.notes.ui;

import csheets.ext.notes.ui.panels.GeneralNotePanel;
import csheets.ext.Extension;
import csheets.ext.contacts.Contact;
import csheets.ext.notes.NoteController;
import csheets.ext.notes.NoteExtension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

/**
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 */
public class NoteExtensionUI extends UIExtension {

    /**
     * Controller the text note
     */
    private NoteController noteController;

    /**
     * Contact active in the system
     */
    private Contact mainContact;

    /**
     * A panel in which note will be display
     */
    private JComponent jSideBar;
    /**
     * Icon for display view
     */
    private Icon icon;

    private GeneralNotePanel notePanel;

    /**
     * Constructor
     *
     * @param extension
     * @param uiController
     */
    public NoteExtensionUI(Extension extension, UIController uiController) {
        super(extension, uiController);
    }

    /**
     * @return icon image with identify the extend choise
     */
    @Override
    public Icon getIcon() {
        if (icon == null) {
            icon = new ImageIcon(NoteExtension.class.getResource("res/img/note.gif"));
        }
        return icon;
    }

    /**
     * Returns a side bar that provides editing of text note.
     *
     * @return a side bar
     */
    @Override
    public JComponent getSideBar() {
        if (jSideBar == null) {
            jSideBar = new NotePanel(this, uiController);
        }
        return jSideBar;
    }

    /**
     *
     * @return NotePanel is the expecific time of the Generenal Panel
     */
    public NotePanel getNotePanel() {
        return (NotePanel) notePanel;
    }

    public void setUserContact(Contact contact) {
        this.mainContact = contact;
    }

    public Contact getUserContact() {
        noteController = new NoteController(this);
        return noteController.getContactActive();
    }
}
