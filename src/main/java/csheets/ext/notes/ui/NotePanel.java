package csheets.ext.notes.ui;

import csheets.ext.notes.ui.panels.GeneralNotePanel;
import csheets.ext.notes.Note;
import csheets.ext.notes.NoteController;
import csheets.ext.notes.ui.panels.NoteCreationStrategy;
import csheets.ext.notes.ui.panels.NoteManagementStrategy;
import csheets.ext.notes.ui.panels.NotePopupPanel;
import csheets.ext.notes.ui.panels.NotePopupPanelView;
import csheets.ext.notes.ui.panels.NoteViewStrategy;
import csheets.ext.notes.ui.panels.PopupContext;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingUtilities;

/**
 * Panel for Note Edition
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 */
public class NotePanel extends GeneralNotePanel<Note> {

    public NotePanel(NoteExtensionUI noteUI, UIController uiController) {
        super(noteUI, uiController, "Notes");
        createNote.setText("New Note");
        createNote.addActionListener(new CreateAction(this));
        viewHistory.setText("View History");
        viewHistory.addActionListener(new ViewAction(this));
        jlist.addMouseListener(new ClickingAction(noteUI, this));
    }

    private class ClickingAction extends MouseAdapter {

        private final NoteExtensionUI noteExtensionUI;

        private final NotePanel thePanel;

        public ClickingAction(NoteExtensionUI ui, NotePanel panel) {
            this.noteExtensionUI = ui;
            this.thePanel = panel;
        }

        @Override
        public void mouseClicked(MouseEvent me) {
            if (SwingUtilities.isLeftMouseButton(me) && me.getClickCount() >= 2) {
                doubleClickAction();
            }
            if (SwingUtilities.isLeftMouseButton(me) && me.getClickCount() == 1) {
                oneClickAction();
            }
        }

        private void oneClickAction() {
            jlist.getSelectedValue();
        }

        private void doubleClickAction() {

            final int selectedContact = jlist.getSelectedIndex();

            if (selectedContact >= 0) {
                PopupContext pop = new PopupContext(new NoteManagementStrategy(uiExtension,
                        new NotePopupPanel(new NoteController(uiExtension), thePanel, contact)));
                pop.executePopupWindow();
            }
        }
    }

    class CreateAction implements ActionListener {

        private final NotePanel thePanel;

        private CreateAction(NotePanel panel) {
            this.thePanel = panel;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            PopupContext pop = new PopupContext(new NoteCreationStrategy(uiExtension,
                    new NotePopupPanel(new NoteController(uiExtension), thePanel, contact)));

            pop.executePopupWindow();
        }

    }

    class ViewAction implements ActionListener {

        private final NotePanel thepanel;

        public ViewAction(NotePanel the) {
            this.thepanel = the;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (jlist.getSelectedValue() != null) {
                PopupContext pop = new PopupContext(new NoteViewStrategy(uiExtension,
                        new NotePopupPanelView(new NoteController(uiExtension), thepanel, jlist.getSelectedValue())));
                pop.executePopupWindow();
            }
        }

    }
}
