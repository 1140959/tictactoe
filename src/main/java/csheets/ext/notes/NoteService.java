/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.notes;

import csheets.ext.contacts.Contact;
import csheets.ext.notes.ui.NotesDTO;
import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 */
public class NoteService implements ManagingNoteService<Note, NotesDTO> {

    @Override
    public Note create(NotesDTO theNote) throws Exception {
        if (theNote.getText() == null || theNote.getText().isEmpty() || theNote.getText().length() < 10) {
            throw new Exception("Text not valid\nPlease insert a text with at least 10 characters.");
        }

        final LocalDateTime dueDay = theNote.getTimeStamp();

        if (dueDay == null) {
            throw new Exception("Due date not valid! Date must be greater or equal to today!");
        }

        String title = createTitlebyText(theNote.getText());
        return new Note(theNote.getText(), title, theNote.getTimeStamp());
    }

    @Override
    public Note edit(String description,Note note) throws Exception {
        String title=createTitlebyText(description);
        note.alterNote(description,title);
        return note;
    }

    @Override
    public boolean remove(Note toRemove, Contact contact) throws Exception {
        List<Note> list = contact.notes();
        return list.remove(toRemove);
    }

    private String createTitlebyText(String text) {
        int sizeString = text.length();
        if (sizeString > 5) {
            if (text.contains(" ")) {
                String parse[] = text.split(" ");
                return parse[0];
            } else {
                return text.substring(10);
            }
        }
        return text;
    }

    @Override
    public boolean addHistorytoListNote(Note noteContact, Note newNote) {
        HistoryNote historyNote = new HistoryNote(newNote.getText(), newNote.getTitle(), LocalDateTime.now(), newNote.getTime());
        return noteContact.addNewElement(historyNote);
    }

    @Override
    public boolean addNewHistory(Note newNote) {
        HistoryNote historyNote = new HistoryNote(newNote.getText(), newNote.getTitle(), LocalDateTime.now(), newNote.getTime());
        return newNote.addNewElement(historyNote);
    }
}
