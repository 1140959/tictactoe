package csheets.ext.notes;

import csheets.persistence.RepositoryEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

/**
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 */
@Entity
public class HistoryNote implements RepositoryEntity<HistoryNote,Long> {

    /**
     * This class repository id;
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    private String text = null;
    private String title = null;;
    private LocalDateTime change = null;;
    private LocalDateTime timestamp = null;;

    public HistoryNote() {
    }

    public HistoryNote(String text, String title, LocalDateTime change, LocalDateTime timestamp) {
        this.text = text;
        this.title = title;
        this.change = change;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Titulo: " + this.title + "  Descricao: " + this.text + " Created: "
                + this.timestamp.getHour() + ":" + this.timestamp.getMinute()
                + "\n Change: " + this.change.getHour() + ":" + this.change.getMinute();
    }

    /**
     * Gets the id of this entity on the repository
     *
     * @return The primary key
     */
    @Override
    public Long id() {
        return this.id;
    }

    /**
     * Checks if this entity and the other are the same in the repository
     *
     * @param otherEntity Entity to Compare
     * @return True or false
     */
    @Override
    public boolean sameHas(HistoryNote otherEntity) {
        return this.id()==otherEntity.id();
    }

    /**
     * Sets the entity's id
     *
     * @param id Primary key created
     */
    @Override
    public void setID(Long id) {
        this.id=id;
    }
}
