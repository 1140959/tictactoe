/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.notes;

import csheets.ext.contacts.CompanyContact;
import csheets.ext.contacts.Contact;
import csheets.ext.notes.ui.NoteExtensionUI;
import csheets.ext.notes.ui.NotesDTO;
import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * @author Bruno Fernandes <1140958@isep.ipp.pt>
 */
public class NoteController {

    private final NoteExtensionUI theUIExtension;

    public NoteController(NoteExtensionUI theUIExtension) {
        this.theUIExtension = theUIExtension;
    }

    public NoteController() {
        this.theUIExtension = null;
    }

    public List<HistoryNote> getListHistory(Note note) {
        return note.getListHistory();
    }

    public Contact getContactActive() {
        /*final String loggedUser = SystemUser.getInstance().myName();
        ContactRepository contacts = PersistenceContext.repositories().contacts();
        Contact theSystemUser = contacts.findBySystemUser(loggedUser);
        if (theSystemUser != null) {
            setMainUserContact(theSystemUser);
            return theSystemUser;
        }
        return theSystemUser;*/
        return new CompanyContact("Company", null);
    }

    /**
     * Sets the main user contact
     *
     * @param theContact
     */
    public void setMainUserContact(Contact theContact) {
        this.theUIExtension.setUserContact(theContact);
    }

    public Note createNote(String description, LocalDateTime date) throws Exception {
        NotesDTO notedto = new NotesDTO(description, date);
        NoteService service = new NoteService();
        Note note = service.create(notedto);
        return note;
    }

    public boolean saveNote(Note newNote, Contact contact) {
        List<Note> notes = contact.notes();
        return notes.add(newNote);
    }

    public Note editNote(String description,Note note) throws Exception {
        NoteService serv = new NoteService();        
        return serv.edit(description,note);
    }

    public boolean removeNote(Contact contact, Note noteselect) throws Exception {
        System.out.println("testeRemove");
        NoteService serv = new NoteService();
        return serv.remove(noteselect, contact);
    }

    public boolean addNewHistory(Note newNote) {
        NoteService serv = new NoteService();
        return serv.addNewHistory(newNote);
    }
    
    public boolean addHistorytoListNote(Note noteContact,Note newNote) {
        NoteService serv = new NoteService();
        return serv.addHistorytoListNote(noteContact, newNote);
    }
}
