/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ui;

import java.awt.Label;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JPanel;
import org.jdatepicker.DateModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilCalendarModel;

/**
 * The timestamp date picker
 *
 * @author Tiago Gabriel 1440775
 */
public class TimeStampDatePicker {

    /**
     * Calendar Model
     */
    private final UtilCalendarModel model;

    /**
     * Panel for the date picker
     */
    private final JDatePanelImpl datePanel;

    /**
     * Panel of the picker
     */
    private final JDatePickerImpl datePicker;

    /**
     * Properties of the picker
     */
    final static Properties p = new Properties();

    /**
     * Label format
     */
    final static LabelFormatter format = new LabelFormatter();

    public TimeStampDatePicker() {
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");
        model = new UtilCalendarModel();
        datePanel = new JDatePanelImpl(model, p);
        datePicker = new JDatePickerImpl(datePanel, format);
    }

    /**
     * Gets the panel for with the picker
     *
     * @return Panel
     */
    public JPanel getDatePicker() {
        return datePicker;
    }

    /**
     * Get the selected date of the picker
     *
     * @return A calendar object
     */
    public Calendar getSelectedDate() {
        return (Calendar) datePicker.getModel().getValue();
    }

    /**
     * Sets the calendar picker for edition of an event
     *
     * @param date The date
     */
    public void setForEdition(Calendar date) {
        DateModel<Calendar> theModel = (DateModel<Calendar>) datePicker.getModel();
        theModel.setValue(date);
    }

    // Formatter for the Timestamp label - example watched online
    static class LabelFormatter extends AbstractFormatter {

        private final String datePattern = "dd-MM-yyyy";
        private final SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

        @Override
        public Object stringToValue(String text) throws ParseException {
            return dateFormatter.parseObject(text);
        }

        @Override
        public String valueToString(Object value) throws ParseException {
            if (value != null) {
                Calendar cal = (Calendar) value;
                return dateFormatter.format(cal.getTime());
            }

            return "";
        }
    }

}
