/**
 * Technical documentation regarding the work of the team member (1100588) Guilherme Amorim during week2. 
 * 
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * <p>
 * -In this section you should register important notes regarding your work during the week.
 * For instance, if you spend significant time helping a colleague or if you work in more than a feature.-
 *
 * <h2>2. Use Case/Feature: Core 04.1</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-9">Link</a>
 * <p>
 * Core04.1 - Navigation Window
 * Cleansheets should have a navigation window (sidebar).
 * The navigation should display the following contents: workbooks; spreadsheets; non-empty cells; formulas; values.
 * A double click in one of the elements should update the mouse focus to show that element.
 * The contents of the navigator should be automatically updated.
 * 
 * <h2>3. Requirement</h2>
 * The user should be able to see a Navigation Window in the system's sidebar. This window should display the content of
 * Cleansheets (Workbooks, Spreadsheets, non-empty cells, etc...). This content should be presented in a tree.
 * By clicking on an item, the user should navigate to item he clicked. As the CleanSheets suffer changes, the Navigation Window should be
 * able to update automatically its content. 
 * 
 * <p>
 * <b>Use Case "Navigation Window":</b> The user clicks on the Navigation sidebar. The system displays its Navigation Window. The user
 * clicks in one of the displayed elements. The system updates the mouse focus to show that element; The user modifies an element. The system
 * updates the Navigation Window automatically.
 *  
 * <h2>4. Analysis</h2>
 * The first thing to do when approaching this use-case is to learn how to create a sidebar and how will it have access to all the required
 * elements. I need to know where can I find the existent spreadsheet or how can I find the project's formulas.
 * 
 * The Navigator sidebar will only have a tree that will have the active workbook as its root. From there, the system will display all its
 * spreadsheets, followed by the non-empty cells and the used formulas.
 * 
 * About the implementation: my idea is to basically work around 3 classes named NavigationUI, NavigationTree and NavigationController.
 * 
 * NavigationUI will be responsible for creating the user interface and for adding the elements to the tree.
 * NavigationTree will be responsible for the logic behind the addition of the elements to the tree.
 * NavigationController will have some methods used by NavigationTree.
 * 
 * <h3>First "analysis" sequence diagram</h3>
 *
 * The following diagram matches with my first idea but later that idea was completely thrown away:
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1100588\sprint2\core04.1_firstdesign_1.jpg" alt="image">
 * 
 * <h2>5. Design</h2>
 *
 * <b>SSD:</b>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1100588\sprint1\core04.1_design_ssd.jpg" alt="image">
 *
 * <h3>5.1. Functional Tests</h3>
 * 
 * <h3>5.3. Classes</h3>
 * 
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1100588\sprint1\core04.1_classdiagram.jpg" alt="image">
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * <h2>6. Implementation</h2>
 * 
 * See:<p>
 * <a href="../../../../csheets/ext/navigationwindow/package-summary.html">csheets.ext.navigationwindow</a><p>
 * <a href="../../../../csheets/ext/navigationwindow/ui/package-summary.html">csheets.ext.navigationwindow.ui</a>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * Helped a teammate developing his issue (Column Sort).
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. The group spoke about...
 * <p>
 * Today
 * <p>
 * 1. Studied how to create a sidebar.
 * 2. Made the UC Analysis.
 * 3. Started the UC Design.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * ...
 * <p>
 * Today
 * <p>
 * 1. Modified the design and finished it.
 * 2. Started implementing the UC in a menu (not on the side bar).
 * 3. Started implementing the tests.
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 * <b>Wednesday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. ...
 * <p>
 * Today
 * <p>
 * 1. Continued implementing the issue.
 * 2. Completed all tests.
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 * <b>Thursday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. ...
 * <p>
 * Today
 * <p>
 * 1. Finished the worklog.
 * 2. Added one more method that gets all formulas.
 * <p>
 * Blocking:
 * <h2>10. Self Assessment</h2> 
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3 - Acomplished: When it comes to functionality, the issue is on an good level. It was implemented as a menu instead of side bar. Also,
 * when clicking on an element, the mouse focus doesn't update.
 * <p>
 * <b>Evidences:</b>
 * <p>
 * -
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * 5 - Perfect: Managed to help some coleagues during the free time that I had through the week.
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * 2 - Acceptable: The analysis is decent. The design is a bit poor. Only 2 diagrams.
 * 
 * @author 1100588
 */

package csheets.worklog.n1100588.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

