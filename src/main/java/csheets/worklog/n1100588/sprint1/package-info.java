/**
 * Technical documentation regarding the work of the team member (9954321) John Doe during week1. 
 * 
 * <b>-Note: this is a template/example of the individual documentation that each team member must produce each week/sprint. Suggestions on how to build this documentation will appear between '-' like this one. You should remove these suggestions in your own technical documentation-</b>
 *
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * -Notes about the week's work.-
 * <p>
 * -In this section you should register important notes regarding your work during the week.
 * For instance, if you spend significant time helping a colleague or if you work in more than a feature.-
 *
 * <h2>2. Use Case/Feature: Core 01.1</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-105">Link</a>
 * <p>
 * [Core01.1] Enable and Disable Extension
 * It should exist a new window that allows to enable and disable extension of Cleansheet. A disabled extension
 * means that all its funcionalities are disabled.
 * 
 * <h2>3. Requirement</h2>
 * Setup extension to enable and disable other extensions. The user should be able to activate and deactivate the existent extensions.
 * When activated, a new menu should appear. The menu would be composed by 2 panels with a list with the Enabled Extensions and the Disabled Extensions.
 * When double-clicking on an active extension, it would switch to the Disabled Extensions' list and the program would disable it. The same would occur
 * the way around.
 * 
 * <p>
 * <b>Use Case "Enable and Disable Extensions":</b> The user clicks on the Extensions' tab. The user clicks on Manage Extensions. 
 * The system presents a menu with the lists including the Enabled and Disabled Extensions. The user double-clicks on an extension.
 * The system enables/disables the chosen extension and presents it in the other list.
 *  
 * <h2>4. Analysis</h2>
 * Since "Enable and Disable Extensions" is a new extension, we need to study how the system loads and creates an Extension. We should also study how
 * the existent extensions were developed.
 * After studying ExtensionExample and its package components we realized that we need to create 2 new packages: csheets.ext.manageextensions and
 * csheets.ext.manageextensions.ui. In the first package we should have a class to create the extension. In the UI package, there'll be 3 classes named:
 * ExtensionManagerAction, ExtensionManagerMenu, UIExtensionManager (and the package-info aswell).
 * 
 * The will create a Menu that presents 2 lists, including the Enabled and the Disabled extensions. Clicking on an extension, if it's Enabled, the system
 * will disable the extension, and vice-versa.
 * 
 * This management will be made by accessing the file extensions.props.
 * « in development »
 * 
 * <h3>First "analysis" sequence diagram</h3>
 *
 * 
 * <h2>5. Design</h2>
 *
 * <b>SSD:</b>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1100588\sprint1\core01.1_manageextensions_ssd.jpg" alt="image">
 *
 * <h3>5.1. Functional Tests</h3>
 * 
 * <h3>5.3. Classes</h3>
 * 
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1100588\sprint1\core01.1_classdiagram1.jpg" alt="image">
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * <h2>6. Implementation</h2>
 * 
 * See:<p>
 * <a href="../../../../csheets/ext/manageextensions/package-summary.html">csheets.ext.manageextensions</a><p>
 * <a href="../../../../csheets/ext/manageextensions/ui/package-summary.html">csheets.ext.manageextensions.ui</a>
 * 
 * <h2>7. Integration/Demonstration</h2>
 *
 * 
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. The group spoke about...
 * <p>
 * Today
 * <p>
 * 1. Studied the project.
 * 2. Started the UC Analysis.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. ...
 * <p>
 * Today
 * <p>
 * 1. Studied the project.
 * 2. Finished the first analysis of the UC.
 * 3. Started the design.
 * 4. Created a Manage Extensions extension.
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 * <b>Wednesday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. ...
 * <p>
 * Today
 * <p>
 * 1. Studied the project.
 * 2. Updated the design.
 * 3. Implemented a ManageExtensions' GUI (not commited due to some bugs)
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 * <b>Thursday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. ...
 * <p>
 * Today
 * <p>
 * 1. Updated the design
 * 2. Updated the worklog
 * <p>
 * Blocking:
 * <h2>10. Self Assessment</h2> 
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 4 - Outstanding: That issue was implemented without any fault. However, the created diagrams (design) are not 100% correct.
 * <p>
 * <b>Evidences:</b>
 * <p>
 * -
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * 5 - Perfect: The team communicates really well and everyone was helpful and friendly.
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * 4 - Outstanding: The team was able to create a good amount of documentation that helps to the comprehension of the issue.
 * 
 * @author 1100588
 */

package csheets.worklog.n1100588.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

