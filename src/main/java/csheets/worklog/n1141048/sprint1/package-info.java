/**
 * Technical documentation regarding the work of the team member (1141048) António Ferreira during week1.
 * 
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: yes</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * -Notes about the week's work.-
 * -In this section you should register important notes regarding your work during the week.
 * For instance, if you spend significant time helping a colleague or if you work in more than a feature.-
 *  
 * <ul>
 * <li>Help some team members setup their development environment 
 * <li>Analyze the use case requirements
 * <li>Analyze the code that was already implemented and test possible implementations of the solution
 * <li>Gather the progress of each members tasks
 * </ul>
 * 
 * <h2>2. Use Case/Feature: Core01.1</h2>
 * <p>
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-1">LPFOURNB-1</a>
 * <p>
 * Core01.1- Enable and Disable Extensions
 * 
 * <p>
 * It should exist a new window that allows to enable and disable extensions of Cleansheets. A disabled extension means that 'all' its functionalities are disabled.
 * 
 * <h2>3. Requirement</h2>
 * <p>
 * Create an extension that shows a new window with the list of all available extensions and allows the user to chose with ones to enable and disable.
 * 
 * <p>
 * <b>Use Case "Enable or Disable an Extension":</b> 
 * <p>
 * The user opens the extension manager window. The system shows the window with a list of all the available extensions for the user to
 * enable or disable. For each one of the extensions available the user may select to disable or enable it. After all the alterations the user
 * is able to chose Save to make the changes take effect in the next startup, or Close and undo all the changes. </p>
 * 
 *  
 * <h2>4. Analysis</h2>
 * 
 * <p>
 * The first thing to analyze is the process by with the application loads all the extensions. The class ExtensionManager is the one responsible 
 * for that process and analyzing it we can concluded that are 2 files that are used in the process of loading and extension. The first is located
 * in the res folder and as the the of extensions.props and the file  ./extensions.props is for the user extensions. 
 * This files are loaded using java Proprieties with parses the content of the file and generates an hash of the extensions available. After that 
 * for each class the the system loops through all the class in the hash and using the load method it loads the extensions.
 * 
 * <p>
 * After the first analyzes we talk with the client about the requirements and decided that instead of not loading the extensions during the application startup
 * we disable the extension through the UI. Each extension can have 3 types of UI components, we go through all the elements and set their status as disabled.
 * 
 * <img src="doc-files/activate_deactivate_Analysis.png" alt="image"> 
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * <p>
 * After the user disables an extension it should appear faded out.
 * <p>
 * After the user enables an extension it should appear again.
 * 
 * <h3>5.2. UC Realization</h3>
 * 
 * To realize this user story created a new extension called ManageExtensions and changed the UIExtention.
 * 
 * <h3>5.3. Classes</h3>
 * 
 * <p>
 * csheets/ext/ExtensionManager.java
 * 
 * <p>
 * csheets/ext/manageextensions/ui/ManageExtensionsAction.java
 * 
 * <p>
 * ui/ext/UIExtension.java
 * Manages the UI and has the state with enables and disables UIExtentions.
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *  
 * <h2>6. Implementation</h2>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <b>Monday</b>
 * <p>
 * Today
 * <ol>
 * <li>Learn the Jira platform
 * <li>Participate in the creation of the team for each area
 * <li>Delegate the task for each member of the team
 * </ol>
 * 
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <ol>
 * <li>Learn the Jira platform
 * <li>Participate in the creation of the team for each area
 * <li>Delegate the task for each member of the team
 * <li>Learn the worklog structure and create the worklog
 * </ol>
 * Today
 * <ol>
 * <li>Help members of the team setup their dev environment 
 * <li>Learn the code base and analyse the use case
 * <li>Complete the analyze of the requirements and code base
 * </ol>
 * 
 * <b>Wednesday</b>
 * <p>
 * Yesterday I worked on: 
 * <ol>
   <li>Help members of the team setup their dev environment 
 * <li>Learn the code base and analyse the use case
 * </ol>
 * 
 * <p>
 * Today
 * <ol>
 * <li>Complete the analyze of the requirements and code base
 * <li>Start the code implementation
 * </ol>
 * 
 * <p>
 * Blocking
 * <ol>
 * <li>Complete the analyze of the requirements and code (difficulties analyzing the code)
 * </ol>
 * 
 * 
 * <b>Friday</b>
 * <p>
 * Yesterday I worked on: 
 * <ol>
 * <li>Start the code implementation
 * </ol>
 * 
 * <p>
 * Today
 * <ol>
 * <li>Finalize the code implementation
 * <li>Finalize the worklog and UC documentation
 * <li>Functional presentation of the application
 * <li>Plan next week
 * </ol>
 * 
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * @author 1141048
 */

package csheets.worklog.n1141048.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author 1141048
 */
class _Dummy_ {}

