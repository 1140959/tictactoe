/**
 * Technical documentation regarding the work of the team member (1141048) António Ferreira during week2.
 *
 * <b>Scrum Master: no</b>
 *
 * <p>
 * <b>Area Leader: no</b>
 *
 * <h2>1. Notes</h2>
 *
 *
 * <h2>2. Use Case/Feature: Core01.1</h2>
 * <p>
 * Issue in Jira:
 * <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-45">LPFOURNB-45</a>
 * <p>
 * Lang07.1- BeanShell Window
 *
 * <h2>3. Requirement</h2>
 * <p>
 * THe requirements are the following:
 * <ul>
 * <li>The user should be able to make/edit and run script using BeanShell
 * scripting language.
 * <li>The default script should:
 * <ol>
 * <li> Open a workbook
 * <li> Create a macro
 * <li> Run the created macro
 * <li> Display a window with the result
 * </ol>
 * </ul>
 *
 * <p>
 * <b>Use Case "Create/Edit a BeanShell Script":</b>
 * <p>
 * The user goes to the extensions menu there he selects the Macro Extension.
 * <p>
 * In the macro extension window he selects the option to edit the macro using
 * BeanShell script.
 * <p>
 * The default script is loaded.
 * <p>
 * The user edits the default script
 * <p>
 * The user may chose to run the script or to exit the window
 *
 *
 * <h2>4. Analysis</h2>
 *
 * <p>
 * The first thing to analyze is how BeanShell works. BeanShell is a shell
 * scripting language where every line can be evaluated and the result value of
 * the script is the result of the last line of the scirpt.
 *
 * <img src="doc-files/BeanShell_Analysis.png" alt="image">
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * <p>
 * Running a simple sum script in BeanShell should return the correct value.
 *
 * <h3>5.2. UC Realization</h3>
 *
 * To create this UC it was necessary to create a new class Called BeanShellImpl
 * that is a wrapper for the beanshell script so there are no direct
 * dipendencies to the BeanShell.
 *
 * <img src="doc-files/lang71_design.png" alt="image">
 *
 * <h3>5.3. Classes</h3>
 *
 * <p>
 * BeanShellController.java - Ensures the isolation between the implementation
 * and the UI
 * <p>
 * BeanShellScript.java - Is a wraper that calls bsh.Interpreter directly
 * <p>
 * CreateBeanShellAction.java - Responsible for the Menu Action that creates the
 * UI
 * <p>
 * CreateBeanShellAction.java - The UI where the user can edit and run the
 * script
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * Wraper - use of an wrapper class to ensure easy transitions between BeanSheel
 * versions
 *
 * <h2>6. Implementation</h2>
 *
 * <h2>7. Integration/Demonstration</h2>
 * 
 * <h2>8. Final Remarks</h2>
 *
 * <h2>9. Work Log</h2>
 *
 * <b>Monday</b>
 * <p>
 * Today
 * <ol>
 * <li>Started analysis
 * </ol>
 *
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on:
 * <ol>
 * <li>Analysis
 * </ol>
 * Today
 * <ol>
 * <li>Finished analysis
 * <li>Started design
 * </ol>
 *
 * <b>Wednesday</b>
 * <p>
 * Yesterday I worked on:
 * <ol>
 * <li>Analysis
 * <li>Design
 * <li>Tests
 * </ol>
 * Today
 * <ol>
 * <li>Finished test implementation
 * <li>Finished design
 * </ol>
 * 
 * <b>Wednesday</b>
 * <p>
 * Yesterday I worked on:
 * <ol>
 * <li>Implementation and creation of the script
 * <li>Peer programming with 1141329
 * </ol>
 * Today
 * <ol>
 * <li>Help other members implement tests
 * </ol>
 *
 * <h2>10. Self Assessment</h2>
 *
 * @author 1141048
 */
package csheets.worklog.n1141048.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1141048
 */
class _Dummy_ {
}
