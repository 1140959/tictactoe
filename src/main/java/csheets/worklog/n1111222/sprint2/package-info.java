/**
 * Technical documentation regarding the work of the team member (1111222) Rui Barbosa during week2. 
 * 
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * -Notes about the week's work.-
 * <p>
 *
 * <h2>2. Use Case/Feature: IPC02.1</h2>
 * 
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-54
 * Identification:
 * IPC02.1- Find Workbooks
 * 
 * Description:
 * <p>
 * The extension should add a new window (sidebar) to search for workbook files in the
 * file system. The user should be able to enter the name of a directory of the
 * file system to be used as the root of the search. The search should include
 * this directory and all its contents (including subdirectories). The results
 * of the search should appear in a list (as files are found). It should be 
 * possible for the user to open a workbook from this list by double clicking in 
 * it. The search can be based solely on the file name extension. for instance,
 * find the files with .cls extension.
 * 
 * <h2>3. Requirement</h2>
 * Enable the user to search for workbook files in the local file system.
 * The user sould be able to see available workbook files as they're found.
 * The user can open the files. This will add a new Workbook to the cleanSheet isntance.
 * 
 * <p>
 * <b>Use Case "Find Workbooks":</b>  
 * The user initializes the extension. The system asks for the parent directory
 * to search. The user enters the parent path. The system asks for file extension.
 * The user specifies the file extension. The system start searching and inserts
 * the finding workbooks in a list as the workbooks are found. The user selects
 * a workbook to open. The system adds that workbook to the list of workbooks
 * of the cleansheet instance.
 *  
 * <h2>4. Analysis</h2>
 * The cleanSheet already has an Open feature incorporated. This use-case will take
 * advantage of that. It will find all workbook files in the directory and sub-directories
 * specified by the user. After that the user can open several of the workbooks listed
 * by the search. The application will now call the "Open" feature that will put the
 * opened workbooks in the cleanSheet instance's workbook list. This list is located 
 * at "Window" menu at main window UI.
 * 
 * 
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram describes, in general, the anaysis. It doesn't really means that will correspond to future code.
 * <p>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1111222/sprint2/AnalysisSequenceDiagram2.png" alt="image">
 * <p>
 * 
 * As we can see from the code, if we are requesting a extension that is not already present in the cell, it is applied at the moment and then returned. The extension class (that implements the Extension interface) what will do is to create a new instance of its cell extension class (this will be the <b>delegator</b> in the pattern). The constructor receives the instance of the cell to extend (the <b>delegate</b> in the pattern). For instance, StylableCell (the delegator) will delegate to CellImpl all the method invocations regarding methods of the Cell interface. Obviously, methods specific to StylableCell must be implemented by it.
 * Therefore, to implement a cell that can have a associated comment we need to implement a class similar to StylableCell.
 * 
 * <h3>Tests</h3>
 * <p> Test if parent directory is exist on the local file system.
 * <p> Test if workbooks are found on reffered directory and sub-directories.
 * <p> Test if selected file can be added to workbook'list.
 * 
 * <h2>5. Design</h2>
 * 
 * <h3>5.1. Functional Tests</h3>
 * Test if files are found on a given directory and sub-directories.
 * Test if files are found filtered on extensions.
 * Test if a workbook is open correctly.
 * 
 * <h3>5.2. UC Realization</h3>
 * 
 * <h3>5.3. Sequence Diagrams</h3>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1111222/sprint2/Design2.png" alt="image">
 * <p> 
 * 
 * <h2>6. Work Log</h2> 
 * <p>
 * <b>Day 1: 06-06-2016</b>
 * 1. Meeting with client. 
 * 2. Analysis done.
 * 3. Design started.
 * <p>
 * <p>
 * <b>Day 2: 07-06-2016</b>
 * 1. Design finished. 
 * 2. Implementation and tests started. 
 * 3. Time spent on learning previous implemented Open Workbook-Feature.
 * <p>
 * <p>
 * <b>Day 3: 08-06-2016</b>
 *  On this day when tests started I realized that furher analysis would be needed.
 *  The service Interface, non-Functional controller were created and follow
 *  by their tests classes.
 * <p>
 * <p>
 * <b>Day 4: 09-06-2016</b>
 *  Implemented the code. UI finished.
 *  Test passed but they need a valid directory to run.
 *  There wasn't enought time to get a relative path for tests.
 * <p>
 * 
 */

package csheets.worklog.n1111222.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}
