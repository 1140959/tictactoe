/**
 * Technical documentation regarding the work of the team member (1111222) Rui Barbosa during week1. 
 * 
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * -Notes about the week's work.-
 * <p>
 * -Nothing on notes yet.
 *
 * <h2>2. Use Case/Feature: IPC01.1</h2>
 * 
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-116
 * <p>
 * Identification:
 * IPC01.1- Start Sharing
 * 
 * Description:
 * It should be possible to establish a connection with other instance of 
 * Cleansheets in the local network. It should be possible to send the 
 * contents of a range of cells to another instance of Cleansheets. 
 * The other instance should display the received contents in the same cell 
 * address as the original cells. It should be possible to configure the port 
 * to be used for network connections. It should be possible to find other 
 * instances of Cleansheets available in e local network. These instances 
 * should be listed in a new window (sidebar window). The user should be able 
 * to select one of the discovered instances to connect to when establishing 
 * the connection to send the contents of the range of cells. At the moment it 
 * is only required to send the value of the cells. 
 * 
 * <h2>3. Requirement</h2>
 * Enable the user to search for other instances of cleansheets (throw a sidebar window) 
 * in a local network choosing one to perform a connection. The user can configure
 * the port for the connection. With the connection done, the user will send data
 * containing a range of cells that sould be presented in the same cell adress as the
 * original.
 * 
 * <p>
 * <b>Use Case "Start Sharing":</b>  
 * The user will enter the port to be used for network connections. After that the system 
 * will scan for available cleansheets on that network. The user choose one. The system
 * asks for the range of cells to send. The user enters it and the system sends the data
 * to the other cleansheet.
 *  
 * <h2>4. Analysis</h2>
 * This feature will be seen as an extension. So it should appear in the extensions menu
 * on the main window. The main window is the one the user sees when the app starts.
 * When the user chooses this feature it should appear a small window to configure the port.
 * After the user enter the port number, a request to the controller is sent. The controller
 * calls a service, that it's specialized on network communications, passing the port user
 * entered and receives a list of available adresses. The user chooses the destinity and 
 * then chooses the cells to send. The system will call another service to the same specialized
 * provider passing both cleansheet adress and the cells.
 * 
 * it's a class named Frame
 * that extends JFrame. This JFrame contains another class called MenuBar that extends 
 * JMenuBar. This class has the menu of the exte
 * 
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram describes, in general, the anaysis. It doesn't really means that will correspond to future code.
 * <p>
 * <img src="doc-files/AnalysisSequenceDiagram.png" alt="image"> 
 * <p>
 * 
 * As we can see from the code, if we are requesting a extension that is not already present in the cell, it is applied at the moment and then returned. The extension class (that implements the <code>Extension</code> interface) what will do is to create a new instance of its cell extension class (this will be the <b>delegator</b> in the pattern). The constructor receives the instance of the cell to extend (the <b>delegate</b> in the pattern). For instance, <code>StylableCell</code> (the delegator) will delegate to <code>CellImpl</code> all the method invocations regarding methods of the <code>Cell</code> interface. Obviously, methods specific to <code>StylableCell</code> must be implemented by it.
 * Therefore, to implement a cell that can have a associated comment we need to implement a class similar to <code>StylableCell</code>.
 * 
 * <h3>Tests</h3>
 * <p> Test if inserted port is valid.
 * <p> Test if the network is accessible.
 * <p> Test if there are at least one instance available.
 * <p> Test if the cells are sended.
 * 
 * <h2>5. Design</h2>
  *<h3>Sender behaviour</h3>
 * <img src="doc-files/ipc01_01_design_send.png" alt="image"> 
 * 
 * <h3>Receiver behaviour</h3>
 * <img src="doc-files/ipc01_01_design_receive.png" alt="image">
 * 
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we see that the core functionality of this use case is to be able to add an attribute to cells to be used to store a comment/text. We need to be able to set and get its value.
 * Following this approach we can start by coding a unit test that uses a subclass of <code>CellExtension</code> with a new attribute for user comments with the corresponding method accessors (set and get). A simple test can be to set this attribute with a simple string and to verify if the get method returns the same string.
 * As usual, in a test driven development approach tests normally fail in the beginning. The idea is that the tests will pass in the end. 
 * <p>
 * see: <code>csheets.ext.comments.CommentableCellTest</code>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will need to create a subclass of Extension. We will also need to create a subclass of UIExtension. For the sidebar we need to implement a JPanel. In the code of the extension <code>csheets.ext.style</code> we can find examples that illustrate how to implement these technical requirements.
 * The following diagrams illustrate core aspects of the design of the solution for this use case.
 * <p>
 * <b>Note:</b> It is very important that in the final version of this technical documentation the elements depicted in these design diagrams exist in the code!
 * 
 * <h3>Extension Setup</h3>
 * The following diagram shows the setup of the "comments" extension when cleansheets is run.
 * <p>
 * <img src="doc-files/core02_01_design.png" alt="image">
 * 
 *
 * <h3>User Selects a Cell</h3>
 * The following diagram illustrates what happens when the user selects a cell. The idea is that when this happens the extension must display in the sidebar the comment of that cell (if it exists).
 * <p>
 * <img src="doc-files/core02_01_design2.png" alt="image">
 * 
 * <h3>User Updates the Comment of a Cell</h3>
 * The following diagram illustrates what happens when the user updates the text of the comment of the current cell. To be noticed that this diagram does not depict the actual selection of a cell (that is illustrated in the previous diagram).
 * <p>
 * <img src="doc-files/core02_01_design3.png" alt="image">
 * 
 * <h3>5.3. Classes</h3>
 * 
 * -Document the implementation with class diagrams illustrating the new and the modified classes-
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * -Describe new or existing design patterns used in the issue-
 * <p>
 * -You can also add other artifacts to document the design, for instance, database models or updates to the domain model-
 * 
 * <h2>6. Implementation</h2>
 * 
 * -Reference the code elements that where updated or added-
 * <p>
 * -Also refer all other artifacts that are related to the implementation and where used in this issue. As far as possible you should use links to the commits of your work-
 * <p>
 * see:<p>
 * <a href="../../../../csheets/ext/comments/package-summary.html">csheets.ext.comments</a><p>
 * <a href="../../../../csheets/ext/comments/ui/package-summary.html">csheets.ext.comments.ui</a>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * -In this section document your contribution and efforts to the integration of your work with the work of the other elements of the team and also your work regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * -In this section present your views regarding alternatives, extra work and future work on the issue.-
 * <p>
 * As an extra this use case also implements a small cell visual decorator if the cell has a comment. This "feature" is not documented in this page.
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * -Insert here a log of you daily work. This is in essence the log of your daily standup meetings.-
 * <p>
 * Example
 * <p>
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. -nothing-
 * <p>
 * Today
 * <p>
 * 1. Analysis of the...
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. ...
 * <p>
 * Today
 * <p>
 * 1. ...
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * -Insert here your self-assessment of the work during this sprint.-
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex: mais de 50%) e apresentam código que para além de não ir contra a arquitetura do cleansheets segue ainda as boas práticas da área técnica (ex: sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the implementation of the design pattern ...-
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author alexandrebraganca
 */

package csheets.worklog.n1111222.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

