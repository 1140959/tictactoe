/**
 * Technical documentation regarding the work of the team member (1130806) Luis Mendes during week1.
 *
 * <p>
 * <b>Scrum Master: -(yes/no)- No</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- yes</b>
 *
 * <h2>1. Notes</h2>
 *
 * -Nothing
 * <p>
 * -In this section you should register important notes regarding your work
 * during the week. For instance, if you spend significant time helping a
 * colleague or if you work in more than a feature.-
 *
 * <h2>2. Use Case/Feature: Ipc01.1 Start Sharing</h2>
 *
 * The Issue:
 * <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-51">http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-51</a>
 *
 * <h2>3. Requirement</h2>
 * <p>
 * # It should be possible to establish a connection with other instance of
 * Cleansheets in the local network.
 * <p>
 * # It should be possible to send the contents of a range of cells to another
 * instance of Cleansheets.
 * <p>
 * # The other instance should display the received contents in the same cell
 * address as the original cells.
 * <p>
 * # It should be possible to configure the port to be used for network
 * connections.
 * <p>
 * # It should be possible to find other instances of Cleansheets available in
 * local network.
 * <p>
 * # These instances should be listed in a new window (sidebar window).
 * <p>
 * # The user should be able to select one of the discovered instances to
 * connect to when establishing the connection to send the contents of the range
 * of cells.
 * <p>
 * # At the moment it is only required to send the value of the cells.
 *
 * <p>
 * <b>Use Case "Start Sharing":</b>
 *
 * <p>
 * # The user selects a range of cells to share.
 * <p>
 * # The user starts the Sharing process.
 * <p>
 * # The system displays the available cleansheets instances on local network.
 * <p>
 * # The user select the instance to share.
 * <p>
 * # The user pushes a button to send cell value.
 * <p>
 * # The system sends the cell or range of cells
 *
 * <h2>4. Analysis</h2>
 *
 * <p>
 * Since sharing cells will be supported in a new extension to cleansheets we
 * need to study how extensions are loaded by cleansheets and how they work.
 * <p>
 * The extensions must be subclasses of the Extension abstract class and need to
 * be registered in special files.
 * <p>
 * We have to implement some threads to listen and receive data
 *
 * <h3>First "analysis" sequence diagram</h3>
 *
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).
 * <p>
 * <img src="doc-files/Ipc01_1_analysis_start_sharing1.png" alt="image">  *
* <h2>5. Design</h2>
 *
 *
 * <p>
 * <img src="doc-files/ipc01_1_design_start_sharing_send.png" alt="image">
 *
 * <p>
 * <img src="doc-files/ipc01_1_design_start_sharing_receive.png" alt="image">
 *
 * <h3>5.1. Functional Tests</h3>
 * <p>
 * # test if we can find other cleansheets instances over the local network
 * <p>
 * # test if we can create and establish a connection between two cleansheets
 * instances over the local network
 * <p>
 * # test if we can send and receive a range of cells between two cleansheets
 * instances over the local network
 *
 *
 *
 * <h3>5.2. UC Realization</h3>
 *
 * <p>
 * <b>Note:</b> It is very important that in the final version of this technical
 * documentation the elements depicted in these design diagrams exist in the
 * code!
 *
 * <h3>Extension Setup</h3>
 * The following diagram shows the setup of the "comments" extension when
 * cleansheets is run.
 * <p>
 * <img src="doc-files/core02_01_design.png" alt="image">
 *
 *
 * <h3>User Selects a Cell</h3>
 * The following diagram illustrates what happens when the user selects a cell.
 * The idea is that when this happens the extension must display in the sidebar
 * the comment of that cell (if it exists).
 * <p>
 * <img src="doc-files/core02_01_design2.png" alt="image">
 *
 * <h3>User Updates the Comment of a Cell</h3>
 * The following diagram illustrates what happens when the user updates the text
 * of the comment of the current cell. To be noticed that this diagram does not
 * depict the actual selection of a cell (that is illustrated in the previous
 * diagram).
 * <p>
 * <img src="doc-files/s_start_sharing1.png" alt="image">
 *
 * <h3>5.3. Classes</h3>
 *
 * -Document the implementation with class diagrams illustrating the new and the
 * modified classes-
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Describe new or existing design patterns used in the issue-
 * <p>
 * -You can also add other artifacts to document the design, for instance,
 * database models or updates to the domain model-
 *
 * <h2>6. Implementation</h2>
 *
 * -Reference the code elements that where updated or added-
 * <p>
 * -Also refer all other artifacts that are related to the implementation and
 * where used in this issue. As far as possible you should use links to the
 * commits of your work-
 * <p>
 * see:
 * <p>
 * <a href="../../../../csheets/ext/comments/package-summary.html">csheets.ext.comments</a><p>
 * <a href="../../../../csheets/ext/comments/ui/package-summary.html">csheets.ext.comments.ui</a>
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -In this section document your contribution and efforts to the integration of
 * your work with the work of the other elements of the team and also your work
 * regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 *
 * <h2>8. Final Remarks</h2>
 *
 * -In this section present your views regarding alternatives, extra work and
 * future work on the issue.-
 * <p>
 * As an extra this use case also implements a small cell visual decorator if
 * the cell has a comment. This "feature" is not documented in this page.
 *
 *
 * <h2>9. Work Log</h2>
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings.-
 * <p>
 * Example
 * <p>
 * <b>Monday - 2016.05.30 - I Worked on Analysis</b>
 * <p>
 * <b>Tuesday I worked on: Project study, group design elaboration, create
 * documentation</b>
 * <p>
 * <b>Wednesday I worked on: design diagrams and define the code
 * implementation.</b>
 * <p>
 * <b>thursday I worked on: 
 * implementation.</b>
 * <p>
 * 1. -nothing-
 * <p>
 * Today
 * <p>
 * 1. Analysis of the...
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. ...
 * <p>
 * Today
 * <p>
 * 1. ...
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 50%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the
 * implementation of the design pattern ...-
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 * @author Luis Mendes 1130806
 */
package csheets.worklog.n1130806.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author Luis Mendes 1130806
 */
class _Dummy_ {
}
