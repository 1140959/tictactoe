/**
 * Technical documentation regarding the work of the team member (1130806) Luis Mendes during week2.
 *
 * <p>
 * <b>Scrum Master: -(yes/no)- No</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- No</b>
 *
 * <h2>1. Notes</h2>
 *
 * - Nothing so far
 *
 * <h2>2. Use Case/Feature: IPC05.1 Chat Send Message</h2>
 *
 * The Issue:
 * <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-63">http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-63</a>
 *
 * <h2>3. Requirement</h2>
 * <p>
 * # Add an option that can be used to send text messages to another instance of
 * Cleansheets.
 * <p>
 * # The message should be displayed in a popup window in the other instance of
 * Cleansheets.
 * <p>
 * # The popup should disappear after some short period (for instance 5
 * seconds).
 * <p>
 * # Cleansheets should have have a new sidebar window to display all the
 * messages.
 * <p>
 * # The sidebar should be based on a tree control that shows the messages
 * grouped by thread of conversation (i.e., each reply will be a child of the
 * parent message).
 * <p>
 * # It should be possible to reply to a message by double clicking on it in the
 * tree.
 *
 * <h2>4. Analysis</h2>
 *
 * <b>Use Case "Start chat": </b>
 * <p>
 * # The user selects "Start chat" menu option
 * <p>
 * # The user insert a nickname
 * <p>
 * # The user press "Start" and the chat is activated
 * <p>
 * <b>Use Case "Send message":</b>
 * <p>
 * # The user selects "Send message" menu option
 * <p>
 * # The user enter the destination IP address
 * <p>
 * # The system ask user to insert the message to send
 * <p>
 * # The user inserts a message and press "Send"
 * <p>
 * # The message is sent and the frame is closed.
 * <p>
 * <b>Use Case "Chat Side Bar":</b>
 * <p>
 * # The system show all the messages send and received
 * <p>
 * # The user selects a message witch he wants to reply
 * <p>
 * # The system open a frame to user insert a message to send
 * <h3>First "analysis" sequence diagram</h3>
 * <h4>Start chat (SSD)</h4>
 * <p>
 * <img src="doc-files/Ipc05_1_SSD_start_chat.png" alt="image">
 *
 * <h4>Start chat (SD)</h4>
 * <p>
 * <img src="doc-files/Ipc05_1_analysis_start_chat.png" alt="image"> *
 *
 * <h4>Send message (SSD)</h4>
 * <p>
 * <img src="doc-files/Ipc05_1_SSD_send_message.png" alt="image">
 *
 * <h4>Send message (SD)</h4>
 * <p>
 * <img src="doc-files/Ipc05_1_analysis_send_message.png" alt="image"> *
 *
 * <h2>5. Design</h2>
 * Cleansheets is able to allow change messages with other instances in network
 * using chat.
 *
 * <h3>Class diagram</h3>
 * <img src="doc-files/Ipc05_1_ClassDiagram.png" alt="image"> *
 *
 * <h3>Design Start Chat</h3>
 * <p>
 * <img src="doc-files/Ipc05_1_Design_start_chat.png" alt="image">
 *
 * <h3>Design Send Message</h3>
 * <p>
 * <img src="doc-files/Ipc05_1_Design_send_Message.png" alt="image">
 *
 * <h1>6. Tests</h1>
 * <p>
 * On this use case the important functionality is to be able to establish a
 * connection between cleansheets instances, in our local network, and start to
 * chat. However, there aren't too many tests in this use case, since the UI is
 * not to test
 *
 * <p>
 * We can set and code some unit tests:
 * <p>
 * - Send Messages: test if we can send messages between two cleansheets
 * instances. This test was not concluded, because I did not get the waited
 * result, for the difficulty in testing, even so I has the certainty of the
 * functioning of the service
 * <p>
 * - Receive messages: test if we can receive message between the two
 * cleansheets instances. Some problem as the Send Message test
 * <p>
 * See:
 * <p>
 * csheets\ext\chat\ChatMessageConnectionTest.java
 * csheets\ext\chat\ChatNetworkDataTest
 *
 *
 * <h2>9. Work Log</h2>
 *
 * <p>
 * Saturday - 2016.06.04 - I Worked on Analysis. "Start Chat" analysis closed.
 * <p>
 * Sunday - 2016.06.05 - I Worked on code implementation and classe diagram,
 * "Start chat" and "Chat Side Bar" implementation
 * <p>
 * Monday - 2016.06.06 - Analysis closed. "Send message" design finished. Design
 * probably closed. I start implementing the send message use case
 * <p>
 * Tuesday - 2016.06.07 - Continue working on send message Use Case. Class
 * Diagram adjusted. The messages do not appear correctly in JTree
 * <p>
 * Wednesday - 2016.06.08 - I Worked on Javadoc and code review. Test some
 * classes
 * <p>
 * Thursday - 2016.06.08 - Tests and implementation done. Code review. Javadoc review.
 *
 * <p>
 * <b>Evidences:</b>
 * <p>
 * <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/ab88cfc2a78761178fd290cd66c7596f0a98c1b9">
 * beginning of the analysis. Start Chat use case, finish. Send Message
 * Ongoing</a>
 * <p>
 * <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/4a7e2f6a314702b55f188294463e61c53ee65b11">
 * Class Diagram, Design Start Chat, SSD Start Chat </a>
 * <p>
 * <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/e841ee88442b8fba21c4c92ee0ed435be045a1d9">
 * Start Chat implementation </a>
 * <p>
 * <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/d3e39b36c23b96c5cda8228e7e5f8e291affec9d">
 * Start Chat tests. Initiate tests. </a>
 * <p>
 * <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/32939639ce7902fc7982d53afc0db6704c574da5">
 * Comment tests. Build is unstable on QADEI-Jenkins·</a>
 * <p>
 * <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/21acc4e7f9d358c9e337b48b2e795d3bb40d334a?at=master">
 * Design complete. Class diagram, Design Start Chat and Design Send Message</a>
 * <p>
 * <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/851a992a5739c5110f02c1edfa49d8bc38a69eb1">
 * Send message UI definition. Send and receive message, with a popup
 * windows</a>
 *
 * <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/f89fc64d6df91d9ec17ec927042e34677246ffab">
 * Done chat panel. Log Messages by Conversation</a>
 *
 * <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/ad08adc3f329d803e958436153a81a9387df9817">
 * ChatMessageConnectionTest and ChatNetworkDataTest</a>
 * 
 * <h2>6. Final Remarks</h2>
 *
 * According to what was requested in this use case , I think it was achieved
 * the objectives . I must mention that for the implementation of the chat service 
 * I had to resort on Google some help.
 *
 * @author Luis Mendes 1130806
 */
package csheets.worklog.n1130806.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author Luis Mendes 1130806
 */
class _Dummy_ {
}
