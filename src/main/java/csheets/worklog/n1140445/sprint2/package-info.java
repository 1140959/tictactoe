/**
 * Technical documentation regarding the work of the team member (1140445) Hugo Soares during week2. 
 * 
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 *
 * <h2>2. Use Case/Feature: Core03.1</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-163">LPFOURNB-163</a>
 * <p>
 * [Core03.1] Column Sort: 
 * Sort the contents of a column of cells. It should be possible to select the order: ascending or descending.
 * The interaction with the user can be based solely on menu options. It should be possible to sort columns
 * with numeric and/or text contents.
 * 
 * <h2>3. Requirement</h2>
 * Setup extension for Sort Columns. The user must be able to chose 1 option (ascending or descending).
 * When the user choose the option the program must have sort columns of cells. 
 * It should be possible to sort columns with numeric and/or text contents.
 * 
 * <p>
 * <b>Use Case "Column Sort":</b> The user clicks on the Extensions' tab. The user clicks on Sort Columns. 
 * The system presents 2 options (ascending or descending). The user choose one of there options.
 * The system show the sorted column based on the chosen option.
 * 
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140445\sprint2\ColumnSort_SSD.jpg" alt="image" width=900>
 *  
 * <h2>4. Analysis</h2>
 * Since sort coluns will be supported in a new extension to cleansheets we need to study how extensions are loaded by cleansheets and how they work.
 * The first sequence diagram tells us that extensions must be subclasses of the Extension abstract class and need to be registered in special files.
 * The Extension class has a method called getUIExtension that should be implemented and return an instance of a class that is a subclass of UIExtension.
 * In this subclass of UIExtension there is a method (getSideBar) that returns the sidebar for the extension. A sidebar is a JPanel.
 * In this JPanel will be implemented columnSort extension that will have 2 options ( ascending or descending ) .
 * You will choose one of these options and the program automatically sorts the columns.
 * It will be created sortColumns class will have implemented ascending or descending methods and the controller will call the respective method .
 * 
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts a proposal for the realization of the previously described use case. We call this diagram an "analysis" use case realization because it functions like a draft that we can do during analysis or early design in order to get a previous approach to the design. For that reason we mark the elements of the diagram with the stereotype "analysis" that states that the element is not a design element and, therefore, does not exists as such in the code of the application (at least at the moment that this diagram was created).
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140445\sprint2\ColumnSort_Analysis.jpg" alt="image" width=900>
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * The tests made, verify if the content of the cells would be sorted correctly through the methods ascendSort() and descendSort in class <code>Sort</code>.
 * Therefore, an woorkbook was created, as well as the Spreadsheet and three cells. Each cell has their own content, which will allow to compare the content of these cells, and sort them alphabetically.
 * <p>
 * see: <code>csheets.ext.columnSort.SortTest</code>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will need to create a subclass of Extension. We will also need to create a subclass of UIExtension. For the sidebar we need to implement a JPanel. In the code of the extension <code>csheets.ext.simple</code> we can find examples that illustrate how to implement these technical requirements.
 * The following diagrams illustrate core aspects of the design of the solution for this use case.
 * 
 * <h3>Extension Setup</h3>
 * The following diagram shows the setup of the "comments" extension when cleansheets is run.
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140445\sprint2\ColumnSort_DS.jpg" alt="image" width=900>
 * 
 * <h3>Sequence Diagram</h3>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140445\sprint2\ColumnSort_DS2.jpg" alt="image" width=900>
 * 
 * <h3>5.3. Classes</h3>
 * <h3>Class Diagram</h3>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140445\sprint2\Class Diagram.jpg" alt="image" width=900>
 * 
 * <h2>6. Implementation</h2>
 * 
 * see:<p>
 * <a href="../../../../csheets/ext/columnSort/package-summary.html">csheets.ext.columnSort</a><p>
 * <a href="../../../../csheets/ext/columnSort/ui/package-summary.html">csheets.ext.columnSort.ui</a>
 * 
 * <h2>7. Integration/Demonstration</h2>

 * 
 * <h2>9. Work Log</h2> 
 * 
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. -nothing-
 * <p>
 * Today:
 * <p>
 * 1. Studied the issue.
 * 2. Started the analysis.
 * 3. Finished the firts analysis of the UC.
 * 4. Started the Design.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. First analysis oh the UC.
 * 2. Started the Design.
 * <p>
 * Today:
 * <p>
 * 1. Finish the analysis and the Design.
 * 2. Started unit tests.
 * 3. Create a Column Sort extension.
 * 3. Started the implementatio UI.
 * <p>
 * Blocking:
 * <p>
 * 1. Bug Inside the class SpreadsheetImpl in GetColumn () method.
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. Unit Test sort class.
 * 2. Column Sort extension.
 * <p>
 * Today:
 * <p>
 * 1. Finish the unit test Sort class.
 * 2. Create the class Sort(inside the class are the ascending and descending methods).
 * 3. Finish the implementation UI.
 * 4. Finish worklog sprint2.
 * <p>
 * Blocking:
 * <p>
 * -nothing-
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * 5 - Perfect: That issue was implemented without any fault.
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 5 - Perfect: The team communicates really well and everyone was helpful and friendly.
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 5 - Perfect: The team was able to create a good amount of documentation that helps to the comprehension of the issue.
 * 
 * @author 1140445
 */

package csheets.worklog.n1140445.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

