/**
 * Technical documentation regarding the work of the team member (1140445) Hugo Soares during week1. 
 * 
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * <p>
 * In this week the issue was developed as a group.
 * Each group member had to make the analysis of the problem and respective design.
 * The issue was successfully accomplished.
 *
 * <h2>2. Use Case/Feature: Core01.1</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-102">LPFOURNB-102</a>
 * <p>
 * [Core01.1] Enable and Disable Extensions: 
 * It should exist a new window that allows to enable and disable extensions of Cleansheets. A disabled extension means that all its functionalities are disabled.
 * 
 * <h2>3. Requirement</h2>
 * Setup extension for enable or disable extensions. The user must be able to enable or disable extensions.
 * When an extension is activated the program must have its funcionalities enable. 
 * When a extension is desactivated the program must have its funcionalities disabled.
 * The FI starts when te user choses the Enable/Disable Extension option on the extension menu.
 * The menu would be composed by 2 panels with a list with the Enabled Extensions and the Disabled Extensions.
 * 
 * <p>
 * <b>Use Case "Enable and Disable Extensions":</b> The user clicks on the Extensions' tab. The user clicks on Manage Extensions. 
 * The system presents a menu with the lists including the Enabled and Disabled Extensions. The user double-clicks on an extension.
 * The system enables/disables the chosen extension and presents it in the other list.
 * 
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140445\sprint1\EnableDisableExtensions_SSD.png" alt="image" width=900>
 *  
 * <h2>4. Analysis</h2>
 * After studying ExtensionExample and its package components we realized that we need to create 2 new packages: csheets.ext.manageextensions and
 * csheets.ext.manageextensions.ui. In the first package we should have a class to create the extension. In the UI package, there'll be 3 classes named:
 * ExtensionManagerAction, ExtensionManagerMenu, UIExtensionManager (and the package-info aswell).
 * 
 * The will create a Menu that presents 2 lists, including the Enabled and the Disabled extensions. Clicking on an extension, if it's Enabled, the system
 * will disable the extension, and vice-versa.
 * 
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * 
 *
 * <h3>5.2. UC Realization</h3>
 * 
 * <p>
 * <b>Note:</b> It is very important that in the final version of this technical documentation the elements depicted in these design diagrams exist in the code!
 * 
 * <h3>Extension Setup</h3>
 * 
 * <h3>5.3. Classes</h3>
 * 
 * 
 * <h2>6. Implementation</h2>
 * 
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * 
 * <h2>8. Final Remarks</h2>

 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. The Group examined the proposed issue.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * 1. Started the project
 * 2. Studied the project..
 * 3. Started the analysis.
 * 4. Finished the first analysis of the UC.
 * <p>
 * 1. ...
 * <p>
 * Today
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 * <b>Wednesday</b>
 * <p>
 * 1. Started the design.
 * 2. Created a Manage Extensions extension.
 * 3. Started implementation of UI.
 * <p>
 * 1. ...
 * <p>
 * Today
 * <p>
 * Finish the implementation.
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 * <b>Thursday</b>
 * <p>
 * Finish the implementation.
 * <p>
 * Today
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * -Insert here your self-assessment of the work during this sprint.-
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * 4 - Outstanding: That issue was implemented without any fault. However, the created diagrams (design) are not 100% correct.
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the implementation of the design pattern ...-
 * 
 * <h3>10.2. Teamwork: ...</h3>
 *  5 - Perfect: The team communicates really well and everyone was helpful and friendly.
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 *  4 - Outstanding: The team was able to create a good amount of documentation that helps to the comprehension of the issue.
 * 
 * @author 1140445
 */

package csheets.worklog.n1140445.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

