/**
 * Technical documentation regarding the work of the team member (1100910) José Marques during week1. 
 * 
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- yes</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * -Notes about the week's work.-
 *
 * <h2>2. Use Case/Feature: Lang01.1</h2>
 * 
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-27
 * <p>
 * Analysis 1100910 - Block of Instructions
 * 
 * <h2>3. Requirement</h2>
 * Add the possibility of writing blocks (or sequences) of instructions. A block must be delimited by curly braces and its instructions must be separated by ';'. The instructions of a block are executed sequentially and the block 'result' is the result of the last statement of the block. For example, the formula '= {1+ 2; sum (A1:A10); B3 + 4 }' must result in the sequential execution of all expressions and the result is the value of the expression 'B3 + 4'. Add the assign operator (its symbol is ':='). This operator assigns  to its left the result of the right expression. At the moment the left of the assign operator can only be a cell reference. The 'FOR' loop should also be implemented based on instruction blocks. For example, the formula '= FOR {A1: = 1 ; A1&lt;10; A2: = A2 + A1; A1: = A1 + 1 }' executes a for loop in which: the first expression is the initialization, the second term is the boundary condition, all other expressions are performed for each iteration of the loop.
 * 
 * <p>
 * <b>Use Case "Block of Instructions":</b> The user selects the cell where he/she wants to input a sequence of instructions and type it. The current cell will keep the value of the last instructions. When assign operator (:=) is used, the reference on the left must be set with the result of the expression (until it finds the ";" operator) on the right.
 * 
 * 
 * <h2>4. Analysis</h2>
 * Since block or sequences of instructions is a new feature to cleansheets we need to study how expressions are interpreted in cleansheets and how they work.
 * 
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts the first sketch for the realization of the previously described use case.
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1100910\sprint1\lang01_01_analisys_1st_sketch.jpg" alt="image"> 
 * <p>
 * The following diagram depicts how new grammar works to fulfill the requirements.
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1100910\sprint1\assign.jpg" alt="image"> 
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1100910\sprint1\block.jpg" alt="image"> 
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1100910\sprint1\expression.jpg" alt="image"> 
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1100910\sprint1\expression2.jpg" alt="image"> 
 * <p>
 * Also we will also need to create a class to deal with "FOR" instructions as we can see by the following diagram:
 * -Not implemented-
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * -Not implemented-
 *
 * <h3>5.2. UC Realization</h3>
 *
 * <h3>Block of Instructions</h3>
 * 
 * <h3>Assign result to current cell</h3>
 * 
 * <h3>Assign Results to other cells</h3>
 * 
 * <h3>FOR loop based on instruction blocks</h3>
 * 
 * <h3>5.3. Classes</h3>
 * 
 * <h3>Assign</h3>
 * 
 * <h3>Block</h3>
 * 
 * <h3>For</h3>
 * 
 * 
 * <h2>6. Implementation</h2>
 * 
 * All taks were completed except FOR loop that we are still working on.
 * We detect an issue on instruction blocks. It only accept two instructions. We are working on it to solve this asap
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * -Not started-
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * -Not started-
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <p>
 * <b>Monday</b>
 * <p> Daily scrum meeting
 * <p> Issues division and assignement
 * <p> Project analysis and architecture study
 * <p>
 * <b>Tuesday</b>
 * <p> Code analysis and documentation
 * <p> Support team members 
 * <p>
 * <b>Wednesday</b>
 * <p> Scrum Meeting with teacher
 * <p> Continue analysis
 * <p> First sequence diagram completed
 * <p> Formula.g peer programming
 * <p>
 * <b>Thursday</b>
 * <p> Daily Scrum meeting. Defining First delivery
 * <p> Peer Programing: Defining Formulas.g for "FOR" instruction done
 * <p> Finish analysis
 * <p> Presentation to client - It is needeed to solve some issues: Blocks only accept 2 instructions and FOR is not working yet 
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author 1100910@isep.ipp.pt
 */

package csheets.worklog.n1100910.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author josemarques
 */
class _Dummy_ {}

