/**
 * Technical documentation regarding the work of the team member (1100910) José Marques during week2.
 *
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -Notes about the week's work.-
 *
 * <h2>2. Use Case/Feature: Lang02.1</h2>
 *
 * Issue in Jira:
 * <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-30" target="_blank">LPFOURNB-30</a>
 * - Temporary Variables
 *
 * <h2>3. Requirements</h2>
 * Add support for temporary variables. The name of temporary variables must
 * start with the '_' sign. When a variable is referred in a formula for the
 * first time it is created. To set the value of a variable it must be used on
 * the left of the assign operator (':='). Temporary variables are variables
 * that only exist in the context of the execution of a formula. Therefore, it
 * is possible for several formulas to use temporary variables with the same
 * name and they will be different instances. Example: '= {_Counter:=1;
 * WhileDo(Eval( “A“&_Counter)&lt; 0; {C1:=C1+Eval(“B“&_Counter);
 * _Counter:=_Counter+1 }) }' . In this example, the cell C1 will get the sum of
 * all the values of column B in that the corresponding values in column A are
 * greater than zero.
 *
 * <h3> Use Case "Variable":</h3>
 * The user inserts a formula. If this formula contains a variable, the system
 * will create it and formulate the result.
 *
 * <h2>2. Analysis</h2>
 * The following sequence diagram depicts how it should be implemented:
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1100910\sprint2\Lang02.1- Temporary Variables_Analysis.png" alt="Lang02.1- Temporary Variables Analysis">
 * <p>
 * To use the concept of variable we will need to change Formula.g to support
 * them and a way to be recognized by CleanSheets. After studing the best way to
 * recognize the variables I've realized that a symbol table is probably the
 * best way to do it.
 *
 *
 * <h2>5. Design</h2>
 * I've realized that we will need to change ExcelExpressionCompiler in order to
 * verify conditions to create a new variable. It will be created a Variable
 * class to represent a variable. Most of the changes will be performed on
 * ExcelExpressionCompiler.java. At a first approach it will be created an array
 * of variables to store the temporary variables. Later on, this array should be
 * deleted and replaced by a repository, and correct functions accordingly
 * <p>
 * The following sequence diagram depicts how it will be implemented:
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1100910\sprint2\Lang02.1- Temporary Variables_Design.png" alt="Lang02.1- Temporary Variables Design">
 * 
 * <h3>5.1. Functional Tests</h3>
 * Tested with formula "={_var:=3; _var2:=5; A1:=_var}". More deep tests are
 * needed
 * Junit test to Variable.compareTo()
 *
 * <h3>5.3. Classes</h3>
 * Variable.java: New class to represents the variable
 * <p>
 * ExcelExpressionCompiler: Significant changes to detect variables
 * <p>
 * Formulas.g: add grammar rules to detect variables
 *
 *
 * <h2>6. Implementation</h2>
 * This is how we have implemented in the grammar to recognize variables:
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1100910\sprint2\Lang02.1- Temporary Variables_Design_Grammar.jpg" alt="Lang02.1- Temporary Variables Grammar">
 * 
 * It was created a variable class (csheets.core.formula.lang.variable) with the
 * attibutes to represent a variable (a name and a value).
 * <p>
 * In ExcelExpressionCompiler.java It was created an array to store up to 10
 * temporary variables. It was significantly changed the method convert(Cell
 * cell, Tree node) to detect and store new variables. Later this array should
 * be replaced by a repository as well as methods that are using it, must be
 * replaced to search the repo instead.
 *
 *
 * <pre>
 * {@code
 *   if (node.getChildCount() == 1) // Convert unary operation
 * {
 * …
 * } else if (node.getChildCount() == 2) {
 * …
 *  } else if (operator instanceof Assign) {
 *   //This will check if there is any assign to a variable
 *   String childContent = node.getChild(0).getText();
 *   if (childContent.contains("_")) {
 *    System.out.println("Variable Found!");
 *    varList[varIndex] = (Variable) createVar(cell, node);
 *    varIndex++;
 *   }
 *  }
 * }
 * </pre> 
 * Here, if it detects an assignement ":=", it will check if there is any
 * "_" at the beginning of the string. If there is, it will invoque the
 * createVar() method witch will create the temp variable
 * <pre>
 * {@code
 * private Expression createVar(Cell cell, Tree node) throws FormulaCompilationException {
 * BinaryOperator op = Language.getInstance().getBinaryOperator(node.getText());
 * String var = node.getChild(0).getText().substring(1);
 * String content = node.getChild(1).getText();
 * Variable test = new Variable(var, Value.parseValue(content));
 * return new Variable(var, Value.parseValue(content));
 * }
 * }
 * </pre> 
 * This is how a var is created. As we can see it is created just like we
 * explain above in the sequence diagram depicted above in design
 *
 *
 * <pre>
 * {@code
 * protected Expression convert(Cell cell, Tree node) throws FormulaCompilationException {
 * …
 *  try {
 *   switch (node.getType()) {
 * …
 *    case FormulaLexer.VARIABLE:
 *     //Get the variable name
 *     String varName = node.getText().substring(1);
 *     //creates the variable whitch will be assigned
 *     Variable var = new Variable();
 *     //Search in the temporary array for the variable and assign it to the previously created
 *     for (int i = 0; i < varIndex; i++) {
 *      if (varList[i].getVarName().equals(varName)){
 *       var = varList[i];
 *      }
 *     }
 *     //return the value of the variable
 *     return new Literal(Value.parseValue(var.getVarValue().toString(), Value.Type.NUMERIC, Value.Type.TEXT));
 * }
 * </pre> 
 * As we can see, if it is detected a variable token, it will search the
 * temp array for that variable and returs its value
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 * -Not started-
 *
 * <h2>8. Final Remarks</h2>
 * -Not started-
 *
 *
 * <h2>9. Work Log</h2>
 *
 * <p>
 * <b>Monday</b>
 * <p>
 * Daily Scrum meeting with Scrum Master and Area Leader to define achievements
 * for this week and check how previous US was finished
 * <p>
 * Analysis of US
 * <p>
 * Continue working to finish implementation of FOR and fixing erros of US
 * Lang01.1- Block of Instructions
 * <p>
 * Integrate new team member (1141048) to understand how Lang works
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Start working on Design of the US
 * <p>
 * Meeting with teacher PSM to discuss how can we solve the bug with previous US
 * (Lang01.1).
 * <p>
 * After this meeting and discuss with another colleague, I've find out that the
 * approach we had, wasn't the best one. We will need to rething the Assign
 * class to implement Expression instead of BinaryOperator. We will try to
 * complete this US later on this project.
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Continue working on Design
 * <p>
 * Start implementing Domain classes and redifine other classes
 * <p>
 * First compile time tests successfully performed
 * <p>
 * <b>Thursday</b>
 *
 * <h2>10. Self Assessment</h2>
 * -Not started-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 * -Not started-
 * <h3>10.2. Teamwork: ...</h3>
 * <p>
 * Integrate new team member (1141048) to understand how functions work
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 * -Not started- @author 1100910@isep.ipp.pt
 */
package csheets.worklog.n1100910.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author josemarques
 */
class _Dummy_ {
}
