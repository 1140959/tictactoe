/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

/**
 * Technical documentation regarding the work of the team member (1140174) Sérgio Silva during week2.
 *
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -Notes about the week's work.-
 * <p>
 * - Only on Tuesday i finished the last week issue so a will have less time to implement thi FI.
 * - Wednesday i started the Analysis and Requirements for the sprint 2 issue, and i decided so i wont delay the work for everyone and for the good of the team i decided to start the Implementation insted of making tests primarily.
 * - Thursday, the day for the presentation i give my best to have the most functional code to show, as this evaluation is binary, it works or it wont. The only thing that wasn't working was the import of Professions from a XML.
 * - Friday i expect to have the issue implemented with the JPA, Import from XML and JUnit tests so i can in the weekend start with the analysis for the issue from sprint 3.
 *
 * <h2>2. Use Case/Feature: CRM01.2- Contact Edition</h2>
 *
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-76"><b>CRM01.2- Contact Edition</b></a></p>
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-211"><b>CRM01.2- Analysis</b></a></p>
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-212"><b>CRM01.2- Design</b></a></p>
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-213"><b>CRM01.2- Tests</b></a></p>
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-214"><b>CRM01.2- Implementation</b></a></p>
 *
 * <b>Description</b>
 * <p>  A contact may also be a company. If a contact is a company then it has a name (no first and no last
 name). A person contact may now be related to a company contact. A person contact may have also a
 profession. The profession should be selected from a list. The list of professions should be loaded (and/or
 updated) from a external xml file or an existing configuration file of Cleansheets. The window for company
 contacts should display all the person contacts that are related to it. The company window should also
 have an agenda. The agenda of a company should be read only and display all the events of the individual
 contacts that are related to it.
 *
 *
 * <h2>3. Requirement</h2>
 * <p> Personal Contacts now has two new attributes Company and Profession
 * <p> The List of companies are loaded from the existing ones created within csheets
 * <p> New Company Contacts with a list of users that have that company in their attributes
 * <p> The Company can see the Agenda in read only mode for all his personal contacts
 * <p> The list of professions are loaded from a external xml file on the start of the extension
 *
 *
 *
 * <h2>4. Analysis</h2>
 * <p>As i have implemented the latest issue already with Personal and Company contacts that extends Contact (class that only has a Agenda, because it's a attribute in both types of contacts.
 * <p>Create a class/parser Profession that on start of the extension will reload is list of professions from the xml file.
 * <p>In the UI for Personal Contact panel i have to create two new attributes, for Company and Profession, they should be comboBox's reloaded every time the panel is loaded os it will be updated.
 * <p>The new UI for Company Contact Panel will only have one editable attribute that is the name, then there will be an dynamic Jlist that will contain all the Personal Contacts associated with the Company.
 * <p>Also the Panel for Events when a company is selected should display all the contacts events for the associated contacts with the company, read only mode.
 * <p>I will implement all the JPA in this issue, contemplating the latest Issue too because i didn't have the opportunity before to do so, as all the tests are being implemented in inMemory.
 *
 *
 * <p> Notes:
 *
 * <p>The first user to be created should be the logged user on Windows, as the extension warn of that in the start and gets the first name and last name automatically, allowing only to upload a photo.
 * <p> As i work with the objects in the UI comboBox's, if somenone change a Company name, the name will change in the compony comboBox of the Personal Contact.
 *
 * <h3>5.3. Classes</h3>
 *
 * - Class Diagram implementation
 *
 * <img src="../../../../../\lapr4-2016-2nb\src\main\java\csheets\worklog\n1141074\sprint2\diagram.png" alt="image"  width="900">
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Patterns used in the issue
 * <p>
 * - Same has sprint 1, because this is a incrementation from the last issue.
 *
 * <h2>6. Implementation</h2>
 **
 * <h3>Sequence diagram for "class diagram"</h3>
 * This diagram demonstrating the possibility for implementation.
 * <p>
 * <img src="../../../../../\lapr4-2016-2nb\src\main\java\csheets\worklog\n1141074\sprint2\DiagramVP.png" alt="image"  width="900">
 *
 * <h3>Sequence diagram for "CRM01.2 Create PersonalContact"</h3>
 * This diagram specify the creating of a new Personal contact.
 * <p>
 * <img src="../../../../../\lapr4-2016-2nb\src\main\java\csheets\worklog\n1141074\sprint2\CRM01.2 Create PersonalContact.png" alt="image"  width="900">
 *
 * <h3>Sequence diagram for "CRM01.2 Create CompanyContact"</h3>
 * This diagram specify the creating of a new Company contact.
 * <p>
 * <img src="../../../../../\lapr4-2016-2nb\src\main\java\csheets\worklog\n1141074\sprint2\CRM01.2 Create CompanyContact.png" alt="image"  width="900">

 * @author 1141074 - Sérgio Silva
 */

package csheets.worklog.n1141074.sprint2;

/**
 * Worklog about Sprint 2
 *
 * @author 1141074 - Sérgio Silva
 */
class _Dummy_ {}
