/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

/**
 * Technical documentation regarding the work of the team member (1140174) Sérgio Silva during week3.

 * <b>Scrum Master: -(yes/no)- no</b>
 *
 *
 * <b>Area Leader: -(yes/no)- no</b>
 *
 *
 * <h2>1. Notes</h2>
 * <p> -Notes about the week's work.-
 *
 * - As we changed to a different module first of all i'm going to analyze the code that's already been made to develop a solution to my issue according to the existing code.
 *
 * <h2>2. Use Case/Feature: IPC01.3 Multiple Sharing</h2>
 *
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-53"><b>IPC01.3- Multiple Sharing</b></a></p>
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-227"><b>IPC01.3- Analysis</b></a></p>
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-228"><b>IPC01.3- Tests</b></a></p>
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-229"><b>IPC01.3- Design</b></a></p>
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-230"><b>IPC01.3- Implementation</b></a></p>
 *
 * <b>Description</b>
 * <p>  IPC01.3) Multiple Sharing  !
 * It should be possible to have multiple cell shares active at the same time. Each of the shares should have
 * a unique name. The location (i.e., range address) of the share in each instance of Cleansheets may be
 * different. It should be possible to share ranges that include cells with formulas.
 * Difficulty: Hard.
 * Mandatory: Yes.
 *
 *
 * * <h2>3. Requirement</h2>
 * <p> Have multply instances running of the cell share
 * <p> Add the atribute name to the instances
 * <p> Allow for different cell range in each instance
 * <p> Share ranges that include cells with formulas
 *
 *
 * <h2>4. Analysis</h2>
 *
 *
 * <h3>Notes:</h3>
 *
 *
 * <h2>5. Design</h2>
 *
 *
 * <h3>5.1. Functional Tests</h3>
 *
 *
 * <h3>5.2. UC Realization</h3>
 *
 *
 * <h2>6. Implementation</h2>
 *
 *
 * <h2>8. Final Remarks</h2>
 *
 *
 * <h2>9. Work Log</h2>
 * <h3>Friday</h3>
 * <p> Yesterday I worked on:
 * <p> 1. Implemented some of the latest requisites to the presentation (That worked very well)
 * <p> Today
 * <p> 1. Finished the issue with all the assignments.
 * <p> 2. Made some tests to the existing important methods and Classes
 * <p> Blocking:
 * <p> 1. -nothing-
 * <h3>Saturday</h3>
 * <p> Yesterday I worked on:
 * <p> 1. Finished the issue with all the assignments.
 * <p> 2. Made some tests to the existing important methods and Classes
 * <p> Today
 * <p> 1. Implementation of JPA.
 * <p> 2. Learning the code from the issues before this issue (from sprint 1 and sprint 2)
 * <p> Blocking:
 * <p> 1. -nothing-
 *
 *
 * <h2>10. Self Assessment</h2>
 *
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * <b>Evidences:</b>
 *
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 *
 * <h3>10.3. Technical Documentation:</h3>
 *
 * @author 1141074 - Sérgio Silva
 */

package csheets.worklog.n1141074.sprint3;

/**
 * Worklog about Sprint 3
 *
 * @author 1141074 - Sérgio Silva
 */
class _Dummy_ {
}
