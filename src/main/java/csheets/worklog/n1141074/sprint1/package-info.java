/*
 * Copyright (c) - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Eduardo Silva <1141074@isep.ipp.pt>, 2016
 */

/**
 * Technical documentation regarding the work of the team member (1140174) Sérgio Silva during week1.
 *
 *
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- yes</b>
 *
 * <h2>1. Notes</h2>
 *
 * -Notes about the week's work.-
 * <p>
 * -First of all i analyzed the code and understanding what to do with the issue.
 * Next i've analyzed the issue and created some design possibility implementations, sub dividing the into 6 use cases. It will be easier to implement with this division.
 * Started the implementation and structural code has been created.
 *
 * <h2>2. Use Case/Feature: CRM01.1- Contact Edition</h2>
 *
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-75"><b>CRM01.1- Contact Edition</b></a></p>
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-96"><b>CRM01.1- Analysis 1141074</b></a></p>
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-118"><b>CRM01.1- Implementation 1141074</b></a></p>
 *
 * <p>
 * <b>Description</b>
 * <p>
 *A sidebar window that provides functionalities for creating, editing and removing contacts. Each contact should have a first and last name and also a photograph. Each contact should also have one agenda in which events related to the contact should be displayed. For the moment, events have only a due date (i.e., timestamp) and a textual description. It should be possible to create, edit and remove events. The agenda may be displayed in a different sidebar. This sidebar should display a list of all events: past, present and future. One of the contacts should be the user of the session in the computer where Cleansheets is running. If this user has events then, when their due date arrives, Cleansheets should display a popup window notifying the user about the events. This popup window should automatically disappear after a small time interval (e.g., 5 seconds).
 *
 *
 * <h2>3. Requirement</h2>
 * <p>
 * Allow the creation, editing and erasing of contacts.
 * Each contact should have a individual agenda with events.
 * The events can be created, edited and erased.
 * Each event only have a timestamp and a description
 * The system should detect the logged user and notify the user about their specific events on due date.
 * The notifications should only be visible for 5 seconds.
 *
 *
 * <h2>4. Analysis</h2>
<p>
 * 1. Once the extension is activated an action is processed that will verify using a "Singleton SYSTEM USER" if the user logged on the system is already a created contact, if so it will activate the specific agenda and events.
 * 2. The user create a new contact using a form, introducing first name, last name and a photograph. When create button is select  the system will validate in the ContactRepository if there is any contact with same first and last name. Once validated it will persist it with JPA to the db.
 * 3. To edit or remove a contact, the user will choose the contacts tab, and on the list of contacts it will click twice and it will open a pop-up with the data from the selected user to edit or delete. The changes in the users will be persisted to the db.
 * 4. If a system user is a existing contact the user will have the option to create a event, after selecting to create a event, the actionCreateEvent will be processed that will open a pop-up with the possibility to create new event, with timestamp and description. The system will validate if the event don't exists with the same timestamp and description. After validation the data is persisted on the db.
 * 5. For editing the user will double click on a event in the list of events, and the actionEditEvent will activate the pop-up with the event with the data to edit or delete.
 *
 * <h3>5.3. Classes</h3>
 *
 * - Class Diagram implementation
 *
 * <img src="../../../../../\lapr4-2016-2nb\src\main\java\csheets\worklog\n1141074\sprint1\diagram.png" alt="image"  width="900">
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Patterns used in the issue
 * <p>
 * - STRATEGY - Used in the create button that depending on the context uses the correct panel
 * - MVC - Gives the model or Panel from UI to the Controller so that the controller can directly update them
 * - DTO or Value Objects - Used when creating Contacts or Event to encapsulate the data in a value object
 * - Contact Builder's - Used to the construct of Complex Objects as the Contacts. For Event's in the moment there is no need to implement it this way.
 *
 * <h2>6. Implementation</h2>
 *
 * -Reference the code elements that where updated or added-
  * <p>
 * see:<p>
 * <a href="../../../../../lapr4-2016-2nb/src/main/java/csheets/ext/contacts/ui/ContactController.java">csheets.ext.contacts.ui.ContactController.java</a>
 * <a href="../../../../../lapr4-2016-2nb/src/main/java/csheets/ext/contacts/ui/ContactService.java">csheets.ext.contacts.ui.ContactService.java</a>
 * <a href="../../../../../lapr4-2016-2nb/src/main/java/csheets/ext/contacts/ui/ContactDTO.java">csheets.ext.contacts.ui.ContactDTO.java</a>
 * <a href="../../../../../lapr4-2016-2nb/src/main/java/csheets/ext/contacts/PersonalContactBuilder.java">csheets.ext.contacts.PersonalContactBuilder.java</a>
 *
 * <h3>Sequence diagram for "activate ContactsExtensions"</h3>
 * This diagram demonstrate the functionality to verify if the logged user is a existing contact and activate the specific agenda.
 * <p>
 * <img src="../../../../../\lapr4-2016-2nb\src\main\java\csheets\worklog\n1141074\sprint1\ssd1.png" alt="image"  width="900">
 *
 * <h3>Sequence diagram for "Creating contact"</h3>
 * This diagram specify the creating of a new contact.
 * <p>
 * <img src="../../../../../\lapr4-2016-2nb\src\main\java\csheets\worklog\n1141074\sprint1\ssd2.png" alt="image"  width="900">
 *
 * <h3>Sequence diagram for "Editing and removing contact"</h3>
 * This diagram specify the editing and/or removing of an existing contact.
 * <p>
 * <img src="../../../../../\lapr4-2016-2nb\src\main\java\csheets\worklog\n1141074\sprint1\ssd3.png" alt="image"  width="900">
 *
 * <h3>Sequence diagram for "Creating event"</h3>
 * This diagram specify the creating of a new event.
 * <p>
 * <img src="../../../../../\lapr4-2016-2nb\src\main\java\csheets\worklog\n1141074\sprint1\ssd4.png" alt="image"  width="900">
 *
 * <h3>Sequence diagram for "Editing and removing event"</h3>
 * This diagram specify the editing and/or removing of an existing event.
 * <p>
 * <img src="../../../../../\lapr4-2016-2nb\src\main\java\csheets\worklog\n1141074\sprint1\ssd5.png" alt="image"  width="900">
 *
 * <h3>Sequence diagram for "Notification for event on due date"</h3>
 * This diagram specify the procedure when a pop-up is displayed for 5 seconds when a due date for an event is achieved.
 * <p>
 * <img src="../../../../../\lapr4-2016-2nb\src\main\java\csheets\worklog\n1141074\sprint1\ssd6.png" alt="image"  width="900">
 *
 * <h3>Sequence diagram for "Class diagram"</h3>
 * This diagram demonstrate the functionality to verify if the logged user is a existing contact and activate the specific agenda.
 * <p>
 * <img src="../../../../../\lapr4-2016-2nb\src\main\java\csheets\worklog\n1141074\sprint1\ssd7.png" alt="image"  width="900">
 *
 *
 * @author 1141074 - Sérgio Silva
 */

package csheets.worklog.n1141074.sprint1;

/**
 * Worklog about Sprint 1
 *
 * @author 1141074 - Sérgio Silva
 */
class _Dummy_ {}

