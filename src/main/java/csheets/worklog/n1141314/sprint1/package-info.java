/**
 * Technical documentation regarding the user story ipc01_01: start sharing. Daniel Monteiro - 1141314
 *
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>2. Use Case/Feature: IPC01.1</h2>
 *
 * Issue in Jira: 
 * <p>
 *  *(Analysis): http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-115
 * <p>
 *  *(Implementation): http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-120
 *
 * <h2>1. Requirements</h2>
 * <p>
 * A new extension which aim is to support the communication/sharing of data between different instances of Cleansheets.<br>
 * It should be possible to establish a connection with other instance of Cleansheets in the local network.<br> It
 * should be possible to send the contents of a range of cells to another instance of Cleansheets.<br> The other
 * instance should display the received contents in the same cell address as the original cells.<br>
 * It should be possible to configure the port to be used for network connections. It should be possible to
 * find other instances of Cleansheets available in e local network.<br> These instances should be listed in a new
 * window (sidebar window).<br> The user should be able to select one of the discovered instances to connect
 * to when establishing the connection to send the contents of the range of cells.<br> At the moment it is only
 * required to send the value of the cells.
 * 
 * <p>
 * <b>Use case "Start Sharing": </b>
 * <p>
 * * The user starts the sharing process.<p>
 * * The system displays all other instances of Cleansheets in the network.<p>
 * * The user select the instance. <p>
 * * The user selects the cell or range of cells which he wants to send.<p>
 * * The user confirms.<p>
 * * The system notifies the user for the success of the operation.
 * 
 *
 * <h2>2. Analysis</h2>
 * Sharing cells will be a new extension to cleansheets, so we
 * need to check how extensions are loaded and how they are implemented.<br>
 * Like we saw on RCOMP discipline, we will need to set up a port for communications between instances of Cleansheets in the network.<br>
 * After that, we will need that the instance that want to send the cells, to broadcast the network in order to find all instances of Cleansheets.<br>
 * With that, the system will show the user the instances found and ask to select one of the instances.<br>
 * The user will select and confirm the choice, so the system can send the cells and notify the user for the success of the operation.<br>
 * 
 * 
 * <h3>First "analysis" sequence diagrams</h3>
 * <p>
 * This is the sketch diagram of our understanding of this use case.<br> For that
 * reason we mark the elements of the diagrams with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application.
 * 
 * <p>
 * <img src="doc-files/ipc01_1_analysis_start_sharing1.png" alt="image"> 
 *
 * <h2>3. Tests</h2>
 * <p>
 * To test a use case like this, that uses the existing network to establish<br>
 * communication between Cleansheets instances, we need to start a connection between two or more Cleansheets instances
 * and to be able to send/receive data.<br>
 * To make this possible we can set up some test units like:<br>
 * - Set the port: test if the port given by the user is valid.<br>
 * - Get other cleansheet instances: broadcast the network and check if any other cleansheets instance is found.<br>
 * - Create a new connection: test if we can establish a connection between two cleansheets instances over the network;<br>
 * - Send range cells: test to check if we can send cells to another instance over the network.<br>
 * 
 * Tests made is on package csheets.ext.sharesharing on tests package.
 * 
 * 
 * <h2>4. Design</h2>
 * 
 * <p>
 * <img src="doc-files/ipc01_1_design_start_sharing_send.png" alt="image"> 
 * 
 * <p>
 * <img src="doc-files/ipc01_1_design_start_sharing_receive.png" alt="image"> 
 * 
 *
 *  
 *
 * <h2>5. Coding</h2>
 * 
 * Coded all the classes of package csheets.ext.startsharing
 * 
 * 
 *
 * <h2>6. Final Remarks</h2>
 *  This use case was too difficult to implement regarding the use of threads and sockets, with the few knowledge that we got from RCOMP.
 *  Still, the use case is implemented 100% but still has a bug that we couldn't resolve. The first time we sent cells, the receiver gets 
 *  them (as shown on console) but doesn't show them. After that, you send again , and it already shows the cells received.
 * 
 * <h2>7. Work Log</h2> 
 * 
 * <p>
 * <b>Monday - 30.05.2016</b>
 * <p>
 * Worked on analysis documentation (Requirements and analysis).
 * <p>
 * <b>Tuesday - 31.05.2016</b>
 * <p>
 * Worked on design diagrams and division of code implementation.
 * <p>
 * <b>Wednesday - 01.06.2016</b>
 * <p>
 * Started coding the domain part of the use case (broadcast,listener,networkData,networkService
 * <p>
 * <b>Thursday - 02.06.2016</b>
 * <p>
 * Finish some bugs and review code to the presentation. Continue to implement the use case.
 * <p>
 * <b>Friday - 03.06.2016</b>
 * <p>
 * Spent all the time implementing the use case.
 * <p>
 * <b>Saturday -  04.06.2016</b>
 * <p>
 * Spent all the time implementing the use case.Finished broadcasting and selecting the instance to connect.
 * <p>
 * <b>Sunday - 05.06.2016</b>
 * <p>
 * Finishing the use case. Send cells is functional.
 * <p>
 * 
 * 
 *
 * @author Daniel Monteiro 1141314
 */

package csheets.worklog.n1141314.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author Daniel Monteiro 1141314
 */
class _Dummy_ {}

