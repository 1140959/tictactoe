/**
 * Technical documentation regarding the user story ipc01_2: Sharing’s Automatic Update. Daniel Monteiro - 1141314
 *
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- yes</b>
 *
 * <h2>2. Use Case/Feature: IPC01.2</h2>
 *
 * Issue in Jira: 
 * <p>
 *  *(Analysis): http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-155
 * <p>
 *  *(Design): http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-156
 * <p>
 *  *(Testes): http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-161
 * <p>
 *  *(Implementation): http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-162
 *
 *
 * <h2>1. Requirements</h2>
 * <p>
 *  Once a connection is stablished between two instances of Cleansheets updates made in one side must be<br>
 *  automatically sent to the other side. The data shared must include now also the style of the cells. At the<br>
 *  moment It is not necessary to support the sharing of cells with formulas.<br>
 * 
 * 
 * 
 * <p>
 * <b>Use case "Sharing’s Automatic Update": </b>
 * <p>
 * * The user starts the sharing process.<p>
 * * The system displays all other instances of Cleansheets in the network.<p>
 * * The user select the instance. <p>
 * * The system request the user to choose if he wants automatic update on cells.
 * * The user answers.
 * * The user selects the cell or range of cells which he wants to send.<p>
 * * The user confirms.<p>
 * * The system notifies the user for the success of the operation.
 * 
 * 
 * <h2>2. Analysis</h2>
 * This use case is a continuation of the previous use case "Start sharing". It follows the same<br>
 * implementation and code usage but insted of sending just one time after connecting to another instance, it has to send<br>
 * every time the user fill a cell.<br> 
 * To apply automatic sharing to "Start sharing", i can do this in 2 ways. Or being automatic when the user send cells for the first time or,<br>
 * before connect to another instance, the user has an option to select if he wants the automatic sharing. 
 * To be "visible" to the user, i will implement the second way.
 * 
 * 
 * <h3>First "analysis" sequence diagrams</h3>
 * Using most of week1 diagram of Start Sharing use case, i will just apply an observer 
 * using a controller for this use case and apply that to the SharingPanel.
 * Trying to implement a simple way using the previous code implemented to improve code reutilization.
 * I will find a bit of problems because of Start Sharing bug that is documented in that use case package info.
 * <p>
 * <img src="doc-files/ipc01_2_analysis_automatic_sharing.png" alt="image"> 
 * 
 * 
 * <h2>3. Tests</h2>
 * There aren't too many tests in this use case, since it bases most of the code on IPC01_1.
 * I will test AutomaticUpdateObject class and methods on AutomaticUpdateController.
 * 
 *  All tests done is on package csheets.ext.startsharing.automaticupdate on Test Package.
 * 
 * 
 * 
 * <h2>4. Design</h2>
 * 
 *  I decided to create a AutomaticUpdateObject class to gather the cell and its stylablecell class<br>
 *  so i can send it all in just one object.
 *  After receiving this object, i will just need to separate one from another, and put them in the spreadsheetTable.
 * 
 * <p>
 * <img src="doc-files/doc-files/ipc01_2_design_automatic_sharing.png" alt="image">  
 *  
 *
 * <h2>5. Coding</h2>
 * 
 * 
 *  Coded all classes from package csheets.ext.startsharing.automaticupdate
 * 
 *
 * <h2>6. Final Remarks</h2>
 *  
 *  
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 *  Since im the area leader of this week, i have done a workbook in OneNote platform to manage<br>
 *  all information that my teammates gave me regarding their use cases.
 *  The workbook is on this package with the name "IPCteam_workbook_*****" with *** belonging to the last daily team meeting.
 * 
 * <p>
 * <b>Monday - 06.06.2016</b>
 * <p>
 *  Use Case Analysis and Design. Area Leader Work: Setting a OneNote workbook to gather team information about all use cases.
 * <p>
 * <b>Tuesday - 07.06.2016</b>
 * <p>
 *  Design done. Started implementation and tests. Area Leader Work: updated workbook with use cases information that teammates gave me.
 * <p>
 * <b>Wednesday - 08.06.2016</b>
 * <p>
 *  Implementing and testing. done 80% of the code and tests
 * <p>
 * <b>Thursday - 09.06.2016</b>
 * <p>
 * Finishing the implementation and tests. Presentation of the use case.
 * <p>
 * <b>Friday</b>
 * <p>
 * Fixing a problem regarding getting nullpointers when receiving an object. All set now. Good to close the use case.
 * <p>
 * 
 *
 * @author Daniel Monteiro 1141314
 */

package csheets.worklog.n1141314.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author Daniel Monteiro 1141314
 */
class _Dummy_ {}

