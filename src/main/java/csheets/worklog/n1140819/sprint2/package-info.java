/**
 * Technical documentation regarding the work of the team member (1140819) José Miranda during week1
 * on the Use Case/Feature: IPC07.1 - Choose Game and Partner. 
 * 
 * <p>
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 * ---
 *
 * <h2>2. Use Case/Feature: IPC07.1 - Choose Game and Partner</h2>
 * 
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-69
 * 
 * 
 * <h2>3. Requirement</h2>
 * <p>With this extension it should be possible for two instances of Cleansheets to establish 
 * a connection to play games. Cleansheets should display a list of all instances of Cleansheets 
 * in the local network. It should be possible to invite another instance to play one of the 
 * following games: tic-tac-toe (https://en.wikipedia. org/wiki/Tic-tac-toe) or battleships 
 * (https://en.wikipedia.org/wiki/Battleship_(game)). Each instance should be able to define 
 * the profile of the user. The user name should be the user name of the system. The user should 
 * be able to configure an icon or photograph to be associated to his profile. The name of the users 
 * and icon should be displayed in the list of available users to play with. The user should be able 
 * to play several games at the same time. There should be a sidebar to display all the active games 
 * and also all the online users (i.e., instances of Cleansheets). It should be possible to end a game. 
 * At this moment it is not required to effectively implement the games.
 * 
 * <p>
 * <b>Use Case "Choose Game and Partner":</b>
 * <p>
 * The user selects the Games extension.
 * <p>
 * The system requires the user's name and icon/photograph to be associated to his profile.
 * <p>
 * The user inserts the required information.
 * <p>
 * The system dysplays the available games and asks the user to select one.
 * <p>
 * The user selects the game.
 * <p>
 * The system displays a list with the names and respective icons of all available users to 
 *      play with (other instances of Cleansheets in the local network).
 * <p>
 * The user selects the desired opponent.
 * <p>
 * The system creates a connection between the two instances and the game starts.
 *
 * <h2>4. Analysis</h2>
 * This feature will be available on the extensions menu and will require the use of at least
 * one new sidebar window so it is important to study how the UI layer is structured and 
 * implemented in order to maintain a consistent struture.
 * It will be necessary a class to aggregate the information of the user (name and icon). This information
 * will be very important because it will be sent and displayed on every other instances that will
 * be using this feature.
 * Once the user might select more than one game (and to prevent future alterations), 
 * the use of a "Game interface" will be mandatory. Thus, each existing game will implement this interface.
 * In order to find other instances it will be necessary to send UDP brodcast messages in the local 
 * network and to wait for UDP answers to update the list of instances. On the IPC01.1 - Start Sharing, 
 * ate this stage already implemented, it was necessary to implement a similar solution, thus the study 
 * and if possible the reuse of part of this code is also very important. 
 * When the user selects the desired opponent, it it is necessary to established a TCP connection with
 * its instance. Once again the IPC01.1 - Start Sharing, had to established a TCP connection between two
 * instances, so it might be possible to reuse part of this code.
 * 
 * <h3>First "analysis" sequence diagrams</h3>
 * <p>
 *  <img src="../../../../../../../src/main/java/csheets/worklog/n1140819/sprint2/IPC07.1_analysis.png" alt="image">
 * 
 * <p>
 * 
 * <h3>Tests</h3>
 * <p>Following the apoach above described, it is possible to identify the following tests:
 *  <p>- test if it is possible to find other instances.
 *  <p>- test if the information of the other users (name and icon) are correctly presented.
 *  <p>- test if it is possible to establish a connection with another instance.
 *  <p>- test if it is possible to establish several connections at the same time.
 *  <p>- test if it possible to end the a game at any time.
 * 
 * <h2>5. Design</h2>
 * <p>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1140819/sprint2/design.png" alt="image">
 * <p>
 * <h3>5.1. Functional Tests</h3>
 * ---
 * 
 * <h3>5.2. UC Realization</h3>
 * ---
 * 
 * <h3>5.3. Classes</h3>
 * ---
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * ---
 *
 * <h2>6. Implementation</h2>
 * ---
 * 
 * <h2>7. Integration/Demonstration</h2>
 * ---
 * 
 * <h2>8. Final Remarks</h2>
 * ---
 * 
 * <h2>9. Work Log</h2> 
 * 
 * ---
 * 
 * 
 * <b>Saturday</b>
 * <p>
 * Today
 * <p>
 * I worked on the requirements of this issue and started the its analysis.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <p>
 * 
 * <b>Sunday</b>
 * <p>
 * Today
 * <p>
 * I worked on the analysis of this issue. 
 * From this work has resulted a first draft of the sequence diagram.
 * I have also made an analysis of the IPC01.1 - Start Sharing - use case, in order to
 * find the common features with this issue.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Monday</b>
 * <p>
 * Today
 * <p>
 * I worked on the Design and I have started the UI implementation.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Today
 * <p>
 * I finished some final details of the design. From this, has resulted the final sequence diagram.
 * I have also worked on the use case implementation.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Today
 * <p>
 * I worked on the code implementation of the use case and tests.
 * <p>
 * Blocking:
 * <p>
 * <b>Thursday</b>
 * <p>
 * Today
 * <p>
 * 1. I worked on the code implementation and tests. 
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <h2>10. Self Assessment</h2> 
 * ---
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * ---
 * 
 * <p>
 * <b>Evidences:</b>
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author josemiranda
 */

package csheets.worklog.n1140819.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

