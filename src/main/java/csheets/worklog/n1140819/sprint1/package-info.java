/**
 * Technical documentation regarding the work of the team member (1140819) José Miranda during week1
 * on the Use Case/Feature: IPC01.1 - Start Sharing. 
 * 
 * <p>
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 * ---
 *
 * <h2>2. Use Case/Feature: IPC01.1 - Start Sharing</h2>
 * 
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-113
 * 
 * 
 * <h2>3. Requirement</h2>
 * <p>It should be possible to establish a connection with other instance of Cleansheets in the local network. 
 * It should be possible to send the contents of a range of cells to another instance of Cleansheets. 
 * The other instance should display the received contents in the same cell address as the original cells.
 * It should be possible to configure the port to be used for network connections. 
 * It should be possible to find other instances of Cleansheets available in e local network. 
 * These instances should be listed in a new window (sidebar window). 
 * The user should be able to select one of the discovered instances to connect to when establishing 
 * the connection to send the contents of the range of cells. 
 * At the moment it is only required to send the value of the cells.
 * 
 * <p>
 * <b>Use Case "Start Sharing":</b>
 * The user iniciates the Sharing process.
 * The system asks if the user wants to change the default port to establish the connection.
 * The user selects the desired port.
 * The system presents the available Cleansheet instances.
 * The user selects one instace and confirms the selected option.
 * The system requests the user to select the cell or the range of cells 
 * which content he pretends to display on the selected Cleansheet instance. 
 * The user selects the cell or the range of cells.
 * The system sends the content of the selected cell or range of cells and displays them on 
 * the second instance (in the the same cell address as the original cells). 
 * 
 *
 * <h2>4. Analysis</h2>
 * The contents that should be sent to the new instance of the Cleansheet are the content of the cell
 * as well as its address so that the this instance may display the cell's content in the same cell 
 * address as the original cells. This information is contained in the CellImpl class which implements 
 * the Cell interface.
 * In order to be possible to send the content of one cell/range of cells of an instance of the Cleansheet 
 * to another instance, the first step should be to find the available instances. One aproach might be to 
 * creat a connection service that would be responsible for the connections. This service would use an
 * UDPServer which would send an UDP broadcast message in the local network and at the same time would be
 * waiting for UDP messages from the UDPClients. The ip address of the instances that would respond 
 * to the UDP broadcast would be saved on a list and presented to the user to select the one he pretends to 
 * send the cells data for.  The service would create a new instance a TCPServer and the user select the 
 * cell/range of cells to be sent. After this proccess the conditions to send the cells data should be reunited.
 * The TCPServer server sends the necessary  information to a TCPClient created by the second instance 
 * of the Cleansheet.
 * 
 * 
 * <h3>First "analysis" sequence diagrams</h3>
 * The following diagrams depicts a proposal for the realization of the previously described use case.
 * We call this diagram an "analysis" use case realization because it functions like a draft that 
 * we can do during analysis or early design in order to get a previous approach to the design. 
 * For that reason the elements might not represent design elements and, therefore, might not exist 
 * as such in the code of the application 
 * (at least at the moment that this diagram was created).
 * <p>
 *  <img src="../../../../../../../src/main/java/csheets/worklog/n1140819/sprint1/Analysis.png" alt="image">
 * 
 * <h3>Tests</h3>
 * <p>Following the apoach above described, it is possible to identify the following tests:
 *  <p>- test if it is possible to find other instances (UDP).
 *  <p>- test if it is possible to establish a TCP connection with another instance.
 *  <p>- test if it is possible to send the contents of a cell/range of cells and the contents 
 * are correctly received.
 * 
 * <h2>5. Design</h2>
 *<h3>Sender behaviour</h3>
 * <img src="doc-files/ipc01_01_design_send.png" alt="image"> 
 * 
 * <h3>Receiver behaviour</h3>
 * <img src="doc-files/ipc01_01_design_receive.png" alt="image">
 * 
 * <h3>5.1. Functional Tests</h3>
 * ---
 * 
 * <h3>5.2. UC Realization</h3>
 * ---
 * 
 * <h3>5.3. Classes</h3>
 * ---
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * ---
 *
 * <h2>6. Implementation</h2>
 * ---
 * 
 * <h2>7. Integration/Demonstration</h2>
 * ---
 * 
 * <h2>8. Final Remarks</h2>
 * ---
 * 
 * <h2>9. Work Log</h2> 
 * 
 * ---
 * 
 * <p>
 * <b>Monday</b>
 * <p>
 * Today
 * <p>
 * 1. I worked on the planning of the use case, particularly on the description 
 * of this use case "Start Sharing", its Requirements, and Analysis. 
 * From this work has resulted a first draft of the sequence diagrams.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Today
 * <p>
 * 1. I worked on the design. 
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Today
 * <p>
 * 1. I finished the design and worked on the code implementation. 
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Thursday</b>
 * <p>
 * Today
 * <p>
 * 1. I worked on the code implementation. 
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 *  * <p>
 * <b>Friday</b>
 * <p>
 * Today
 * <p>
 * 1. I worked on the code implementation. 
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <h2>10. Self Assessment</h2> 
 * ---
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * ---
 * 
 * <p>
 * <b>Evidences:</b>
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author josemiranda
 */

package csheets.worklog.n1140819.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

