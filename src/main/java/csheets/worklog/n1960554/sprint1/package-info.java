/**
 * Technical documentation regarding the work of the team member (1960554) Paulo Silva during week1. 
 * 
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that each team member 
 * must produce each week/sprint. Suggestions on how to build this documentation will appear 
 * between '-' like this one. You should remove these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * -Notes about the week's work.-
 * <p>
 * -Begining of the project planning.-
 *
 * <h2>2. Use Case/Feature: IPC01.1</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-114"> Application Startup</a>
 * <p>
 * -IPC01.1- Start Sharing-
 * It should be possible to establish a connection with other instance of Cleansheets 
 * in the local network. It should be possible to send the contents of a range of cells 
 * to another instance of Cleansheets. The other instance should display the received 
 * contents in the same cell address as the original cells. It should be possible to 
 * configure the port to be used for network connections. It should be possible to find other instances of Cleansheets available in e local network. These instances should be listed in a new window (sidebar window). The user should be able to select one of the discovered instances to connect to when establishing the connection to send the contents of the range of cells. At the moment it is only required to send the value of the cells.
 * 
 * <h2>3. Requirement</h2>
 * Create the functionality of communication with other instances of cleansheets in the same network
 * Share the contents of a cell or a range of cells to another instance of Cleansheets.
 * Alloy the configuration of a specific port to use that communication.
 * Develop the functionality of receiving this cells contents, and place them in the same address as the original. 
 * <p>
 * <b>Use Case "Start Sharing":</b> 
 * The user select a range of cells and send them into a instance available.
 * 
 *  
 * <h2>4. Analysis</h2>
 * Since sharing cells will be supported in a new extension to cleansheets we need to study how extensions are loaded by cleansheets and how they

 * 
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts a proposal for the realization of the previously described use case. We call this diagram an "analysis" use case realization because it functions like a draft that we can do during analysis or early design in order to get a previous approach to the design. For that reason we mark the elements of the diagram with the stereotype "analysis" that states that the element is not a design element and, therefore, does not exists as such in the code of the application (at least at the moment that this diagram was created).<p>
 *  <img src="doc-files/ipc01_1_analysis.png" alt="image"> 
 *
 * <h2>5. Design</h2>
 * <p>Receiver
 * <img src="doc-files/ipc01_1_design_start_sharing_receive.png" alt="image"> 
 * <p>Sender
 * <img src="doc-files/ipc01_1_design_start_sharing_send.png" alt="image"> 
 * <h3>5.1. Functional Tests</h3>
 * Test if it is possible to use the choosen port.
 * Test if the are other instances of cleansheets
 * Test if the select cells are sent to the choosen instance
*
 * <h3>5.2. UC Realization</h3>
 * <p>
 * <b>Note:</b> It is very important that in the final version of this technical documentation the elements depicted in these design diagrams exist in the code!
 * 
 * <h3>5.3. Classes</h3>
 * 
 * -Document the implementation with class diagrams illustrating the new and the modified classes-
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * 
 * <h2>6. Implementation</h2>
 * 
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <b>Monday</b>
 * <p> Start the analysis, 1 hour. 
 * 
 * <p><b>Tuesday</b>
 * <p> Complete the analysis and create de SSD, 2 hours
 * 
 * <p><b>Wednesday</b>
 * <p>Work in the java doc, 3 hours
 * 
 * <p><b>Thursday</b>
 * <p> Test the application functionalities, 1 hour
 * @author Paulo Silva
 */

package csheets.worklog.n1960554.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

