/**
 * Technical documentation regarding the work of the team member (1960554) Paulo Silva during week2. 
 * 
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that each team member must produce each week/sprint. Suggestions on how to build this documentation will appear between '-' like this one. You should remove these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * -Notes about the week's work.-
 * <p>
 * -In this week, I read the requirements of the Feature, and studied the best way to solve it.-
 * -Argued with the other members of the group to share with them my propose.-
 * -Learn all the needs tho implement the solution 
 *
 * <h2>2. Use Case/Feature: IPC03.1- Search in Another Instance	</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-57">Application Startup</a> -
 * <p>
 * -It should be possible to send a request for searching workbooks to another instance of Cleansheets. 
 * The search should be based on the name of the workbook (a pattern of the name). The search should only
 * include workbooks that are open in the remote instance of Cleansheets. The reply must inform if the
 * workbook was find or not. If the workbook was find then the reply must also include a summary of the
 * contents of the workbook. This summary should include the name of the worksheets and the values of
 * the first non-empty cells of each worksheet.-
 * 
 * <h2>3. Requirement</h2>
 * <p>Create a new extention to alloy the search of workbooks opened in other instances in the same Network
 * Open the search panel to write the name of the workbook to do the action.
 * The reply must inform if the workbook was found Yes/No, and in case of True, the response should be a summary
 * information of the workbook, such as the name of the worksheets and the value of first non empty cell.
 * <p>
 * <b>Use Case "Search in Another Instance":</b> 
 * <p>It should be possible to send a request for searching workbooks to another instance of Cleansheets. 
 * The search should be based on the name of the workbook (a pattern of the name). The search should only
 * include workbooks that are open in the remote instance of Cleansheets. The reply must inform if the
 * workbook was find or not. If the workbook was find then the reply must also include a summary of the
 * contents of the workbook. This summary should include the name of the worksheets and the values of
 * the first non-empty cells of each worksheet
 * 
 *  
 * <h2>4. Analysis</h2>
 * Since Search in Another Instance will be supported in a new extension to cleansheets we need to study how extensions are loaded by cleansheets and how they work.
 * The first approach represented in the image bellow, shows the sketch of the
 * classes to be implemented.
 * <img src="doc-files/ipc03_01.design_analysis.png" alt="image">  
 *  
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts a proposal for the realization of the previously described use case. 
 * We call this diagram an "analysis" use case realization because it functions like a draft that we 
 * can do during analysis or early design in order to get a previous approach to the design. 
 * For that reason we mark the elements of the diagram with the stereotype "analysis" that states 
 * that the element is not a design element and, therefore, does not exists as such in the code of the 
 * application (at least at the moment that this diagram was created).
 * <p>
 * 
 * 
 * 
 * From the previous diagram we see that we need to add a workbook name to search, so we adapted the diagram to support that fixture. 
 
 * <h3>Analysis of Core Technical Problem</h3>
 * After this we reach this possible implementation, an it result in this:
 *  <img src="doc-files/ipc03_01.design_v02.png" alt="image">  
 * 
 * <h2>5. Design</h2>
 * <p> to be done...
 * <h3>5.1. Functional Tests</h3>
 * <p> Possible unit tests to implement:
 * <p> -Test if the connection to other instance exists
 * <p> -Test if the workbook exists in this instance 
 * see: 
 * <p><code>csheets.ext.distributedsearch</code>
 * <p><code>csheets.ext.distributedsearch.ui</code>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will need to create a subclass of Extension. We will also need to create a subclass of UIExtension. For the sidebar we need to implement a JPanel. In the code of the extension <code>csheets.ext.distributedsearch</code> we can find examples that illustrate how to implement these technical requirements.
 * The following diagrams illustrate core aspects of the design of the solution for this use case.
 * <p>
 * <b>Note:</b> It is very important that in the final version of this technical documentation the elements depicted in these design diagrams exist in the code!
 * 
 * <h3>Extension Setup</h3>
 * The following diagram shows the setup of the "comments" extension when cleansheets is run.
 * <p>
 * <img src="doc-files/core02_01_design.png" alt="image">
 * 
 *
 * <h3>User Selects a Cell</h3>
 * The following diagram illustrates what happens when the user selects a cell. The idea is that when this happens the extension must display in the sidebar the comment of that cell (if it exists).
 * <p>
 * <img src="doc-files/core02_01_design2.png" alt="image">
 * 
 * <h3>User Updates the Comment of a Cell</h3>
 * The following diagram illustrates what happens when the user updates the text of the comment of the current cell. To be noticed that this diagram does not depict the actual selection of a cell (that is illustrated in the previous diagram).
 * <p>
 * <img src="doc-files/core02_01_design3.png" alt="image">
 * 
 * <h3>5.3. Classes</h3>
 * 
 * -Document the implementation with class diagrams illustrating the new and the modified classes-
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * -Describe new or existing design patterns used in the issue-
 * <p>
 * -You can also add other artifacts to document the design, for instance, database models or updates to the domain model-
 * 
 * <h2>6. Implementation</h2>
 * 
 * -Reference the code elements that where updated or added-
 * <p>
 * -Also refer all other artifacts that are related to the implementation and where used in this issue. As far as possible you should use links to the commits of your work-
 * <p>
 * see:
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * -In this section document your contribution and efforts to the integration of your work with the work of the other elements of the team and also your work regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * -In this section present your views regarding alternatives, extra work and future work on the issue.-
 * <p>
 * As an extra this use case also implements a small cell visual decorator if the cell has a comment. This "feature" is not documented in this page.
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * -Insert here a log of you daily work. This is in essence the log of your daily standup meetings.-
 * <p>
 * Example
 * <p>
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. Analysis to use case
 * <p>
 * Today
 * <p>
 * 1. Analysis of the...
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. ...
 * <p>
 * Today
 * <p>
 * 1. ...
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * -Insert here your self-assessment of the work during this sprint.-
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex: mais de 50%) e apresentam código que para além de não ir contra a arquitetura do cleansheets segue ainda as boas práticas da área técnica (ex: sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the implementation of the design pattern ...-
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author  Paulo Silva - 1960554@isep.ipp.pt
 */

package csheets.worklog.n1960554.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class Dummy {}

