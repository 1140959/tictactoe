/**
 * Created by Tiago on 07-06-2016.
 *
 * Technical documentation regarding the work of the team member (1140775) Tiago Gabriel during week2.
 *
 * <p><b>Scrum Master: no</b>
 *
 * <p><b>Area Leader: no</b>
 *
 * <h2>1. Notes</h2>
 *
 * <p> Finishing last week's implementation. Study the requirements for this week use case </p>
 *
 * <h2>2. Use Case/Feature: Ccre01.2 Auto-Description of Extension</h2>
 *
 * <p><b>Issue in Jira:</b><a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-2">Auto-Description of Extensions</a>
 * <p><b>Description:</b> Extensions should have associated metadata.
 * Particularly, extensions should have a version number, a name and a description.
 * Cleansheets should display to the user the metadata of the extensions before loading them.
 * The user should be able to cancel the loading of an extension and also to select the version
 * of the extension to be loaded (if there are more than one).</p>
 *
 * <h2>3. Requirement</h2>
 * <p>- Before Cleansheets opens, it will be shown a frame with two panels</p>
 * <p>- The right panel will show all the extension available. The left panel will have all the extensions
 * chosen to be loaded</p>
 * <p>- The extension will be presented with this format: VERSION NAME DESCRIPTION</p>
 * <p>- If there aren't extensions chosen to be loaded, Cleansheets won't be start</p>
 *
 * <b>Use Case "Auto-Description of Extensions":</b>
 * <p>The frame is a JFrame</p>
 * <p>The panels are JPanel</p>
 * <p>The frame will have 4 buttons (JButton) to add or retrieve extension from the list</p>
 * <p>Plus two buttons(JButton), one for load and the other for cancel</p>
 *
 * <h2>4. Analysis</h2>
 * <p>The user starts Cleansheets. A frame is presented, having the list of all extensions.
 * The user chooses the extensions he/she wants to load. The user presses "Load" and the application will be loaded
 * with the extensions that were chosen.</p>

 * <h2>5. Design</h2>
 * <h3>Sequence Diagram</h3>
 * <p><img src="../../../../../../../\src\main\java\csheets\worklog\n1140775\sprint2\Core01.2_AutoDescription.png" alt="image"></p>
 *
 * <h3>Classes and Packages</h3>
 * <p><img src="../../../../../../../\src\main\java\csheets\worklog\n1140775\sprint2\Core02.1_ClassDiagram.png" alt="image"></p>
 *
 * <h2>6. Implementation</h2>
 * <p>See packages:</p>
 * <p>* csheets</p>
 * <p>* csheets.ext</p>
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * <h2>8. Final Remarks</h2>
 *
 *
 * <h2>9. Work Log</h2>
 *
 * <p><b>MONDAY - 06/JUN/2016</b></p>
 * <p><i>Yesterday: </i>nothing</p>
 * 
 * <p><i>Today:</i></p>
 * <p> 1. Meeting with the client. Understanding the requirements of the given Use case</p>
 * <p> 2. Worked on the last week's implementation (Contact's Edition)</p>
 * 
 * <p><i>Blockings:</i> Finish the last week's use case</p>
 *
 * <p><b>TUESDAY - 07/JUN/2016</b></p>
 * <p><i>Yesterday:</i></p>
 * <p>Working on the last week's use case.</p>
 * <p>Helping some elements of the class with their use case requirements</p>
 * 
 * <p><i>Today:</i></p>
 * <p>Second presentation of the first feature to the supervisor</p>
 * <p>New Feature analysis and part of the design done</p>
 * <p>Helping other colleagues on their analysis</p>
 * 
 * <p><i>Blockings: </i>Nothing</p>
 *
 * <p><b>WEDNESDAY - 08/JUN/2016</b></p>
 * <p><i>Yesterday: </i>see tuesday</p>
 * <p><i>Today:</i></p>
 * <p>Helped my colleagues with their use cases: IPC and CRM</p>
 * <p>Testing use case using mock classes to test the use case</p>
 * <p><i>Blockings:</i>Spent to much time helping other students.</p>
 *
 * <p><b>THURSDAY - 09/JUN/2016</b></p>
 * <p><i>Yesterday: </i> FILL ME </p>
 * <p><i>Today:</i> FILL ME </p>
 * <p><i>Blockings:</i> FILL ME </p>
 *
 * <h2>10. Self Assessment</h2>s
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * <p><b>Evidences:</b> FILL ME
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 * @author 1140775@isep.ipp.pt
 */
package csheets.worklog.n1140775.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 *
 * @author Tiago Gabriel
 */
class _Dummy_ {}