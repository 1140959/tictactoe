/**
 * Technical documentation regarding the work of the team member (1140775) Tiago Gabriel during week1. 
 * <p><b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p><b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * <p> Working with all group, brainstorming about the requirements of the given use case.
 * 
 * <h2>2. Use Case/Feature: CRM01.1- Contact Edition</h2>
 * 
 * <p><b>Issue in Jira:</b><a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-75">Contact Edition</a>
 * <p><b>Description:</b> A sidebar window that provides functionalities for creating, editing and removing contacts. 
 * Each contact should have a first and last name and also a photograph. 
 * Each contact should also have one agenda in which events related to the contact should be displayed. 
 * For the moment, events have only a due date (i.e., timestamp) and a textual description. 
 * It should be possible to create, edit and remove events. The agenda may be displayed in a different sidebar. 
 * This sidebar should display a list of all events: past, present and future. 
 * One of the contacts should be the user of the session in the computer where Cleansheets is running. 
 * If this user has events then, when their due date arrives, Cleansheets should display a popup window notifying the user about the events. 
 * This popup window should automatically disappear after a small time interval (e.g., 5 seconds).
 * 
 * <h2>3. Requirement</h2>
 * <p> - Two sidebars: Contact Sidebar + Agenda Sidebar
 * <p> - Contact Sidebar: The user should be able to create, edit and remove contacts
 * <p> - Agenda Sidebar: List all events associated to the contact's agenda
 * <p> - One of the contacts should be the user of the session
 * <p> - Cleansheets should display a popup window notifying the user about events
 * 
 * 
 * <b>Use Case "Contact Edition":</b>
 * The sidebar is a JTabbedPanel. It has two tabs, one for listing contacts and other for listing the respective agenda.
 * By selecting a contact, the user can acess the agenda's tab to see all events associated. Also, by selecting a contact
 * or an event, it is possible to create a new or managing (edit or remove) the object selected.
 *
 * <h2>4. Analysis</h2>
 * The user selects the "Contacts" view. The ContactsExtensionUI opens up on the sidebar. If there's not a contact correspondent
 * to the machine's user, then the user needs to create a new contact (he's own).
 * After creating the user, the agenda is created. At that point, the user can now create events.
 * When an event is created, the event is scheduled automatically by the system.
 * 
 * <h3>System sequence diagra</h3>
 *
 * <h4>Contact Validation</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140775\sprint1\general.jpg" alt="image"> 
 * 
 * <h4>Contact Creation</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140775\sprint1\contact_creation.jpg" alt="image"> 
 * 
 * <h4>Contact Edition</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140775\sprint1\contact_edition.jpg" alt="image"> 
 * 
 * <h4>Event Creation</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140775\sprint1\event_creation.jpg" alt="image"> 
 * 
 * <h4>Event Edit / Remove</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140775\sprint1\event_edition.jpg" alt="image"> 
 *
 * <h2>5. Design</h2>
 * <h3>Classes and Packages</h3>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140775\sprint1\domain_model.jpg" alt="image"> 
 * 
 * <h2>6. Implementation</h2>
 * See packages:
 * <p> * csheets.contacts
 * <p> * csheets.contacts.ui
 * 
 * <h2>7. Integration/Demonstration</h2>
 * <p> -
 * 
 * <h2>8. Final Remarks</h2>
 * <p> -
 * 
 * <h2>9. Work Log</h2> 
 * <p>
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. -nothing-
 * <p>
 * Today
 * <p>
 * 1. Meeting with the client;
 * <p>
 * 2. Started the analysis of issue CRM01.1 - Contact's Edition
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. Started the analysis of issue CRM01.1 - Contact's Edition
 * <p>
 * Today
 * <p>
 * 1. Continuing the analysis of issue CRM01.1
 * <p>
 * Blocking:
 * <p>
 * 1. Understanding the requirements
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. Analysis of issue CRM01.1
 * <p>
 * Today
 * <p>
 * 1. Finishing the analysis of issue CRM01.1
 * 2. Creation of diagrams
 * 3. Starting codification of use case
 * <p>
 * Blocking:
 * <p>
 * 1. Understanding the design
 * <b>Thursday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. Analysis of issue CRM01.1
 * 2. Creation of diagrams
 * 3. Starting codification of use case
 * <p>
 * Today
 * <p>
 * 2. Finished the creation of diagrams
 * 3. Codification of use case
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * 
 * 
 * <h2>10. Self Assessment</h2>
 * <p>Some dificulties in understanding the project
 * <p>Dificulties in compreend the project requirements
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * <p>
 * See packages:
 * <p> * csheets.contacts
 * <p> * csheets.contacts.ui
 * <p> * csheets.repositories
 * 
 * <p>
 * <b>Evidences:</b>
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/39d736dd4ba0665f4644f7451b25be5d1a6d702d">link1</a>
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/b83588966e09b484ef9136c199bd31d7d16af708">link2</a>
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/396d935f554a567063d507fa4e730b1968e7af11">link3</a>
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/1e8dd8277aa0350be046b96acff829a98592dd97">link4</a>
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/2a087bb3f5e6a2f8e2f43b6338efe3a68d8f1b1f">link5</a>
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * <p>Continuous improvement.. Still adapting
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * <p> None
 * 
 * @author 1140775@isep.ipp.pt
 */

package csheets.worklog.n1140775.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

