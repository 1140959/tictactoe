/**
 * Technical documentation regarding the work of the team member (1050126) Ricardo Rio during week1. 
 * 
 * <p>
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * It was not too complicated to get into the flow of the Cleensheets java structure.
 * <p>
 * The team organized how we were going attack this Use Case, and helped one another in understanding 
 * what the objective is.
 *
 * <h2>2. Use Case/Feature: Core01.1 "Activate and Deactivate Extensions"</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-104?filter=-1">LPFOURNB-104</a>
 * <p>
 * It should exist a new window that allows to enable and disable extentions of Cleansheets. A disabled extension means that all its functionalities are disabled.
 * 
 * <h2>3. Requirement</h2>
 * <h2>Use Case Core01.1 "Activate and Deactivate Extensions":</h2>
 * The user will opens a new menu that will allow the user to activate or deactivate
 * extensions. A list of existing extensions will appear on this menu. The
 * user can alter the extensions by activating or deactivating as he or she sees fit.  
 * 
 *  
 * <h2>4. Analysis</h2>
 * 
 * <h3>First "analysis" Simple Sequence Diagram </h3>
 * <img src="doc-files/SSD_activate_deactivate_Analysis.png" alt="image"> 
 *
 * <h3>First "analysis" Sequence Diagram</h3>
 *
 * <img src="doc-files/activate_deactivate_Analysis.png" alt="image"> 
 * <p>
 * It will be necessary to get all the extentions activated and deactivated, 
 * assuming there is a class that has this ability. After getting a list of 
 * extentions give the user the ability to activate and deactivate each one.
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * ... 
 * <h3>5.2. UC Realization</h3>
 * ...
 * <p>
 * <b>Note:</b> It is very important that in the final version of this technical documentation the elements depicted in these design diagrams exist in the code!
 * 
 * <h3>Extension Setup</h3>
 * ...
 * 
 * <h3>5.3. Classes</h3>
 * 
 * ...
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * ...
 * 
 * <h2>6. Implementation</h2>
 * 
 * ...
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * ...
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * ...
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <p>
 * <b>Monday</b>
 * <p>
 * On this day I worked on:
 * <p>
 * 1. Analysing the classes and possible solutions
 * <p>
 * Blocking: none
 * <p>
 * <b>Tuesday</b>
 * <p>
 * On this day I worked on: 
 * <p>
 * 1. Analysing and making diagrams of User Case...
 * <p>
 * Blocking: none
 * <p>
 * <b>Wednesday</b>
 * <p>
 * On this day I worked on: 
 * <p>
 * 1. Helping team in organizing design and implementation. 
 * <p>
 * Blocking: none
 * <p>
 * <b>Thursday</b>
 * <p>
 * On this day I worked on: 
 * <p>
 * 1. Helping team in finalizing design and implementation. 
 * <p>
 * Blocking: none
 * <h2>10. Self Assessment</h2> 
 * 
 * 
 * <h3>10.1. Design and Implementation:3</h3> (Choose one!!!!!)
 * 5 - Perfect - We were able to design and implement all options that 
 * were in the use case and was able to test all functions and new classes. 
 * 
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 5 - Perfect - The team help each other in resolving problems and creating 
 * simple options to resolve Core 01.1)  
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 4 - Outstanding - We were able to create all technical documentation and are easy to understand.
 * 
 * @author Ricardo Rio
 */

package csheets.worklog.n1050126.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 */
class _Dummy_ {}

