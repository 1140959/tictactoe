/**
 * Technical documentation regarding the work of the team member (1050126) Ricardo Rio during week2. 
 * 
 * <p>
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * Continuing the inspection of how CleenSheet works is quite interesting. 
 * <p>
 * The new team has divided to solve various use cases 
 *
 *
 * <h2>2. Use Case/Feature: IPC08.1 "File Sharing"</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-72">LPFOURNB-72</a>
 * <p>
 * Window or sidebare should apear and help the user share files. Adding the 
 * option to choose the directory to share and a directory to download files 
 * from other Cleensheets. 
 * 
 * <h2>3. Requirement</h2>
 * <h2>Use Case Core08.1 "File Sharing":</h2>
 * Cleansheets should have a new option to share the files contained in a 
 * specific directory. The user should be able to specify the directory to 
 * share (output). These files should now be listed on other instances of 
 * Cleansheets (in a specific window, for instance, in a sidebar). The list 
 * should include the name of the files and its size. It is also required to 
 * configure the local directory that will receive the downloaded files 
 * (input). The configuration of file sharing should be persistent. For the 
 * moment it is not required to implement the download of files, however it is 
 * necessary to keep the list of files updated automatically. It is also 
 * necessary to update the list of files that where selected for download in 
 * the input list. This list should include the name of the file, its size, 
 * its source and its status (download in progress, up to date, etc.).
 *  
 * <h2>4. Analysis</h2>
 * 
 * <h3>First "analysis" Simple Sequence Diagram </h3>
 * <img src="doc-files/SSD_File_sharing_Analysis.png" alt="image"> 
 *
 * <h3>First "analysis" Sequence Diagram</h3>
 *
 * <img src="doc-files/File_sharing_Analysis.png" alt="image"> 
 * <p>
 * It will be necessary do a broadcast on the network to know if there are other
 * Cleansheets running and wait for a response. There will also be necessary 
 * to build a simple protocol so that Cleansheet know it's taking to other 
 * Cleansheets. 
 * <h2>5. Design</h2>
 * <h3>Local File List</h3>
 * Loads a list of files in a directory choosen by the user.
 * <img src="doc-files/File_sharing_ReadLocalFiles.png" alt="image">
 * <h3>BroadCast Local File List</h3>
 * Simple broadcast of filenames on the network via UDP to port 50505. 
 * <img src="doc-files/File_sharing_SenderFileLists.png" alt="image"> 
 * <h3>Receive files from other files from Cleansheet</h3>
 *
 * <img src="doc-files/File_sharing_ReceiverFileLists.png" alt="image">
 * 
 * <h3>5.1. Functional Tests</h3>
 *  Testes were made on classes FileInfo and FileList All other classes do not 
 *  need tests or are too complicated to test. 
 * 
 * <h3>5.2. UC Realization</h3>
 * 
 * <img src="doc-files/File_sharing_UCRealization.png" alt="image"
 * 
 * <p>
 * The Threads function independently using the class FileList, which is prepared 
 * to work with synchronization with all 3 threads
 * 
 * <h3>Extension Setup</h3>
 * ...
 * 
 * <h3>5.3. Classes</h3>
 * A list of Class Groups
 * 
 * <h3> - Main Classes</h3>
 * <p>
 * <b>csheets.ext.filesharing.FileInfo</b>
 * <p>
 * <b>csheets.ext.filesharing.FileList</b>
 * <p>
 * <b>csheets.ext.filesharing.FileNameLoader</b>
 * 
 * 
 * <h3> - Controllers - Assembler - Extensions</h3>
 * 
 * <b>csheets.ext.filesharing.FileSharingAssembler</b>
 * <p>
 * <b>csheets.ext.filesharing.FileSharingController</b>
 * <p>
 * <b>csheets.ext.filesharing.FileSharingNetworkController</b>
 * <p>
 * <b>csheets.ext.filesharing.FileSharingExtension</b>
 *
 * <h3> - Threads </h3>
 * 
 * <b>csheets.ext.filesharing.FileSharingBroadCastReceiver</b>
 * <p>
 * <b>csheets.ext.filesharing.FileSharingBroadCastSender</b>
 * <p>
 * <b>csheets.ext.filesharing.FileSharingTableUpdater</b>
 * 
 * <h3> - UI Classes</h3>
 * 
 * <b>csheets.ext.filesharing.ui.FileSharingMenu</b>
 * <p>
 * <b>csheets.ext.filesharing.ui.FileSharingPanel</b>
 * <p>
 * <b>csheets.ext.filesharing.ui.FileSharingUI</b>
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * ...
 * 
 * <h2>6. Implementation</h2>
 * 
 * The method used on this implementation was to create 3 threads:
 *  1. UI table updater <b>csheets.ext.filesharing.FileSharingTableUpdater</b> 
 * this thread woould update the Table with the network files every 30 seconds.
 * <p>
 *  2. BroadCast Files <b>csheets.ext.filesharing.FileSharingBroadCastSender</b> 
 * this thread will send a list of local files with in the share folder every 20 seconds.
 * <p>
 *  3. Receive BroadCast Files <b>csheets.ext.filesharing.FileSharingBroadCastReceiver</b>
 * this thread will always wait for any type of connection (UDP) with the necessary tags
 * and update or add the FileInfo to the FileList and setting a timestamp for that file.
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * ...
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * There are some aspects that may not work well in this implementation for example
 * if there are too many files in the shared folder it will use some or may use alot 
 * of resource but in background.  
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <p>
 * <b>Monday</b>
 * <p>
 * On this day I worked on:
 * <p>
 * 1. Analysing the classes and possible solutions
 * <p>
 * Blocking: none
 * <p>
 * <b>Tuesday</b>
 * <p>
 * On this day I worked on: 
 * <p>
 * 1. Analysing and making diagrams of User Case...
 * 2. Starting designs...
 * <p>
 * Blocking: none
 * <p>
 * <b>Wednesday</b>
 * <p>
 * On this day I worked on: 
 * <p>
 * 1. Testes and Implementation 
 * <p>
 * Blocking: none
 * <p>
 * <b>Thursday</b>
 * <p>
 * On this day I worked on: 
 * <p>
 * 1. Final implementations and JavaDocs
 * <p>
 * Blocking: none
 * <h2>10. Self Assessment</h2> 
 * 
 * <h3>10.1. Design and Implementation:3</h3> 
 * 4 - Outstanding - Two options were missing for presentation due to lack of time: one was the 
 * persistanse and the other was placing the File to the Download list. 
 * These two options are very simple to complete and will be completed on the weekend.
 * The design toke some time to make, protocol of communication is quite simple 
 * as seen in the diagram:
 * <p>
 * <img src="doc-files/file_sharing_TEST_SendBroadCastFiles.png" alt="image">
 * 
 * on the second Instance will always wait for communication when it receives it
 * it will verify if the communication is from a Cleansheet. That is if it's 
 * starts with "CSLAPR4:" 
 * After conforming that it's a Cleansheet communication it see if the file is 
 * on the list, if not it will add it and give it a timestamp, it it does existe
 * it will update the timestamp.
 * 
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * With This Issue there was no need to plan designs with the team because this 
 * issue is completly indipendent.
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 3- Accomplished - there was not enough time to organize this javadoc. 
 * 
 * @author Ricardo Rio
 */

package csheets.worklog.n1050126.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 */
class _Dummy_ {}

