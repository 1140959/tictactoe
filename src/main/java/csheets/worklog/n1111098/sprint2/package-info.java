/**
 * Technical documentation regarding the work of the team member (1111098) Carlos Ferreira during week1. 
 * 
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that each team member must produce each week/sprint. Suggestions on how to build this documentation will appear between '-' like this one. You should remove these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: - no</b>
 * 
 * <p>
 * <b>Area Leader: - no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * -Notes about the week's work.-
 * <p>
 * (...)

 * <h2>2. Use Case/Feature: Core06.1</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-195"> LPFOURNB-103</a>
 * <p>
 * IPC 06.1) Secure Communications
 * Add a new mechanism to add the possibility to secure communications between all instances of cleansheets. 
 * 
 * <h2>3. Requirement</h2>
 * Allow the user the possibility to secure the communications between instances. 
 * Creation of a window to log all incoming and outgoing communications with other instances where all secure and unsecure communications are visible.
 * <p>
 * <b>Use Case Secure Communications</b> CleanSheets must be able to send and receive encrypted and non encrypted communications according to the user´s option.
 * All communications (secure and unsecure will be logged in a new window). If the message is too large, only the first 100? characters will be shown.
 * <h2>4. Analysis</h2>
 * <p>
 * Sequence Diagram for Send Communication Process 
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1111098\sprint2\Send_Communications.png" alt="image" width=900> 
 * <p>
 * "Send communication"
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1111098\sprint2\Receive_Communications.png" alt="image" width=900> 
 * <p>
 * Sequence Diagram for Receive Communication Process 
 *"Receivecommunication"
 * The FI will consist of 2 parts, communications log and encryption/decryption of message.
 * 
 * <p>
 * Encryption/decryption implementation.
 * 
 * <h3>5.3. Classes</h3>
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * <h2>6. Implementation</h2>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * <h2>9. Work Log</h2> 
 * 
 * -Insert here a log of you daily work. This is in essence the log of your daily standup meetings.-
 * Example
 * <b>Monday</b>
 * Yesterday I worked on:
 * 1. -nothing-
 * <p>
 * Today
 * <p>
 * 1. Started Analysis
 * <p>
 * Blocking:
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. Finished Analysis
 * 2. Started Design
 * <p>
 * Today
 * <p>
 *  * <b>Wednesday</b>
 * Yesterday I worked on: 
 * <p>
 * Today
 * <b>Thursday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * Today
 * <p>
 * 1. Revision and Compilation of Use Case
 * 2. Worklog Documentation
 * <h2>10. Self Assessment</h2> 
 * <h3>10.1. Design and Implementation:3</h3>
 * <p>
 * <b>Evidences:</b>
 * <h3>10.2. Teamwork: ...</h3>
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author alexandrebraganca
 */

package csheets.worklog.n1111098.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

