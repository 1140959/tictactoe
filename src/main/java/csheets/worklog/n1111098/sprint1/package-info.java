/**
 * Technical documentation regarding the work of the team member (1111098) Carlos Ferreira during week1. 
 * 
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that each team member must produce each week/sprint. Suggestions on how to build this documentation will appear between '-' like this one. You should remove these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: - no</b>
 * 
 * <p>
 * <b>Area Leader: - no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * -Notes about the week's work.-
 * <p>
 *In this week, work was developed as a team on the development of the Functionality. 
 *Individual work consisted on the analysis of the issue, and the development of one version of 
 * the user interface. This UI version was not implemented.
 * The funcionality was sucessfully implemented.
 *
 * <h2>2. Use Case/Feature: Core01.1</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-103"> LPFOURNB-103</a>
 * <p>
 * -Include the identification and description of the feature-
 * Core O1.1) Enable and Disable Extensions
 * 
 * It should exist a new window that allows to enable and disable extensions of CleanSheets.\n
 * A disabled extensions means that all its funcionalities are disabled.
 * 
 * <h2>3. Requirement</h2>
 * Setup extension for enabling or disabling extensions. The user must be able to enable or disable non-core extensions.
 * When an extension is enabled all its funcionalities are enabled. 
 * When an extension is disabled all its functionalities are disabled.
 *
 * <p>
 * <b>Use Case Enable or Disable Extension:</b> The user selects the Manage Extensions Option under the Extensions menu tab.
 * The System displays all extensins and if the are enabled or disabled. The user changes the extensions state.
 * The system implements the changes made by the user.
 *  * <img src="../../../../../../../\src\main\java\csheets\worklog\n1111098\sprint1\EnableDisableExtensions_SSD.png" alt="image" width=900> 
 * 
"Enable/Disable Extensions"
 * 
 * <h2>4. Analysis</h2>
 * Since non-core functionalities will be implemented as extensions, it is necessary to develop a functionality 
 * to allows the user the option to enable or disable extensions as needed/wanted.
 * This functionality will also be implemented as an extension and will be loaded as an extension.
 
 * <h3>5.3. Classes</h3>
 * 
 * 
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * <h2>6. Implementation</h2>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * <h2>9. Work Log</h2> 
 * 
 * -Insert here a log of you daily work. This is in essence the log of your daily standup meetings.-
 * Example
 * <b>Monday</b>
 * Yesterday I worked on:
 * 1. -nothing-
 * <p>
 * Today
 * <p>
 * 1. Analysis of the Use Case Functionality
 * <p>
 * Blocking:
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. Analysis of the Use Case Functionality
 * <p>
 * Today
 * <p>
 * 1. Finished Work on the Analysis of the Use Case Functionality
 * 2. Started Work on the UI Design (not implemented)
 * <p>
 *  * <b>Wednesday</b>
 * Yesterday I worked on: 
 * <p>
 * 1. Finished Work on the Analysis of the Use Case Functionality
 * 2. Started Work on the UI Design
 * <p>
 * Today
 * <p>
 * 1. Finished Work of the UI 
 * <b>Thursday</b>
 * <p>
 * Yesterday I worked on: 
 * <p> 
 * * 1. Finished Work of the UI 
 * <p>
 * Today
 * <p>
 * 1. Revision and Compilation of Use Case
 * 2. Worklog Documentation
 * <h2>10. Self Assessment</h2> 
 * <h3>10.1. Design and Implementation:3</h3>
 * <p>
 * <b>Evidences:</b>
 * <h3>10.2. Teamwork: ...</h3>
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author alexandrebraganca
 */

package csheets.worklog.n1111098.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

