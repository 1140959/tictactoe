/**
 * Technical documentation regarding the work of the team member (1140777) Joel Ferreira during week1. 
 * 
 * <p>
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 * - Wednesday: working with the team setting up JPA and creating the diagrams of the use case
 *
 * <h2>2. Use Case/Feature: CRM01.1- Contact Edition</h2>
 * 
 * <p><b>Issue in Jira:</b>
 * <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-75">link</a>
 * 
 * <p>
 * <b>Description:</b> A sidebar window that provides functionalities for creating, editing and removing contacts. 
 * Each contact should have a first and last name and also a photograph. 
 * Each contact should also have one agenda in which events related to the contact should be displayed. 
 * For the moment, events have only a due date (i.e., timestamp) and a textual description. 
 * It should be possible to create, edit and remove events. The agenda may be displayed in a different sidebar. 
 * This sidebar should display a list of all events: past, present and future. 
 * One of the contacts should be the user of the session in the computer where Cleansheets is running. 
 * If this user has events then, when their due date arrives, Cleansheets should display a popup window notifying the user about the events. 
 * This popup window should automatically disappear after a small time interval (e.g., 5 seconds).
 * 
 * <h2>3. Requirements</h2>
 * <p> * Creation of one sidebar: for an agenda with contacts
 * <p> * The sidebar that allows to create, edit and remove contacts;
 * <p> * Each contact should have a first and a last name;
 * <p> * Each contact should have a photograph;
 * <p> * Each contact should have a agenda to display related contacts;
 * <p> * Should be possible to create, edit and remove events from the agenda;
 * <p> * All events should be displayed: past, present and future;
 * <p> * The user logged in, should be one of the contacts;
 * <p> * A user should be notified about upcoming events by a pop-up;
 * <p> * The notification pop-ups should automatically disappear after a pre-determined time;
 *  
 * <h2>4. Analysis</h2>
 * <p> We need to study how sidebars are created and loaded in cleansheets as well as how contacts and events are created and managed.
 * <p> We will need to create a sidebar that houses the contacts
 * <p> When we load the sidebar, a panel should be presented to the user,  and according to the user logged on the computer, it's own agenda is displayed (each contact has an agenda)
 * <p> With the agenda loaded, the user selects the operation he/she pretends to make
 * <p> Possible operations:
 * <p> * Create a event
 * <p> * edit a event
 * <p> * remove a event
 * <p> * search, create, edit and remove contacts (if a contact is removed, the agenda associated to that contact, should also be removed)
 *
 * <h3>System sequence diagram</h3>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140777\sprint1\SSD_CRM01.1.jpg" alt="image"> 
 *
 * <p> Notes:
 * <p> A sidebar is a JPanel
 * <p>There is a method (getSideBar) that returns the sidebar, we need to check the method and how it works
 * <p>The capacity of generating this sidebars (on/off) should be controlled by enabling and disabling a plugin
 * <p>If the plugin is disabled, notifications should be disabled to
 *
 * <p> Issues to keep in mind:
 * <p> * The form of authentication does not guaranty oneness (there can be two different users with the same first and last name)
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * <p>
 * From the requirements and analysis, we think that the core functionality of this use case is to be able to add, remove and edit events and contacts. 
 * We need to be able to have persistance for storing this information.
 * Following this approach, we can start by coding the sidebar and the fields / buttons that we will need to perform the actions. At the same time, we need to setup the database to save the data.
 * 
 * <p>*We will need to test a user creation, check if the user is recognized and if the agenda is displayed;
 * <p>*We will need to test a edition and deletion of a user;
 * <p>*We will need to test that when a user is deleted, his agenda is deleted;
 * <p>*We will need to test the managment of events;
 * 
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will need to create a Service that will allow a baseline between the repositories and actions performed by the user. 
 * For the sidebar we need to implement a JPanel. 
 * 
 * <h4>Contact Validation</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140777\sprint1\general.jpg" alt="image"> 
 * <h4>Contact Creation</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140777\sprint1\contact_creation.jpg" alt="image"> 
 * 
 * <h4>Contact Edition</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140777\sprint1\contact_edition.jpg" alt="image"> 
 * 
 * <h4>Event Creation</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140777\sprint1\event_creation.jpg" alt="image"> 
 * 
 * <h4>Event Edit / Remove</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140777\sprint1\event_edition.jpg" alt="image"> 
 * 
 * <h3>5.3. Classes</h3>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140777\sprint1\domain_model.jpg" alt="image"> 
 * 
 * 
 * <h2>6. Implementation</h2>
 * See packages:
 * <p> * csheets.contacts
 * <p> * csheets.contacts.ui
 * 
 * <h2>7. Integration/Demonstration</h2>
 * <p> -
 * 
 * <h2>8. Final Remarks</h2>
 * <p> -
 * 
 * <h2>9. Work Log</h2> 
 * <p>
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. -nothing-
 * <p>
 * Today
 * <p>
 * 1. Meeting with the client;
 * <p>
 * 2. Started the analysis of issue CRM01.1
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. Started the analysis of issue CRM01.1
 * <p>
 * Today
 * <p>
 * 1. Continuing the analysis of issue CRM01.1
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. Analysis of issue CRM01.1
 * <p>
 * Today
 * <p>
 * 1. Finishing the analysis of issue CRM01.1
 * 2. Creation of diagrams
 * 3. Starting codification of use case
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <b>Thursday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. Analysis of issue CRM01.1
 * 2. Creation of diagrams
 * 3. Starting codification of use case
 * <p>
 * Today
 * <p>
 * 2. Finished the creation of diagrams
 * 3. Codification of use case
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * 
 * 
 * <h2>10. Self Assessment</h2>
 * <p>
 * I had some difficulties understanding the project and how I was supposed to keep track of my work; 
 * Part of the difficulty was to understand all the tools we have to use;
 * There was little time to produce any real programming, since the team had to grasp a lot in just a few days;
 * I should be able to do more in the next iterations;
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * <p>
 * See packages:
 * <p> * csheets.contacts
 * <p> * csheets.contacts.ui
 * 
 * <p>
 * <b>Evidences:</b>
 * <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/f0ab1177755e537bd05c2ca4e0b713278ea32d92">link1</a>
 * <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/4096658146e8fa4ed7282f2d4fc549077207ec00">link2</a>
 * <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/d6c1a1684b97b548beffd6b910de50b49a42aba5">link3</a>
 * <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/643b3985a11be43aadda588fadc890dbc9f308ee">link4</a>
 * <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/5a62f66244a9e67692ebfea4227e76295c306259">link5</a>
 * 
 * 
 * 
 * 
 *
 * <h3>10.2. Teamwork: ...</h3>
 * <p>
 * Can be improved;
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * <p>
 * Nothing besides this document (so far)
 * 
 * @author 1140777@isep.ipp.pt
 */

package csheets.worklog.n1140777.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

