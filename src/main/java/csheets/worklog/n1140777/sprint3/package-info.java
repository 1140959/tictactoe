/**
 * Technical documentation regarding the work of the team member (1140777) Joel Ferreira during week3. 
 * 
 * <p>
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 *
 * <h2>2. Use Case/Feature: IPC05.2- Chat Participants</h2>
 * 
 * <p><b>Issue in Jira:</b>
 * <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-64">link</a>
 * 
 * <p>
 * <b>Description:</b> 
 * 
 * <h2>3. Requirements</h2>
 *  
 * <h2>4. Analysis</h2>
 * 
 * <h3>Notes:</h3>
 * 
 * 
 * <h2>5. Design</h2>
 * 
 *
 * <h3>5.1. Functional Tests</h3>
 
 * 
 * <h3>5.2. UC Realization</h3>
 * 
 * 
 * 
 * <h2>6. Implementation</h2>
 * 
 * 
 * <h2>8. Final Remarks</h2>
 *
 * <h2>9. Work Log</h2> 
 * <h3>Friday</h3>
 * <p> Yesterday I worked on:
 * <p> 1. tests and implementation CRM01.2 - Address Edition
 * <p> Today
 * <p> 1. Corrections and implementation of XML import CRM02.1 - Address Edition
 * <p> Blocking:
 * <p> 1. -nothing-
 * <h3>Saturday</h3>
 * <p> Yesterday I worked on:
 * <p> 1. Corrections and implementation of XML import CRM02.1 - Address Edition
 * <p> Today
 * <p> 1. Analysis of UC IPC05.2- Chat Participants
 * <p> Blocking:
 * <p> 1. -nothing-
 * 
 * <h2>10. Self Assessment</h2>
 * 
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * <b>Evidences:</b>
 *
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * 
 * <h3>10.3. Technical Documentation:</h3>
 * 
 * 
 * @author 1140777@isep.ipp.pt
 */

package csheets.worklog.n1140777.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

