/**
 * Technical documentation regarding the work of the team member (1140777) Joel Ferreira during week2. 
 * 
 * <p>
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 * * Tiago (1140775) spent about an hour discussing with me the diagrams and bets implementation approach (6/6)
 * * Had to make changes in TESTS / implementation that are not yet reflected in diagrams (WIP)
 *
 * <h2>2. Use Case/Feature: CRM02.1- Address Edition</h2>
 * 
 * <p><b>Issue in Jira:</b>
 * <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-78">link</a>
 * 
 * <p>
 * <b>Description:</b> 
 * There should be a new sidebar window to create, edit and remove addresses associated with contacts
 * (individuals or companies). Each address must include: street, town, postal code, city and country. Each
 * contact should have two addresses: the main address and a secondary address. In the case of a Portuguese
 * address the postal code should be validated. It should be possible to import and update the the list of
 * valid Portuguese postal codes from an external file (xml file or other format).
 * 
 * <h2>3. Requirements</h2>
 * <p> Create a new sidebar window
 * <p> In the new sidebar must be possible to create, edit and remove addresses associated to contacts
 * <p> Each address has: street, town, postal code, city and country
 * <p> Each contact should have two addresses: main and secondary
 * <p> If the country is Portugal postal code must be validated
 * <p> Valid postal codes must be imported by a external file
 *  
 * <h2>4. Analysis</h2>
 * <p> I will need to create a sidebar just as the group created in CRM01.1
 * <p> There should be enough fields for the two types of address (main and secondary)
 * <p> There should be a repository (?) for the postal codes
 * <p> There should be 3 actions available to the user: create, edit and remove an address
 * 
 * <h3>Notes:</h3>
 * <p> The button for "create address" should only be available if there are less than 2 addresses introduces
 * <p> The button for "remove address" and "edit address" should only be enabled if the contact as any address associated
 * <p> Must check the best way to "store" the Portuguese postal code
 * <p> Try to have at least xml, txt and csv formats accepted in importation
 * 
 * <h3>System sequence diagram</h3>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140777\sprint2\SSD.jpg" alt="image"> 
 * 
 * <h2>5. Design</h2>
 * <h3>Class Diagram</h3>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140777\sprint2\class_diagram.jpg" alt="image"> 
 *
 * <h3>5.1. Functional Tests</h3>
 * From the requirements and analysis, it's clear that the core functionality of this use case is to be able to add, remove and edit addresses to contacts. 
 * Following this approach, we can start by coding the sidebar and the fields / buttons that we will need to perform the actions. 
 * At the same time, we need to setup the database to save the data.
 * 
 * <h3>5.2. UC Realization</h3>
 * <h4>Add Address</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140777\sprint2\CreateAddress.jpg" alt="image"> 
 * 
 * <h4>Edit / Remove Address</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140777\sprint2\EditAddress.jpg" alt="image"> 
 * 
 * 
 * <h2>6. Implementation</h2>
 * <p> chseets.ui.address
 * <p> chseets.ui.address.ui
 * <p> chseets.ui.address.ui.panels
 * 
 * 
 * <h2>8. Final Remarks</h2>
 * <p> Although far from perfect, the basic functions are implemented. It is still needed to implement xml importation and address validation (for Portuguese addresses for exeample);
 * <p> The fact that JPA is not implemented, brings more difficulties everytime we need to test the build;
 * <p> TDD is a very different, and by that reason, more difficult approach to what we are used to;
 * <p> Have to finish the Issue in the next few days;
 * <h2>9. Work Log</h2> 
 * <h3>Friday</h3>
 * <p> Yesterday I worked on:
 * <p> 1. Finished the creation of diagrams of use case CRM01.1 - Contact Edition
 * <p> 2. Codification of use case CRM01.1 - Contact Edition
 * <p> Today
 * <p> 1. Analysis of use case CRM02.1 - Address Edition
 * <p> Blocking:
 * <p> 1. -nothing-
 * <h3>Monday</h3>
 * <p>Yesterday I worked on:
 * <p>1. Analysis of use case CRM02.1 - Address Edition
 * <p>Today
 * <p> 1. Design of use case CRM02.1 - Address Edition
 * <p> Blocking:
 * <p> 1. Implementation at risk due to UC CRM01.1 not being functional yet
 * <h3>Tuesday</h3>
 * <p> Yesterday I worked on:
 * <p> 1. Design of use case CRM02.1 - Address Edition
 * <p> Today
 * <p> 1. Conclusion of CRM02.1 - Address Edition design
 * <p> 2. Start of implementation / tests
 * <p> Blocking:
 * <p> 1. none
 * <h3>Wednesday</h3>
 * <p> Yesterday I worked on:
 * <p> 1. Conclusion of CRM02.1 - Address Edition design
 * <p> 2. Start of implementation / tests
 * <p> Today
 * <p> 1. Address class tests
 * <p> 2. UI implementation
 * <p> Blocking:
 * <p> 1. JPA not implemented
 * <h3>Thursday</h3>
 * <p> Yesterday I worked on:
 * <p> 1. Address class tests
 * <p> 2. UI implementation
 * <p> Today
 * <p> 1. tests and implementation
 * <p> Blocking:
 * <p> 1. JPA not implemented
 * 
 * <h2>10. Self Assessment</h2>
 * <p>Having a lot of difficulties applying EAPLI patterns; Struggling to maintain a simple approach without using not recommended actions (like gets and sets)
 * <p>Was very difficult to organize and make a clean code; Not satisfied with the status at the moment; Going to try to pick up and clean after avaluation;
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * <b>Evidences:</b>
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/ecd15e045dd9697bbe5c6ba4335186395049c523">link1</a> (Worklog)
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/4a408fe5f959f207a44133a7ca96ff441145a95d">link2</a> (Analysis)
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/148ab1e35f5f51934b2c83cafaf5ff50c3d5f531">link3</a> (Design)  
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/fba809fbcfec76abea50621a31616302af525e8c">link4</a> (Design)
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/ed0595f8aa919ccbfa30c5660d4b48d1b9dd5581">link5</a> (Tests and Implementation)
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/1e04d0330eaf5892484e7cdff9f24d2b1c1a94a2">link6</a> (Tests and Implementation)
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * Improved a lot this week; 
 * 
 * <h3>10.3. Technical Documentation:</h3>
 * <p> Can be improved, but due to the lack of time, is not up to date to reflet everything that was developed
 * 
 * @author 1140777@isep.ipp.pt
 */

package csheets.worklog.n1140777.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

