/**
 * Technical documentation regarding the work of the team member (1090698) Mário Ferreira during week3. 
 * 
 * <p>
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 * It is my individually analysis of the Core05.1 Email Configuration
 * 
 *
 * <h2>2. Use Case/Feature: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-12" target="_blank">Core05.1 Email Configuration</a></h2>
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-219" target="_blank">Core05.1 Email Configuration - Analysis</a><br>
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-220" target="_blank">Core05.1 Email Configuration - Design</a><br>
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-221" target="_blank">Core05.1 Email Configuration - Tests</a><br>
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-222" target="_blank">Core05.1 Email Configuration - Implementation</a><br>
 * <br>
 * <h2>3. Requirement</h2>
 * <b>Email Configuration:</b><br>
 * The new extension should have a window to setup email. This window should be used to setup the
 * required configurations for email. For instance, the account data and server to be used to send emails.
 * All the configuration data should be saved in a proper file (used to save global data). The window should
 * have a button to send a test email. This test email should get its contents (destination, subject and body)
 * from the contents of specific cells (to be selected when the user select the test email button). The window
 * should display a preview of the email and the result of the test.
 * <br>
 * <h2>4. Analysis</h2>
 * It is requested to create a new window for configurations and test email.
 * For this issue is necessary:<br>
 * - Create new extension email configuration.<br>
 * - Create new window to setup email. (is necessary host, email, password and port)<br>
 * - Create new window to test email. (is necessary data email and email to send) <br>
 * - Save data email configurations. (save global data)<br>
 * - Button for test send email. (send and preview email)<br>
 * - Send contents of specific cells in email. (select cells for send in email)<br>
 * <br><br>
 * <h3>Image example of the new window:</h3>
 * The image below shows the expected look in the functionality.
 * <br>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint3/_WORK_IN_PROGRESS_.png" alt="Image example 1"> 
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint3/_WORK_IN_PROGRESS_.png" alt="Image example 2"> 
 * 
 * <br><br>
 * <h3>First "analysis" sequence diagram</h3>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint3/emailconfig_diagram_1.png" alt="First diagram sequence"> 
 * <br>
 * <h2>5. Design</h2>
 * Below is an example of diagram sequence for the functionality implemented in the application. 
 * <br>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint3/_WORK_IN_PROGRESS_.png" alt="Image"> 
 * <br><br>
 * <h3>5.1. Functional Tests</h3>
 * - WORK IN PROGRESS -
 * <br><br>
 * <h2>6. Implementation</h2>
 * - WORK IN PROGRESS -
 * <br><br>
 * <h2>7. Integration/Demonstration</h2>
 * - WORK IN PROGRESS -
 * <br><br>
 * Below is an example of functionality implemented on working in the application.
 * <br><br>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint3/_WORK_IN_PROGRESS_.png" alt="Example"> 
 * <br>
 * <h2>8. Work Log</h2> 
 * <p>Resume work in all days.
 * <br><br>
 * <b>Weekend and Monday</b>
 * <br>1. Analysis of the issue.
 * <br>2. Analysis of the code and application.
 * <br>3. Create documentation (analysis).
 * <br><br>
 * <b>Tuesday</b>
 * <br>- WORK IN PROGRESS -
 * <br><br>
 * <b>Wednesday</b>
 * <br>- WORK IN PROGRESS -
 * <br><br>
 * <b>Thursday</b>
 * <br>- WORK IN PROGRESS -
 * <br><br>
 * <b>Friday</b>
 * <br>- WORK IN PROGRESS -
 * <br><br>
 * <h3>9. Global comment</h3>
 * <br>- WORK IN PROGRESS -
 * <br><br>
 * @author 1090698@isep.ipp.pt
 */

package csheets.worklog.n1090698.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author 1090698@isep.ipp.pt
 */
class _Dummy_ {}

