/**
 * Technical documentation regarding the work of the team member (1090698) Mário Ferreira during week1. 
 * 
 * <p>
 * <b>Scrum Master: no</b>
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 * This first sprint is accomplished in groups. The analysis is carried out individually, after a group analysis.
 * 
 * <h2>2. Use Case/Feature: Lang01.1 - Block of Instructions</h2>
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-109" target="_blank">Lang01.1 - Block of Instructions - Analysis</a><br>
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-123" target="_blank">Lang01.1 - Block of Instructions - Implementation</a><br>
 * <br>
 * - Add the possibility of writing blocks (or sequences) of instructions<br>
 * - Add the assign operator (its symbol is ":=")<br>
 * - The FOR loop should also be implemented based on instruction blocks.<br>
 * <h2>3. Requirement</h2>
 * <b>Block of Instructions:</b><br>
 * Add the possibility of writing blocks (or sequences) of instructions. A block must be delimited by curly
 * braces and its instructions must be separated by ";". The instructions of a block are executed sequentially
 * and the block "result" is the result of the last statement of the block. For example, the formul "= {1+2; sum (A1:A10), B3 + 4 }"
 * must result in the sequential execution of all expressions and the result is the value of the expression "B3 + 4". 
 * <br><br>
 * <b>Assign Operator:</b>
 * Add the assign operator (its symbol is ":="). This operator assigns to its left the result of the right expression. At the moment the left of the assign operator can only be a cell reference.
 * <br><br>
 * <b>FOR loop:</b>
 * The FOR loop should also be implemented based on instruction blocks.
 * For example, the formula "= FOR {A1:  = 1 ; A1&lt;10; A2:  = A2 + A1; A1:  = A1 + 1 }" executes a for loop in which:
 * the first expression is the initialization, the second term is the boundary condition, all other expressions are performed for each iteration of the loop.
 * <br>
 * 
 *  
 * <h2>4. Analysis</h2>
 * 
 * We need to add the possibility of writing a block or sequence of instructions. The function will receive one string that will be divided by ";".<br>
 * The information received must stay divided and treated individually and sequentially. In the selected cell, is inserted the result of the last instruction.<br>
 * In case of error, the function must return an error message.<br>
 * 
 * 
 * <h3>First "analysis" sequence diagram</h3>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint1/lang01_01_assign.png" alt="Assign"> 
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint1/lang01_01_design.png" alt="Design"> 
 * <br>
 * <h2>5. Implementation</h2>
 * For implementation, it was necessary create regular expressions for correct validation.
 * <br><br>
 * 
 * <pre>
 * assign
 * : CELL_REF AOP^ expression2
 * ; 
 * </pre>
 * <br>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint1/lang01_01_ex_assign.jpg" alt="Assign"> 
 * <br>
 * <pre>
 * block
 * : LBRAC! expression2 (SEMI! expression2)* RBRAC!
 * ;
 * </pre>
 * <br>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint1/lang01_01_ex_block.jpg" alt="Block"> 
 * <br>
 * <pre>
 * expression
 * : EQ! expression2 EOF!
 * ; 
 * </pre>
 * <br>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint1/lang01_01_ex_expression.jpg" alt="Expression">
 * <br>
 * <pre>
 * expression2
 * : comparison
 * | block
 * | assign
 * ; 
 * </pre>
 * <br>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint1/lang01_01_ex_expression2.jpg" alt="Expression"> 
 * <br>
 * <br>
 * <h2>6. Integration/Demonstration</h2>
 * Below is an example of functionality implemented on working in the application.
 * <br><br>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint1/lang01_01_cleansheets.png" alt="Example"> 
 * <br>
 * <h2>7. Work Log</h2> 
 * <p>
 * Resume work in all days.
 * <p>
 * <b>Monday</b>
 * <p>
 * 1. Scrum analysis.<br>
 * 2. Issues Division.<br>
 * 3. Analysis of the issue.<br>
 * 4. Analysis of the code and application.<br>
 * <p>
 * <b>Tuesday</b>
 * <p>
 * 1. Analysis of the issue.<br>
 * 2. Analysis of the code and application.<br>
 * 3. Create documentation (diagrams and analysis)<br>
 * 4. Create regular expressions<br>
 * <p>
 * <b>Wednesday</b>
 * <p>
 * 1. Analysis of the code and application.<br>
 * 2. Create documentation (diagrams and analysis)<br>
 * 3. Create regular expressions<br>
 * 4. Create class Assign<br>
 * <p>
 * <b>Thursday</b>
 * <p>
 * 1. Create documentation (diagrams and analysis)<br>
 * 2. Edit regular expressions<br>
 * 3. Create code java<br>
 * <p>
 * <b>Friday</b>
 * <p>
 * 1. New analysis of the code and application<br>
 * 2. Finish documentation (diagrams and analysis)<br>
 * <br>
 * <h3>8. Teamwork and global opinion: ...</h3>
 * We carry out the analysis in group and individually, so as to better understanding all the work and flows.
 * Because of short time available, we had to engage in an organized manner that has been achieved.
 * Globally speaking, we can overcome the problems and doubts which were taking appearing.
 * <br>
 * <br>
 * @author 1090698@isep.ipp.pt
 */

package csheets.worklog.n1090698.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author 1090698@isep.ipp.pt
 */
class _Dummy_ {}

