/**
 * Technical documentation regarding the work of the team member (1090698) Mário Ferreira during week2. 
 * 
 * <p>
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 * It is my individually analysis of the Lang03.1 Conditional Formatting of Cells.
 * 
 *
 * <h2>2. Use Case/Feature: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-33" target="_blank">Lang03.1 Conditional Formatting of Cells</a></h2>
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-171" target="_blank">Lang03.1 Conditional Formatting of Cells - Analysis</a><br>
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-172" target="_blank">Lang03.1 Conditional Formatting of Cells - Design</a><br>
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-173" target="_blank">Lang03.1 Conditional Formatting of Cells - Tests</a><br>
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-174" target="_blank">Lang03.1 Conditional Formatting of Cells - Implementation</a><br>
 * <br>
 * - Update the 'style' extension so that it can be used for the conditional formatting.<br>
 * - Applied when the formula evaluates to true or false.<br>
 * - The editing of these settings should be done in a sidebar window.<br>
 * <h2>3. Requirement</h2>
 * <b>Condicional Formatting of Cells:</b><br>
 * Update the "style" extension so that it can be used for the conditional formatting of cells based on the result of the execution of formulas.
 * For the style of a cell to be conditional it must have an associated formula and two formatting styles.
 * One of the styles is applied when the formula evaluates to true and the other when it evaluates to false.
 * The editing of these settings should be done in a sidebar window.
 * <br><br> 
 *  
 * <h2>4. Analysis</h2>
 * It is requested to create a new the side window In order to use conditional formatting.
 * <br>The side window will be enabled through the the upper menu, always exist the Possibility to disable.
 * <br>To apply conditional formatting, is necessary to choose the cells in that a style is applied, and the formula to be applied.
 * <br>If return true formula is applied a style, but will be applied another.
 * <br>If the formula return true, is apply one style, if return false is apply other style.
 * <br><br>
 * <h3>Image example of the application form:</h3>
 * The image below shows the expected look in the functionality.
 * <br>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint2/lang03_01_example.png" alt="Image example"> 
 * 
 * <br>
 * <h2>5. Design</h2>
 * Below is an example of diagram sequence for the functionality implemented in the application. 
 * <br>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint2/lang03_01_design.png" alt="Image"> 
 * <br><br>
 * <h3>5.1. Functional Tests</h3>
 * For tests i created the class ConditionalFormattingControllerTest.java in new package csheets.ext.style.ui
 * <h2>6. Implementation</h2>
 * For implementation i created the classes ConditionalFormattingController.java and ConditionalFormattingPanel.java, and edited StyleUIExtension.java
 * <br>All the implementation made, was only developed in package csheets.ext.style.ui
 * <br><br>
 * <h2>7. Integration/Demonstration</h2>
 * Is possible to insert a formula which return true or false.
 * <br>Exists 4 buttons for the selected color and background color of the cell. If result of the formula is true, is applied true style, else false is applied style.
 * <br>It is also possible to remove all styles of a cell.
 * <br> Below is an example of functionality implemented on working in the application.
 * <br><br>
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1090698/sprint2/lang03_01_example2.png" alt="Example"> 
 * <br>
 * <h2>8. Work Log</h2> 
 * For this week created a different planning, because I got a day of your holiday in the company where i work.
 * <br>This way have concentrated most of the testing and implementation for Thursday, it would have all day available to devote to the project, unlike the remaining weeks.
 * <p>Resume work in all days.
 * <br><br>
 * <b>Weekend and Monday</b>
 * <br>1. Analysis of the issue.
 * <br>2. Analysis of the code and application.
 * <br>3. Create documentation (analysis).
 * <br><br>
 * <b>Tuesday</b>
 * <br>1. Create documentation (analysis).
 * <br>2. Finish analysis.
 * <br>3. Starting the test code.
 * <br><br>
 * <b>Wednesday</b>
 * <br>1. New analysis of code and application
 * <br>2. Created sequence diagram
 * <br>3. Created new test code
 * <br><br>
 * <b>Thursday</b>
 * <br>1. Finish sequence diagram
 * <br>2. Finish test code
 * <br>3. Finish implementation code
 * <br>4. Finish Worklog
 * <br><br>
 * <b>Friday</b>
 * <br>1. Edit sequence diagram
 * <br>2. Create new test code
 * <br>3. Added new buttons for color style and reset.
 * <br>4. Edit Worklog
 * <h3>9. Global comment</h3>
 * The Most of the deployment was done on Thursday because of the personal disponibility, as previously mentioned.
 * <br>During the analysis of all the issue, i had the help of Team Leader José Lopes (1080696).
 * <br>After the comments from presentation, added 2 new buttons to edit the style the color of cells, and a new button to remove all styles of a cell.
 * <br>
 * @author 1090698@isep.ipp.pt
 */

package csheets.worklog.n1090698.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author 1090698@isep.ipp.pt
 */
class _Dummy_ {}

