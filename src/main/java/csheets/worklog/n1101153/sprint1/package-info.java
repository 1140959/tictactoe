/**
 * Technical documentation regarding the work of the student 1101153 - Andre Teixeira during week1. 
 * 
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * <p>
 * -We had some issues acessing ISEP's network and the internet.
 *
 * <h2>2. Use Case/Feature: Lang01.1-Instructions Block</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-27">http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-27</a>
 * <p><b> Lang01.1- Block of Instructions:</b></p>
 * <p>- Add the possibility of writing blocks (or sequences) of instructions.</p>
 * <p>- Add the assign operator (its symbol is ':=').</p>
 * <p>- Implement The 'FOR' loop based on instruction blocks. </p>
 * 
 * <h2>3. Requirement</h2>
 * 
 * <b>Block:</b> A block must be delimited by curly braces and its instructions must be separated by ';'. The instructions of a block are executed sequentially and the block 'result' is the result of the last statement of the block. For example, the formula '=
 * {1+ 2; sum (A1:A10), B3 + 4 }' must result in the sequential execution of all expressions and the result is the value of the expression 'B3 + 4'. 
 * <p>
 * <b>Assign operator:</b> The assign operator assigns to its left the result of the right expression. At the moment the left of the assign operator can only be a cell reference. 
 * <p>
 * <b>FOR loop:</b> The formula '=FOR{A1:=1;A1&lt;10;A2:=A2+A1;A1:=A1+1}' executes a for loop in which: the first expression is the initialization, the second term is the boundary condition, all other expressions are performed for each iteration of the loop.
 * 
 *  
 * <h2>4. Analysis</h2>
 * Since the block of instructions begins and ends with curly braces, first of all we need to study the grammar and how the lexer and the parser works, so that cleansheets can recognizes that delimiters. Then 
 * we must guarantee that it can treat all the functions separated by the ';'.
 * 
 * After guarantee the recognition of the curly braces by the grammar, the assign operator must also be implemented so that the FOR loop can be function.
 * 
 * All the changes to the grammar must be a applyed to the ANTLR class Formula.g .
 * 
 * <h3>First "analysis" sequence diagram to the lexer and parser of ANTLR</h3>
 * The following sequence diagram demonstrates how the ANTLR deals with the cells contents. The lexer is constructed and the parser is created based upon the grammar.
 * <img src="../../../../../../../src/main/java/csheets/worklog/n1101153/sprint1/lang01_01_design.jpg" alt="image"> 
 * 
 * <h3>Analysis of the formulas and expressions</h3>
 * The next diagram represents how the operations work after the parse tree been created.
 *
 * <h2>5. Implementation</h2>
 * 
 * <p>As I mentioned in the Analysis, first of all we must implement the regular expressions in the grammar 'formula.g' . The curly braces were created as Miscellaneous operators:
 * <pre>
 * LBRAC   : '{' ; 
 * RBRAC   : '}' ;
 * </pre>
 * 
 * <p>Then the assign operator:
 * <pre>
 * AOP		: ':=' ;
 * </pre>
 * <p>We could then construct the regex needed to recognize blocks of instructions and assign operations:
 * <p>expression
  *<p>          : EQ! expression2 EOF!
 * <p>          ;
 * <p><img src="../../../../../../../src/main/java/csheets/worklog/n1101153/sprint1/expression.jpg" alt="image"> 
 * <p>expression2
 * <p>		 : comparison
 * <p>           | block
 * <p>           | assign
 * <p>           ;
 * <p><img src="../../../../../../../src/main/java/csheets/worklog/n1101153/sprint1/expression2.jpg" alt="image"> 
 * <p>assign
 * <p>            : CELL_REF AOP^ expression2
 * <p>            ;
 * <p><img src="../../../../../../../src/main/java/csheets/worklog/n1101153/sprint1/assign.jpg" alt="image"> 
 * <p>block 
 * <p>            : LBRAC^ expression2 (SEMI! expression2)* RBRAC!
 * <p>            ;
 * <p><img src="../../../../../../../src/main/java/csheets/worklog/n1101153/sprint1/block.jpg" alt="image"> 
 * <h2>6. Work Log</h2> 
 * 
 * <b>Monday</b>
 * Presentation of the project and to the scrum team. Preparing the platform and analisis of the problem.
 * <b>Tuesday</b>
 * <p>Analysis of cellsheets and investigating how the lexical and the parser works. 
 * 
 * <p>Initializing the report and beginning the diagrams.
 * <p><b>Wednesday</b>
 * <p>Dealing with the regular expressions with the team. Working on the analysis and the report.
 * <p><b>Thursday</b>
 * <p>Final working on diagrams and on the report.
    * 
 * <h2>7. Self Assessment</h2> 
 * Overall: 3 - accomplished. Although the short time to dedicate to the project, I think that all the requirements were accomplished and have adquired the understanding of the problem domain.
 * 
 * 
 * @author 1101153
 */

package csheets.worklog.n1101153.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

