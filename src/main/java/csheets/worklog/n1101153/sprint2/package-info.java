/**
 * Technical documentation regarding the work of the student 1101153 - Andre Teixeira during week2. 
 * 
 * <p>
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * <p>
 * Because there were some incomplete tasks from previous sprint, we've dedicated some time from the current week to finish them.
 * The limited time constraints the realization of the project, so although it was fully implemented, there is space for major improvements.
 *
 * <h2>2. Use Case/Feature: Lang04.1</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-36">Lang04.1 - Insert Function Basic Wizard</a>
 * <p>
 * Cleansheets should have a menu option to launch a wizard to aid the user in calling functions in formulas. This new window 
 * should display a list of possible functions. The construction of this list should be made dynamically based on the properties 
 * file and self-description of the functions. When a function is selected in the list its syntax should be displayed in a edit 
 * box. The 'syntax' should include the name of the functions and its parameters. For example, for the factorial function, that 
 * only has one parameter, the following text should be displayed in the edit box '= FACT(Number)'. The window should also contain 
 * an area to display a text describing the selected function (i.e., help text). The window should have an 'Apply' and a 'Cancel' 
 * button. If the user selects the 'Apply' button the text of the syntax of the function should be written in the formula bar.
 * 
 * <h2>3. Requirement</h2>
 * This is a extension so an integration in the extension manager was needed.
 * 
 * <p><b>Use Case "Select function from wizard":</b> The user selects a function to apply from a jList. When the function is highlighted, it will be presented some description of the formula and the parameters needed. Will be posible to the user to fill the parameters in a textbox. If apply, the formula will be putted in the selected cell.
 * 
 *  
 * <h2>4. Analysis</h2>
 * The list of the available fuctions is dynamic and is based on the language.props properties file located on \\src\main\resources\csheets\res. The lines from text file must be iterated to fullfill the jList.
 * To access the available functions we need to evoke the getFunctions() method from Language.java class. The retrieved functions array will then update perhaps a jList in a jFrame.
 * The user can then select the desired function.
 * After selecting a function, the controller will retrieve the parameters needed though the evoke of the getArguments() method of the FuctionCall.java class, and present them in text boxes so that the user can personalize them.
 * The button 'Apply' will then be available so that the user can accept the changes and pass them to the formula box and evaluate the result on the desired cell. * 
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram shows the process described above.
 * <p><img src="../../../../../../../src/main/java/csheets/worklog/n1101153/sprint2/Lang041 Analysis 0.jpg" alt="image"> 
 * <p>It's obvious that we will need an user interface so must analyse how the integration in cellsheets is made, has the concerne is to reuse the existing code.  
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Unit Tests</h3>
 * <p> Since this is test driven design, I've implemented 3 unit test to the FunctionWizard and FunctionWizardController
 * classes.
 * <p>The following tests to the <b>FunctionWizard class</b> assures that it's retrieving the right Factorial and Average classes
 * <pre>
 *   &#64;Test
 *   public void testGetFunctionsFactorial() {
 *       System.out.println("Get functions wizard test - Factorial function.");
 *       final Function[] functionList;
 *       FunctionWizard wizard = new FunctionWizard();
 *       functionList=wizard.getFunctions();
 *       Function func = null;
 *       
 *       // find and get the Factorial function
 *       for (int i = 0; i &lt; functionList.length; i++) {
 *           if (functionList[i].getIdentifier().equals("FACT")) {
 *               func = functionList[i];
 *           }
 *       }
 *       assertTrue(func instanceof Factorial);
 *   }
 *
 *   &#64;Test
 *   public void testGetFunctionsAverage() {
 *       System.out.println("Get functions wizard test - Average function.");
 *       final Function[] functionList;
 *       FunctionWizard wizard = new FunctionWizard();
 *       functionList=wizard.getFunctions();
 *       Function func = null;
 *       
 *       // find and get the Average function
 *       for (int i = 0; i &lt; functionList.length; i++) {
 *           if (functionList[i].getIdentifier().equals("AVERAGE")) {
 *               func = functionList[i];
 *           }
 *       }
 *       assertTrue(func instanceof Average);
 *   }
 * </pre>
 * <p> The unit tests to the <b>FunctionWizardController</b> assures that it's returning the right value of the average function
 * 
 * <pre>
 *   &#64;Test
 *   public void testGetFunctionValue() throws IllegalValueTypeException {
 *       System.out.println("Get function value wizard test.");
 *       FunctionWizardController instance = new FunctionWizardController();
 *       Function[] functionList;
 *       
 *       functionList=instance.getFunctions();
 *       
 *       Function func = null;
 *       functionList = instance.getFunctions();
 *       
 *       // find and get the Average function
 *       for (int i = 0; i &lt; functionList.length; i++) {
 *           if (functionList[i].getIdentifier().equals("AVERAGE")) {
 *               func = functionList[i];
 *           }
 *       }
 *
 *       // Parameters list
 *       List&lt;Double&gt; parameters = new ArrayList&lt;&gt;();
 *       parameters.add(new Double(1));
 *       parameters.add(new Double(2));
 *       parameters.add(new Double(3));
 *       parameters.add(new Double(4));
 *       parameters.add(new Double(5));
 *       
 *       // calculate the result of the average
 *       Value value = instance.getFunctionValue(func, parameters);
 *
 *       double result = value.toDouble();
 *       double expected = 3; // AVERAGE(1,2,3,4,5) = 3
 *
 *       assertEquals("The average should be 3.", expected, result, 0);
 *   }
 * </pre>
 * 
 * UPDATE: The above tests was theorical, so after the implementation some changes occur to the unit tests of the FunctionWizard class.
 * There wasn't needed of the FunctionWizardController to retrieve the value of the function, since it sends directly the formula to the uicontroller,
 * so the tests doesn't apply anymore. We commented them so the project could sucessfully compile. Unfortunately because lack of time, I couldn't adapt them
 * <h3>5.2. UC Realization</h3>
 * For this UC we will need a jFrame with a jList to present the available functions. A FunctionWizardController 
 * will control the data exchange between the UI and the FunctionWizard class that will have the responsability of 
 * getting the list of available functions and setting the result based on the parameters passed by the controller.
 * A text box was implemented to permit the user to introduce the parameters to the formula and apply it to the worksheet.
 * <p>The next sequence diagram shows how the interaction is made.
 * <p><img src="../../../../../../../src/main/java/csheets/worklog/n1101153/sprint2/Lang041 Design 0.jpg" alt="image"> 
 * <h3>5.3. Classes</h3>
 * <p>The following diagram represents some of the most representative classes needed for realization and their dependences.
 * <p><img src="../../../../../../../src/main/java/csheets/worklog/n1101153/sprint2/Lang41 Classes.jpg" alt="image"> 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * All he code was fully integrated with the existing code. 
 * <p>The <b>Model-view-controller pattern</b> (MVC) was applyed to the FunctionWizardController since it permited the UI to interact with de model classes.
 * <h2>6. Implementation</h2>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <b>Monday</b>
 *
 * <p> - Yesterday I worked on the some functions from previous sprint that could not been completed.
 * 
 * <p> - Today:
 * <p>1. Daily meet with the area leader to review the sprint and what's to be done from previous.
 * <p>2. Analysis of the Use Case and working on the diagrams.
 * <p>3. Viewing the interaction of the UI controller and the class functions.
 * 
 * <p><b>Tuesday</b>
 *
 * <p> - Yesterday I worked on the analysis and the worklog.
 * 
 * <p> - Today:
 * <p>1. Daily meet with the area leader to review the sprint and what's to be done from previous.
 * <p>2. Skelleton of the FunctionWizard and FunctionWizardController.
 * <p>3. Unit tests to the FunctionWizard and FunctionWizardController classes.
 *
 * <p><b>Wednesday</b>
 *
 * <p> - Yesterday I worked on the unit tests (Test Driven Development).
 * 
 * <p> - Today:
 * <p>1. Daily meet with the area leader to review the sprint and what's to be done from previous.
 * <p>2. Development of the UC.
 *
 * <p><b>Thrusday</b>
 *
 * <p> - Yesterday I worked on Implementation.
 * 
 * <p> - Today:
 * <p>1. Finish the implementation and submission.
 * <p>2. Worklog.
 * 
 * <h2>10. Self Assessment</h2> 
 * I estimate an overall overcome of 4 since I understand that I follow the best practices of development and 
 * implemented the solution in 100%. The concepts was fully understood.
 * <h3>10.1. Design and Implementation:</h3>
 * The tests cover and passed the major functionalitys.
 * 
 * <b>Evidences:</b>
 * 
 * <p><b>Analysis:</b>  <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/bdf80c0c59d93992bf74572a04b3c410c0d01bac">LPFOURNB-191</a>
 * <p><b>Tests:</b>  <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/5b11c3795346ebb1b2382ebf7b96a8713822219d">LPFOURNB-192</a>
 * <p><b>Design:</b> <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/a0ba17a46aa49a288f522aa1fba3cd3dab406db6">LPFOURNB-194</a>
 * <p><b>Implementation:</b> <a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/57ee134b37d00859689f28d06f23c9c55ca94245">LPFOURNB-193</a>
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * There was an interaction within the scrum team that was fundamental to bypass some dificultys. Without the discipline the availability of all the team mates, surely the sprint couldn't be sucessfully completed.
 * 
 * @author 1101153
 */

package csheets.worklog.n1101153.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class Dummy {}

