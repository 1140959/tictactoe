/**
 * Technical documentation regarding the work of the team member (1130108) Pedro Monteiro during week_2. 
 * 
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * <h2>1. Use Case/Feature: IPC 04.01</h2>
 * 
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-60
 * 
 * 
 * <h2>2. Requirement</h2>
 * Setup extension for import/export. The user selects a range of cells that will be the source or
 * destination for the text file, selects the file to import/export, choose the column separator char
 * and also define if the first line of the text file is a table header and select if he want to 
 * include the header in the imported data.
 * <p>
 * <b>Use case "Import/Export Text":</b>
 * <p>
 * The user selects the cell or range of cells that will be the source or destination of the text file. 
 * <p>
 * The user selects the process that we want to execute(import or export).
 * <p>
 * The user selects the text file.
 * <p>
 * The user selects the column separator. 
 * <p>
 * The user choose if the first line of the text file is a table header and selects if he want to include
 * the header in the imported data.
 * <p>
 * The system execute and report the sucess of the process.
 * 
 * * <h2>3. Analysis</h2>
 * To implement this use case we must create a new extension and add an entry to
 * the extensions menu items. That entry should give to the user the option to
 * select Import/Export data from/to a text file. When the user selects the
 * required option, it should open a window where he can select the
 * origin/destiny text file, the column separator, if there will be a header line
 * and if he wants to include it in the operation. But first, we must select a 
 * range of cells to use as a source or destination.
 * 
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagrams depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realizations because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagrams with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * these diagrams were created).
 *
 * <h3>Import Text SD</h3>
 * <img src="doc-files/ipc04_01_import_analysis.png" alt="image">
 * <h3>Export Text SD</h3>
 * <img src="doc-files/ipc04_01_export_analysis.png" alt="image">
 * 
 * <h3>Analysis of Core Technical Problem</h3>
 * The main technical problem is to use threads to improve the efficency. Other
 * issue is in the import process, we need to select a range of cells to be the
 * source of imported text file. So we need to guess the quantity of cells 
 * needed to fill all text file in the spreadsheet.
 * To improve the application efficency I'll use thread programming in
 * read/write operations.
 * 
 * <h3>Tests</h3>
 * Possible unit tests to implement:
 * <p>
 * - if the imported file is empty;
 * <p>
 * - if the size of the cell range is enough to fit all the imported text file.
 * <h2>4. Design</h2>
 * For this use case is important to divide it this different actions:
 * <p>
 * - Import Text;
 * <p>
 * - Export Text;
 * 
 * <p>
 * I had the necessity to create a value object for the Import/Export
 * information. It contains the column separator value, the file to open or
 * create and the include header information.
 * 
 * <h3>Import Text</h3>
 * After user selects all the information the Service created by the Controller
 * is responsible to create the import thread. See the sequence diagram:
 * <p>
 * <img src="doc-files/ipc04_01_import_design.png" alt="image">
 * <h3>Export Text</h3>
 * After user selects all the information the Service created by the Controller
 * is responsible to create the export thread. See the sequence diagram:
 * <p>
 * <img src="doc-files/ipc04_01_export_design.png" alt="image">
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <p>
 * <b>Saturday - 04.06.2016</b>
 * <p>
 * Starting on analysis process.
 * Requirement completed.
 * Analysis completed.
 * <p>
 * <b>Monday - 06.06.2016</b>
 * <p>
 * Analysis adjustments.
 * Starting design.
 * Design completed.
 * <p>
 * <b>Tuesday - 07-06-2016</b>
 * Starting code.
 * <p>
 * <b>wednesday - 08-06-2016</b>
 * Code implementation.
 * Tests.
 * <p>
 * <b>Thursday - 09-06-2016</b>
 * Code implementation.
 * Tests.
 * Worklog updated.
 * @author Pedro Monteiro
 */

package csheets.worklog.n1130108.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

