/**
 * Technical documentation regarding the work of the team member (1130108) Pedro Monteiro during week_1. 
 * 
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * <h2>1. Use Case/Feature: IPC 01.01</h2>
 * 
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-117
 * <p>
 * -Include the identification and description of the feature-
 * 
 * <h2>2. Requirement</h2>
 * Setup extension for sharing cells. The user should be able to select a cell
 * or a range of cells and then share with another instance of cleansheets. When
 * activated, a sidebar for the sharing should appear. The sidebar should be
 * composed of a DefaultListModel box to display the available cleansheets
 * instances for sharing, and should allow sending the selected range of cells
 * to the selected cleansheet instance in the local network by pressing a simple
 * button.
 * <p>
 * <b>Use case "Start Sharing":</b>
 * <p>
 * The user selects the cell or range of cells that he wants to share. 
 * <p>
 * The system displays the available cleansheets instances.
 * <p>
 * The user select the instance to share. 
 * <p>
 * The system asks for approval of the owner of that instance and then sends the cell or range of cells.
 *
 *  
 * <h2>3. Analysis</h2>
 * Since sharing cells will be supported in a new extension to cleansheets we
 * need to study how extensions are loaded by cleansheets and how they work.
 * The extensions must be subclasses of the Extension abstract class and need to
 * be registered in special files.
 * The Extension class has a method called getUIExtension that should be
 * implemented and return an instance of a class that is a subclass of
 * UIExtension.
 * In this subclass of UIExtension there is a method (getSideBar) that returns
 * the sidebar for the extension. A sidebar is a JPanel. On that sidebar it will
 * show up all instances of cleansheets available for sharing cell/range of
 * cells.
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagrams depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realizations because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagrams with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * these diagrams were created).
 * <img src="doc-files/ipc01_01_analysis.png" alt="image">
 * <h3>Analysis of Core Technical Problem</h3>
 * We need to access the active cells (the selected ones) in the active
 * spreadsheet and then initiate the sharing process.
 * One cleansheet instance must search for the available cleansheets in the
 * network. Then the ones that are allowing sharing must appear int the list of
 * available cleansheets.
 * One of those cleansheets is then select and the range of active cells is sent
 * to that cleansheet. Those cells must be placed in the
 * exact same position of the origin spreadsheet.
 * <h3>Tests</h3>
 * Basically, from requirements and also analysis, we see that the core
 * functionality of this use case is to be able to establish a connection
 * between cleansheets instances in a local network. So, we need to be able to
 * start a connection between two cleansheets instances in the same local
 * network and transfer data between them.
 * Following this approach we can set and code some unit tests that will aid the
 * design of this use case:
 * <p>
 * - search cleansheets instances: test to check if we can find other
 * cleansheets instances over the local network;
 * <p>
 * - create and accept connection: test to check if we can create and establish
 * a connection between two cleansheets instances over the local network;
 * <p>
 * - send and receive range cells: test to check if we can send and receive a
 * range of cells between two cleansheets instances over the local;
 * <p>
 * As usual, in a test driven development approach tests normally fail in the
 * beginning. The idea is that the tests will pass in the end.
 * <h2>4. Design</h2>
 * <h3>Extension Setup</h3>
 * To realize this user story we will need to create a subclass of Extension. We
 * will also need to create a subclass of UIExtension. 
 <h3>User sends cells through the local network</h3>
 * The following diagram illustrates what happens when the user wants to share a
 * range of cells from his cleansheet with other instance located in the same
 * local network.
 * <img src="doc-files/ipc01_01_design_send.png" alt="image"> 
 * 
 * <h3>User receives cells through the local network</h3>
 * The following diagram illustrates what happens when the user wants to receive
 * a range of cells in his cleansheet from other instance located in the same
 * local network.
 * <img src="doc-files/ipc01_01_design_receive.png" alt="image"> 
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <p>
 * <b>Monday - 30.05.2016</b>
 * <p>
 * Worked on analysis documentation (Requirements and analysis).
 * <p>
 * <b>Tuesday - 31.05.2016</b>
 * <p>
 * Worked on design diagrams and division of code implementation.
 * <p>
 * <b>Wednesday - 01.06.2016</b>
 * <p>
 * Start code implementation.
 * <p>
 * <b>Thursday - 02.06.2016</b>
 * <p>
 * Code implementation.
 * <p>
 * <b>Friday - 03.06.2016</b>
 * 
 * @author Pedro Monteiro
 */

package csheets.worklog.n1130108.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

