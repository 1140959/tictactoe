/**
 * Technical documentation regarding the work of the team member (1141329) Hugo Alvela during week2. 
 * 
 * <p> <b>Scrum Master: -(yes/no)- no</b>
 * <p> <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 * 1. The use case that was assigned to me at first has been changed to this one because of it's higher priority since Lang07.1 is dependant on its implementation.
 * 2. My teammates spent about 1h explaining how the existing languages were being used.
 * 
 * <h2>2. Use Case/Feature: Lang05.1- Macro Window</h2>
 * 
 * <p> Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-39
 * 
 * <p> <b>Description:</b> 
 * A new extension that adds support for macros. The idea behind macros is that they are like mini-programs
written in a language oriented to the end user. The end user will use macros as a way to extend/customize
Cleansheets. Examples of this type of languages are Visual Basic for Applications in MS Office and LibreOffice
Basic in LibreOffice
 * 
 * <h2>3. Requirement</h2>
 * <p> This new extension should add a menu option to open a window to edit and execute a single macro. Macros
should be designed as a complete new language in Cleansheets. However, its initial grammar should be
very simple and based on the formulas of Cleansheets. In particular, a macro is simply a sequence of
formulas that are executed sequentially. The formulas are the same as those used in the cells. Each line
of the macro may contain a formula or be a comment. A comment is a line that starts with the character
";". The lines of the macros must support all that is possible to do with the cell formulas that start with
"=" (but in the macros the lines do not need to start the line with "="). The macro is to be associated
with the current workbook (but, for the moment, it is not required to persist the macro). The result of
executing a macro is the result of the last executed instruction. The new window should have an area to
edit the text of the macro and button to run the macro. The result of the execution of the macro should
also appear in the window.
 * 
 * <p> <b>Use Case "Macro Window":</b> 
 * 
 * <p> The user starts a new macro.
 * <p> The system asks the macro name and the user inserts the desired name.
 * <p> The system asks the instructions for the macro and the user specifies them.
 * <p> The user then can choose to execute the macro.
 * 
 *  
 * <h2>4. Analysis</h2>
 * <h3>Analysis of Core Technical Problem</h3>
 * 
 * <p> I will have to create a new Expression Compiler, a Lexer and a Parser, 
 * aswell as a grammar very similar to the one used on Cleansheets. 
 * Then the usual UI classes will have to be implemented together with a Macro class and a MacroController.
 * 
 * <h3>Notes:</h3>
 * <p> I originally misunderstood the requirement and implemented the UC by using the ExcelExpressionCompiler.
 * <p> This originated in a redesign of the extension, which delayed the conclusion of this document.
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * In this iteration, only the core functionalities of the Macro class have to be thoroughly tested.
 * This class will ensure the well functioning of the Macro itself, as well as guarantee the working state of the ExpressionCompiler implemented.
 *
 * <h3>5.2. UC Realization</h3>
 * <h4>Sequence Diagram</h4>
 * <p> <img src="../../../../../../../\src/main/java/csheets/worklog/n1141329/sprint2/Lang05.1_SD.png" alt="image"> 
 * 
 * <h4>Interaction Use</h4>
 * <p> <img src="../../../../../../../\src/main/java/csheets/worklog/n1141329/sprint2/Lang05.1_SD2.png" alt="image"> 

 * 
 * <h3>5.3. Classes</h3>
 * <p>
 * The following class diagram depicts the relations between classes.
 * <p>
 * <img src="../../../../../../../\src/main/java/csheets/worklog/n1141329/sprint2/Lang05.1_CD.png" alt="image"> 
 * 
 * 
 * <h2>6. Implementation</h2>
 * 
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <p> <b>Monday</b>
 * <p> 1. Analysis of the problem.
 * <p> 2. Analysis of the code.
 * <p> 3. First design proposal.
 * <p> Blocking:
 * <p> 1. -nothing-
 * 
 * <p> <b>Tuesday</b>
 * <p> 1. Code analysis with teammates.
 * <p> 2. Implementation of macro class and first unit tests.
 * <p> Blocking:
 * <p> 1. -nothing-
 * 
 * <p> <b>Wednesday</b>
 * <p> 1. UC partial redesign
 * <p> 2. Lexer, Parser and Expression Compiler implementation and lang package.
 * <p> 3. UI implementation
 * <p> 4. Finishing Analysis document
 * <p> 5. Pair-programming helping teammate
 * <p> Blocking:
 * <p> 1. -nothing-
 * 
 * <p> <b>Thursday</b>
 * <p> 1. Client meeting
 * <p> Blocking:
 * <p> 1. -nothing-
 * 
 * <p> <b>Friday</b>
 * <p> 1. Analysis of new UC
 * <p> Blocking:
 * <p> 1. -nothing-
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * 1. Analysis
 * 2. Design
 * 3. Testing
 * 4. Implementation
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * <p> <b>Evidences:</b>
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/92ab3bc2cc2dd4cde53974b98639bc67348ab9b9">Initial worklog and TestClass</a> 
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/beb8f416546eb8eb69c659abd60f68a6d3d32a15">Macro Class implementation</a> 
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/c82626745d5f768732d0d44bde6650b589919bf8">Initial Design implementation</a> 
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/d803db68dcc7f95233a6e1507db606db11a076f4">Analisys update</a> 
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/7853fdac07e1b978c0103c626dd9a1c9117d10c5">Implementation of Parser, Lexer, ExpressionCompiler and lang package</a> 
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/35bf0737f063def10097c6c6f9e08810db8f5b29">UI implementation</a> 
 * <p><a href="https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/d2c3feaf1abd0fe20f241c0928256189221a49c3">Last analysis update. Minor code fixes and javadoc on classes.</a> 
 * 
 * <h3>10.2. Teamwork: </h3>
 * <p>1. Application mechanics discussion with teammates on Monday and Tuesday
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author Hugo Alvela
 */

package csheets.worklog.n1141329.sprint2;

/**
 * 
 * @author Hugo Alvela
 */
class _Dummy_ {}

