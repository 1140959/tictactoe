/**
 * Technical documentation regarding the work of the team member (1141329) Hugo Alvela during week1. 
 * 
 * <p>
 *
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 *
 * <h2>2. Use Case/Feature: Core01.1- Enable and Disable Extensions</h2>
 * 
 * <p>
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-1
 * 
 * <h2>3. Requirement</h2>
 * <p>
 * It should exist a new window that allows to enable and disable extensions of Cleansheets. 
 * A disabled extension means that 'all' its functionalities are disabled.
 * 
 * <p>
 * <b>Use Case "Enable and Disable Extensions":</b> The user clicks "extentions" option. The System shows avaliable extensions (avaliable and disabled). The user selects to enable/disable desired extensions and confirms.
 * 
 *  
 * <h2>4. Analysis</h2>
 * <p>
 * First, we need to study how extensions are loaded by cleansheets and how they work.
 * The first sequence diagram in the section <a href="../../../../overview-summary.html#arranque_da_aplicacao">Application Startup</a> tells us that extensions must be subclasses of the Extension abstract class and need to be registered in special files (one for default aplication, other for the user specific).
 * One possible solution would be to keep all extensions in the default application extensions file, while keeping all the other active ones in the User extensions file.
 * Then, a map would be formed informing the UI about all the enabled and disabled extensions.
 * Mandatory Extensions (ie: Style and Extension Manager) should be greyed out from this list to prevent system errors or bugs.
 * 
 * <h3>First "analysis" sequence diagram</h3>
 * <p>
 * The following diagram depicts a proposal for the realization of the previously described use case.
 * <p>
 * <img src="../../../../../../../\src/main/java/csheets/worklog/n1141329/sprint1/uc_realization1_analysis.png" alt="image"> 
 * 
 * <h3>Analysis of Core Technical Problem</h3>
 * This project evolves around creating extensions for an existing software and therefore there is a need to enable and disable said extensions.
 * To acomplish this, another extension must be implemented that detects all other active extensions and allows the user to select which ones to enable/disable.
 * Since there are two files which are read at the program's start, this approach requires a slight code change in that only one of these files would be loaded, while the other keeps the list of all the program's extensions.
 * This way, it would be possible to distinguish enabled/disabled extensions and persist that information between sessions.
 * This approach would require an application restart for the changes to take effect. 
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * <p>
 * Basically, from requirements and analysis, the core functionality of this use case is to be able to enable or disable extensions.
 *
 * <h3>5.2. UC Realization</h3>
 * <p>
 * To implement this use case, one of the config files loaded on program start would be disabled. 
 * With one file keeping the list of all extensions and another one the user's active ones, the program would only enable the desired extensions on load, while allowing the those settings to be changed and persisted.
 * 
 * <p> Note: System restart required for the changes to take effect.
 * 
 * <h3>5.3. Classes</h3>
 * <p>
 * The following class diagram depicts the relations between classes.
 * <p>
 * <img src="../../../../../../../\src/main/java/csheets/worklog/n1141329/sprint1/uc_realization1_analysis_cd.png" alt="image"> 
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * 
 * <h2>6. Implementation</h2>
 * 
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * 
 * <h2>8. Final Remarks</h2>
 * <p>
 * Due to the client specificly mentioning that once the extensions had been loaded it wouldnt be possible to simple "unload" them, another solution to simply disable their behaviour was implemented. Thehrefore, this specific design was not implemented.
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <p>
 * <b>Monday</b>
 * <p>
 * 1. Group forming.
 * <p>
 * 2. Client reunion.
 * <p>
 * <b>Tuesday</b>
 * <p>
 * 1. Code analysis.
 * <p>
 * <b>Wednesday</b>
 * <p>
 * 1. Use case analysis and design.
 * <p>
 * <b>Thursday</b>
 * <p>
 * 1. Specifications exclaration with the client.
 * <p>
 * 2. Design and implementation of the UC as it was decided by the group.
 * <p>
 * <b>Friday</b>
 * <p>
 * 1. Client meeting
 * 
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * 1. Analysis
 * 2. Design
 * 3. Pair programming
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * <p>
 * <b>Evidences:</b>
 * 
 * <h3>10.2. Teamwork: Pair programing on thursday</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author Hugo Alvela
 */

package csheets.worklog.n1141329.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author Hugo Alvela
 */
class _Dummy_ {}

