/**
 * Technical documentation regarding the work of the team member (1130960) Joao Semedo during week1. 
 * 
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * -Notes about the week's work.-
 * <p>
 * Issues with internet connectivity in work area moderately delaying task development
 * <h2>2. Use Case/Feature: Lang08.1</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-48">LPFOURNB-48</a>
 * <p>
 * 
 * 
 * It should be possible to export the contents of an workbook, worksheet or part of an
 * worksheet to an XML file. As we want to optimize as much as possible the process the 
 * solution should not rely on any third party library. Cleansheets should have a window to
 * configure the XML tags to use for each type of element. The export should only 
 * include the value of the cells. The export menu option should appear in the File menu
 * 
 * <h2>3. Requirements</h2>
 * javax.swing.JFrame extension for UI.
 * FocusOwnerAction extension for ExportXMLController.
 * The user must be able to select the file name to export.
 * The user must be able to choose either to export entire workbook, spreadsheet range(1-1) or
 * cell range from chosen worksheet.
 * The user should be able to choose which tags for each type of element : 
 *  worksheet,spreadsheet(s),cell(s) and cell content
 * 
 * <p>
 * <b>Use Case "Lang08.1- XML Export":</b>  Add the possibility of exporting cell values to xml in the File menu
 * 
 *  
 * <h2>4. Analysis</h2>
 * <h3>Analysis of XML Export</h3>
 * 
 * The following diagram depicts a proposal for the realization of the previously described use case.
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1130960\sprint2\lang_8_01_01_design.png" alt="image"> 
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <b>Monday</b>
 * Impossibility to attend
 * <p>
 * <b>Tuesday</b>
 * Analysis of Use Case requirements
 * <p>
 * <b>Wednesday</b>
 * Design finish
 * <p>
 * <b>Thursday</b>
 * Initial presentation.
 * @author 1130960@isep.ipp.pt
 */

package csheets.worklog.n1130960.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author 1130960@isep.ipp.pt
 */
class _Dummy_ {}

