/**
 * Technical documentation regarding the work of the team member (1130960) Joao Semedo during week1. 
 * 
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that each team member must produce each week/sprint. Suggestions on how to build this documentation will appear between '-' like this one. You should remove these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * -Notes about the week's work.-
 * <p>
 * Issues with internet connectivity in work area moderately delaying task development
 * <h2>2. Use Case/Feature: Lang01.1</h2>
 * 
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-125
 * <p>
 *  Block of Instructions
 * 
 * 
 * 
 * 
 * <h2>3. Requirement</h2>
 *  A block must be delimited by curly
 * braces and its instructions must be separated by ";". The instructions of a block are executed sequentially
 * and the block "result" is the result of the last statement of the block. For example, the formula
 * "= {1+ 2; sum (A1:A10), B3 + 4 }"
 * must result in the sequential execution of all expressions and the result
 * is the value of the expression "B3 + 4"
 * .Add the assign operator (its symbol is ":="). This operator assigns to its left the result of the right expression. 
 * 
 * At the moment the left of the assign operator can only
 * be a cell reference. The FOR loop should also be implemented based on instruction blocks. For example,
 * the formula "= FOR {A1:  = 1 ; A1&lt;10; A2:  = A2 + A1; A1:  = A1 + 1 }"
 * executes a for loop in
 * which: the first expression is the initialization, the second term is the boundary condition, all other
 * expressions are performed for each iteration of the loop.
 * 
 * <p>
 * <b>Use Case "Lang01.1- Block of Instructions":</b>  Add the possibility of writing blocks (or sequences) of instructions
 * 
 *  
 * <h2>4. Analysis</h2>
 * As all block of instructions start with "{" and end with "}" , it is required an in-depth knowledge and study of how
 * the grammar, lexer and parser works.
 *  To be able to guarantee the recognition of the delimiter, cleansheets must be able to treat functions separated by a ";" value.
 *  After the successful recognition of the delimiters, the assignment operator must be properly implemented to be able to implement 
 * a FOR loop function.
 * 
 * All changes in the grammar must be applied to the Antler class Formula.g .
 * 
 * <h3>First "analysis" sequence diagram to lexer and parser of ANTLR</h3>
 * The following diagram depicts a proposal for the realization of the previously described use case.
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1130960\sprint1\lang_01_01_design.png" alt="image"> 
 * 
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <b>Monday</b>
 * Initial introduction to project to the scrum team. Problem analysis.
 * <p>
 * <b>Tuesday</b>
 * Analysis of cleansheets and studying the lexical and parser workings 
 * <p>
 * Working on diagram design
 * <p>
 * <b>Wednesday</b>
 * Only worked in initial report - Impossibility to attend regular schedule
 * <p>
 * <b>Thursday</b>
 * Initial presentation.
 * @author 1130960@isep.ipp.pt
 */

package csheets.worklog.n1130960.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author 1130960@isep.ipp.pt
 */
class _Dummy_ {}

