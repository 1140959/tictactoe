/**
 * Technical documentation regarding the work of the team member (1140959) David Pinheiro during week1. 
 * 
 * <p>
 * 
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * -Notes about the week's work.-
 * This week, beeing the first, it whas heavier in terms of analisys since it was my first
 * contact with the project and i had to study the manual and the code.
 * I also tried whenever i could, to help the Scrum Master planning the next sprint.
 *
 * <h2>2. Use Case/Feature: CRM01.1- Contact Edition</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-75">Click Here</a>
 * 
 * 
 * <h2>3. Requirement</h2>
 * A sidebar window that provides functionalities for creating, editing and removing contacts. 
 * Each contact should have a first and last name and also a photograph. 
 * Each contact should also have one agenda in which events related to the contact should be displayed. 
 * For the moment, events have only a due date (i.e., timestamp) and a textual description. 
 * It should be possible to create, edit and remove events. The agenda may be displayed in a different sidebar. 
 * This sidebar should display a list of all events: past, present and future. 
 * One of the contacts should be the user of the session in the computer where Cleansheets is running. 
 * If this user has events then, when their due date arrives, Cleansheets should display a popup window notifying the user about the events. 
 * This popup window should automatically disappear after a small time interval (e.g., 5 seconds).
 * <p>
 *
 * <b>Use Case "Contact Edition":</b> 
 * The user activates the extention to add contacts
 * The system displays the sidebar so that the user can create/edit/remove a contact
 * The user the create new contact option
 * The system ask for the first and last names as well as the user's photograph
 * The user inserts the required data and confirms the submition
 * The system validates the data (duplicate names) and saves the new user.
 * When there is at least one contact the user can edit,remove it or add a new event.
 *
 * <h2>4. Analysis</h2>
 * Since all subtasks of the Contact edition Use case will be supported in a new extension to cleansheets we need 
 * to activate the extention, and then we will see a sidebar where we will be able to do the uc requirements.
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * In terms of functional tests we decided to test if the the folowing cases:
 * - Test the unicity of the Contacts
 * - Test if when deleting a contact, its events are deleted also
 * - We must ensure that past events can not be deleted
 * 
 * <h3>5.2. UC Realization</h3>
 * We will use a service to controll the acess between the UI and the repositories,
 * being the one responsible to create the objects.
 * We have seperated the UC in multiple tasks being, create contact,edit contact,
 * delete contact, create event, edit event,and delete event.
 * 
 * <h3>5.3. Sequence Diagrams</h3>
 * <h4>5.3.1 Create Contact</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140959\sprint1\contact_creation.jpg" alt="image">
 * <h4>5.3.2 Edit/Delete Contact</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140959\sprint1\contact_edition.jpg" alt="image">
 * <h4>5.3.3 Create Event</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140959\sprint1\event_creation.jpg" alt="image">
 * <h4>5.3.4 Edit/Delete Event</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140959\sprint1\event_edition.jpg" alt="image">
 * 
 * <h3>5.4. Class Diagram</h3>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140959\sprint1\domain_model.jpg" alt="image">
 * 
 * <h2>6. Final Remarks</h2>
 * 
 * <h2>7. Work Log</h2> 
 *
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. -nothing-
 * <p>
 * Today
 * <p>
 * 1. Analysis of the Use Case
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. Analysis of the Use Case
 * <p>
 * Today
 * <p>
 * 1. Analysis of the Use Case
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. Analysis of the Use Case
 * <p>
 * Today
 * <p>
 * 1. Analysis of the Use Case
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <h2>8. Self Assessment</h2> 
 * This week was such a small week i feel that i've done what was expected but
 * if given enough time i could have detailed the analisys of the uc much more detail
 * 
 * <b>Evidences:</b>
 * <p>
 * - description: .-
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * Being this the first week, all members of the team worked on the same use case,in such a small time frame we all
 * discussed and helped each other on the implementation and design of this UC.
 * 
 * 
 */

package csheets.worklog.n1140959.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

