/**
 * Technical documentation regarding the work of the team member (1140959) David Pinheiro during week2. 
 * 
 * <p>
 * 
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * -Notes about the week's work.-
 *
 * <h2>2. Use Case/Feature: CRM03.1- Email and Phone Edition</h2>
 * 
 * Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-81">Click Here</a>
 * <p>
 * <b>Description:</b> There should be a window to create, edit and remove telephone numbers and emails of contacts. Cleansheets
 * should only accept syntactically correct emails and telephone numbers. Each company can have a
 * main email and telephone number and several secondary emails and telephone numbers. Each individual
 * contact can have the following types of telephone numbers: work; home; celular1 and celular2. Each
 * individual contact can have a main email and several secondary emails. Cleansheets should validate the
 * syntax of the telephone numbers based on the country code. The list o countries and its phone prefixes
 * should be imported and updated from an external file. It should also validate the format o emails.
 * 
 * <h2>3. Requirement</h2>
 * <p>
 * - there will have to be designed a different windows for a Company or a Individual Contact
 * <p>
 * - the email has to be verified to be in a valid format
 * <p>
 * - there will be a file (XML) containing the country phone prefixes that will be validated with a XSD file
 * <p> 
 * - there are no required field, the contact must have at least one email or phone numer
 *
 * <h2>4. Analysis</h2> 
 * <p>
 * <b>Possible Operations</b>
 * <p>
 * There will be three different operations, they will differ by either being for a company or an Individual Contact:
 * <p>
 * Create Email/Phone Number
 * <p>
 * Edit Email/Phone Number
 * <p>
 * Remove Email/Phone Number
 * <p>
 * <b>System sequence diagram</b>
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140959\sprint2\SSD.jpg" alt="image"> 
 * <p>
 * <b>Mockups</b>
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140959\sprint2\Mockup.png" alt="image" width="900"> 
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * In terms of functional tests we decided to test if the the folowing cases:
 * - Test the construction of the file containing the phone prefixes
 * - Test if the data was saves correctly into a company or normal contact
 * - Test the validity of emails and phone numbers 
 * - Test if the contact ends up with at least an email or phone number
 *
 * <h3>5.2. UC Realization</h3>
 *
 * <h3>5.3. Sequence Diagrams</h3>
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140959\sprint2\Sequence_Diagram.jpg" alt="image" width="900"> 
 *
 *
 * <h3>5.5. Contry Code File Format</h3>
 * <p>
 * The input file was chosen to be XML with the folowing structure
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140959\sprint2\xml.png" alt="image" width="900"> 
 * 
 * <h2>6. Final Remarks</h2>
 * 
 * <h2>7. Work Log</h2> 
 *
 * <table summary="Work Log" border="1">
 *	<tr>
 *		<th>Day</th><th>Log</th><th>Blocking</th>
 *	</tr>
 *	<tr>
 *		<td>Monday</td>
 *              <td>Concluded the Analysis, added the ssd and UI mockups</td>
 *              <td> Missing Company Contacts</td>
 *	</tr>
 *	<tr>
 *		<td>Tuesday</td>
 *              <td>Started the design, added the ssd , class diagram and developed the XML file to be used</td>
 *              <td> Missing Company Contacts</td>
 *	</tr>
 *	<tr>
 *		<td>Wedneday</td>
 *              <td>Finhished the design and started the tests and implementation</td>
 *     <td> Missing Company Contacts</td>
 *	</tr>
 *	<tr>
 *		<td>Thursday</td>
 *              <td>Added more tests and completly implemented the ui,  cant save because of missing company contacts</td>
 *	    <td> Missing Company Contacts</td>
 *	</tr>
 * </table>
 * <h2>8. Self Assessment</h2> 
 * 
 * <b>Evidences:</b>
 * <p>
 * - description: .-
 * 
 * <h3>10.2. Teamwork: ...</h3>
 *
 * 
 */

package csheets.worklog.n1140959.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

