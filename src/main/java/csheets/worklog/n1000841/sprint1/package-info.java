/**
 * Technical documentation regarding the work of the team member (1000841) José Egídio Caldeira during week1. 
 * 
 * <p>
 * <b>Scrum Master: - yes</b>
 * 
 * <p>
 * <b>Area Leader: - no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * -Notes about the week's work.-
 * <p>
 * During this week, i spent some time reading the requirements to understand methodology to apply during this LAPR, 
 * and get to know all requirements
 * that will be developing.
 * Analyze the architecture of the CleanSheets implementation.
 * Being the Scrum Master, i've needed to coordinate with the team the division of functional areas, and the creation of subtask for all
 * members. 
 * Beside that, i've been doing the analysis of our assignated issue and on thursday made the planning for the nexts sprints.
 *
 * <h2>2. Use Case/Feature: CRM01.1 - Contact Edition</h2>
 * 
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-75"><b>CRM01.1- Contact Edition</b></a></p>
 * <p>
 * A sidebar window that provides functionalities for creating, editing and removing contacts. 
 * Each contact should have a first and last name and also a photograph. 
 * Each contact should also have one agenda in which events related to the contact should be displayed. 
 * For the moment, events have only a due date (i.e., timestamp) and a textual description. 
 * It should be possible to create, edit and remove events. 
 * The agenda may be displayed in a different sidebar. 
 * This sidebar should display a list of all events: past, present and future. 
 * One of the contacts should be the user of the session in the computer where Cleansheets is running. 
 * If this user has events then, when their due date arrives, Cleansheets should display a popup window notifying the user about the events. 
 * This popup window should automatically disappear after a small time interval (e.g., 5 seconds).
 * 
 * <h2>3. Requirement</h2>
 * * A sidebar window that provides functionalities for creating, editing and removing contacts. 
 * Each contact should have a first and last name and also a photograph. 
 * Each contact should also have one agenda in which events related to the contact should be displayed. 
 * For the moment, events have only a due date (i.e., timestamp) and a textual description. 
 * It should be possible to create, edit and remove events. 
 * The agenda may be displayed in a different sidebar. 
 * This sidebar should display a list of all events: past, present and future. 
 * One of the contacts should be the user of the session in the computer where Cleansheets is running. 
 * If this user has events then, when their due date arrives, Cleansheets should display a popup window notifying the user about the events. 
 * This popup window should automatically disappear after a small time interval (e.g., 5 seconds).
 * 
 * <p>
 * In a generic way, this week the CRM area will be developing the functionallity to:
 * "create contact", "edit contact" or "remove contact". 
 * If option is create or edit, the system shows a form to insert the required data
 * The system require to insert first name, last name and photograph
 * The user inserts data and press create
 * A new contact is created
 * If option is remove:
 * The system shows the contacts that exits and could be selected to be deleted
 * the user select the user and press delete
 * the system ask for confirmation about the deletion
 * the user confirm
 * the system removes the contact and all his events
 * the user selects the pane of agenda
 * the system shows options to create/edit or remove events
 * if option is create the system shows a form where user most insert information about the event (due date and textual description)
 * the user press save and the system save the event
 * the system now shows the event in the sidebar (past, present or future)
 * If the user choose option to remove event, the user must choose the event to be removed and then press remove, the system ask for confirmation
 * the user confirm and the event is removed
 * 
 * For me in particular, i've been responsible for design the sequence for edit or remove a contact.
 * 
 * <b>Use Case "Edit or remove a contact":</b> 
 * The system displays a sidebar window with the contacts availables for user to choose. 
 * The user select a contact and a new ContactInformation window appear with 
 * the information about that user.
 * The user inserts the new data (first name, last name and photograph) to edit
 * or press a remove button
 * If edit is pressed, then the information is validated and if it is ok, 
 * the information is saved into repository, else an error message will be displayed.
 * If the removed button is pressed, then the contact information is deleted and all 
 * the events related to that contact too.
 * 
 *  *  
 * <h2>4. Analysis</h2>
 * We already know that contacts will be supported in a new extension to cleansheets.
 * Once the extension is activated, an action is processed and using a <i>"singleton SYSTEM USER"</i>
 * will verify if that user logged is already a contact created and has 
 * 
 * 
 * <h3>Sequence diagrams</h3>
 * The following diagrams depicts a proposal for the realization of the previously 
 * described use case, we have made an option to show severals diagrams, one for each functionality
 * that the user could do. Create/edit/ remove contacts and events, show agenda for the user
 * that is logged in the machine.
 * the user could create, edit or remove a contact or an event to his/her agenda.
 * That is achived from a new form that is presented to the user and where it is 
 * possible to insert first, last name and a photograph. If the option is for 
 * an event, then the user insert the event description and the date (timestamp)
 * of the event.
 * The system will validate the information passed by the user and if it is correct
 * it will be persisted with the JPA.
 * 
 * <h3>CRM01.1 - Contact Edition Class diagram</h3>
 * 
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1000841\sprint1\CRM1.1_ContactEdition.png" alt="image"> 
 * <h3>CRM01.1 - Contact Edition</h3>
 * 
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1000841\sprint1\CRM01.1_ContactEdition.png" alt="image"> 
 * 
 * <h3>Create Contact</h3>
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1000841\sprint1\CRM01.1_ContactCreation_SD.jpg" alt="image"> 
 * 
 * 
 * <h3>Create a new event</h3>
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1000841\sprint1\CRM01.1_EventCreation.png" alt="image"> 
 * 
 * <h3>Edit or Remove event</h3>
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1000841\sprint1\CRM01.1_EditEvent_SD.png" alt="image"> 
 *
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we see that the core functionality of this use case is to be able to add 
 * an attribute to cells to be used to store a comment/text. We need to be able to set and get its value.
 * Following this approach we can start by coding a unit test that uses a subclass of <code>CellExtension</code> with a new attribute 
 * for user comments with the corresponding method accessors (set and get). A simple test can be to set this attribute with a simple 
 * string and to verify if the get method returns the same string.
 * As usual, in a test driven development approach tests normally fail in the beginning. The idea is that the tests will pass in the end. 
 * <p>
 * see: <code>csheets.ext.comments.CommentableCellTest</code>
 *
 * <h3>5.2. UC Realization</h3>
 * 
 * <p>
 * <b>Note:</b> It is very important that in the final version of this technical documentation the elements 
 * depicted in these design diagrams exist in the code!
 * 
 * 
 * <h3>5.3. Classes</h3>
 * 
 * -Document the implementation with class diagrams illustrating the new and the modified classes-
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * -Describe new or existing design patterns used in the issue-
 * <p>
 * -You can also add other artifacts to document the design, for instance, database models or updates to the domain model-
 * 
 * <h2>6. Implementation</h2>
 * 
 * -Reference the code elements that where updated or added-
 * <p>
 * -Also refer all other artifacts that are related to the implementation and where used in this issue. As far as possible you should use links to the commits of your work-
 * <p>
 * see:
 * <a href="../../../../csheets/ext/comments/package-summary.html">csheets.ext.comments</a><p>
 * <a href="../../../../csheets/ext/comments/ui/package-summary.html">csheets.ext.comments.ui</a>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * For the demonstration in this week i was designing the use case to edit and remove contacts.
 * Beside that i participate in discussion of the best way to implement the solution
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * This week, it was a little difficult to organize the time and the best way to work
 * this was because, the week was shortest.
 * <p>
 * As an extra this use case also implements a small cell visual decorator if the cell has a comment. This "feature" is not documented in this page.
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * This week I was the Scrum Master, so part of my time was spent planning
 * the team's rotation, to guarantee that all the members were able to work in all
 * required areas in the next few weeks.
 * 
 * The remaining time, was spent doing analysis and studying the cleanSheats application.
 * 
 * <p>
 * Example
 * <p>
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. -nothing-
 * <p>
 * Today
 * <p>
 * 1. Analysis of the...
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. ...
 * <p>
 * Today
 * <p>
 * 1. ...
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * -Insert here your self-assessment of the work during this sprint.-
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex: mais de 50%) e apresentam código que para além de não ir contra a arquitetura do cleansheets segue ainda as boas práticas da área técnica (ex: sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the implementation of the design pattern ...-
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author alexandrebraganca
 */

package csheets.worklog.n1000841.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

