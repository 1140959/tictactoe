/**
 * Technical documentation regarding the work of the team member (1000841) José Egídio Caldeira during week1. 
 * 
 * <p>
 * <b>Scrum Master: - yes</b>
 * 
 * <p>
 * <b>Area Leader: - yes</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * This week has begun with the roadmap for all 4 sprints, the need to have it finished on monday make's it 
 * priority number one.
 * After that i had begun to analyze how to implement my functional increment of this week.
 * 
 * <p>
 *
 * <h2>2. Use Case/Feature: Core02.2- Tooltip and User Associated to Comment</h2>
 * 
 * <p>Issue in Jira: <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-4"><b>Core02.2- Tooltip and User Associated to Comment</b></a></p>
 * <p>
 * Cleansheets should register the name of the user that creates comments and each cell should support
 * several comments. When the mouse pointer is hovering above a cell and the cell has comments then these
 * comments should be displayed in a form similar to a "tooltip". The name of the author of each comment
 * should also appear in all displays of comments. Comments should be persisted with the workbook.
 * 
 * <h2>3. Requirement</h2>
 * 1. Register the name of the user that creates comments
 * 2. Each cell should support several comments.
 * 3. When mouse pointer is hovering above a cell, if that cell has comment then these comments and 
 *    its authors should be displayed in a form similar a "tooltip".
 * 4. Comments should be persisted with the workbook.
 * 
 * <p>
 *  
 * <b>Use Case: "Register the name of the user that creates comments":</b> 
 * when the user creates a comment, the system will get the SystemUser that is logged and
 * associate that user with the comment created, the comment will be saved in the following format:
 * &lt;user&gt;: comment
 * 
 * <b>Use Case: support for several comments in a cell </b>
 * The JPanel panel, that today shows only one comment, we will need to guarantee that is possible to show more
 * than one (list of comments).
 * 
 * <b>Use Case: show tooltip with the user and comment when mouse is hover</b>
 * We will need to create/change the actual decorator, to have a setToolTip with the user and comment in the 
 * cell that mouse is hover. For that we will need to capture a mouse move event and if the cell where mouse is
 * hasComment == true, then it will have to show the tooltip, we will manage that using setTooltip("string_with_users_and_comments").
 * 
 * <b>Use Case: Persisting comments with workbook</b>
 * when we save the document it is already persisting comments, so I will not implement JPA persistence on
 * this use case.
 *   
 * <h2>4. Analysis</h2>
 * Because of the requirements 1 and 2, i will need to create a new class "Comment", this class will be 
 * the object that represent a comment and will have two strings as attributes (strUserName and strComment).
 * 
 * <h3>4.1. JUnit Tests</h3>
 * 1. having a user name and a test text that simulate a comment test the toString of comment,
 * this will be essential to return information to the toolTip.
 * 
 * <h3>Sequence diagrams</h3>
 * ...work in progress ...
 * 
 * <h3>CRM01.1 - Contact Edition Class diagram</h3>
 * 
 * <p>
 * <img src="doc-files/core02_01_design2.png" alt="image"> 

 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * From the requirements and also analysis, we see that the core functionality of this use case is to be able to
 * store a list of comments, each one of them with the comment itself and the user that has created it. So we will
 * need to be able to test set and get of that list of comments. We will need to store the user name of each comment
 * too, so we will need to also test the set and get of the user name.

 * Following this approach we can start by coding a unit test that uses a subclass of <code>CellExtension</code> with a new attribute 
 * for user comments with the corresponding method accessors (set and get). A simple test can be to set this attribute with a simple 
 * string and to verify if the get method returns the same string.
 * As usual, in a test driven development approach tests normally fail in the beginning. The idea is that the tests will pass in the end. 
 * <p>
 * see: <code>csheets.ext.comments.CommentableCellTest</code>
 *
 * <h3>5.2. UC Realization</h3>
 * 
 * <p>
 * <b>Note:</b> It is very important that in the final version of this technical documentation the elements 
 * depicted in these design diagrams exist in the code!
 * 
 * 
 * <h3>5.3. Classes</h3>
 * 
 * -Document the implementation with class diagrams illustrating the new and the modified classes-
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * -Describe new or existing design patterns used in the issue-
 * <p>
 * -You can also add other artifacts to document the design, for instance, database models or updates to the domain model-
 * 
 * <h2>6. Implementation</h2>
 * 
 * To implement a list of comments in a cell I need to change the class CommentableCell, the old String that
 * represented a comment was changed to a list of Comment.
 * <p>
 * To guarantee that a comment always have 
 * <p>
 * see:
 * <a href="../../../../csheets/ext/comments/package-summary.html">csheets.ext.comments</a><p>
 * <a href="../../../../csheets/ext/comments/ui/package-summary.html">csheets.ext.comments.ui</a>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * For the demonstration in this week i was designing the use case to edit and remove contacts.
 * Beside that i participate in discussion of the best way to implement the solution
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * This week, it was a little difficult to organize the time and the best way to work
 * this was because, the week was shortest.
 * <p>
 * As an extra this use case also implements a small cell visual decorator if the cell has a comment. This "feature" is not documented in this page.
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * 
 *  
 * <p>
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 *
 * <p>
 * 1. Correcting roadmap plan.
 * 2. Initialize the analysis for my actual issue (core 02.2).
 * <p>
 * Today
 * <p>
 * 1. Finishing the roadmap for the team for all 4 sprints
 * 2. Analysis of the FI Core02.2- Tooltip and User Associated to Comment
 * 
 * <p>
 * Blocking:
 * <p>
 * 1. Understanding how to setTooltip in the cells of CleanSheets, knowing that
 * the CleanSheets works like a JTree.
 * 
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on:  
 * <p>
 * 1. Daily scrum with the team.
 * 2. Continue analysis of my functionality.
 * <p>
 * Today
 * <p>
 * 1. finished the analysis and understand how the decorator are implemented
 * 2. begin of design
 * <p>
 * Blocking:
 * <p>
 * 1. Was blocking in how to implement the Tooltip in a cell and how to render it.
 * 
 *  * <p>
 * <b>Wednesday</b>
 * <p>
 * Yesterday I worked on:  
 * <p>
 * 1. Re-analysis to save a list of comments
 * 
 * <p>
 * Today
 * <p>
 * 1. creating class for Comment and respectively CommentTest
 * 2. Beginning the implementation of classes and changes in class CommentableCell and Controller, need to update CommentableCellTest
 * 3. Investigate how to format a tooltip for presenting information of user and comment when a cell is commented, found useful information on:
 *    www.java2s.com/Tutorial/Java/0240__Swing/CreatingHTML32formattedtexttooltipthetextstringbeginswithhtmlinanycase.htm
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * -Insert here your self-assessment of the work during this sprint.-
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex: mais de 50%) e apresentam código que para além de não ir contra a arquitetura do cleansheets segue ainda as boas práticas da área técnica (ex: sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the implementation of the design pattern ...-
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author alexandrebraganca
 */

package csheets.worklog.n1000841.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

