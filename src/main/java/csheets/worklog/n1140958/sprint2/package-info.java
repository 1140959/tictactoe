/**
 * Technical documentation regarding the work of the team member (1140958) Bruno Fernandes during week2.
 *
 * <p>
 * <b>Scrum Master: no</b>
 *
 * <p>
 * <b>Area Leader: no</b>
 *
 * <h2>1. Notes</h2>
 * - This week I created and developed the issue I was assigned. - Working with
 * the setting up JPA and creating the diagrams of the use case.
 *
 * <h2>2. Use Case/Feature: CRM01.1- Contact Edition</h2>
 *
 * <p>
 * <b>Issue in Jira:</b>
 * <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-84">http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-84</a>
 *
 * <p>
 * <b>Description:</b>
 * <p>
 * The issue allows with user to create, editing and remove one or more text
 * notes associated with contacts.
 * <p>
 * Each note contains a text and a timestamp, but the first line of text is
 * interpreted as the title of the note.
 * <p>
 * You can show in the sidebar one list with all user notes.
 * <p>
 * The select a user notes this and show the history note, the history contains
 * all text and the timestamp changes when it is occurs.
 *
 *
 * <h2>3. Requirements</h2>
 * <p>
 * Possibilty to create, editing and remove note with contact.
 * <p>
 * Contact may have one or more notes.
 * <p>
 * You can show in the sidebar one list with all notes.
 * <p>
 * One note contains one or more lines, but the first is considered the title.
 * <p>
 * When is create a note, is associated a timestamp.
 * <p>
 * Cleensheets maintains history of modifications for each text note.
 * <p>
 * Text note is selected, show a information with all history and timestamp of
 * modifications for each text note.
 *
 *
 * <h2>4. Analysis</h2>
 * <p>
 * Create option to create and show the text note.
 * <p>
 * Area to create also allows to editing and remove text note.
 * <p>
 * The local show all text notes.
 * <p>
 * Selected a text note and show all of your history.
 *
 *
 * <h3>System sequence diagram</h3>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140958\sprint1\CRM04.1_NoteEdition.png" alt="image" width="900">
 *
 * <p>
 * Notes:
 *
 * <p>
 * Issues to keep in mind:
 * <p>
 * The form of authentication does not guaranty oneness (there can be two
 * different users with the same first and last name)
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * In terms of functional tests we decided to test if the the folowing cases:
 * <p>
 * Test if to contact exist in the system;
 * <p>
 * - Test of the content the text node;
 * <p>
 * - Test to title note, because the title is the first line the text note;
 *
 * <h3>5.2. UC Realization</h3>
 *
 * <h4>Diagram Create text note</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140958\sprint1\CRM04.1_SDCreateNote.png" alt="image" width="900">
 *
 * <h4>Diagram Editing and Remove text note</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140958\sprint1\CRM04.1_SDEditRemoveNote.png" alt="image" width="900">
 *
 * <h4>Diagram history text note</h4>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140958\sprint1\CRM04.1_SDHistoryNote.png" alt="image" width="900">
 *
 * <h3>5.3. Classes</h3>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140958\sprint1\CRM04.1_ClassDiagram.png" alt="image" width="900">
 *
 *
 * <h2>6. Implementation</h2>
 * See packages:
 * <p>
 * csheets.notes
 * <p>
 * csheets.notes.ui
 * <p>
 * csheets.notes.ui.panels
 * <p>
 * csheets.notes.persistence
 *
 * <h2>7. Integration/Demonstration</h2>
 * <p>
 * -
 *
 * <h2>8. Final Remarks</h2>
 * <p>
 * -
 *
 * <h2>9. Work Log</h2>
 * <p>
 * <b>Saturday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1- Started Analysis and Diagrams use case.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Sunday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1- Continue Analysis;
 * <p>
 * 2- Finish SD to create text note and inicialized other diagram;
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. -nothing-
 * <p>
 * Blocking:
 * <p>
 * 1. JPA not implemented
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1- End analyis;
 * <p>
 * 2- End the use case diagram;
 * <p>
 * 3- Inicialized implementation and use case tests;
 * <p>
 * Blocking:
 * <p>
 * 1. JPA not implemented
 * 2. Persistence Contact and persistence note;
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. Development tests and implementation;
 * <p>
 * Blocking:
 * <p>
 * 1. JPA not implemented
 * <p>
 * <b>Thursday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. Development tests and implementation;
 * <p>
 * Blocking:
 * <p>
 * 1. JPA not implemented
 *
 *
 *
 * <h2>10. Self Assessment</h2>
 *
 *
 *
 * <h3>10.1. Design and Implementation:3</h3>
 * <p>
 * See packages:
 * <p>
 * csheets.notes
 * <p>
 * csheets.notes.ui
 * <p>
 * csheets.notes.ui.panels
 * <p>
 * csheets.notes.persistence
 *
 *
 *
 * <p>
 * <b>Evidences:</b>
 * https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/9652cdcc097b388264951b51f2f5935d5ce93ba5
 * https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/f077401b16b4bf0d35a2c1731f5b591ab40354e7
 * https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/af12db02c1aa3750e4afc11d9c11c61df5e1e42d
 * https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/af902d40a9c773f0b521c4287125e9589c51aa81
 * https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/d489ed82e6028c4439edacddcf603377afc52f6e
 * https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/2604df52c16c0098c48f803568cde7a699901a3d
 * https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/7a8db4aac338b5e2ea51609a505e7676d7402c20
 * https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/471f205102c8b67cde76aec857ae51d7ff1b5a7a *
 *
 *
 * <h3>10.2. Teamwork: ...</h3> 
 * Improved a lot this week; 
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 * @author 1140958@isep.ipp.pt
 */
package csheets.worklog.n1140958.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
