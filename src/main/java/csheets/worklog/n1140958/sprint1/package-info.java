/**
 * Technical documentation regarding the work of the team member (1140958) Bruno Fernandes during week1.
 *
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that
 * each team member must produce each week/sprint. Suggestions on how to build
 * this documentation will appear between '-' like this one. You should remove
 * these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -Notes about the week's work.-
 *
 *
 * <h2>2. Use Case/Feature: Core01.1</h2>
 *
 * Issue in Jira:
 * <a href="http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-75">http://jira.dei.isep.ipp.pt:8080/browse/LPFOURNB-75</a>
 *
 * <h2> Description </h2>
 * <p>
 * A sidebar window that provides functionalities for creating, editing and
 * removing contacts. Each contact should have a first and last name and also a
 * photograph. Each contact should also have one agenda in which events related
 * to the contact should be displayed. For the moment, events have only a due
 * date (i.e., timestamp) and a textual description. It should be possible to
 * create, edit and remove events. The agenda may be displayed in a different
 * sidebar. This sidebar should display a list of all events: past, present and
 * future. One of the contacts should be the user of the session in the computer
 * where Cleansheets is running. If this user has events then, when their due
 * date arrives, Cleansheets should display a popup window notifying the user
 * about the events. This popup window should automatically disappear after a
 * small time interval (e.g., 5 seconds).
 *
 *
 * <h2>3. Requirement</h2>
 * <p>
 * - Create two sidebars window, one for contacts and another for agenda.
 * <p>
 * - Creating, editing and removing contacts to the list.
 * <p>
 * - The contact contains de first and last name and also photograph.
 * <p>
 * - Each contact contains one for agenda in which events related.
 * <p>
 * - The events contains a limit date and a description text.
 * <p>
 * - Possibility to create, edit and remove events.
 * <p>
 * - The sidebar should display a list of all events.
 * <p>
 * - This user is informed automatically when date of events finish
 *
 *
 *
 * <b>Use Case "Contact Edition":</b>
 *
 *
 *
 * <h2>4. Analysis</h2>
 * The user create a new contacto in the system. The user puts a new contact in
 * your list. The user create new events to your events list. Puts a description
 * and a due date in event. System inform when date of events finish.
 *
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.2. UC Realization</h3>
 *
 *
 * <h3>User Validation</h3>
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140958\sprint1\CRM01.1_OpenContactEdition.png" alt="image" width="900">
 *
 *
 * <h3>Create Contact</h3>
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140958\sprint1\CRM01.1_Edit_Contact_SD.png" alt="image" width="900">
 *
 *
 * <h3>Editing Contact</h3>
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140958\sprint1\CRM01.1_ContactCreation_SD.png" alt="image" width="900">
 *
 *
 * <h3>Create Event</h3>
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140958\sprint1\CRM01.1_EventCreation.png" alt="image" width="900">
 *
 *
 * <h3>Editing Event</h3>
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140958\sprint1\CRM01.1_EditEvent_SD.png" alt="image" width="900">
 *
 *
 * <h3>5.3. Classes</h3>
 * <p>
 * <img src="../../../../../../../\src\main\java\csheets\worklog\n1140958\sprint1\CRM1.1_ContactEditionClasses.png" alt="image" width="900">
 *
 *
 * <h2>6. Implementation</h2> 
 * <p>
 * Packages:
 * <p>
 * - csheets.contacts
 * <p>
 * - csheets.contacts.ui
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 * 
 *
 *
 * <h2>8. Final Remarks</h2>
 * 
 *
 *
 * <h2>9. Work Log</h2>
 * <p>
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. -nothing-
 * <p>
 * Today
 * <p>
 * 1. Meeting with the client;
 * <p>
 * 2. Started the analysis of issue CRM01.1
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. Started the analysis of issue CRM01.1
 * <p>
 * Today
 * <p>
 * 1. Continuing the analysis of issue CRM01.1
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. Analysis of issue CRM01.1
 * <p>
 * Today
 * <p>
 * 1. Finishing the analysis of issue CRM01.1 2. Creation of diagrams
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <b>Thursday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. Analysis of issue CRM01.1 2. Update of diagrams
 * <p>
 * Today
 * <p>
 * 2. Finished the creation of diagrams
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 50%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/f1fa940acc372248c3d8b0187400382adfda572c
 * https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/6077c72757503ba3067d619c24a1d4dc9d60877e
 * https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/4be3061b42364abdc05ae0d286dbf56beaa60fc2
 * https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/52d1c573f273896fc31adf78cca00a8dc681e3ea
 * https://bitbucket.org/lei-isep/lapr4-2016-2nb/commits/e752fbebdee18a5a58068c830f0b411189b5e870
 *
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 * @author 1140958@isep.ipp.pt
 */
package csheets.worklog.n1140958.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author @1140958
 */
class _Dummy_ {
}
