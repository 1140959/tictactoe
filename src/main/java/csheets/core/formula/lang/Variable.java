/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.Cell;
import csheets.core.Value;
import csheets.core.formula.Reference;
import csheets.core.formula.util.ExpressionVisitor;
import java.util.SortedSet;

/**
 * This class is represents a variable and it is used to create, store and
 * return variables.
 *
 * @author José Marques - 1100910@isep.ipp.pt
 */
public class Variable implements Reference {

    /**
     * Represents the variable name
     */
    private final String varName;
    /**
     * Stores the variable value
     */
    private Value varValue;

    /**
     * Creates an empty variable
     */
    public Variable() {
        this.varName = "";
    }

    /**
     * Create a new variable
     *
     * @param varName the variable name
     * @param varValue the variable value
     */
    public Variable(String varName, Value varValue) {
        this.varName = varName;
        this.varValue = varValue;
    }

    @Override
    /**
     * @return the value of the variable
     */
    public Value evaluate() {
        return this.getVarValue();
    }

    @Override
    public SortedSet<Cell> getCells() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    /**
     * @return 0 if the variable name is the same
     */
    public int compareTo(Reference reference) {
        Variable VariableReference = (Variable) reference;
        if ((VariableReference.getVarName().compareTo(this.getVarName()) == 0)) {
            return VariableReference.getVarValue().compareTo(this.getVarValue());
        } else {
            return -1;
        }
    }

    @Override
    public Object accept(ExpressionVisitor visitor) {
        return null;
    }

    /**
     * Represents the variable name
     *
     * @return the varName
     */
    public String getVarName() {
        return varName;
    }

    /**
     * Stores the variable value
     *
     * @return the varValue
     */
    public Value getVarValue() {
        return varValue;
    }
}
