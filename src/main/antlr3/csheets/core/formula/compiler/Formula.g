grammar Formula;

options {
	language=Java;
	output=AST;
}	
    
   
@parser::header {
package csheets.core.formula.compiler;
}
 
@lexer::header {
package csheets.core.formula.compiler;
}

// Alter code generation so catch-clauses get replace with 
// this action.
@rulecatch {
	catch (RecognitionException e) {
		reportError(e);
		throw e; 
	}
}

@members {
	protected void mismatch(IntStream input, int ttype, BitSet follow)
		throws RecognitionException 
	{
    	throw new MismatchedTokenException(ttype, input);
	}

	public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow)
		throws RecognitionException 
	{
		throw e; 
	}
	
	@Override
  	protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
    	throw new MismatchedTokenException(ttype, input);
 	}
}
	         
/* Lang01.2 - Added a new formula language that begins with # */
expression
        : EQ! instruction EOF!
        /*| HASH! currency EOF!*/
        ;

/* Created for Lang01.2 
currency
        : CURRENCY_CODE LBRAC! currency_arithmetic RBRAC!
        ;
     
currency_arithmetic
        : monetaryval
                ( ( PLUS^ | MINUS^ | MULTI^ | DIV^ ) monetaryval )*
        ;
*/
/* Created for Lang01.1 - added block and assign */
instruction
	: comparison
        | block
        | assign
        ;

assign
        : reference AOP^ instruction        
        ;

block 
        : LBRAC! instruction (SEMI^ instruction)* RBRAC!
	;

comparison
	: concatenation
                ( ( EQ^ | NEQ^ | GT^ | LT^ | LTEQ^ | GTEQ^ ) concatenation )?
	;

concatenation
	: arithmetic_lowest ( AMP^ arithmetic_lowest )*
	;

arithmetic_lowest
	: arithmetic_low ( ( PLUS^ | MINUS^ ) arithmetic_low )*
	;

arithmetic_low
	: arithmetic_medium ( ( MULTI^ | DIV^ ) arithmetic_medium )*
	;

arithmetic_medium
	: arithmetic_high ( POWER^ arithmetic_high )?
	;

arithmetic_high
	: arithmetic_highest ( PERCENT^ )?
	;

arithmetic_highest
	: ( MINUS^ )? atom
	;

atom
	: function_call
        | reference
	| literal
	| LPAR! comparison RPAR!
	;

/* Changed for Lang01.1 - to accept For as a function */
function_call
	: FUNCTION^ ( LPAR! | LBRAC! ) 
                ( instruction ( SEMI! instruction )* )?
                ( RPAR! | RBRAC! )
	;

reference
	: CELL_REF ( ( COLON^ ) CELL_REF )?
        | VARIABLE
	;
	
VARIABLE 
        : VARID ((LETTER)|(DIGIT))+
        ;

literal
	: NUMBER
	| STRING
	;

/* Added for Lang01.2
monetaryval
        : NUMBER CURRENCY_SYMBOL
        ;
        
CURRENCY_SYMBOL
        : DOLLAR
        | EURO
        | POUND
        ;
        
CURRENCY_CODE
        : USD
        | EUR
        | GBP
        ;
*/
fragment 
LETTER  : ('a'..'z'|'A'..'Z') 
        ;
  
FUNCTION 
        : ( LETTER )+ 
	;	
 
CELL_REF
	: ( ABS )? LETTER ( LETTER )? ( ABS )? ( DIGIT )+
	;

/*VARIABLE : VARID LETTER ((LETTER)|(DIGIT))+;*/

/* String literals, i.e. anything inside the delimiters */
STRING	: QUOT 
          (options {greedy=false;}:.)*
          QUOT  { setText(getText().substring(1, getText().length()-1)); }
	;  	

QUOT    : '"' ;

/* Numeric literals */
NUMBER  : ( DIGIT )+ ( COMMA ( DIGIT )+ )? ;

fragment 
DIGIT   : '0'..'9' ;

/* Currency Code Symbol
DOLLAR  : '$' ;
EURO    : '€' ;
POUND   : '£' ;
*/
/* Currency Text Code
USD     : 'dollar' ;
EUR     : 'euro' ;
GBP     : 'pound' ;
*/
/* Currency operator
HASH    : '#' ;
*/
/* Comparison operators */
EQ      : '=' ;
NEQ     : '<>' ;
LTEQ    : '<=' ;
GTEQ    : '>=' ;
GT      : '>' ;
LT      : '<' ;

/* Assign operator */
AOP     : ':=' ;

/* Text operators */
AMP     : '&' ;

/* Arithmetic operators */
PLUS    : '+' ;
MINUS   : '-' ;
MULTI   : '*' ;
DIV     : '/' ;
POWER   : '^' ;
PERCENT : '%' ;

/* Reference operators */
fragment 
ABS     : '$' ;
fragment 
EXCL    : '!' ;
COLON   : ':' ;
VARID   : '_' ;
 
/* Miscellaneous operators */
COMMA   : ',' ;
SEMI    : ';' ;
LPAR    : '(' ;
RPAR    : ')' ;
LBRAC   : '{' ; 
RBRAC   : '}' ;

/* White-space (ignored) */
WS: ( ' '
	| '\r' '\n'
	| '\n'
	| '\t'
	) {$channel=HIDDEN;}
	;
	
	
 